package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.Chat;

/**
 * Created by zhuodong on 8/13/14.
 */
public class UpdateChatEvent {
    Chat chat;

    public UpdateChatEvent(Chat chat) {
        this.chat = chat;
    }

    public Chat getChat() {
        return chat;
    }
}
