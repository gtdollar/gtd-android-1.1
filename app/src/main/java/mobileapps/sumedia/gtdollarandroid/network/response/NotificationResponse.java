package mobileapps.sumedia.gtdollarandroid.network.response;

/**
 * Created by zhuodong on 8/29/14.
 */

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

import mobileapps.sumedia.gtdollarandroid.model.Notification;

public class NotificationResponse extends BaseResponse{

    @Expose
    private List<Notification> notifications = new ArrayList<Notification>();

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

}
