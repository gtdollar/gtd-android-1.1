//package mobileapps.sumedia.gtdollarandroid.helper;
//
//import android.content.SharedPreferences;
//
//import mobileapps.sumedia.gtdollarandroid.GTDApplication;
//
///**
// * Created by 2359media on 21/7/14.
// */
//public class GTDAccountStorage {
//
//    public static final String PREFS_NAME   = "pref_account";
//
//    public static final String PREF_KEY_USER_NAME = "userName";
//    public static final String PASSWORD     = "password";
//
//    public static final String PREF_KEY_TOKEN = "token";
//    public static final String PREF_KEY_USER_ID = "userId";
//
//    static SharedPreferences sharedPreferences = GTDApplication.getInstance().getSharedPreferences(PREFS_NAME,0);
//
//    private GTDAccountStorage() {
//    }
//
//    public static void setUserId(final int userId) {
//        sharedPreferences.edit().putInt(PREF_KEY_USER_ID,userId).commit();
//    }
//
//    public static int getUserId() {
//        return 326;
//        //return sharedPreferences.getInt(PREF_KEY_USER_ID,0);
//    }
//
//    public static void setUserName(final String userName) {
//        sharedPreferences.edit().putString(PREF_KEY_USER_NAME,userName).commit();
//    }
//
//    public static String getUserName() {
//        return sharedPreferences.getString(PREF_KEY_USER_NAME,null);
//    }
//
//    public static void setToken(final String token) {
//        sharedPreferences.edit().putString(PREF_KEY_TOKEN,token).commit();
//    }
//
//    //TODO REMOVE mocked token
//    public static String getToken() {
//        return sharedPreferences.getString(PREF_KEY_TOKEN,"1234");
//    }
//
//
//
//}
