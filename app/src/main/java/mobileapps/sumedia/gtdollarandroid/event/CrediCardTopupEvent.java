package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 11/25/14.
 */
public class CrediCardTopupEvent extends BaseEvent<String> {
    public CrediCardTopupEvent(EventStatus status) {
        super(status);
    }

    public CrediCardTopupEvent(EventStatus status, String response) {
        super(status, response);
    }
}
