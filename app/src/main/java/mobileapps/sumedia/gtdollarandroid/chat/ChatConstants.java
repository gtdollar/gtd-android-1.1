package mobileapps.sumedia.gtdollarandroid.chat;

import android.content.Context;
import android.provider.BaseColumns;

import java.util.ArrayList;

import mobileapps.sumedia.gtdollarandroid.R;

/**
 * Created by zhuodong on 8/13/14.
 */
public final class ChatConstants implements BaseColumns {

    private ChatConstants() {
    }

    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.gtdollar.chat";
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.gtdollar.chat";
    public static final String DEFAULT_SORT_ORDER = "_id ASC"; // sort by auto-id

    public static final String DIRECTION = "from_me";
    public static final String MESSAGE = "message";
    public static final String MESSAGE_TYPE = "message_type";
    public static final String DATE = "date";
    public static final String DELIVERY_STATUS = "delivery_status"; // SQLite can not rename columns, reuse old name

    public static final String JID = "jid";
    public static final String PACKET_ID = "pid";

    public static final String AVATAR_URL = "avatar_url";
    public static final String USER_NAME = "user_name";

    public static final String PROPERTIES = "properties";


    // boolean mappings
    public static final int INCOMING = 0;
    public static final int OUTGOING = 1;
    public static final int DS_NEW = 0; //< this message has not been sent/displayed yet
    public static final int DS_SENT_OR_READ = 1; //< this message was sent but not yet acked, or it was received and read
    public static final int DS_ACKED = 2; //< this message was XEP-0184 acknowledged
    public static final int DS_FAILED = 3; //< this message was returned as failed

    public static ArrayList<String> getRequiredColumns() {
        ArrayList<String> tmpList = new ArrayList<String>();
        tmpList.add(DATE);
        tmpList.add(DIRECTION);
        tmpList.add(JID);
        tmpList.add(MESSAGE);
        return tmpList;
    }

    public static final int MESSAGE_TYPE_COUNT = 7;
    public static final int VIEW_TYPE_COUNT = MESSAGE_TYPE_COUNT*2;

    public enum MessageType {
        text,
        audio,
        image,
        video,
        location,
        credit,
        link;
    }

    public static int viewLayout(int viewType){
        int type = viewType % MESSAGE_TYPE_COUNT;
        if(viewType/MESSAGE_TYPE_COUNT >= 1) {
            // outgoing
            if(type == MessageType.image.ordinal()){
                return R.layout.item_chat_image_outgoing;
            }else if(type == MessageType.video.ordinal()){
                return R.layout.item_chat_video_outgoing;
            }else if(type == MessageType.location.ordinal()){
                return R.layout.item_chat_location_outgoing;
            }else if(type == MessageType.credit.ordinal()){
                return R.layout.item_chat_credit_outgoing;
            }else if(type == MessageType.audio.ordinal()){
            return R.layout.item_chat_audio_outgoing;
            } else {
                return R.layout.item_chat_text_outgoing;
            }
        }else {
            if(type == MessageType.image.ordinal()){
                return R.layout.item_chat_image_incoming;
            }else if(type == MessageType.video.ordinal()){
                return R.layout.item_chat_video_incoming;
            }else if(type == MessageType.location.ordinal()){
                return R.layout.item_chat_location_incoming;
            }else if(type == MessageType.credit.ordinal()){
                return R.layout.item_chat_credit_incoming;
            }else if(type == MessageType.audio.ordinal()){
                return R.layout.item_chat_audio_incoming;
            }else {
                return R.layout.item_chat_text_incoming;
            }
        }
    }

    public static int messageViewType(int direction,String messageType) {
        int base = direction*MESSAGE_TYPE_COUNT;
        if (MessageType.text.name().equals(messageType)) {
            return MessageType.text.ordinal()+base;
        }else if (MessageType.audio.name().equals(messageType)) {
            return MessageType.audio.ordinal()+base;
        }else if (MessageType.image.name().equals(messageType)) {
            return MessageType.image.ordinal()+base;
        } else if (MessageType.video.name().equals(messageType)) {
            return MessageType.video.ordinal()+base;
        } else if (MessageType.location.name().equals(messageType)) {
            return MessageType.location.ordinal()+base;
        } else if (MessageType.credit.name().equals(messageType)) {
            return MessageType.credit.ordinal()+base;
        } else return MessageType.link.ordinal()+base;
    }

    public static final String USER_1 = "freesuraj-5397bf2a4d46e@chat.gtdollar.com";
    public static final String USER_2 = "jared-53ea498ee72a8@chat.gtdollar.com";

    public static final String MESSAGE_KEY_SENDER_NAME = "senderFullName";
    public static final String MESSAGE_KEY_DATE = "date";
    public static final String MESSAGE_KEY_SENDER_AVATAR_URL = "senderAvatarUrl";
    public static final String MESSAGE_KEY_RECEIVER_NAME = "receiverFullName";
    public static final String MESSAGE_KEY_RECEIVER_AVATAR_URL = "receiverAvatarUrl";
    public static final String MESSAGE_KEY_RECEIVER_JID = "receiverJid";
    public static final String MESSAGE_KEY_RECEIVER_ID = "receiverUserId";
    public static final String MESSAGE_KEY_SENDER_JID = "senderJid";
    public static final String MESSAGE_KEY_SENDER_ID = "senderUserId";
    public static final String MESSAGE_KEY_TYPE = "type";
    public static final String MESSAGE_KEY_AUDIO_LENGTH = "audio_length";
    public static final String MESSAGE_KEY_PATH = "filePath";
    public static final String MESSAGE_KEY_VIDEO_THUMBNAIL = "videoThumbnail";
    public static final String MESSAGE_KEY_VIDEO_LENGTH = "video_length";
    public static final String MESSAGE_KEY_LOCATION_LAT = "latitude";
    public static final String MESSAGE_KEY_LOCATION_LNG = "longitude";

    public static boolean isFile(String messageType) {
        return MessageType.audio.name().equals(messageType) || MessageType.image.name().equals(messageType)
           || MessageType.video.name().equals(messageType);
    }

    public static String messageTypeMimeType(String messageType) {
        if (MessageType.audio.name().equals(messageType)) {
            return "audio/3gpp";
        } else if (MessageType.video.name().equals(messageType)) {
            return "video/mp4";
        } else if (MessageType.image.name().equals(messageType)) {
            return "image/jpeg";
        }
        return "image/jpeg";
    }

    public static boolean isSupportedVideo(String mimeType) {
        if(mimeType.equals("video/mp4") || mimeType.equals("video/3gpp") || mimeType.equals("video/3gp")) return true;
        return false;
    }

    public static int getErrorMsg(String messageType) {
        if (MessageType.text.name().equals(messageType)) {
            return R.string.notification_text_msg_error;
        }else if (MessageType.audio.name().equals(messageType)) {
            return R.string.notification_audio_msg_error;
        }else if (MessageType.image.name().equals(messageType)) {
            return R.string.notification_img_msg_error;
        } else if (MessageType.video.name().equals(messageType)) {
            return R.string.notification_video_msg_error;
        } else if (MessageType.location.name().equals(messageType)) {
            return R.string.notification_location_msg_error;
        } else if (MessageType.credit.name().equals(messageType)) {
            return R.string.notification_credit_msg_error;
        }
        else return R.string.notification_text_msg_error;
    }

    public static String getNotifyMsg(Context context,String messageType, String message) {
        if (MessageType.text.name().equals(messageType)) {
            return message;
        }else if (MessageType.audio.name().equals(messageType)) {
            return context.getString(R.string.notification_audio_msg);
        }else if (MessageType.image.name().equals(messageType)) {
            return context.getString(R.string.notification_img_msg);
        } else if (MessageType.video.name().equals(messageType)) {
            return context.getString(R.string.notification_video_msg);
        } else if (MessageType.location.name().equals(messageType)) {
            return context.getString(R.string.notification_location_msg);
        } else if (MessageType.credit.name().equals(messageType)) {
            return context.getString(R.string.notification_credit_msg);
        }
        else return message;
    }

    public static String getDescriptionMsg(Context context, int viewType, String message) {
        int type = viewType % MESSAGE_TYPE_COUNT;

        if(type == MessageType.image.ordinal()){
            return context.getString(R.string.notification_img_msg);
        }else if(type == MessageType.video.ordinal()){
            return context.getString(R.string.notification_video_msg);
        }else if(type == MessageType.location.ordinal()){
            return context.getString(R.string.notification_location_msg);
        }else if(type == MessageType.credit.ordinal()){
            return context.getString(R.string.notification_credit_msg);
        }else if(type == MessageType.audio.ordinal()){
            return context.getString(R.string.notification_audio_msg);
        } else {
            return message;
        }


//        if(viewType/MESSAGE_TYPE_COUNT >= 1) {
//            // outgoing
//            if(type == MessageType.image.ordinal()){
//                return context.getString(R.string.notification_img_msg);
//            }else if(type == MessageType.video.ordinal()){
//                return context.getString(R.string.notification_video_msg);
//            }else if(type == MessageType.location.ordinal()){
//                return context.getString(R.string.notification_location_msg);
//            }else if(type == MessageType.credit.ordinal()){
//                return context.getString(R.string.notification_credit_msg);
//            }else if(type == MessageType.audio.ordinal()){
//                return context.getString(R.string.notification_audio_msg);
//            } else {
//                return message;
//            }
//        }else {
//            if(type == MessageType.image.ordinal()){
//                return context.getString(R.string.notification_img_msg);
//            }else if(type == MessageType.video.ordinal()){
//                return context.getString(R.string.notification_video_msg);
//            }else if(type == MessageType.location.ordinal()){
//                return context.getString(R.string.notification_location_msg);
//            }else if(type == MessageType.credit.ordinal()){
//                return context.getString(R.string.notification_credit_msg);
//            }else if(type == MessageType.audio.ordinal()){
//                return context.getString(R.string.notification_audio_msg);
//            } else {
//                return message;
//            }
//        }
    }
}

