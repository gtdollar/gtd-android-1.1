package mobileapps.sumedia.gtdollarandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by zhuodong on 9/11/14.
 */
public class Geometry implements Parcelable {

    @Expose
    private LatLng location;
    @Expose
    private Viewport viewport;

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public Viewport getViewport() {
        return viewport;
    }

    public void setViewport(Viewport viewport) {
        this.viewport = viewport;
    }

    public static class Viewport {

        @Expose
        private LatLng northeast;
        @Expose
        private LatLng southwest;

        public LatLng getNortheast() {
            return northeast;
        }

        public void setNortheast(LatLng northeast) {
            this.northeast = northeast;
        }

        public LatLng getSouthwest() {
            return southwest;
        }

        public void setSouthwest(LatLng southwest) {
            this.southwest = southwest;
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.location, 0);
    }

    public Geometry() {
    }

    private Geometry(Parcel in) {
        this.location = in.readParcelable(LatLng.class.getClassLoader());
    }

    public static final Parcelable.Creator<Geometry> CREATOR = new Parcelable.Creator<Geometry>() {
        public Geometry createFromParcel(Parcel source) {
            return new Geometry(source);
        }

        public Geometry[] newArray(int size) {
            return new Geometry[size];
        }
    };
}