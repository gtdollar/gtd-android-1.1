package mobileapps.sumedia.gtdollarandroid.network.response;

import java.util.List;

import mobileapps.sumedia.gtdollarandroid.model.Order;

/**
 * Created by zhuodong on 8/29/14.
 */
public class OrdersResponse extends BaseResponse{
    List<Order> orders;

    public List<Order> getOrder() {
        return orders;
    }

    public void setOrder(List<Order> order) {
        this.orders = order;
    }
}
