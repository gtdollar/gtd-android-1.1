/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.PagingBaseAdapter;
import mobileapps.sumedia.gtdollarandroid.event.AlertButtonClickEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.GetContactEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.PhoneContact;
import mobileapps.sumedia.gtdollarandroid.model.User;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.widget.GenericAlertPopup;
import mobileapps.sumedia.gtdollarandroid.widget.ObjectCursor;
import mobileapps.sumedia.gtdollarandroid.widget.ObjectCursorLoader;

import static mobileapps.sumedia.gtdollarandroid.model.PhoneContact.ContactsQuery;

/**
 * This fragment displays a list of contacts stored in the Contacts Provider. Each item in the list
 * shows the contact's thumbnail photo and display name. On devices with large screens, this
 * fragment's UI appears as part of a two-pane layout, along with the UI of
 * {@link ContactDetailFragment}. On smaller screens, this fragment's UI appears as a single pane.
 * <p/>
 * This Fragment retrieves contacts based on a search string. If the user doesn't enter a search
 * string, then the list contains all the contacts in the Contacts Provider. If the user enters a
 * search string, then the list contains only those contacts whose data matches the string. The
 * Contacts Provider itself controls the matching algorithm, which is a "substring" search: if the
 * search string is a substring of any of the contacts data, then there is a match.
 * <p/>
 * On newer API platforms, the search is implemented in a SearchView in the ActionBar; as the user
 * types the search string, the list automatically refreshes to display results ("type to filter").
 * On older platforms, the user must enter the full string and trigger the search. In response, the
 * trigger starts a new Activity which loads a fresh instance of this fragment. The resulting UI
 * displays the filtered list and disables the search feature to prevent furthering searching.
 */
public class ContactsListFragment extends ListFragment implements
    AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<ObjectCursor<PhoneContact>> {

    // Defines a tag for identifying log entries
    private static final String TAG = "ContactsListFragment";

    private ContactsAdapter mAdapter; // The main query adapter

    List<Object> phoneContact = new ArrayList<Object>();
    List<?> userContact = new ArrayList<Object>();

    String phoneNumber;

    public static ContactsListFragment newInstance() {
        ContactsListFragment fragment = new ContactsListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Fragments require an empty constructor.
     */
    public ContactsListFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Create the main contacts adapter
        mAdapter = new ContactsAdapter(getActivity());
        JobManager.getInstance().getContacts();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the list fragment layout
        View view = inflater.inflate(R.layout.fragment_contact_list, container, false);
        View headerView = inflater.inflate(R.layout.include_googlenearby_header,null);
        ((Button)headerView).setText(R.string.string_my_contact);
        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), InviteFriendsListActivity.class);
                startActivity(intent);
            }
        });
        ((ListView) view.findViewById(android.R.id.list)).addHeaderView(headerView);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Set up ListView, assign adapter and set some listeners. The adapter was previously
        // created in onCreate().
        setListAdapter(mAdapter);
        getListView().setOnItemClickListener(this);

        //getLoaderManager().initLoader(ContactsQuery.QUERY_ID, null, this);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

        position = position -1;

        Object item = mAdapter.getItem(position);

//        final Uri uri = Contacts.getLookupUri(
//            contact.getId(),
//            contact.getLookupKey());
        if (item instanceof PhoneContact) {
//            Intent intent = new Intent(getActivity(), SendInviteSmsActivity.class);
//            intent.putExtra(Const.INTENT_EXTRA_CONTACT, ((PhoneContact) item));
//            startActivity(intent);
            ((ContactsAdapter.ViewHolder) v.getTag()).mIcon.performClick();
        } else if (item instanceof User) {
            //TODO
            if (TextUtils.isEmpty(((User) item).getJabberId()) && !TextUtils.isEmpty(((User) item).getPhone())) {
                phoneNumber = ((User) item).getPhone();
                Utils.displaySelection(((BaseActivity) getActivity()), GenericAlertPopup.ALERT_DIAL, getString(R.string.popup_call, ((User) item).getName()));
                return;
            }
            //((User) item).setJabberId("f.r.e.esuraj-53e43e3aa3c5d@chat.gtdollar.com");
            Utils.chatWith(getActivity(), ((User) item).getId(), ((User) item).getJabberId(), ((User) item).getName(), !TextUtils.isEmpty(((User) item).getPicture()) ? ((User) item).getPicture() : ((User) item).getCover());
        }
    }


    @Override
    public Loader<ObjectCursor<PhoneContact>> onCreateLoader(int id, Bundle args) {

        // If this is the loader for finding contacts in the Contacts Provider
        // (the only one supported)
        if (id == ContactsQuery.QUERY_ID) {
            Uri contentUri = ContactsQuery.CONTENT_URI;

            // Returns a new CursorLoader for querying the Contacts table. No arguments are used
            // for the selection clause. The search string is either encoded onto the content URI,
            // or no contacts search string is used. The other search criteria are constants. See
            // the ContactsQuery interface.
            return new ObjectCursorLoader<PhoneContact>(getActivity(),
                contentUri,
                ContactsQuery.PROJECTION,
                ContactsQuery.SELECTION,
                ContactsQuery.SORT_ORDER, PhoneContact.FACTORY);
        }

        Log.e(TAG, "onCreateLoader - incorrect ID provided (" + id + ")");
        return null;
    }

    @Override
    public void onLoadFinished(Loader<ObjectCursor<PhoneContact>> loader, ObjectCursor<PhoneContact> data) {
        // This swaps the new cursor into the adapter.
        if (loader.getId() == ContactsQuery.QUERY_ID) {
            //mAdapter.removeAllItems();

            phoneContact.clear();
            if (data != null && data.moveToFirst()) {
                do {
                    phoneContact.add(data.getModel());
                } while (data.moveToNext());
            }
            //mAdapter.removeAllItems();
            phoneContact.addAll(0, userContact);

            mAdapter.setItems(phoneContact);
        }
    }

    @Override
    public void onLoaderReset(Loader<ObjectCursor<PhoneContact>> loader) {
        if (loader.getId() == ContactsQuery.QUERY_ID) {
            // When the loader is being reset, clear the cursor from the adapter. This allows the
            // cursor resources to be freed.
            mAdapter.removeAllItems();
        }
    }

    public void onEventMainThread(GetContactEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED) {
            if (userContact != null) userContact.clear();
            userContact = event.getResponse().getList();
            for (int i = 0; i < userContact.size(); i++) {
                if(!(userContact.get(i) instanceof User)) {
                    //if(TextUtils.isEmpty(((User) userContact.get(i)).getId())) {
                        userContact.remove(userContact.get(i));
                    //}
                }
            }
            phoneContact.addAll(0, userContact);
            mAdapter.setItems(phoneContact);
            //mAdapter.addMoreItems(contacts,false);
        }
    }

    /**
     * Gets the preferred height for each item in the ListView, in pixels, after accounting for
     * screen density. ImageLoader uses this value to resize thumbnail images to match the ListView
     * item height.
     *
     * @return The preferred height in pixels, based on the current theme.
     */
    private int getListPreferredItemHeight() {
        final TypedValue typedValue = new TypedValue();

        // Resolve list item preferred height theme attribute into typedValue
        getActivity().getTheme().resolveAttribute(
            android.R.attr.listPreferredItemHeight, typedValue, true);

        // Create a new DisplayMetrics object
        final DisplayMetrics metrics = new DisplayMetrics();

        // Populate the DisplayMetrics
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        // Return theme value based on DisplayMetrics
        return (int) typedValue.getDimension(metrics);
    }


    /**
     * This is a subclass of CursorAdapter that supports binding Cursor columns to a view layout.
     * If those items are part of search results, the search string is marked by highlighting the
     * query text. An {@link android.widget.AlphabetIndexer} is used to allow quicker navigation up and down the
     * ListView.
     */
    public class ContactsAdapter extends PagingBaseAdapter<Object> {

        int iconSize;

        /**
         * Instantiates a new Contacts Adapter.
         *
         * @param context A context that has access to the app's layout.
         */
        public ContactsAdapter(Context context) {
            super(context);
            iconSize = getListPreferredItemHeight();
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        protected int getViewResourceId(int position) {
            return R.layout.item_contact_list;
        }

        @Override
        protected Renderer<Object> getRenderer(View rootView) {
            return new ViewHolder(rootView);
        }

        /**
         * A class that defines fields for each resource ID in the list item layout. This allows
         * ContactsAdapter.newView() to store the IDs once, when it inflates the layout, instead of
         * calling findViewById in each iteration of bindView.
         */
        class ViewHolder implements Renderer<Object> {
            @InjectView(android.R.id.icon)
            QuickContactBadge mIcon;
            @InjectView(android.R.id.text1)
            TextView mText1;
            @InjectView(R.id.swipe)
            SwipeLayout mSwipe;

            Object item;

            ViewHolder(View view) {
                ButterKnife.inject(this, view);
            }

            @OnClick(R.id.iv_delete)
            void onDeleteItem(View view) {
                if(item instanceof User) {
                    mAdapter.remove(item);
                    mAdapter.notifyDataSetChanged();
                    JobManager.getInstance().removeContact(((User) item).getId());
                }
            }

            @Override
            public void render(Object item, int position) {
                this.item = item;
                if (item instanceof PhoneContact) {
                    mSwipe.setSwipeEnabled(false);
                    PhoneContact phoneContact = ((PhoneContact) item);
                    mText1.setText(phoneContact.getDisplayName());
                    if (phoneContact.isPhoneContact() && !TextUtils.isEmpty(phoneContact.getAvatarUri())) {
                        Picasso.with(getContext()).load(Uri.parse(phoneContact.getAvatarUri())).placeholder(R.drawable.ic_contact_picture_holo_light).fit().into(mIcon);
                    } else {
                        mIcon.setImageResource(R.drawable.ic_contact_picture_holo_light);
                    }

                    // Generates the contact lookup Uri
                    final Uri contactUri = Contacts.getLookupUri(
                        phoneContact.getId(), phoneContact.getLookupKey()
                    );

                    // Binds the contact's lookup Uri to the QuickContactBadge
                    mIcon.assignContactUri(contactUri);

                } else if (item instanceof User) {
                    mSwipe.setSwipeEnabled(true);
                    User user = ((User) item);
                    mText1.setText(user.getName());
                    if (user.getPhone() != null) {
                        mIcon.assignContactFromPhone(user.getPhone(), true);
                    } else if (user.getEmail() != null) {
                        mIcon.assignContactFromEmail(user.getEmail(), true);
                    } else {
                        mIcon.assignContactUri(null);
                    }

                    if (!TextUtils.isEmpty(user.getPicture())) {
                        Picasso.with(getContext()).load(user.getPicture()).placeholder(R.drawable.ic_contact_picture_holo_light).fit().into(mIcon);
                    } else {
                        mIcon.setImageResource(R.drawable.ic_contact_picture_holo_light);
                    }
                }
            }
        }
    }

    public void onEventMainThread(AlertButtonClickEvent event) {
        if(event.isPositive() && event.getAlertType() == GenericAlertPopup.ALERT_DIAL   ) {
            Utils.dial(getActivity(),phoneNumber);
        }
    }

}
