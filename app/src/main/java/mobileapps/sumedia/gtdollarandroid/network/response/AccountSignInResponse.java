package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;

import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.model.Account;

/**
 * Created by zhuodong on 8/20/14.
 */
public class AccountSignInResponse extends AccountBaseResponse {

    private Account data;
    @Expose
    private String token;

    public Account getData() {
        return data;
    }

    public void setData(Account data) {
        this.data = data;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void saveToPref() {
        UserDataHelper.saveAccount(data);
        UserDataHelper.setAccountToken(token);
    }

    @Override
    public String toString() {
        return "AccountSignInResponse{" +
            "token='" + token + '\'' +
            '}';
    }
}


