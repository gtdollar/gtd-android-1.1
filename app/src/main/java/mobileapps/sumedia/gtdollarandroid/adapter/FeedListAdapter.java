package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.Feed;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.widget.CircleImageView;

/**
 * Created by 2359media on 4/7/14.
 */

public class FeedListAdapter extends PagingBaseAdapter<Feed> {

    //int colorWhite, colorUnRead;

    public FeedListAdapter(Context ctx) {
        super(ctx);
        //colorWhite = ctx.getResources().getColor(R.color.white);
        //colorUnRead = ctx.getResources().getColor(R.color.unread);
    }

    @Override
    protected int getViewResourceId(int position) {
        return R.layout.item_feed;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public Renderer<Feed> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }


    class ViewHolder implements Renderer<Feed> {
        @InjectView(R.id.iv_feed_thumb)
        CircleImageView mIvFeedThumb;
        @InjectView(R.id.iv_read_indicator)
        ImageView mIvReadIndicator;
        @InjectView(R.id.tv_feed_subject)
        TextView mTvFeedSubject;
        @InjectView(R.id.tv_feed_date)
        TextView mTvFeedDate;
        @InjectView(R.id.tv_feed_desc)
        TextView mTvFeedDesc;
        @InjectView(R.id.tv_feed_currency)
        TextView mTvFeedCurrency;
        @InjectView(R.id.tv_feed_amount)
        TextView mTvFeedAmount;
        @InjectView(R.id.ll_amount_holder)
        LinearLayout mLlAmountHolder;


        View view;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
            this.view = view;
        }

        public void render(Feed item, int position) {
            if (item.getBaseType() == Feed.TYPE_ACCOUNT) {
                mIvFeedThumb.setImageResource(R.drawable.ic_account_transfer);
                mLlAmountHolder.setVisibility(View.VISIBLE);
                mTvFeedCurrency.setText(item.getSubjectAvatar());
                mTvFeedAmount.setText(item.getExtra());

            } else if (item.getBaseType() == Feed.TYPE_NOTIFICATIONS || item.getBaseType() == Feed.TYPE_CHAT) {

                mLlAmountHolder.setVisibility(View.INVISIBLE);

                if (!TextUtils.isEmpty(item.getSubjectAvatar())) {
                    Picasso.with(getContext()).load(item.getSubjectAvatar()).fit().centerCrop().into(mIvFeedThumb);
                } else {
                    mIvFeedThumb.setImageResource(R.mipmap.ic_launcher);
                }
            }

            mTvFeedDesc.setText(item.getDescString(getContext()));
            mTvFeedSubject.setText(item.getSubjectName());
            mTvFeedDate.setText(item.getDateString());

            mIvReadIndicator.setVisibility(item.getIsRead() ? View.INVISIBLE : View.VISIBLE);
            //view.setBackgroundColor(item.getIsRead() ? colorWhite : colorUnRead);
        }
    }
}
