package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.OrderResponse;

/**
 * Created by zhuodong on 9/8/14.
 */
public class CreateOrderEvent extends BaseEvent<OrderResponse> {
    public CreateOrderEvent(EventStatus status) {
        super(status);
    }

    public CreateOrderEvent(EventStatus status, OrderResponse response) {
        super(status, response);
    }
}
