package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.AddContactEvent;
import mobileapps.sumedia.gtdollarandroid.event.AlertButtonClickEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.GetMerchantEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetServicesEvent;
import mobileapps.sumedia.gtdollarandroid.event.ScaleBitmapEvent;
import mobileapps.sumedia.gtdollarandroid.event.SetMerchantDetailEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.User;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.widget.AspectRatioImageView;
import mobileapps.sumedia.gtdollarandroid.widget.GenericAlertPopup;

public class MerchantDetailActivity extends BasePictureActivity {


    @InjectView(R.id.iv_merchant_cover)
    AspectRatioImageView mIvMerchantCover;
    @InjectView(R.id.tv_merchant_name)
    EditText mTvMerchantName;
    //@InjectView(R.id.iv_merchant_picture)
    //CircleImageView mIvMerchantPicture;
    @InjectView(R.id.iv_merchant_video)
    ImageView mIvMerchantVideo;
    //@InjectView(R.id.tv_merchant_name_2)
    //TextView mTvMerchantName2;
    @InjectView(R.id.iv_phone)
    ImageView mIbPhone;
    @InjectView(R.id.iv_follow)
    ImageView mIvFollow;
    @InjectView(R.id.iv_chat)
    ImageView mIvChat;
    @InjectView(R.id.tv_contact_us)
    TextView mTvContactUs;

    User merchant;
    String merchantId;
    boolean isEditable;

    boolean createNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.string_profile);
        setContentView(R.layout.activity_merchant_detail);
        ButterKnife.inject(this);
        //adjustMenuButtonsToFitScreen();
        merchant = getIntent().getParcelableExtra(Const.INTENT_EXTRA_MERCHANT_KEY);
        merchantId = getIntent().getStringExtra(Const.INTENT_EXTRA_MERCHANT_ID);
        createNew = getIntent().getBooleanExtra(Const.INTENT_EXTRA_NEW_MERCHANT,false);

        if(createNew) {
            mTvContactUs.setVisibility(View.VISIBLE);
        }

        if (merchantId.equals(UserDataHelper.getUserId())) {
            //merchant = UserDataHelper.getUser();
            mIvFollow.setAlpha(0.7f);
            mIvFollow.setEnabled(false);

            mIvChat.setAlpha(0.7f);
            mIvChat.setEnabled(false);
        }

        if (merchant != null) {
            setMerchantDetails();
        }

        if(merchantId.equals(UserDataHelper.getUserId()) || merchant == null) {
            JobManager.getInstance().getMerchant(merchantId);
        }


        isEditable = (merchantId.equals(UserDataHelper.getUserId()));

        mTvMerchantName.setEnabled(isEditable);
        mIvMerchantCover.setEnabled(isEditable);
        if (isEditable) {
            mTvMerchantName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    if (i == EditorInfo.IME_ACTION_DONE && !TextUtils.isEmpty(textView.getText())) {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("name", textView.getText().toString());
                        JobManager.getInstance().updateProfile(params);
                        //mTvMerchantName2.setText(textView.getText());
                        return true;
                    }
                    return false;
                }
            });
        }

        onTabSelected(findViewById(R.id.tab_service));
    }

    private void setMerchantDetails() {
        if (!TextUtils.isEmpty(merchant.getCover())) {
            Picasso.with(this).load(merchant.getCover()).into(mIvMerchantCover); // 牛逼，赞一个
        }else if (!TextUtils.isEmpty(merchant.getPicture())) {
            Picasso.with(this).load(merchant.getPicture()).into(mIvMerchantCover);
        }

//        if (!TextUtils.isEmpty(merchant.getPicture())) {
//            Picasso.with(this).load(merchant.getPicture()).into(mIvMerchantPicture);
//        }

        mTvMerchantName.setText(merchant.getName());
        //mTvMerchantName2.setText(merchant.getName());


        if (TextUtils.isEmpty(merchant.getPhone())) {
            mIbPhone.setAlpha(0.7f);
            mIbPhone.setEnabled(false);
        }

        mIvMerchantVideo.setVisibility(merchant.hasVideo()? View.VISIBLE : View.GONE);

        if(TextUtils.isEmpty(merchant.getJabberId())) {
            mIvChat.setAlpha(0.7f);
            mIvChat.setEnabled(false);
        }
    }

//    private void setMerchantServices() {
//        mLlService.removeAllViews();
//        if (merchant.getServices() != null) {
//            mLlServiceContainer.setVisibility(View.VISIBLE);
//            List<Service> serviceList = merchant.getServices();
//            int size = getResources().getDimensionPixelSize(R.dimen.service_secion_height);
//            for (int i = 0; i < serviceList.size(); i++) {
//                ImageView iv = new ImageView(this);
//                Service service = serviceList.get(i);
//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, size);
//                iv.setLayoutParams(params);
//                iv.setPadding(5, 5, 5, 5);
//                Picasso.with(this).load(service.getPhoto()).placeholder(R.drawable.ic_launcher).fit().centerCrop().into(iv);
//                mLlService.addView(iv);
//                iv.setTag(i);
//                iv.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Utils.viewService(MerchantDetailActivity.this, (ArrayList<Service>) merchant.getServices(), ((Integer) view.getTag()));
//                    }
//                });
//            }
//        }
//    }


    @OnClick({R.id.tab_service, R.id.tab_details})
    void onTabSelected(View view) {
        if(view.isSelected())return;
        view.setSelected(true);

        if(view.getId() == R.id.tab_service) {
            findViewById(R.id.tab_service_selected).setVisibility(View.VISIBLE);
            findViewById(R.id.tab_detail_selected).setVisibility(View.GONE);
            findViewById(R.id.tab_details).setSelected(false);
            showFragment(ServiceListFragment.newInstance(merchant));
        }else {
            findViewById(R.id.tab_service_selected).setVisibility(View.GONE);
            findViewById(R.id.tab_detail_selected).setVisibility(View.VISIBLE);
            findViewById(R.id.tab_service).setSelected(false);
            showFragment(MerchantDetailFragment.newInstance(merchant));

        }
    }

    private void showFragment(Fragment f) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fl_content, f)
            .commitAllowingStateLoss();
        fragmentManager.executePendingTransactions();
    }

//    @OnClick(R.id.ll_profile)
//    public void onClickProfile() {
//        Utils.viewNearbyShop(this, merchant.getLatlong());
//    }

    @OnClick(R.id.iv_merchant_video)
    public void onClickPlay() {
        if (merchant == null) return;
        Utils.playLink(this,merchant.getVideoId());
    }

    @OnClick(R.id.iv_merchant_cover)
    public void onClickCover() {
        if (!isEditable) return;
        GenericSelectionDialog.newInstance(GenericSelectionDialog.TYPE_PICTURE, null).show(getSupportFragmentManager(), "");
    }


    @OnClick({R.id.iv_phone, R.id.iv_follow, R.id.iv_nearby, R.id.iv_chat, R.id.iv_map})
    public void onMenuClick(View view) {
        if (merchant == null) return;
        switch (view.getId()) {
            case R.id.iv_phone:
                //toast(this, "Phone");
                if (!TextUtils.isEmpty(merchant.getPhone())) {
                    Utils.displaySelection(this, GenericAlertPopup.ALERT_DIAL,getString(R.string.popup_call, merchant.getName()));
                }
                break;
            case R.id.iv_follow:
                //toast(this, "Add User");
                JobManager.getInstance().addContact(merchantId);
                break;
            case R.id.iv_nearby:
                //toast(this, "Shopping");
                //Utils.viewServiceList(this, merchant);
                Utils.viewMerchantMap(this, merchant.getLatlong(), merchant.getName());

                break;
            case R.id.iv_chat:
                //toast(this, "Message");
                Utils.chatWith(this, merchant.getId(),merchant.getJabberId(), merchant.getName(), !TextUtils.isEmpty(merchant.getPicture()) ? merchant.getPicture() : merchant.getCover());
                break;
            case R.id.iv_map:
                Utils.viewNearbyShop(this, merchant.getLatlong());

                //toast(this, "Location");
                break;
            default:
                break;
        }
    }


    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    public void onEventMainThread(GetMerchantEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED && event.getResponse() != null) {
            User user = event.getResponse().getUser();
            if (!user.getId().equals(merchantId)) return;
            merchant = user;
            setMerchantDetails();
            EventBus.getDefault().post(new SetMerchantDetailEvent(merchant));
            if(merchantId.equals(UserDataHelper.getUserId())) {
                JobManager.getInstance().getService();
            }
        }
    }

    public void onEventMainThread(AddContactEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED) {
            Utils.toast(this, getString(R.string.toast_followed));
        }
    }

    public void onEventMainThread(ScaleBitmapEvent event) {
        Utils.toggleEventPopup(this, event, new Utils.StatusListener<ScaleBitmapEvent>() {
            @Override
            public void onSucceed(ScaleBitmapEvent event) {

                if (event.getResponse() && !tempPath.isEmpty()) {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("cover", tempPath);
                    Picasso.with(MerchantDetailActivity.this).load(new File(tempPath)).fit().centerCrop().into(mIvMerchantCover);
                    JobManager.getInstance().updateProfile(params);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.merchant_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_more) {
            Utils.viewMerchantOptions(this,merchantId);
            return true;
        } else if (id == R.id.action_share) {
            Utils.screenShotAndShare(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onEventMainThread(AlertButtonClickEvent event) {
        if(event.isPositive() && event.getAlertType() == GenericAlertPopup.ALERT_DIAL   ) {
            Utils.dial(this,merchant.getPhone());
        }
    }

    public void onEventMainThread(GetServicesEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED) {
            if(merchant != null) {
                merchant.setServices(event.getResponse().getService());
                EventBus.getDefault().post(new SetMerchantDetailEvent(merchant));
            }
        }
    }

    @OnClick(R.id.tv_contact_us)
    public void onClickContactUs(View view) {
        if (merchant == null) return;
        Intent intent = new Intent(this, MerchantProfileActivity.class);
        intent.putExtra(Const.INTENT_EXTRA_MERCHANT_KEY, merchant);
        intent.putExtra(Const.INTENT_EXTRA_IS_UPDATING, false);
        startActivity(intent);
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == Const.REQUEST_START_STANDALONE_PLAYER && resultCode != RESULT_OK) {
//            YouTubeInitializationResult errorReason =
//                YouTubeStandalonePlayer.getReturnedInitializationResult(data);
//            if (errorReason.isUserRecoverableError()) {
//                errorReason.getErrorDialog(this, 0).show();
//            } else {
//                String errorMessage =
//                    String.format(getString(R.string.error_player), errorReason.toString());
//                Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
//            }
//        }
//    }
}
