package mobileapps.sumedia.gtdollarandroid.model;

import android.database.Cursor;

import java.util.Map;

import mobileapps.sumedia.gtdollarandroid.chat.ChatConstants;
import mobileapps.sumedia.gtdollarandroid.helper.GsonHelper;
import mobileapps.sumedia.gtdollarandroid.widget.CursorCreator;


/**
 * Created by zhuodong on 8/13/14.
 */
public class Chat implements CursorCreator<Chat>{
    int id;

    int direction;
    String message;
    String messageType;
    long date;
    int deliveryStatus;

    String JID;
    String packetID;

    String avatarUrl;
    String name;

    Map<String,String> properties;

    @Override
    public Chat createFromCursor(Cursor c) {
        Chat chat = new Chat();

        chat.id = c.getInt(c.getColumnIndex(ChatConstants._ID));
        chat.direction = c.getInt(c.getColumnIndex(ChatConstants.DIRECTION));
        chat.message = c.getString(c.getColumnIndex(ChatConstants.MESSAGE));
        chat.messageType = c.getString(c.getColumnIndex(ChatConstants.MESSAGE_TYPE));
        chat.date = c.getLong(c.getColumnIndex(ChatConstants.DATE));
        chat.deliveryStatus = c.getInt(c.getColumnIndex(ChatConstants.DELIVERY_STATUS));

        chat.JID = c.getString(c.getColumnIndex(ChatConstants.JID));
        chat.packetID = c.getString(c.getColumnIndex(ChatConstants.PACKET_ID));

        chat.properties = GsonHelper.jsonToMap(c.getString(c.getColumnIndex(ChatConstants.PROPERTIES)));

        return chat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(int deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getJID() {
        return JID;
    }

    public void setJID(String JID) {
        this.JID = JID;
    }

    public String getPacketID() {
        return packetID;
    }

    public void setPacketID(String packetID) {
        this.packetID = packetID;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public static final Chat FACTORY = new Chat();

}
