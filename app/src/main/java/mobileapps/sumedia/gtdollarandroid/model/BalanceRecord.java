package mobileapps.sumedia.gtdollarandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zhuodong on 8/21/14.
 */
public class BalanceRecord implements Parcelable {

    @Expose
    private String id;
    @Expose
    private String direction;
    @Expose
    private String sender;
    @Expose
    private String senduser;
    @Expose
    private String receiver;
    @Expose
    private String recvuser;
    @Expose
    private String userid;
    @Expose
    private String username;
    @SerializedName("currency_rate")
    @Expose
    private String currencyRate;
    @Expose
    private String oamount;
    @SerializedName("oamount_ex")
    @Expose
    private String oamountEx;
    @SerializedName("settle_amount")
    @Expose
    private String settleAmount;
    @Expose
    private String amount;
    @Expose
    private String tdate;
    @Expose
    private String period;
    @Expose
    private String ostatus;
    @Expose
    private String type;
    @Expose
    private String otype;
    @Expose
    private String status;
    @Expose
    private double ofees;
    @Expose
    private String fees;
    @Expose
    private double onets;
    @Expose
    private String nets;
    @Expose
    private String comments;
    @Expose
    private String ecomments;
    @Expose
    private boolean canview;
    @Expose
    private boolean canrefund;
    @Expose
    private boolean candispute;
    @Expose
    private String checkone;
    @Expose
    private String checktwo;
    @Expose
    private String checkmanager;
    @Expose
    private String currency;
    @Expose
    private String country;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenduser() {
        return senduser;
    }

    public void setSenduser(String senduser) {
        this.senduser = senduser;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getRecvuser() {
        return recvuser;
    }

    public void setRecvuser(String recvuser) {
        this.recvuser = recvuser;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(String currencyRate) {
        this.currencyRate = currencyRate;
    }

    public String getOamount() {
        return oamount;
    }

    public void setOamount(String oamount) {
        this.oamount = oamount;
    }

    public String getOamountEx() {
        return oamountEx;
    }

    public void setOamountEx(String oamountEx) {
        this.oamountEx = oamountEx;
    }

    public String getSettleAmount() {
        return settleAmount;
    }

    public void setSettleAmount(String settleAmount) {
        this.settleAmount = settleAmount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTdate() {
        return tdate;
    }

    public void setTdate(String tdate) {
        this.tdate = tdate;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getOstatus() {
        return ostatus;
    }

    public void setOstatus(String ostatus) {
        this.ostatus = ostatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOtype() {
        return otype;
    }

    public void setOtype(String otype) {
        this.otype = otype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getOfees() {
        return ofees;
    }

    public void setOfees(double ofees) {
        this.ofees = ofees;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public double getOnets() {
        return onets;
    }

    public void setOnets(double onets) {
        this.onets = onets;
    }

    public String getNets() {
        return nets;
    }

    public void setNets(String nets) {
        this.nets = nets;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getEcomments() {
        return ecomments;
    }

    public void setEcomments(String ecomments) {
        this.ecomments = ecomments;
    }

    public boolean isCanview() {
        return canview;
    }

    public void setCanview(boolean canview) {
        this.canview = canview;
    }

    public boolean isCanrefund() {
        return canrefund;
    }

    public void setCanrefund(boolean canrefund) {
        this.canrefund = canrefund;
    }

    public boolean isCandispute() {
        return candispute;
    }

    public void setCandispute(boolean candispute) {
        this.candispute = candispute;
    }

    public String getCheckone() {
        return checkone;
    }

    public void setCheckone(String checkone) {
        this.checkone = checkone;
    }

    public String getChecktwo() {
        return checktwo;
    }

    public void setChecktwo(String checktwo) {
        this.checktwo = checktwo;
    }

    public String getCheckmanager() {
        return checkmanager;
    }

    public void setCheckmanager(String checkmanager) {
        this.checkmanager = checkmanager;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.direction);
        dest.writeString(this.sender);
        dest.writeString(this.senduser);
        dest.writeString(this.receiver);
        dest.writeString(this.recvuser);
        dest.writeString(this.userid);
        dest.writeString(this.username);
        dest.writeString(this.currencyRate);
        dest.writeString(this.oamount);
        dest.writeString(this.oamountEx);
        dest.writeString(this.settleAmount);
        dest.writeString(this.amount);
        dest.writeString(this.tdate);
        dest.writeString(this.period);
        dest.writeString(this.ostatus);
        dest.writeString(this.type);
        dest.writeString(this.otype);
        dest.writeString(this.status);
        dest.writeDouble(this.ofees);
        dest.writeString(this.fees);
        dest.writeDouble(this.onets);
        dest.writeString(this.nets);
        dest.writeString(this.comments);
        dest.writeString(this.ecomments);
        dest.writeByte(canview ? (byte) 1 : (byte) 0);
        dest.writeByte(canrefund ? (byte) 1 : (byte) 0);
        dest.writeByte(candispute ? (byte) 1 : (byte) 0);
        dest.writeString(this.checkone);
        dest.writeString(this.checktwo);
        dest.writeString(this.checkmanager);
        dest.writeString(this.currency);
        dest.writeString(this.country);
    }

    public BalanceRecord() {
    }

    private BalanceRecord(Parcel in) {
        this.id = in.readString();
        this.direction = in.readString();
        this.sender = in.readString();
        this.senduser = in.readString();
        this.receiver = in.readString();
        this.recvuser = in.readString();
        this.userid = in.readString();
        this.username = in.readString();
        this.currencyRate = in.readString();
        this.oamount = in.readString();
        this.oamountEx = in.readString();
        this.settleAmount = in.readString();
        this.amount = in.readString();
        this.tdate = in.readString();
        this.period = in.readString();
        this.ostatus = in.readString();
        this.type = in.readString();
        this.otype = in.readString();
        this.status = in.readString();
        this.ofees = in.readDouble();
        this.fees = in.readString();
        this.onets = in.readDouble();
        this.nets = in.readString();
        this.comments = in.readString();
        this.ecomments = in.readString();
        this.canview = in.readByte() != 0;
        this.canrefund = in.readByte() != 0;
        this.candispute = in.readByte() != 0;
        this.checkone = in.readString();
        this.checktwo = in.readString();
        this.checkmanager = in.readString();
        this.currency = in.readString();
        this.country = in.readString();
    }

    public static final Parcelable.Creator<BalanceRecord> CREATOR = new Parcelable.Creator<BalanceRecord>() {
        public BalanceRecord createFromParcel(Parcel source) {
            return new BalanceRecord(source);
        }

        public BalanceRecord[] newArray(int size) {
            return new BalanceRecord[size];
        }
    };
}
