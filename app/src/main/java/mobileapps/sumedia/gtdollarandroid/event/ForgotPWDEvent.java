package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.BaseResponse;

/**
 * Created by zhuodong on 8/20/14.
 */
public class ForgotPWDEvent extends BaseEvent<BaseResponse> {
    public ForgotPWDEvent(EventStatus status) {
        super(status);
    }

    public ForgotPWDEvent(EventStatus status, BaseResponse response) {
        super(status, response);
    }
}
