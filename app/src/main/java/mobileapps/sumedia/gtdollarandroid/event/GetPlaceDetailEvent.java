package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.GooglePlaceDetailResponse;

/**
 * Created by zhuodong on 9/12/14.
 */
public class GetPlaceDetailEvent extends BaseEvent<GooglePlaceDetailResponse> {
    public GetPlaceDetailEvent(EventStatus status) {
        super(status);
    }

    public GetPlaceDetailEvent(EventStatus status, GooglePlaceDetailResponse response) {
        super(status, response);
    }
}
