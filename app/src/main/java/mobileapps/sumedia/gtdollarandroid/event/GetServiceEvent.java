package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.ServiceResponse;

/**
 * Created by zhuodong on 9/5/14.
 */
public class GetServiceEvent extends BaseEvent<ServiceResponse> {
    public GetServiceEvent(EventStatus status) {
        super(status);
    }

    public GetServiceEvent(EventStatus status, ServiceResponse response) {
        super(status, response);
    }
}
