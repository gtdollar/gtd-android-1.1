package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.SetMerchantDetailEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.model.User;

public class MerchantDetailFragment extends Fragment{


    User merchant;
    @InjectView(R.id.tv_types)
    TextView tvTypes;
    @InjectView(R.id.tv_address)
    TextView tvAddress;
    @InjectView(R.id.tv_phone)
    TextView tvPhone;
    @InjectView(R.id.tv_website)
    TextView tvWebsite;


    public static MerchantDetailFragment newInstance(User merchant) {
        MerchantDetailFragment fragment = new MerchantDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(Const.INTENT_EXTRA_MERCHANT_KEY, merchant);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        merchant = getArguments().getParcelable(Const.INTENT_EXTRA_MERCHANT_KEY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_merchant_detail, container, false);
        ButterKnife.inject(this, view);
        setMerchantDetails();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }


    public void onEventMainThread(SetMerchantDetailEvent event) {

        merchant = event.getMerchant();
        setMerchantDetails();

    }

    private void setMerchantDetails() {
        if(merchant == null)return;

        tvTypes.setText(GTDHelper.serviceTypeName(merchant.getCategories()));
        tvAddress.setText(merchant.getAddress());
        //tvContactUs.setText(merchant.get);
        tvPhone.setText(merchant.getPhone());
        tvWebsite.setText(merchant.getWebsite());
    }

}
