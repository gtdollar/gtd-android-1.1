package mobileapps.sumedia.gtdollarandroid.ui;


import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;

public class GenericWebViewActivity extends BaseActivity {

    @InjectView(R.id.wvDetail)
    WebView mWvDetail;

    @InjectView(R.id.pbLoading)
    ProgressBar mPbLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        ButterKnife.inject(this);

        String url = getIntent().getStringExtra("url");

        WebSettings settings = mWvDetail.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
//        if(webMode != Const.WEB_MODE_STATISTICS)
//            settings.setTextZoom(200);
        WebViewClient client = new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                mPbLoading.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(GenericWebViewActivity.this, "There was an error loading the webpage!",
                        Toast.LENGTH_LONG).show();
                mPbLoading.setVisibility(View.GONE);
            }
        };
        mWvDetail.setWebViewClient(client);

        mWvDetail.loadUrl(url);
    }

    @Override
    protected boolean hasBack() {
        return true;
    }
}
