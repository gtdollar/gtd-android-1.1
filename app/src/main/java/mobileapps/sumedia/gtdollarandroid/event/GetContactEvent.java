package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.ContactsResponse;

/**
 * Created by zhuodong on 9/8/14.
 */
public class GetContactEvent extends BaseEvent<ContactsResponse> {
    public GetContactEvent(EventStatus status) {
        super(status);
    }

    public GetContactEvent(EventStatus status, ContactsResponse response) {
        super(status, response);
    }
}
