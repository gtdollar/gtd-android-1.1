package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 4/27/15.
 */
public class AttemptCancelOrderEvent {
    String orderId;

    public AttemptCancelOrderEvent(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }
}
