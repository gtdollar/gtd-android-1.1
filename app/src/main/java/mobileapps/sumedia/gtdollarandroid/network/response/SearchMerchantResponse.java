package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

import mobileapps.sumedia.gtdollarandroid.model.User;

public class SearchMerchantResponse extends BaseResponse{

    @Expose
    private List<User> users = new ArrayList<User>();

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
