package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 4/22/15.
 */
public class GetAddressEvent {
    private final String address;

    public GetAddressEvent(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }
}
