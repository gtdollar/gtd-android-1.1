package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.ChoseItemEvent;
import mobileapps.sumedia.gtdollarandroid.event.DateSetEvent;
import mobileapps.sumedia.gtdollarandroid.event.ScaleBitmapEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Formatter;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.widget.AspectRatioImageView;
import mobileapps.sumedia.gtdollarandroid.widget.DatePickerFragment;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.validations.IsEmail;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

public class ApplyCreditCardActivity extends BasePictureActivity {


    @InjectView(R.id.tv_currency)
    TextView mTvCurrency;

    Form mForm;
    @InjectView(R.id.iv_logo)
    ImageView mIvLogo;
    @InjectView(R.id.tv_credit_card_title)
    TextView mTvCreditCardTitle;
    @InjectView(R.id.iv_chip)
    AspectRatioImageView mIvChip;
    @InjectView(R.id.tv_card_number)
    TextView mTvCardNumber;
    @InjectView(R.id.tv_card_valid_date)
    TextView mTvCardValidDate;
    @InjectView(R.id.tv_card_name)
    TextView mTvCardName;
    @InjectView(R.id.et_name)
    EditText mEtName;
    @InjectView(R.id.et_email)
    EditText mEtEmail;
    @InjectView(R.id.tv_start_date)
    TextView mTvStartDate;
    @InjectView(R.id.et_phone)
    EditText mEtPhone;
    @InjectView(R.id.et_address)
    EditText mEtAddress;
    @InjectView(R.id.tv_type)
    TextView mTvType;
    @InjectView(R.id.tv_region)
    TextView mTvRegion;
    @InjectView(R.id.btn_register)
    Button mBtnRegister;

    Map<String, String> params = new HashMap<String, String>();
    @InjectView(R.id.iv_attach)
    ImageView mIvAttach;

    Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_credit_card);
        ButterKnife.inject(this);
        setTitle(R.string.title_activity_apply_credit_card);

        mTvCurrency.setTag(GenericSelectionDialog.TYPE_CURRENCY);
        mTvRegion.setTag(GenericSelectionDialog.TYPE_COUNTRY_ORIGIN);
        mTvType.setTag(GenericSelectionDialog.TYPE_MERCHANT_TYPE);

        mEtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                mTvCardName.setText(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        buildForm();
    }

    private void buildForm() {
        mForm = new Form(this);
        mForm.addField(Field.using(mEtName).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mEtEmail).validate(NotEmpty.build(this)).validate(
            IsEmail.build(this)));
        mForm.addField(Field.using(mEtPhone).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mTvType).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mEtAddress).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mTvCurrency).validate(NotEmpty.build(this)));

    }

    @OnClick(R.id.btn_register)
    public void onRegister(View view) {
        if (!mForm.isValid()) return;

        if (!TextUtils.isEmpty(mEtEmail.getText())) {
            params.put("email", mEtEmail.getText().toString());
        }

        if (!TextUtils.isEmpty(mEtName.getText())) {
            params.put("name", mEtName.getText().toString());
        }


        if (!TextUtils.isEmpty(mEtPhone.getText())) {
            params.put("phone", mEtPhone.getText().toString());
        }


        if (!TextUtils.isEmpty(mTvRegion.getText())) {
            params.put("region", mTvRegion.getText().toString());
        }

        if (!TextUtils.isEmpty(mTvType.getText())) {
            params.put("categories", GTDHelper.findType(mTvType.getText().toString()) + "");
        }


    }


    @OnClick({R.id.tv_start_date})
    void onSelectTime(View view) {
        DatePickerFragment.newInstance(true).show(getSupportFragmentManager(), "timePicker");
    }

    @OnClick({R.id.tv_currency, R.id.tv_type, R.id.tv_region})
    public void onClickEdit(View view) {
        GenericSelectionDialog.newInstance(((Integer) view.getTag()), ((TextView) view).getText()).show(getSupportFragmentManager(), "");
    }

    @OnClick(R.id.iv_attach)
    public void onClickImage(View view) {

        GenericSelectionDialog.newInstance(GenericSelectionDialog.TYPE_PICTURE, null).show(getSupportFragmentManager(), "");
    }

    public void onEventMainThread(DateSetEvent event) {
        if (event == null) {
            return;
        }

        calendar.set(event.getYear(), event.getMonth(), event.getDay());

        mTvStartDate.setError(null);
        mTvStartDate.setText(Formatter.unixTimeStampToDate(calendar.getTimeInMillis(), Formatter.DATE_FORMAT));

    }

    public void onEventMainThread(ChoseItemEvent event) {
        switch (event.getType()) {
            case GenericSelectionDialog.TYPE_CURRENCY:
                mTvCurrency.setText(event.getItem());
                break;
            case GenericSelectionDialog.TYPE_COUNTRY_ORIGIN:
                mTvRegion.setText(event.getItem());
                break;
            case GenericSelectionDialog.TYPE_MERCHANT_TYPE:
                mTvType.setText(event.getItem());
                break;
        }
        super.onEventMainThread(event);
    }

    public void onEventMainThread(ScaleBitmapEvent event) {
        Utils.toggleEventPopup(this, event, new Utils.StatusListener<ScaleBitmapEvent>() {
                @Override
                public void onSucceed(ScaleBitmapEvent event) {
                    if (event.getResponse() && !tempPath.isEmpty()) {
                        params.put("photo", tempPath);
                        Picasso.with(ApplyCreditCardActivity.this).load(new File(tempPath)).fit().centerCrop().into(mIvAttach);
                    }
                }
            }

        );
    }

//    public void onEventMainThread(UpdateProfileEvent event) {
//        Utils.toggleEventPopup(this, event, new Utils.StatusListener<UpdateProfileEvent>() {
//            @Override
//            public void onSucceed(UpdateProfileEvent event) {
//                Utils.toast(ApplyCreditCardActivity.this, isUpdating ? getString(R.string.toast_updated) : getString(R.string.string_created));
//            }
//        });
//    }
}