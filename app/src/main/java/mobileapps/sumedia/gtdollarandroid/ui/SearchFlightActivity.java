package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FilterQueryProvider;
import android.widget.TextView;

import java.util.Calendar;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.DateSetEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Formatter;
import mobileapps.sumedia.gtdollarandroid.helper.SQLiteHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.widget.DatePickerFragment;

import static mobileapps.sumedia.gtdollarandroid.helper.SQLiteHelper.AirportContract;

public class SearchFlightActivity extends BaseActivity {


    @InjectView(R.id.tv_round_trip)
    TextView mTvRoundTrip;
    @InjectView(R.id.tv_one_way)
    TextView mTvOneWay;
    @InjectView(R.id.et_from)
    AutoCompleteTextView mEtFrom;
    @InjectView(R.id.tv_from_short)
    TextView mTvFromShort;
    @InjectView(R.id.et_to)
    AutoCompleteTextView mEtTo;
    @InjectView(R.id.tv_to_short)
    TextView mTvToShort;
    @InjectView(R.id.divider1)
    View mDivider1;
    @InjectView(R.id.et_leavedate)
    TextView mEtLeavedate;
    @InjectView(R.id.et_arrivingdate)
    TextView mEtArrivingdate;
    @InjectView(R.id.divider2)
    View mDivider2;
    @InjectView(R.id.tv_minus)
    TextView mTvMinus;
    @InjectView(R.id.tv_count)
    TextView mTvCount;
    @InjectView(R.id.tv_plus)
    TextView mTvPlus;
    @InjectView(R.id.divider3)
    View mDivider3;
    @InjectView(R.id.btn_search)
    Button mBtnSearch;

    Calendar calendarStart, calendarEnd;
    private boolean isEditStartDate, isEditEndDate;

    public static final String FLIGHT_DATE_FORMAT = "yyyy-MM-dd";
    public static final int AIRPORT_ID = 0;
    public static final int AIRPORT_CODE = 1;
    public static final int AIRPORT_CITY_NAME = 2;

    int passengerCount = 1;

    SQLiteHelper sqLiteHelper;

    String fromCountryName, toCountryName, fromAirportCode, toAirportCode;
    int fromCountryId, toCountryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitiy_search_flight);
        ButterKnife.inject(this);

        mDivider1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mDivider2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mDivider3.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        calendarStart = Calendar.getInstance();
        calendarEnd = Calendar.getInstance();

        mTvCount.setText(String.valueOf(passengerCount));

        mTvRoundTrip.setSelected(true);

        //AutoCompleteCursorAdapter autoCompleteCursorAdapter = new AutoCompleteCursorAdapter()

        SimpleCursorAdapter autoCompleteCursorAdapter = new AutoCompleteCursorAdapter(this, android.R.layout.simple_list_item_1, null,
            new String[] { AirportContract.AIRPORT_CODE},
            new int[] {android.R.id.text1},
            0);

        mEtFrom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = ((Cursor) parent.getItemAtPosition(position));
                fromCountryId = cursor.getInt(AIRPORT_ID);
                fromCountryName = cursor.getString(AIRPORT_CITY_NAME);
                fromAirportCode = cursor.getString(AIRPORT_CODE);
                mTvFromShort.setText(fromAirportCode);
                Utils.hideKeyBoard(SearchFlightActivity.this,mEtFrom);
            }
        });

        mEtTo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = ((Cursor) parent.getItemAtPosition(position));
                toCountryId = cursor.getInt(AIRPORT_ID);
                toCountryName = cursor.getString(AIRPORT_CITY_NAME);
                toAirportCode = cursor.getString(AIRPORT_CODE);
                mTvToShort.setText(toAirportCode);
                Utils.hideKeyBoard(SearchFlightActivity.this,mEtTo);
            }
        });

        mEtFrom.setAdapter(autoCompleteCursorAdapter);

        mEtTo.setAdapter(autoCompleteCursorAdapter);

        autoCompleteCursorAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence str) {
                return getCursor(str);
            } });

        autoCompleteCursorAdapter.setCursorToStringConverter(new SimpleCursorAdapter.CursorToStringConverter() {
            public CharSequence convertToString(Cursor cur) {
                String countryName = cur.getString(AIRPORT_CITY_NAME);
                if(countryName != null) {
                    countryName = countryName.substring(0, 1).toUpperCase() + countryName.substring(1).toLowerCase();
                }
                return countryName;
            }});

        sqLiteHelper = SQLiteHelper.getInstance(this);

    }

    @OnClick({R.id.tv_round_trip, R.id.tv_one_way})
    public void onTabSelected(View view) {
        mTvRoundTrip.setSelected(view.getId() == R.id.tv_round_trip);
        mTvOneWay.setSelected(view.getId() == R.id.tv_one_way);
    }

    @OnClick({R.id.et_leavedate, R.id.et_arrivingdate})
    void onSelectTime(View view) {
        isEditStartDate = false;
        isEditEndDate = false;

        if (view.getId() == R.id.et_leavedate) {
            isEditStartDate = true;
        } else if (view.getId() == R.id.et_arrivingdate) {
            isEditEndDate = true;
        }

        DatePickerFragment.newInstance().show(getSupportFragmentManager(), "timePicker");
    }

    @SuppressWarnings("ResourceType")
    public void onEventMainThread(DateSetEvent event) {
        if (event == null) {
            return;
        }


        if (isEditStartDate) {
            calendarStart.set(event.getYear(), event.getMonth(), event.getDay());
            mEtLeavedate.setError(null);
            mEtLeavedate.setText(Formatter.unixTimeStampToDate(calendarStart.getTimeInMillis(), FLIGHT_DATE_FORMAT));

        } else if (isEditEndDate) {
            calendarEnd.set(event.getYear(), event.getMonth(), event.getDay());
            mEtArrivingdate.setError(null);
            mEtArrivingdate.setText(Formatter.unixTimeStampToDate(calendarEnd.getTimeInMillis(), FLIGHT_DATE_FORMAT));
        }
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    @OnClick(R.id.tv_minus)
    void onClickReduce(View view) {
        if (passengerCount > 1) {
            passengerCount--;
            mTvCount.setText(String.valueOf(passengerCount));
        }
    }

    @OnClick(R.id.tv_plus)
    void onClickAdd(View view) {
        passengerCount++;
        mTvCount.setText(String.valueOf(passengerCount));
    }

    public Cursor getCursor(CharSequence str) {
        String select = AirportContract.AIRPORT_CODE + " LIKE ? "
        +" or " + AirportContract.CITY_NAME + " LIKE ? "
        +" or " + AirportContract.CITY_NAME_CHINESE + " LIKE ? "
        +" or " + AirportContract.AIRPORT_NAME + " LIKE ? ";

        String[]  selectArgs = { "%" + str + "%","%" + str + "%","%" + str + "%","%" + str + "%"};
        String[] projection = new String[] {
            AirportContract._ID,
            AirportContract.AIRPORT_CODE,
            AirportContract.CITY_NAME};


        return sqLiteHelper.getReadableDatabase().query(AirportContract.TABLE_NAME,projection,select,selectArgs,null,null,null);
    }

    class AutoCompleteCursorAdapter extends SimpleCursorAdapter {


        //int AIRPORT_CODE_INDEX = -1;
        //int CITY_NAME_INDEX = -1;

        AutoCompleteCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
            super(context, layout, c, from, to, flags);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View view =  super.newView(context, cursor, parent);
            ((TextView) view.findViewById(android.R.id.text1)).setTextSize(10);
            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
//            if (AIRPORT_CODE_INDEX == -1) {
//                AIRPORT_CODE_INDEX = cursor.getColumnIndex(AirportContract.AIRPORT_CODE);
//                CITY_NAME_INDEX = cursor.getColumnIndex(AirportContract.CITY_NAME);
//            }

            String countryName = cursor.getString(AIRPORT_CITY_NAME);
            if(countryName != null) {
                countryName = countryName.substring(0, 1).toUpperCase() + countryName.substring(1).toLowerCase();
            }
            String airportCode = cursor.getString(AIRPORT_CODE).toUpperCase();

            ((TextView) view.findViewById(android.R.id.text1)).setText(countryName+" ("+airportCode+")");

        }
    }
}
