//package mobileapps.sumedia.gtdollarandroid.ui;
//
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.ResultReceiver;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentActivity;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentPagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.inputmethod.EditorInfo;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import java.util.Collections;
//import java.util.List;
//import java.util.Random;
//
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import butterknife.OnClick;
//import mobileapps.sumedia.gtdollarandroid.GTDApplication;
//import mobileapps.sumedia.gtdollarandroid.Message;
//import mobileapps.sumedia.gtdollarandroid.MessageDao;
//import mobileapps.sumedia.gtdollarandroid.R;
//import mobileapps.sumedia.gtdollarandroid.SimpleBackgroundTask;
//import mobileapps.sumedia.gtdollarandroid.adapter.SimpleChatAdapter;
//import mobileapps.sumedia.gtdollarandroid.helper.Utils;
//import mobileapps.sumedia.gtdollarandroid.widget.PagingListView;
//import mobileapps.sumedia.gtdollarandroid.widget.Pagingnable;
//
//public class ChatActivityOLD extends FragmentActivity {
//
//
//    @InjectView(R.id.et_message)
//    EditText mEtMessage;
//    @InjectView(R.id.btn_send_message)
//    TextView mBtnSendMessage;
//    @InjectView(R.id.lv_chat)
//    PagingListView mLvChat;
//    @InjectView(R.id.ll_chat_box_holder)
//    LinearLayout mLlChatBoxHolder;
//
//    MessageDao messageDao;
//
//    Random random = new Random();
//
//    int pageNo = 1;
//    public static final int PAGE_SIZE = 10;
//    SimpleChatAdapter chatAdapter;
//    @InjectView(R.id.iv_option)
//    ImageView mIvOption;
//    @InjectView(R.id.vp_chat_func)
//    ViewPager mVpChatFunc;
//
//    boolean isOptionOpened = false;
//    @InjectView(R.id.ll_root)
//    LinearLayout mLlRoot;
//
//    int rootViewHeight;
//    int keyboardHeight;
//
//    public static final int THREAD_HOLD = 100;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_chat);
//        ButterKnife.inject(this);
//        messageDao = GTDApplication.getInstance().getDaoSession().getMessageDao();
//
////        mEtMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
////            @Override
////            public void onFocusChange(View view, boolean b) {
////                Log.d(" focus ",b+"");
////                if(b) {
////                    mVpChatFunc.setVisibility(View.VISIBLE);
////                }
////            }
////        });
//
////        mEtMessage.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                mVpChatFunc.setVisibility(View.VISIBLE);
////            }
////        });
//
//        mEtMessage.setImeOptions(EditorInfo.IME_ACTION_SEND);
//        mEtMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//                if (i == EditorInfo.IME_ACTION_SEND) {
//                    sendMessage(mBtnSendMessage);
//                    return true;
//                }
//                return false;
//            }
//        });
//
//        mVpChatFunc.setAdapter(new ChatFuncPagerAdapter(this.getSupportFragmentManager()));
//
//        setupListView();
//
////        mLlRoot.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
////
////            @Override
////            public void onGlobalLayout() {
////                // TODO Auto-generated method stub
////                mLlRoot.getViewTreeObserver().removeGlobalOnLayoutListener(this);
////                rootViewHeight = mLlRoot.getHeight();
////                Log.d("Root Size", "Size: " + mLlRoot.getHeight());
////            }
////        });
////
////        mLlRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
////            @Override
////            public boolean onPreDraw() {
////                //int heightDifference = mLlRoot.getRootView().getHeight() - mLlRoot.getHeight();
////                //mLlRoot.getViewTreeObserver().removeOnPreDrawListener(this);
////                Log.d("Root Size", "Size: " + mLlRoot.getHeight());
////                Log.d("Keyboard Size", "Size: " + keyboardHeight);
////                return true;
////
////            }
////        });
//
//    }
//
//    public void setupListView() {
//
//        chatAdapter = new SimpleChatAdapter(this);
//        mLvChat.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                mVpChatFunc.setVisibility(View.GONE);
//                Utils.hideKeyBoard(ChatActivityOLD.this, mEtMessage, resultReceiver);
//                return false;
//            }
//        });
//        mLvChat.setAdapter(chatAdapter);
//        mLvChat.setHasMoreItems(true);
//        mLvChat.setPagingnableListener(new Pagingnable() {
//            @Override
//            public void onLoadMoreItems() {
//                Log.d("", " load more");
//                new SimpleBackgroundTask<List<Message>>(ChatActivityOLD.this) {
//
//                    @Override
//                    protected List<Message> onRun() {
//                        return messageDao.queryBuilder().orderDesc(MessageDao.Properties.Created_at).limit(PAGE_SIZE).offset((pageNo - 1) * PAGE_SIZE).list();
//                    }
//
//                    @Override
//                    protected void onSuccess(List<Message> result) {
//                        Collections.reverse(result);
//                        mLvChat.onFinishLoading(result != null && result.size() >= PAGE_SIZE);
//                        //List<Message> items = chatAdapter.getItems();
//                        //items.addAll(0, result);
//                        int index = mLvChat.getFirstVisiblePosition() + result.size();
//                        View v = mLvChat.getChildAt(0);
//                        int top = (v == null) ? 0 : v.getTop();
//                        chatAdapter.addMoreItems(result, false);
//                        mLvChat.setSelectionFromTop(index, top);
//
//                        pageNo++;
//                    }
//                }.execute();
//                //JobManager.getInstance().getNearbyProducts(false,0,0,pageNo+1);
//            }
//        });
//    }
//
//    class KeyboardResultReceiver extends ResultReceiver {
//
//        public KeyboardResultReceiver(Handler handler) {
//            super(handler);
//        }
//
//        @Override
//        protected void onReceiveResult(int resultCode, Bundle resultData) {
//            super.onReceiveResult(resultCode, resultData);
//            Log.d("", " result code " + resultCode);
////            if(resultCode == RESULT_HIDDEN || resultCode == RESULT_UNCHANGED_HIDDEN) {
////                mVpChatFunc.setVisibility(isOptionOpened ? View.VISIBLE : View.GONE);
////            }
//        }
//    }
//
//    KeyboardResultReceiver resultReceiver = new KeyboardResultReceiver(new Handler());
//
//    @OnClick(R.id.iv_option)
//    void onClickOptions(View view) {
////        if (isOptionOpened) {
////            mEtMessage.requestFocus();
////            Utils.showKeyBoard(this, mEtMessage);
////        } else {
////        }
//
//        Utils.hideKeyBoard(this, mEtMessage, resultReceiver);
//
//        mVpChatFunc.setVisibility(isOptionOpened ? View.GONE : View.VISIBLE);
//
//        isOptionOpened = !isOptionOpened;
//
//    }
//
//    @OnClick(R.id.btn_send_message)
//    void sendMessage(View view) {
//        if (mEtMessage.getText().length() == 0) {
//            Toast.makeText(ChatActivityOLD.this, "Can't send empty message", Toast.LENGTH_SHORT).show();
//        } else {
//            final Message message = new Message();
//            message.setMessage_status(Message.STATUS_SENDING);
//            message.setContent(mEtMessage.getText().toString());
//            int direction = random.nextInt(2);
//            Log.d("", " direction " + direction);
//            message.setDirection(direction);
//            message.setMessage_type(Message.MESSAGE_TEXT);
//            message.setCreated_at(System.currentTimeMillis());
//            long id = messageDao.insert(message);
//            message.setId(id);
//            chatAdapter.add(message);
//            mEtMessage.setText("");
//            new SimpleBackgroundTask<Boolean>(ChatActivityOLD.this) {
//
//                @Override
//                protected Boolean onRun() {
//                    try {
//                        Thread.sleep(5000);
//                        return false;
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    return true;
//                }
//
//                @Override
//                protected void onSuccess(Boolean result) {
//                    message.setMessage_status(Message.STATUS_SENT);
//                    messageDao.update(message);
//                    chatAdapter.notifyDataSetChanged();
//                }
//            }.execute();
//        }
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.chat, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    public static class ChatFuncPagerAdapter extends FragmentPagerAdapter {
//
//        public ChatFuncPagerAdapter(FragmentManager fm) {
//            super(fm);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            return OptionFragment.newInstance(position);
//        }
//
//        @Override
//        public int getCount() {
//            return 2;
//        }
//    }
//
//    public static class OptionFragment extends Fragment {
//        int position;
//
//        public static OptionFragment newInstance(int position) {
//            Bundle b = new Bundle();
//            b.putInt("index", position);
//            OptionFragment optionFragment = new OptionFragment();
//            optionFragment.setArguments(b);
//            return optionFragment;
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//            position = getArguments().getInt("index", 0);
//
//            View view = inflater.inflate(position == 0 ? R.layout.item_chat_function_page_1 : R.layout.item_chat_function_page_2, container, false);
//            return view;
//        }
//    }
//}
