package mobileapps.sumedia.gtdollarandroid.network;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zhuodong on 11/25/14.
 */
public class CreditCardTopupRequest implements Parcelable {
    String cardNumber;
    String expMonth;
    String expYear;
    String securityCode;
    String customerEmail;
    String firstName;
    String member;
    String memberCurrency;
    String noc;
    String price;

    public CreditCardTopupRequest() {
    }

    public CreditCardTopupRequest(String cardNumber, String expMonth, String expYear, String securityCode, String customerEmail, String firstName, String member, String memberCurrency, String noc, String price) {
        this.cardNumber = cardNumber;
        this.expMonth = expMonth;
        this.expYear = expYear;
        this.securityCode = securityCode;
        this.customerEmail = customerEmail;
        this.firstName = firstName;
        this.member = member;
        this.memberCurrency = memberCurrency;
        this.noc = noc;
        this.price = price;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.cardNumber);
        dest.writeString(this.expMonth);
        dest.writeString(this.expYear);
        dest.writeString(this.securityCode);
        dest.writeString(this.customerEmail);
        dest.writeString(this.firstName);
        dest.writeString(this.member);
        dest.writeString(this.memberCurrency);
        dest.writeString(this.noc);
        dest.writeString(this.price);
    }

    private CreditCardTopupRequest(Parcel in) {
        this.cardNumber = in.readString();
        this.expMonth = in.readString();
        this.expYear = in.readString();
        this.securityCode = in.readString();
        this.customerEmail = in.readString();
        this.firstName = in.readString();
        this.member = in.readString();
        this.memberCurrency = in.readString();
        this.noc = in.readString();
        this.price = in.readString();
    }

    public static final Creator<CreditCardTopupRequest> CREATOR = new Creator<CreditCardTopupRequest>() {
        public CreditCardTopupRequest createFromParcel(Parcel source) {
            return new CreditCardTopupRequest(source);
        }

        public CreditCardTopupRequest[] newArray(int size) {
            return new CreditCardTopupRequest[size];
        }
    };

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setExpMonth(String expMonth) {
        this.expMonth = expMonth;
    }

    public void setExpYear(String expYear) {
        this.expYear = expYear;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public void setMemberCurrency(String memberCurrency) {
        this.memberCurrency = memberCurrency;
    }

    public void setNoc(String noc) {
        this.noc = noc;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
