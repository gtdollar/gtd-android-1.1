package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 11/15/14.
 */
public class SetRedDotEvent {
    boolean set;

    public SetRedDotEvent(boolean set) {
        this.set = set;
    }

    public boolean isSet() {
        return set;
    }
}
