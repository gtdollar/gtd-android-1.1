package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.RefundEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.BalanceRecord;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;

public class RefundActivity extends BaseActivity {


    @InjectView(R.id.et_id)
    EditText mEtId;
    @InjectView(R.id.et_amount)
    EditText mEtAmount;
    @InjectView(R.id.et_fee)
    EditText mEtFee;
    @InjectView(R.id.et_refundable_amount)
    EditText mEtRefundableAmount;
    @InjectView(R.id.et_refund_desc)
    EditText mEtRefundDesc;
    @InjectView(R.id.btn_next)
    Button mBtnNext;

    BalanceRecord balanceRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refund);
        setTitle(R.string.wallet_payment_refund);
        ButterKnife.inject(this);
        balanceRecord = getIntent().getParcelableExtra(Const.INTENT_EXTRA_BALANCE_RECORD);

        mEtId.setText(balanceRecord.getId());
        mEtAmount.setText(balanceRecord.getOamountEx());
        mEtFee.setText(balanceRecord.getOfees()+"");
        mEtRefundableAmount.setText(balanceRecord.getOamountEx());
    }


    @OnClick({R.id.btn_next})
    public void onClickNext(View view) {
        JobManager.getInstance().accountRefund(balanceRecord.getId(),mEtRefundDesc.getText() != null?mEtRefundDesc.getText().toString():"");
    }

    public void onEventMainThread(RefundEvent event) {
        Utils.toggleEventPopup(this, event, new Utils.StatusListener<RefundEvent>() {
            @Override
            public void onSucceed(RefundEvent event) {

            }
        });
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

}
