package mobileapps.sumedia.gtdollarandroid.asynctask;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.text.TextUtils;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;

import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.GetAddressEvent;

/**
 * An AsyncTask that calls getFromLocation() in the background.
 * The class uses the following generic types:
 * Location - A {@link android.location.Location} object containing the current location,
 * passed as the input parameter to doInBackground()
 * Void     - indicates that progress units are not used by this subclass
 * String   - An address passed to onPostExecute()
 */
public class GetAddressTask extends AsyncTask<Location, Void, String> {

    // Store the context passed to the AsyncTask when the system instantiates it.
    WeakReference<Context> localContext;

    // Constructor called by the system to instantiate the task
    public GetAddressTask(WeakReference<Context> context) {

        // Required by the semantics of AsyncTask
        super();

        // Set a Context for the background task
        localContext = context;
    }

    /**
     * Get a geocoding service instance, pass latitude and longitude to it, format the returned
     * address, and return the address to the UI thread.
     */
    @Override
    protected String doInBackground(Location... params) {
            /*
             * Get a new geocoding service instance, set for localized addresses. This example uses
             * android.location.Geocoder, but other geocoders that conform to address standards
             * can also be used.
             */
        Context context = localContext.get();

        if(context == null)return null;

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());

        // Get the current location from the input parameter list
        Location location = params[0];

        // Create a list to contain the result address
        List<Address> addresses = null;

        // Try to get an address for the current location. Catch IO or network problems.
        try {

                /*
                 * Call the synchronous getFromLocation() method with the latitude and
                 * longitude of the current location. Return at most 1 address.
                 */
            addresses = geocoder.getFromLocation(location.getLatitude(),
                location.getLongitude(), 1
            );

            // Catch network or other I/O problems.
        } catch (IOException exception1) {

            // print the stack trace
            exception1.printStackTrace();

            // Return an error message
            return (context.getString(R.string.IO_Exception_getFromLocation));

            // Catch incorrect latitude or longitude values
        } catch (IllegalArgumentException exception2) {

            // Construct a message containing the invalid arguments
            String errorString = context.getString(
                R.string.illegal_argument_exception,
                location.getLatitude(),
                location.getLongitude()
            );
            // Log the error and print the stack trace
            exception2.printStackTrace();
            //
            return errorString;
        }
        // If the reverse geocode returned an address
        if (addresses != null && addresses.size() > 0) {

            // Get the first address
            Address address = addresses.get(0);
            StringBuffer sb = new StringBuffer();
            if (address.getMaxAddressLineIndex() > 0 && !TextUtils.isEmpty(address.getAddressLine(0))) {
                sb.append(address.getAddressLine(0));
            }

            if (!TextUtils.isEmpty(address.getLocality())) {
                sb.append('\n').append(address.getLocality());
            }

            if (!TextUtils.isEmpty(address.getCountryName())) {
                sb.append('\n').append(address.getCountryName());
            }

//                // Format the first line of address
//                String addressText = getString(R.string.address_output_string,
//
//                    // If there's a street address, add it
//                    address.getMaxAddressLineIndex() > 0 ?
//                        address.getAddressLine(0) : "",
//
//                    // Locality is usually a city
//                    address.getLocality(),
//
//                    // The country of the address
//                    address.getCountryName()
//                );

            // Return the text
            return sb.toString();

            // If there aren't any addresses, post a message
        } else {
            return context.getString(R.string.no_address_found);
        }
    }

    /**
     * A method that's called once doInBackground() completes. Set the text of the
     * UI element that displays the address. This method runs on the UI thread.
     */
    @Override
    protected void onPostExecute(String address) {

        // Turn off the progress bar
        // Set the address in the UI
        EventBus.getDefault().post(new GetAddressEvent(address));
    }
}