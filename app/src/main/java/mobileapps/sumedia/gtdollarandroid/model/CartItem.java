package mobileapps.sumedia.gtdollarandroid.model;

import android.graphics.Bitmap;

/**
 * Created by 2359media on 14/7/14.
 */
public class CartItem {
    Bitmap image;
    String title;

    public CartItem(Bitmap image, String title) {
        super();
        this.image = image;
        this.title = title;
    }
    public Bitmap getImage() {
        return image;
    }
    public void setImage(Bitmap image) {
        this.image = image;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }


}