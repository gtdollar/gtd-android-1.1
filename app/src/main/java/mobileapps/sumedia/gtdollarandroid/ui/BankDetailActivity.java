package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.AlertButtonClickEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.widget.GenericAlertPopup;

public class BankDetailActivity extends BaseActivity {

    @InjectView(R.id.et_bank_name)
    EditText mEtBankName;
    @InjectView(R.id.et_account_type)
    EditText mEtAccountType;
    @InjectView(R.id.et_institution)
    EditText mEtInstitution;
    @InjectView(R.id.et_branch_code)
    EditText mEtBranchCode;
    @InjectView(R.id.et_country)
    EditText mEtCountry;
    @InjectView(R.id.et_account_holder_name)
    EditText mEtAccountHolderName;
    @InjectView(R.id.et_account_number)
    EditText mEtAccountNumber;
    @InjectView(R.id.et_account_number_repeat)
    EditText mEtAccountNumberRepeat;
    @InjectView(R.id.et_additional_info)
    EditText mEtAdditionalInfo;
    @InjectView(R.id.btn_next)
    Button mBtnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_detail);
        ButterKnife.inject(this);
        setTitle(R.string.title_bank_info);
        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.displayAlert(BankDetailActivity.this, GenericAlertPopup.ALERT_WITHDRAW, getString(R.string.popup_withdraw));
            }
        });
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    public void onEventMainThread(AlertButtonClickEvent event) {
        if(event.getAlertType() == GenericAlertPopup.ALERT_WITHDRAW && event.isPositive()) {
            Intent intent = new Intent(this, EWalletActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        }
    }
}
