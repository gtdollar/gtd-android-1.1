package mobileapps.sumedia.gtdollarandroid.event;

import java.util.HashMap;

/**
 * Created by zhuodong on 8/15/14.
 */
public class GetVideoInfoEvent extends BaseEvent<HashMap<String,String>>{

    public GetVideoInfoEvent(EventStatus eventStatus) {
        super(eventStatus);
    }

    public GetVideoInfoEvent(EventStatus status, HashMap<String, String> response) {
        super(status, response);
    }
}
