package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 11/8/14.
 */
public class ReactivateAccountEvent extends BaseEvent {
    public ReactivateAccountEvent(EventStatus eventStatus) {
        super(eventStatus);
    }
}
