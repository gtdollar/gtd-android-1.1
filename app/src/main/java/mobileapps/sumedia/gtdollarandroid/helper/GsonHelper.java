package mobileapps.sumedia.gtdollarandroid.helper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * Created by zhuodong on 8/13/14.
 */
public class GsonHelper {

    static Gson gson = new Gson();

    public static Map<String,String> jsonToMap(String json) {
        Type stringStringMap = new TypeToken<Map<String, String>>(){}.getType();
        Map<String,String> map = gson.fromJson(json, stringStringMap);
        return map;
    }

    public static String mapToJson(Map<String, String> map) {
        return gson.toJson(map);
    }
}
