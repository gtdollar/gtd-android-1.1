package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Rect;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.Chat;
import mobileapps.sumedia.gtdollarandroid.ChatDao;
import mobileapps.sumedia.gtdollarandroid.GTDApplication;
import mobileapps.sumedia.gtdollarandroid.GetVideoInfoAsyncTask;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.ScaleBitmapAsyncTask;
import mobileapps.sumedia.gtdollarandroid.SimpleBackgroundTask;
import mobileapps.sumedia.gtdollarandroid.adapter.ChatAdapter;
import mobileapps.sumedia.gtdollarandroid.chat.ChatConstants;
import mobileapps.sumedia.gtdollarandroid.chat.IXMPPChatService;
import mobileapps.sumedia.gtdollarandroid.chat.XMPPChatServiceAdapter;
import mobileapps.sumedia.gtdollarandroid.chat.XMPPService;
import mobileapps.sumedia.gtdollarandroid.event.AlertButtonClickEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.GetVideoInfoEvent;
import mobileapps.sumedia.gtdollarandroid.event.NewChatEvent;
import mobileapps.sumedia.gtdollarandroid.event.ScaleBitmapEvent;
import mobileapps.sumedia.gtdollarandroid.event.SendLocationEvent;
import mobileapps.sumedia.gtdollarandroid.event.UpdateChatEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.FileUtils;
import mobileapps.sumedia.gtdollarandroid.helper.Formatter;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.widget.GenericAlertPopup;
import mobileapps.sumedia.gtdollarandroid.widget.PagingListView;
import mobileapps.sumedia.gtdollarandroid.widget.Pagingnable;

import static mobileapps.sumedia.gtdollarandroid.chat.ChatConstants.MessageType;
import static mobileapps.sumedia.gtdollarandroid.chat.ChatConstants.MessageType.image;
import static mobileapps.sumedia.gtdollarandroid.chat.ChatConstants.MessageType.video;


@SuppressWarnings("deprecation") /* recent ClipboardManager only available since API 11 */
public class ChatActivity extends BaseActivity {

    //public static final String INTENT_EXTRA_USERNAME = ChatActivity.class.getName() + ".username";
    //public static final String INTENT_EXTRA_MESSAGE = ChatActivity.class.getName() + ".message";

    private static final String TAG = "ChatActivity";

    private static final int DELAY_NEWMSG = 2000;

    //private ContentObserver mContactObserver = new ContactObserver();

    private String mWithJabberID;// = ChatConstants.USER_2;
    private String mUserScreenName = null;
    private String mUserAvatar = null;
    private Intent mServiceIntent;
    private ServiceConnection mServiceConnection;
    private XMPPChatServiceAdapter mServiceAdapter;
    ChatAdapter chatAdapter;


    @InjectView(R.id.et_message)
    EditText mEtMessage;
    @InjectView(R.id.btn_send_message)
    TextView mBtnSendMessage;
    @InjectView(R.id.lv_chat)
    PagingListView mLvChat;
    @InjectView(R.id.ll_chat_box_holder)
    LinearLayout mLlChatBoxHolder;
    @InjectView(R.id.iv_option)
    ImageView mIvOption;
    @InjectView(R.id.vp_chat_func)
    ViewPager mVpChatFunc;
    @InjectView(R.id.tv_cancel_record)
    TextView mTvCancelRecord;
    @InjectView(R.id.ll_recording_progress_holder)
    LinearLayout mLlRecordingProgressHolder;



    ChatDao chatDao;

    int pageNo = 1;
    public static final int PAGE_SIZE = 20;

    Map<String, String> tempProperties;
    Map<String, String> commProperties = new HashMap<String, String>();


    private boolean isOptionOpened;
    private String mSelfAvatar;
    private String mUserId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chat);
        ButterKnife.inject(this);

        getSupportActionBar().setDisplayShowTitleEnabled(true);

        chatDao = GTDApplication.getInstance().getDaoSession().getChatDao();

        //registerForContextMenu(getListView());
        setContactFromUri();
        registerXMPPService();

        initViews();

        commProperties.put(ChatConstants.MESSAGE_KEY_SENDER_NAME, TextUtils.isEmpty(UserDataHelper.getUser().getName()) ? UserDataHelper.getUser().getEmail() : UserDataHelper.getUser().getName());
        commProperties.put(ChatConstants.MESSAGE_KEY_DATE, Formatter.unixTimeStampToDate(System.currentTimeMillis(),Formatter.NOTIFICATION_DATE_FORMAT));
        commProperties.put(ChatConstants.MESSAGE_KEY_SENDER_JID, GTDApplication.getInstance().getConfig().jabberID);
        commProperties.put(ChatConstants.MESSAGE_KEY_SENDER_ID, UserDataHelper.getUser().getId());
        commProperties.put(ChatConstants.MESSAGE_KEY_RECEIVER_ID, mUserId);
        commProperties.put(ChatConstants.MESSAGE_KEY_RECEIVER_JID, mWithJabberID);
        //commProperties.put(ChatConstants.MESSAGE_KEY_RECEIVER_JID, );
        if(!TextUtils.isEmpty(mSelfAvatar)) {
            commProperties.put(ChatConstants.MESSAGE_KEY_SENDER_AVATAR_URL, mSelfAvatar);
        }
        if(!TextUtils.isEmpty(mUserScreenName)) {
            commProperties.put(ChatConstants.MESSAGE_KEY_RECEIVER_NAME, mUserScreenName);
        }
        if(!TextUtils.isEmpty(mUserAvatar)) {
            commProperties.put(ChatConstants.MESSAGE_KEY_RECEIVER_AVATAR_URL, mUserAvatar);
        }
        //setAdapter();
    }

    private void initViews() {

        mEtMessage.setImeOptions(EditorInfo.IME_ACTION_SEND);
        mEtMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEND) {
                    sendMessage(mBtnSendMessage);
                    return true;
                }
                return false;
            }
        });

        mVpChatFunc.setAdapter(new ChatFuncPagerAdapter(this.getSupportFragmentManager()));


        chatAdapter = new ChatAdapter(this);
        mLvChat.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mVpChatFunc.setVisibility(View.GONE);
                Utils.hideKeyBoard(ChatActivity.this, mEtMessage);
                return false;
            }
        });
        mLvChat.setAdapter(chatAdapter);
        chatAdapter.setUserAvatar(mUserAvatar);
        chatAdapter.setSelfAvatar(mSelfAvatar);
        mLvChat.setHasMoreItems(true);
        mLvChat.setPagingnableListener(new Pagingnable() {
            @Override
            public void onLoadMoreItems() {
                Log.d("", " load more");
                new SimpleBackgroundTask<List<Chat>>(ChatActivity.this) {

                    @Override
                    protected List<Chat> onRun() {
                        return chatDao.queryBuilder().where(ChatDao.Properties.Jid.eq(mWithJabberID)).orderDesc(ChatDao.Properties.Date).limit(PAGE_SIZE).offset((pageNo - 1) * PAGE_SIZE).list();
                    }

                    @Override
                    protected void onSuccess(List<Chat> result) {
                        Collections.reverse(result);
                        mLvChat.onFinishLoading(result != null && result.size() >= PAGE_SIZE);
                        //List<Message> items = chatAdapter.getItems();
                        //items.addAll(0, result);
                        int index = mLvChat.getFirstVisiblePosition() + result.size();
                        View v = mLvChat.getChildAt(0);
                        int top = (v == null) ? 0 : v.getTop();
                        chatAdapter.addMoreItems(result, false);
                        mLvChat.setSelectionFromTop(index, top);

                        pageNo++;
                    }
                }.execute();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus)
            bindXMPPService();
        else
            unbindXMPPService();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (hasWindowFocus()) unbindXMPPService();
    }

    private void registerXMPPService() {
        Log.i(TAG, "called startXMPPService()");
        mServiceIntent = new Intent(this, XMPPService.class);
        Uri chatURI = Uri.parse(mWithJabberID);
        mServiceIntent.setData(chatURI);
        mServiceIntent.setAction("org.yaxim.androidclient.XMPPSERVICE");

        mServiceConnection = new ServiceConnection() {

            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.i(TAG, "called onServiceConnected()");
                mServiceAdapter = new XMPPChatServiceAdapter(
                    IXMPPChatService.Stub.asInterface(service),
                    mWithJabberID);

                mServiceAdapter.clearNotifications(mWithJabberID);

                SendLocationEvent event = EventBus.getDefault().getStickyEvent(SendLocationEvent.class);
                if (event != null) {
                    EventBus.getDefault().removeStickyEvent(event);
                    tempProperties = new HashMap<String, String>(commProperties);
                    tempProperties.put(ChatConstants.MESSAGE_KEY_TYPE, MessageType.location.toString());
                    tempProperties.put(ChatConstants.MESSAGE_KEY_LOCATION_LAT, event.getLatitude() + "");
                    tempProperties.put(ChatConstants.MESSAGE_KEY_LOCATION_LNG, event.getLongitude() + "");
                    mServiceAdapter.sendMessage(mWithJabberID, event.getAddress(), tempProperties);
                }

                //updateContactStatus();
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.i(TAG, "called onServiceDisconnected()");
            }

        };
    }

    private void unbindXMPPService() {
        try {
            unbindService(mServiceConnection);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "Service wasn't bound!");
        }
    }

    private void bindXMPPService() {
        bindService(mServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    private void setContactFromUri() {
        Intent i = getIntent();
        mWithJabberID = i.getDataString().toLowerCase();
        if (i.hasExtra(Const.INTENT_EXTRA_USER_NAME)) {
            mUserScreenName = i.getExtras().getString(Const.INTENT_EXTRA_USER_NAME);
        } else {
            mUserScreenName = mWithJabberID;
        }
        mUserId = i.getExtras().getString(Const.INTENT_EXTRA_MERCHANT_ID);
        mUserAvatar = i.getExtras().getString(Const.INTENT_EXTRA_USER_AVATAR);
        //Log.d(""," testing avatar "+mUserAvatar);
        mSelfAvatar = TextUtils.isEmpty(UserDataHelper.getUser().getPicture()) ? UserDataHelper.getUser().getCover() : UserDataHelper.getUser().getPicture();
        setTitle(mUserScreenName);
    }


    @OnClick(R.id.btn_send_message)
    void sendMessage(View view) {
        if (mEtMessage.getText().length() == 0) {
            Toast.makeText(ChatActivity.this, "Can't send empty message", Toast.LENGTH_SHORT).show();
        } else {
            //mBtnSendMessage.setEnabled(false);
            tempProperties = new HashMap<String, String>(commProperties);
            tempProperties.put(ChatConstants.MESSAGE_KEY_TYPE, MessageType.text.toString());
            mServiceAdapter.sendMessage(mWithJabberID, mEtMessage.getText().toString(), tempProperties);
            mEtMessage.setText(null);
            if (!mServiceAdapter.isServiceAuthenticated())
                showToastNotification(R.string.toast_stored_offline);
        }
    }

    private void showToastNotification(int message) {
        Toast toastNotification = Toast.makeText(this, message,
            Toast.LENGTH_SHORT);
        toastNotification.show();
    }

    public static class ChatFuncPagerAdapter extends FragmentPagerAdapter {

        public ChatFuncPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return OptionFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    public static class OptionFragment extends Fragment {
        int position;

        public static OptionFragment newInstance(int position) {
            Bundle b = new Bundle();
            b.putInt("index", position);
            OptionFragment optionFragment = new OptionFragment();
            optionFragment.setArguments(b);
            return optionFragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            position = getArguments().getInt("index", 0);
            View view = inflater.inflate(position == 0 ? R.layout.item_chat_function_page_1 : R.layout.item_chat_function_page_2, container, false);
            ButterKnife.inject(this, view);

            final View btnAudio = view.findViewById(R.id.iv_chat_func_audio);
            if (btnAudio != null) {
                btnAudio.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                        Rect rect = new Rect();
                        btnAudio.getGlobalVisibleRect(rect);
                        ((ChatActivity) getActivity()).sendAudioBtnRect(rect);
                    }
                });
                btnAudio.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                            ((ChatActivity) getActivity()).startRecording();
                            return true;
                        }else if(motionEvent.getAction() == MotionEvent.ACTION_UP) {
                            ((ChatActivity) getActivity()).finishRecording();
                            return true;
                        }
                        return false;
                    }
                });
            }

            return view;
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            ButterKnife.reset(this);
        }

        @Optional
        @OnClick(R.id.iv_chat_func_video)
        public void onRecordVideo() {
            ((ChatActivity) getActivity()).onRecordVideo();
        }

        @Optional
        @OnClick(R.id.iv_chat_func_sales)
        public void onBuy() {
            ((ChatActivity) getActivity()).onBuy();
        }



        @Optional
        @OnClick(R.id.iv_chat_func_photo)
        public void onTakePhoto() {
            ((ChatActivity) getActivity()).onTakePhoto();
        }

        @Optional
        @OnClick(R.id.iv_chat_func_picture)
        public void onChoosePhoto() {
            ((ChatActivity) getActivity()).onChoosePhoto();
        }

        @Optional
        @OnClick(R.id.iv_chat_func_location)
        public void onSendLocation() {
            Intent intent = new Intent(getActivity(), MyLocationMapActivity.class);
            startActivity(intent);
        }

        @Optional
        @OnClick(R.id.iv_chat_func_money)
        public void onSendCredit() {
            TransferCreditPopup.newInstance().show(getActivity().getSupportFragmentManager(), "");
        }

    }

    private void onBuy() {
        Utils.viewMerchant(this, null, mUserId);
    }

    private void onRecordVideo() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            File videoFile = FileUtils.createFile(this, MessageType.video.name());
            tempProperties = new HashMap<String, String>(commProperties);
            tempProperties.put(ChatConstants.MESSAGE_KEY_TYPE, MessageType.video.toString());
            tempProperties.put(ChatConstants.MESSAGE_KEY_PATH, videoFile.getAbsolutePath());
            takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(videoFile));
            takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, Const.VIDEO_LENGTH_LIMIT);
            startActivityForResult(takeVideoIntent, Const.REQUEST_VIDEO_CAPTURE);
        }
    }

    private void onChoosePhoto() {
        Intent i = new Intent(Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        //i.setType("image/*");
        startActivityForResult(i, Const.REQUEST_IMAGE_PICK);
    }

    private void onTakePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = FileUtils.createFile(this, MessageType.image.name());
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(photoFile));
                tempProperties = new HashMap<String, String>(commProperties);
                tempProperties.put(ChatConstants.MESSAGE_KEY_TYPE, MessageType.image.toString());
                tempProperties.put(ChatConstants.MESSAGE_KEY_PATH, photoFile.getAbsolutePath());
                startActivityForResult(takePictureIntent, Const.REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    public void onEventMainThread(AlertButtonClickEvent event) {
        if (event.getAlertType() == GenericAlertPopup.ALERT_TRANSFER && event.isPositive()) {
            tempProperties = new HashMap<String, String>(commProperties);
            tempProperties.put(ChatConstants.MESSAGE_KEY_TYPE, MessageType.credit.toString());
            mServiceAdapter.sendMessage(mWithJabberID, event.getExtra(), tempProperties);
        }
    }


    public void onEventMainThread(NewChatEvent event) {
        chatAdapter.add(event.getChat());
    }

    public void onEventMainThread(UpdateChatEvent event) {
        chatAdapter.update(event.getChat());
    }

    public void onEventMainThread(ScaleBitmapEvent event) {
        Utils.toggleEventPopup(this, event, new Utils.StatusListener<ScaleBitmapEvent>() {
            @Override
            public void onSucceed(ScaleBitmapEvent event) {
                if (event.getResponse()) {
                    if (mServiceAdapter != null) {
                        mServiceAdapter.sendMessage(mWithJabberID, null, tempProperties);
                    }
                }
            }
        });
    }

    public void onEventMainThread(GetVideoInfoEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED) {
            tempProperties.putAll(event.getResponse());
            if (mServiceAdapter != null) {
                mServiceAdapter.sendMessage(mWithJabberID, null, tempProperties);
            }
        }
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Const.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            String currentPath, targetPath;
            currentPath = targetPath = tempProperties.get(ChatConstants.MESSAGE_KEY_PATH);
            new ScaleBitmapAsyncTask(currentPath, targetPath, Const.IMAGE_MAX_WIDTH, Const.IMAGE_MAX_HEIGHT).execute();

        } else if (requestCode == Const.REQUEST_IMAGE_PICK && resultCode == Activity.RESULT_OK) {
            onClickOptions(mIvOption);

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.MIME_TYPE, MediaStore.Images.Media.SIZE};

            Cursor cursor = this.getContentResolver().query(
                selectedImage, filePathColumn, null, null, null);


            if (cursor.moveToFirst()) {
                final String mimeType = cursor.getString(cursor.getColumnIndex(filePathColumn[1]));
                final String size = cursor.getString(cursor.getColumnIndex(filePathColumn[2]));
                final String path = cursor.getString(cursor.getColumnIndex(filePathColumn[0]));

                if (mimeType.startsWith("video/")) {
                    if (!ChatConstants.isSupportedVideo(mimeType)) {
                        Utils.toast(this, getString(R.string.chat_video_format_error));
                        return;
                    } else if (Integer.parseInt(size) > Const.VIDEO_SIZE_LIMIT) {
                        Utils.toast(this, getString(R.string.chat_video_size_error));
                        return;
                    }

                    File distFile = FileUtils.createFile(ChatActivity.this, video.name());
                    if (distFile != null && FileUtils.isFileExit(path)) {
                        tempProperties = new HashMap<String, String>(commProperties);
                        tempProperties.put(ChatConstants.MESSAGE_KEY_TYPE, MessageType.video.toString());
                        tempProperties.put(ChatConstants.MESSAGE_KEY_PATH, distFile.getAbsolutePath());
                        new GetVideoInfoAsyncTask(this, distFile.getAbsolutePath()).execute();
                    }
                } else {
                    File distFile = FileUtils.createFile(ChatActivity.this, image.name());
                    if (distFile != null && FileUtils.isFileExit(path)) {
                        tempProperties = new HashMap<String, String>(commProperties);
                        tempProperties.put(ChatConstants.MESSAGE_KEY_TYPE, MessageType.image.toString());
                        tempProperties.put(ChatConstants.MESSAGE_KEY_PATH, distFile.getAbsolutePath());
                        new ScaleBitmapAsyncTask(path, distFile.getAbsolutePath(), Const.IMAGE_MAX_WIDTH, Const.IMAGE_MAX_HEIGHT).execute();
                    }
                }
            }

//                    new SimpleBackgroundTask<String>(ChatActivity.this) {
//                    @Override
//                    protected String onRun() {
//                        File distFile = FileUtils.createFile(ChatActivity.this, image.name());
//                        if (distFile != null && FileUtils.isFileExit(tempPhotoPath)) {
//                            try {
//                                FileUtils.copy(new File(tempPhotoPath), distFile);
//                                return distFile.getAbsolutePath();
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                        return null;
//                    }
//
//                    @Override
//                    protected void onSuccess(String result) {
//                        if (TextUtils.isEmpty(result)) return;
//                        tempProperties.put(ChatConstants.MESSAGE_KEY_TYPE, MessageType.image.toString());
//                        tempProperties.put(ChatConstants.MESSAGE_KEY_PATH, result);
//                        mServiceAdapter.sendMessage(mWithJabberID, null, tempProperties);
//                    }
//                }.execute();
//            }
            cursor.close();
        } else if (requestCode == Const.REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            new GetVideoInfoAsyncTask(this, tempProperties.get(ChatConstants.MESSAGE_KEY_PATH)).execute();
        }
    }

    @OnClick(R.id.iv_option)
    void onClickOptions(View view) {
//        if (isOptionOpened) {
//            mEtMessage.requestFocus();
//            Utils.showKeyBoard(this, mEtMessage);
//        } else {
//        }

        Utils.hideKeyBoard(this, mEtMessage);

        mVpChatFunc.setVisibility(isOptionOpened ? View.GONE : View.VISIBLE);

        isOptionOpened = !isOptionOpened;

    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                this.finish();
//                return true;
//            case R.id.action_logout:
//                //TODO MOVE THIS TO PROPER LOGOU FUNC logout chat
//                GTDApplication.getInstance().getConfig().destroyConfig();
//                UserDataHelper.clear();
//                chatDao.deleteAll();
//
//                Intent xmppServiceIntent = new Intent(this, XMPPService.class);
//                stopService(xmppServiceIntent);
//
//                Intent intent = new Intent(this, LoginActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                this.startActivity(intent);
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    private MediaRecorder mRecorder = null;
    private MediaPlayer mPlayer = null;

    public void onPlay(String mFileName) {

        if (mPlayer != null) {
            stopPlaying();
        }
        startPlaying(mFileName);
    }

    private void startPlaying(String mFileName) {
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e(TAG, "prepare() failed");
        }
    }

    private void stopPlaying() {
        mPlayer.release();
        mPlayer = null;
    }

    private void startRecording() {
        if (recordingStarted) return;
        recordingStarted = true;
        mLlRecordingProgressHolder.setVisibility(View.VISIBLE);
        mTvCancelRecord.setVisibility(View.GONE);
        startedTime = System.currentTimeMillis();

        if(progressRect == null) {
            mLlRecordingProgressHolder.post(new Runnable() {
                @Override
                public void run() {
                    progressRect = new Rect();
                    mLlRecordingProgressHolder.getGlobalVisibleRect(progressRect);
                    Log.d(""," testing progress rect "+progressRect);
                }
            });
        }

        String keyPath = FileUtils.createFile(this, MessageType.audio.name()).getAbsolutePath();

        tempProperties = new HashMap<String, String>(commProperties);
        tempProperties.put(ChatConstants.MESSAGE_KEY_TYPE, MessageType.audio.toString());
        tempProperties.put(ChatConstants.MESSAGE_KEY_PATH, keyPath);

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(keyPath);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e(TAG, "prepare() failed");
        }

        mRecorder.start();
    }

    private void stopRecording() {
        Log.d(""," testing start recording");
        recordingStarted = false;
        mLlRecordingProgressHolder.setVisibility(View.GONE);

        if (mRecorder != null) {
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
        }

    }

    private void finishRecording() {
        stopRecording();
        MediaMetadataRetriever mediaMetaDataRetriever = new MediaMetadataRetriever();
        mediaMetaDataRetriever.setDataSource(tempProperties.get(ChatConstants.MESSAGE_KEY_PATH));
        tempProperties.put(ChatConstants.MESSAGE_KEY_AUDIO_LENGTH, Integer.parseInt(mediaMetaDataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)) + "");
        mServiceAdapter.sendMessage(mWithJabberID, null, tempProperties);
    }

    boolean recordingStarted;
    long startedTime;

//    @Optional
//    @OnClick(R.id.iv_chat_func_audio)
//    public void onStartRecoding() {
//        recordingStarted = !recordingStarted;
//        if(recordingStarted) {
//            ((ChatActivity) getActivity()).startRecording();
//        }else {
//            ((ChatActivity) getActivity()).finishRecording();
//        }
//    }

    Rect progressRect, recordRect;

    private void sendAudioBtnRect(Rect rect) {
        this.recordRect = rect;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();

//        if(action == MotionEvent.ACTION_DOWN) {
//            if(recordRect != null && recordRect.contains((int)event.getRawX(),(int)event.getRawY())) {
//                Log.d(""," testing start recording");
//                startRecording();
//                return true;
//            }
//        }

        if(!recordingStarted) return super.onTouchEvent(event);

        switch (action) {
            case MotionEvent.ACTION_MOVE:
                if(progressRect == null) return true;

                if(progressRect.contains((int)event.getRawX(),(int)event.getRawY())) {
                    mTvCancelRecord.setVisibility(View.VISIBLE);
                }else {
                    mTvCancelRecord.setVisibility(View.GONE);
                }
                return true;
            case MotionEvent.ACTION_UP:
                if(mTvCancelRecord.getVisibility() == View.GONE && (System.currentTimeMillis() - startedTime) > 1000) {
                    finishRecording();
                }else {
                    stopRecording();
                }
                return true;
        }

        return super.onTouchEvent(event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_view) {
            Utils.viewMerchant(this, null, mUserId);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
