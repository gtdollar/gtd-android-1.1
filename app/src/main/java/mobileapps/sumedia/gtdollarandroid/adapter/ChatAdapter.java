package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.makeramen.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import mobileapps.sumedia.gtdollarandroid.Chat;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.chat.ChatConstants;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.ui.ChatActivity;
import mobileapps.sumedia.gtdollarandroid.ui.EWalletActivity;
import mobileapps.sumedia.gtdollarandroid.ui.MapActivity;

import static mobileapps.sumedia.gtdollarandroid.chat.ChatConstants.MESSAGE_KEY_LOCATION_LAT;
import static mobileapps.sumedia.gtdollarandroid.chat.ChatConstants.MESSAGE_KEY_LOCATION_LNG;

/**
 * Created by 2359media on 17/7/14.
 */
public class ChatAdapter extends PagingBaseAdapter<Chat> {
    Transformation transformation;

    int maxAudioWidth;
    public static final int MAX_AUDIO_LENGTH = 10;
    private String userAvatar;
    private String selfAvatar;

    public ChatAdapter(Context ctx){
        super(ctx);
        this.maxAudioWidth = context.getResources().getDimensionPixelSize(R.dimen.chat_audio_max_size);
        this.transformation = new RoundedTransformationBuilder()
//            .borderColor(context.getResources().getColor(R.color.soft_green))
//            .borderWidthDp(5)
            .cornerRadius(ctx.getResources().getDimensionPixelSize(R.dimen.img_corner_radius))
            .oval(false)
            .build();
    }


    @Override
    public long getItemId(int i) {
        return getItem(i).getId();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public int getItemViewType(int position) {
        Chat chat = getItem(position);
        if(chat == null)return 0;
        //Log.d(""," viewType "+viewType);
        return ChatConstants.messageViewType(chat.getDirection(),chat.getMessageType());
    }

    @Override
    public int getViewTypeCount() {
        return ChatConstants.VIEW_TYPE_COUNT;
    }


    @Override
    protected int getViewResourceId(int position) {
        return ChatConstants.viewLayout(getItemViewType(position));
    }

    @Override
    protected Renderer<Chat> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public void setSelfAvatar(String selfAvatar) {
        this.selfAvatar = selfAvatar;
    }

    class ViewHolder implements Renderer<Chat>{
        @InjectView(R.id.iv_thumb)
        ImageView mIvThumbe;
        @Optional
        @InjectView(R.id.tv_chat_content)
        TextView mTvChatContent;
        @Optional
        @InjectView(R.id.iv_chat_content)
        ImageView mIvChatContent;
        @InjectView(R.id.pb_sending)
        ProgressBar mPbSending;

        @Optional @InjectView(R.id.fl_chat_audio)
        FrameLayout mFlChatAudio;

        View view;

        Chat chat;

        ViewHolder(View view) {
            this.view = view;
            ButterKnife.inject(this, view);
//            mTvChatContent.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View v) {
//                    openOptionsMenu(item);
//                    return true;
//                }
//            });
        }

        @Optional @OnClick(R.id.iv_chat_content)
        public void onClickImage(View view) {
            String messageType = chat.getMessageType();

            if(messageType.equals(ChatConstants.MessageType.image.name())) {
                String path = chat.getPropertyMap().get(ChatConstants.MESSAGE_KEY_PATH);
                Uri data = Uri.fromFile(new File(path));
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(data, "image/*");
                context.startActivity(intent);
            }else if(messageType.equals(ChatConstants.MessageType.video.name())) {
                String path = chat.getPropertyMap().get(ChatConstants.MESSAGE_KEY_PATH);
                Uri data = Uri.fromFile(new File(path));
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(data, "video/*");
                context.startActivity(intent);
            }else if(messageType.equals(ChatConstants.MessageType.location.name())) {
                Intent intent = new Intent(context, MapActivity.class);
                intent.putExtra(Const.INTENT_EXTRA_LATLNG, new LatLng(Double.parseDouble(chat.getPropertyMap().get(MESSAGE_KEY_LOCATION_LAT)), Double.parseDouble(chat.getPropertyMap().get(MESSAGE_KEY_LOCATION_LNG))));
                context.startActivity(intent);
            }else if(messageType.equals(ChatConstants.MessageType.credit.name())) {
                Intent intent = new Intent(context, EWalletActivity.class);
                context.startActivity(intent);
            }
        }

        @Optional @OnClick(R.id.fl_chat_audio)
        public void onClickAudio() {
            String messageType = chat.getMessageType();
            if (messageType.equals(ChatConstants.MessageType.audio.name()) && !TextUtils.isEmpty(chat.getPropertyMap().get(ChatConstants.MESSAGE_KEY_PATH))) {
                ((ChatActivity) context).onPlay(chat.getPropertyMap().get(ChatConstants.MESSAGE_KEY_PATH));
            }
        }

        public void render(Chat item, int position) {
            this.chat = item;
            String messageType = item.getMessageType();

            if(mTvChatContent != null) {
                if(messageType != null && messageType.equals(ChatConstants.MessageType.audio.name()) && item.getPropertyMap() != null){
                    int audioLength = 0;
                    try {
                        audioLength = Integer.parseInt(item.getPropertyMap().get(ChatConstants.MESSAGE_KEY_AUDIO_LENGTH))/1000;
                    }catch(Exception e){}
                    if(audioLength == 0)return;
                    mTvChatContent.setText(audioLength+"\"");
                    int width;
                    if(audioLength >= maxAudioWidth)width = maxAudioWidth;
                    else width = maxAudioWidth/MAX_AUDIO_LENGTH * audioLength;
                    mFlChatAudio.getLayoutParams().width = width;
                }
                else {
                    mTvChatContent.setText(item.getMessage());
                }
            }
            //Log.d(""," path property "+item.getProperties());
            //Log.d(""," path map "+item.getPropertyMap().get(ChatConstants.MESSAGE_KEY_PATH));

//            if(mIvChatContent != null)mIvChatContent.setImageURI(Uri.fromFile(new File(item.getPropertyMap().get(ChatConstants.MESSAGE_KEY_PATH))));
            if(messageType != null && item.getPropertyMap() != null) {
                if(messageType.equals(ChatConstants.MessageType.image.name())) {
                    Picasso.with(context).load(new File(item.getPropertyMap().get(ChatConstants.MESSAGE_KEY_PATH))).transform(transformation).into(mIvChatContent);
                }else if(messageType.equals(ChatConstants.MessageType.video.name())) {
                    Picasso.with(context).load(new File(item.getPropertyMap().get(ChatConstants.MESSAGE_KEY_VIDEO_THUMBNAIL))).transform(transformation).into(mIvChatContent);
                }else if(messageType.equals(ChatConstants.MessageType.location.name())) {
                    Picasso.with(context).load(R.drawable.location_placeholder).transform(transformation).into(mIvChatContent);
                }else if(messageType.equals(ChatConstants.MessageType.credit.name())) {
                    Picasso.with(context).load(R.drawable.ic_chat_money).transform(transformation).into(mIvChatContent);
                }
            }

            if(item.getDirection() == ChatConstants.OUTGOING && item.getDeliveryStatus() == ChatConstants.DS_NEW) {
                mPbSending.setVisibility(View.VISIBLE);
            }else {
                mPbSending.setVisibility(View.GONE);
            }

            if(item.getDirection() == ChatConstants.OUTGOING && !TextUtils.isEmpty(selfAvatar)) {
                Picasso.with(context).load(selfAvatar).placeholder(R.mipmap.ic_launcher).centerCrop().fit().into(mIvThumbe);
            }else if(item.getDirection() == ChatConstants.INCOMING && !TextUtils.isEmpty(userAvatar)) {
                Picasso.with(context).load(userAvatar).placeholder(R.mipmap.ic_launcher).centerCrop().fit().into(mIvThumbe);
            }else {
                mIvThumbe.setImageResource(R.mipmap.ic_launcher);
            }

        }
    }
}
