package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.helper.Formatter;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.model.Account;
import mobileapps.sumedia.gtdollarandroid.network.UnionPayRequest;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

public class SetTopupAmountActivity extends BaseActivity {

    @InjectView(R.id.btn_next)
    Button mBtnNext;
    @InjectView(R.id.et_topup_amount)
    EditText mEtTopupAmount;
    private Form mForm;
    double balance;

    public static final String BILL_NUMBER_FORMAT = "yyyyMMddHHmmss";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_topup_amount);
        ButterKnife.inject(this);
        setTitle(R.string.title_activity_topup);

        mEtTopupAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    onClickNext(mBtnNext);
                    return true;
                }
                return false;
            }
        });
        buildForm();

    }

    private void buildForm() {
        mForm = new Form(this);
        mForm.addField(Field.using(mEtTopupAmount).validate(NotEmpty.build(this)));
    }

    @OnClick(R.id.btn_next)
    public void onClickNext(View view) {
        if (!mForm.isValid()) return;

        Account account = UserDataHelper.getAccount();
        if(account == null)return;
        UnionPayRequest request = new UnionPayRequest(mEtTopupAmount.getText().toString(),account.getEmail()+"-"+ Formatter.unixTimeStampToDate(System.currentTimeMillis(), BILL_NUMBER_FORMAT),account.getEmail(),account.getId());

        Intent intent = new Intent(this, TopupWebViewActivity.class);
        intent.putExtra("unionpay_request",request);
        startActivity(intent);
    }


}
