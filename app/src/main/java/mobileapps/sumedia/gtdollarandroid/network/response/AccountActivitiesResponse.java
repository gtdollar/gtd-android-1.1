package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

import mobileapps.sumedia.gtdollarandroid.model.TransactionActivity;

/**
 * Created by zhuodong on 8/21/14.
 */
public class AccountActivitiesResponse extends AccountBaseResponse{

    @Expose
    private List<TransactionActivity> data = new ArrayList<TransactionActivity>();


    public List<TransactionActivity> getData() {
        return data;
    }

    public void setData(List<TransactionActivity> data) {
        this.data = data;
    }

}

