package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 8/15/14.
 */
public class ScaleBitmapEvent extends BaseEvent<Boolean>{
    public ScaleBitmapEvent(EventStatus status) {
        super(status);
    }

    public ScaleBitmapEvent(EventStatus status, Boolean response) {
        super(status, response);
    }
}
