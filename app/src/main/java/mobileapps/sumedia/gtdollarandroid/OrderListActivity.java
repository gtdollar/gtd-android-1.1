package mobileapps.sumedia.gtdollarandroid;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.adapter.OrderListAdapter;
import mobileapps.sumedia.gtdollarandroid.event.CreateOrderEvent;
import mobileapps.sumedia.gtdollarandroid.event.DateSetEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.GetFeedsEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetMerchantEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetRebateRateEvent;
import mobileapps.sumedia.gtdollarandroid.event.OrderPaymentEvent;
import mobileapps.sumedia.gtdollarandroid.event.UpdateOrderEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.Formatter;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.Order;
import mobileapps.sumedia.gtdollarandroid.model.Service;
import mobileapps.sumedia.gtdollarandroid.model.User;
import mobileapps.sumedia.gtdollarandroid.network.JSONParser;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.ui.ScanServiceActivity;
import mobileapps.sumedia.gtdollarandroid.ui.SimpleScannerActivity;
import mobileapps.sumedia.gtdollarandroid.widget.DatePickerFragment;

public class OrderListActivity extends BaseActivity implements OrderListAdapter.TotalPriceChangedListener {


    HeaderViewHolder headerViewHolder;
    //FooterViewHolder footerViewHolder;
    @InjectView(R.id.lv_order)
    ListView mLvOrder;

    User user, merchant;
    String merchantId;

    OrderListAdapter orderListAdapter;

    List<Service> pendingOrders;
    private double totalPrice, rate;


    TextView mTvTotalPrice;
    TextView mTvDiscount;
    TextView mTvPayableAmount;
    TextView mTvBook;
    TextView mTvFrontDeskPay;
    TextView mTvWalletPay;
    View mLlPayHolder;

    private TextView mTvScan;
    private View mLlBookHolder;

    private boolean isReviewOrder;

    public static final String BILL_NUMBER_FORMAT = "yyyyMMddHHmmss";
    public static final String ORDER_CREATED_DATE_FORMAT = "yyyy-MM-dd HH:mm";

    String symbol;
    long MILLI_SECS_IN_DAY = 86400000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        ButterKnife.inject(this);
        merchantId = getIntent().getStringExtra(Const.INTENT_EXTRA_MERCHANT_ID);

        Order reviewedOrder = getIntent().getParcelableExtra(Const.INTENT_EXTRA_ORDER);

        if (reviewedOrder != null) {
            pendingOrders = reviewedOrder.getServices();
            isReviewOrder = true;
        } else {
            pendingOrders = UserDataHelper.getPendingOrders();
            isReviewOrder = false;
        }

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //setTitle(merchant.getName());

        user = UserDataHelper.getUser();

        final View headerView = LayoutInflater.from(this).inflate(R.layout.include_checkout_header, null);
        headerViewHolder = new HeaderViewHolder(headerView);
        headerViewHolder.render(user, reviewedOrder);

        View footerView = LayoutInflater.from(this).inflate(R.layout.include_checkout_footer, null);
        mTvTotalPrice = (TextView) footerView.findViewById(R.id.tv_total_price);
        mTvDiscount = (TextView) footerView.findViewById(R.id.tv_discount);
        mTvPayableAmount = (TextView) footerView.findViewById(R.id.tv_payable_amount);
        mTvBook = (TextView) footerView.findViewById(R.id.tv_book);
        mTvScan = (TextView) footerView.findViewById(R.id.tv_scan);
        mTvWalletPay = (TextView) footerView.findViewById(R.id.tv_wallet_pay);
        mTvFrontDeskPay = (TextView) footerView.findViewById(R.id.tv_front_desk_pay);
        mLlPayHolder = footerView.findViewById(R.id.ll_pay_holder);
        mLlBookHolder = footerView.findViewById(R.id.ll_book_holder);


        mTvScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderListActivity.this, ScanServiceActivity.class);
                intent.putExtra("merchant_id", merchantId);
                startActivity(intent);
            }
        });

        mTvBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> params = new HashMap<String, String>();
                params.put("merchant", merchant.getId());
                params.put("order_date", headerViewHolder.getOrderDate());
                params.put("effective_date", headerViewHolder.getExpires());

                List<String> ids = new ArrayList<String>();
                for (Service item : orderListAdapter.getItems()) {
                    ids.add(item.getId());
                }

                List<Integer> counts = new ArrayList<Integer>();
                for (Service item : orderListAdapter.getItems()) {
                    counts.add(item.getQuantity());
                }

                params.put("services", TextUtils.join(",", ids));
                params.put("head_count", headerViewHolder.getHeadCount());
                params.put("total_price", Formatter.formatPrice("", totalPrice));
                params.put("discount_price", Formatter.formatPrice("", totalPrice * rate));
                params.put("counts", TextUtils.join(",", counts));
                params.put("delivery_address", headerViewHolder.getDeliveryAddress());
                params.put("contact_number", headerViewHolder.getContactNumber());

                JobManager.getInstance().createOrder(params);
            }
        });

        mTvWalletPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.checkAccount(OrderListActivity.this)) {
                    Utils.payOrder(OrderListActivity.this, order, totalPrice * (1 - rate));
                }
            }
        });

        mTvFrontDeskPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.checkAccount(OrderListActivity.this)) {
                    Intent intent = new Intent(OrderListActivity.this, SimpleScannerActivity.class);
                    startActivity(intent);
                }
            }
        });

        //footerViewHolder = new FooterViewHolder(footerView);

        mLvOrder.addHeaderView(headerView);
        mLvOrder.addFooterView(footerView);

        if (isReviewOrder) {
            mLlBookHolder.setVisibility(View.GONE);
        }

        JobManager.getInstance().getMerchant(merchantId);
    }

    @Override
    public void calculateTotalPrice() {

        totalPrice = 0;
        for (Service service : orderListAdapter.getItems()) {
            totalPrice += factor * service.getQuantity() * service.getPrice();
        }
        if (merchant != null) {
            mTvTotalPrice.setText(Formatter.formatPrice(symbol, totalPrice));
            mTvDiscount.setText(Formatter.formatPrice(symbol, totalPrice * rate));
            mTvPayableAmount.setText(Formatter.formatPrice(symbol, totalPrice * (1 - rate)));
        }

    }

    int factor = 1;

    class HeaderViewHolder {

        @InjectView(R.id.tv_customer_name)
        TextView mTvCustomerName;
        @InjectView(R.id.tv_merchant_name)
        TextView mTvMerchantName;
        @InjectView(R.id.tv_head_count)
        TextView mTvHeadCount;
        @InjectView(R.id.sb_head_count)
        SeekBar mSbHeadCount;
        @InjectView(R.id.tv_date_created)
        TextView mTvDateCreated;
        @InjectView(R.id.tv_expiry_date)
        TextView mTvExpiryDate;
        @InjectView(R.id.tv_bill_number)
        TextView mTvBillNumber;
        @InjectView(R.id.et_phone)
        EditText mEtPhone;
        @InjectView(R.id.et_address)
        EditText mEtAddress;
//        @InjectView(R.id.et_postcode)
//        EditText mEtPostcode;

//        @InjectView(R.id.tv_customer_name)
//        TextView mTvCustomerName;
//        @InjectView(R.id.tv_merchant_name)
//        TextView mTvMerchantName;
//        @InjectView(R.id.tv_head_count)
//        TextView mTvHeadCount;
//        @InjectView(R.id.sb_head_count)
//        SeekBar mSbHeadCount;
//        @InjectView(R.id.tv_date_created)
//        TextView mTvDateCreated;
//        @InjectView(R.id.tv_expiry_date)
//        TextView mTvExpiryDate;
//        @InjectView(R.id.tv_bill_number)
//        TextView mTvBillNumber;

        Calendar calendarOrder, calendarExpire, calendarTemp;
        private boolean isEditStartDate, isEditEndDate;

        HeaderViewHolder(View view) {
            ButterKnife.inject(this, view);
            calendarOrder = Calendar.getInstance();
            calendarExpire = Calendar.getInstance();
            calendarExpire.add(Calendar.DAY_OF_MONTH, 1);
            calendarTemp = Calendar.getInstance();
        }

        @OnClick({R.id.tv_customer_name, R.id.tv_merchant_name})
        public void onClickName(View view) {
            if (view.getId() == R.id.tv_merchant_name) {
                Utils.viewMerchant(OrderListActivity.this, merchant, merchantId);
            } else {
                Utils.viewMerchant(OrderListActivity.this, user, user != null ? user.getId() : null);
            }
        }

        public void render(User user, Order order) {
            mTvCustomerName.setPaintFlags(mTvCustomerName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            mTvCustomerName.setText(user.getName());
            mTvCustomerName.setTag(user.getId());

            mTvMerchantName.setPaintFlags(mTvMerchantName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            mTvMerchantName.setTag(merchantId);

            if (order != null) {
                mTvHeadCount.setText(order.getHeadCount() + "");
                mTvDateCreated.setText(order.getOrderDate());
                mTvExpiryDate.setText(order.getOrderEffectiveDate());

                mTvDateCreated.setEnabled(false);
                mTvExpiryDate.setEnabled(false);
                mSbHeadCount.setVisibility(View.GONE);

                mTvBillNumber.setText(Formatter.formatDate(order.getOrderDate(), ORDER_CREATED_DATE_FORMAT, BILL_NUMBER_FORMAT));

                mEtPhone.setText(order.getContactNumber());
                mEtAddress.setText(order.getDeliveryAddress());

                mEtPhone.setEnabled(false);
                mEtAddress.setEnabled(false);

            } else {


                mEtPhone.setText(user.getPhone());
                mEtAddress.setText(user.getAddress());

                mTvHeadCount.setText(1 + "");
                mSbHeadCount.setMax(8);
                mSbHeadCount.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                        mTvHeadCount.setText((i + 1) + "");
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
                mTvDateCreated.setText(Formatter.unixTimeStampToDate(calendarOrder.getTimeInMillis(), ORDER_CREATED_DATE_FORMAT));
                mTvExpiryDate.setText(Formatter.unixTimeStampToDate(calendarExpire.getTimeInMillis(), ORDER_CREATED_DATE_FORMAT));
                mTvBillNumber.setText(Formatter.unixTimeStampToDate(calendarOrder.getTimeInMillis(), BILL_NUMBER_FORMAT));
            }
        }

        public void setMerchantName(String merchantName) {
            mTvMerchantName.setText(merchantName);
        }

        @OnClick({R.id.tv_date_created, R.id.tv_expiry_date})
        void onSelectTime(View view) {

            isEditStartDate = false;
            isEditEndDate = false;

            if (view.getId() == R.id.tv_date_created) {
                isEditStartDate = true;
            } else if (view.getId() == R.id.tv_expiry_date) {
                isEditEndDate = true;
            }

            DatePickerFragment.newInstance().show(getSupportFragmentManager(), "timePicker");
        }

        public void onEventMainThread(DateSetEvent event) {
            if (event == null) {
                return;
            }

            if (isEditStartDate) {
                calendarOrder.set(event.getYear(), event.getMonth(), event.getDay());
                mTvDateCreated.setText(Formatter.unixTimeStampToDate(calendarOrder.getTimeInMillis(), ORDER_CREATED_DATE_FORMAT));

                if (calendarOrder.getTime().compareTo(calendarExpire.getTime()) >= 0) {
                    calendarExpire.set(event.getYear(), event.getMonth(), event.getDay());
                    calendarExpire.add(Calendar.DAY_OF_MONTH, 1);
                    mTvExpiryDate.setText(Formatter.unixTimeStampToDate(calendarExpire.getTimeInMillis(), ORDER_CREATED_DATE_FORMAT));
                }
            } else if (isEditEndDate) {
                calendarExpire.set(event.getYear(), event.getMonth(), event.getDay());
                mTvExpiryDate.setError(null);
                mTvExpiryDate.setText(Formatter.unixTimeStampToDate(calendarExpire.getTimeInMillis(), ORDER_CREATED_DATE_FORMAT));

                if (calendarExpire.getTime().compareTo(calendarOrder.getTime()) <= 0) {
                    calendarOrder.set(event.getYear(), event.getMonth(), event.getDay());
                    calendarOrder.add(Calendar.DAY_OF_MONTH, -1);
                    mTvDateCreated.setText(Formatter.unixTimeStampToDate(calendarOrder.getTimeInMillis(), ORDER_CREATED_DATE_FORMAT));
                }
            }

            if (merchant != null && merchant.getServices().size() > 0 && merchant.getServices().get(0).getType() == 3) {
                factor = (int) ((calendarExpire.getTimeInMillis() - calendarOrder.getTimeInMillis()) / MILLI_SECS_IN_DAY);
                calculateTotalPrice();
            }
        }


        public String getOrderDate() {
            return Formatter.unixTimeStampToDate(calendarOrder.getTimeInMillis(), Formatter.SERVER_DATE_FORMAT);
            //return calendarOrder.getTimeInMillis()/1000 + "";
        }

        public String getExpires() {
            return Formatter.unixTimeStampToDate(calendarExpire.getTimeInMillis(), Formatter.SERVER_DATE_FORMAT);

            //return calendarExpire.getTimeInMillis()/1000 + "";
        }

        public String getHeadCount() {
            return mTvHeadCount.getText().toString();
        }

        public String getDeliveryAddress() {
            return mEtAddress.getText()!= null?mEtAddress.getText().toString():null;
        }

        public String getContactNumber() {
            return mEtPhone.getText()!= null?mEtPhone.getText().toString():null;
        }
    }

    public void onEventMainThread(DateSetEvent event) {
        if (headerViewHolder != null) headerViewHolder.onEventMainThread(event);
    }

    public void onEventMainThread(GetRebateRateEvent event) {
//        Utils.toggleEventPopup(this, event, new Utils.StatusListener<GetRebateRateEvent>() {
//            @Override
//            public void onSucceed(GetRebateRateEvent event) {
//
//            }
//        });
        if (event.getStatus() == EventStatus.SUCCEED) {
            rate = event.getRebaterate();
            calculateTotalPrice();
        }

    }

    public void onEventMainThread(CreateOrderEvent event) {
        Utils.toggleEventPopup(this, event, new Utils.StatusListener<CreateOrderEvent>() {
            @Override
            public void onSucceed(CreateOrderEvent event) {
                order = event.getResponse().getOrder();
                mLlPayHolder.setVisibility(View.VISIBLE);
                mLlBookHolder.setVisibility(View.GONE);
                orderListAdapter.setDisabled(true);
                Utils.toast(OrderListActivity.this.getApplicationContext(), getString(R.string.toast_order_completed));
                //Toast.makeText(, R.string.toast_order_completed, Toast.LENGTH_LONG).show();

                // INSERT THE NEW ORDER

                new SimpleBackgroundTask<Void>(OrderListActivity.this) {
                    @Override
                    protected Void onRun() {
                        long createdAt = Formatter.getTimeStamp(order.getOrderDate(), ORDER_CREATED_DATE_FORMAT);
                        Feed temp = new Feed();
                        temp.setBaseType(Feed.TYPE_NOTIFICATIONS);
                        temp.setType(Feed.SUBTYPE_NOTIFY_SUBMIT_ORDER);
                        temp.setCreatedAt(createdAt);
                        temp.setExtraId(createdAt+"");
                        temp.setIsRead(false);
                        if (order != null) {
                            temp.setExtra(JSONParser.getFormatJson(order));
                        }

                        if (merchant != null) {
                            temp.setSubjectName(merchant.getName());
                            temp.setSubjectId(merchant.getId());
                            temp.setSubjectAvatar(merchant.getPicture());
                        }

                        GTDApplication.getInstance().getDaoSession().getFeedDao().insert(temp);

                        EventBus.getDefault().post(new GetFeedsEvent(EventStatus.SUCCEED));
                        return null;
                    }
                }.execute();


            }
        });
    }

    public void onEventMainThread(OrderPaymentEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED) {
            this.finish();
        }
    }

    public void onEventMainThread(GetMerchantEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED) {
            merchant = event.getResponse().getUser();
            if (merchant == null) {
                return;
            }

            if (headerViewHolder != null) headerViewHolder.setMerchantName(merchant.getName());

            symbol = merchant.getCurrency() != null ? merchant.getCurrency().getSymbol() : "$";

            orderListAdapter = new OrderListAdapter(this, symbol, this);

            orderListAdapter.setDisabled(isReviewOrder);
            orderListAdapter.setItems(pendingOrders);
            mLvOrder.setAdapter(orderListAdapter);

            calculateTotalPrice();
            JobManager.getInstance().accountGetRebateRate(merchant.getEmail());

        }
    }

    public void onEventMainThread(UpdateOrderEvent event) {
        pendingOrders.add(event.getService());
        orderListAdapter.notifyDataSetChanged();
    }

    Order order;

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

}