package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.BalanceRecordAdapter;
import mobileapps.sumedia.gtdollarandroid.event.BalanceRecordEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;

public class BalanceRecordListActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    @InjectView(R.id.lv_record)
    ListView mLvRecord;
    BalanceRecordAdapter mRecordAdapter;

    public static final int STATUS_ALL = 0;
    public static final int STATUS_COMPLETED = 1;
    public static final int STATUS_CHARGED_BACK = 20;
    public static final int STATUS_REFUNDING = 12;
    //public static final int STATUS_CHARGED_BACK = 20;

    int status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_list);
        ButterKnife.inject(this);
        status = getIntent().getIntExtra(Const.INTENT_EXTRA_RECORD_STATUS, STATUS_ALL);

        switch (status) {
            case STATUS_ALL:
                setTitle(R.string.balance_transaction_all);
                break;
            case STATUS_COMPLETED:
                setTitle(R.string.wallet_transaction_completed);
                break;
            case STATUS_CHARGED_BACK:
                setTitle(R.string.wallet_charged_back);
                break;
            case STATUS_REFUNDING:
                setTitle(R.string.wallet_transaction_refunding);
                break;
        }

        mRecordAdapter = new BalanceRecordAdapter(this);
        mLvRecord.setAdapter(mRecordAdapter);
        mLvRecord.setOnItemClickListener(this);
        JobManager.getInstance().accountBalanceRecord(status);
    }

    public void onEventMainThread(BalanceRecordEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED && event.getResponse() != null && event.getType() == status) {
            mRecordAdapter.setItems(event.getResponse().getData());
        }
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this, RecordDetailActivity.class);
        intent.putExtra(Const.INTENT_EXTRA_BALANCE_RECORD,mRecordAdapter.getItem(i));
        intent.putExtra(Const.INTENT_EXTRA_DISPLAY_REFUND,status == STATUS_COMPLETED);
        startActivity(intent);
    }
}
