package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.ActionBar;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.ServiceListAdapter;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.OrderPaymentEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.Service;
import mobileapps.sumedia.gtdollarandroid.model.User;


public class ServiceListActivity extends BaseActivity implements ServiceListAdapter.OrderChangedListener {

    @InjectView(R.id.gv_service)
    GridView mGvService;

    List<Service> serviceList;

    User merchant;

    int orderCount;

    View shopHolder;
    View ivCart;

    TextView tvItemCount;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_list);
        ButterKnife.inject(this);

        final ServiceListAdapter adapter = new ServiceListAdapter(this);


        getActionBar().setDisplayShowCustomEnabled(true);

        shopHolder = getLayoutInflater().inflate(R.layout.action_bar_service,null);
        tvItemCount = (TextView) shopHolder.findViewById(R.id.tv_item_count);
        ivCart =  shopHolder.findViewById(R.id.iv_shop_cart);
        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.RIGHT);
        getActionBar().setCustomView(shopHolder, lp);

        shopHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(orderCount > 0) {
                    UserDataHelper.setPendingOrders(adapter.getPendingOrder());
                    Utils.viewOrders(ServiceListActivity.this, merchant.getId(),null);
                }
            }
        });

        merchant = getIntent().getParcelableExtra(Const.INTENT_EXTRA_MERCHANT_KEY);
        setTitle(merchant.getName());

        serviceList = merchant.getServices();


        adapter.setListener(this);

        mGvService.setAdapter(adapter);

        adapter.setItems(serviceList);

        mGvService.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                if(serviceList != null && serviceList.size() > position) {
                    Utils.viewService(ServiceListActivity.this, (java.util.ArrayList<Service>) serviceList,position);
                }
            }
        });

    }

    @Override
    public void onOrderChanged(int count) {
        orderCount = count;
        tvItemCount.setVisibility(orderCount > 0?View.VISIBLE:View.INVISIBLE);
        tvItemCount.setText(orderCount + "");
        ivCart.setAlpha(orderCount>0?1.0f:0.5f);
    }

    public void onEventMainThread(OrderPaymentEvent event) {
        if(event.getStatus() == EventStatus.SUCCEED) {
            this.finish();
        }
    }

}
