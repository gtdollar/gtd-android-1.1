package mobileapps.sumedia.gtdollarandroid.chat;

public class ChatXMPPAdressMalformedException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ChatXMPPAdressMalformedException(Throwable cause) {
		super(cause);
	}

	public ChatXMPPAdressMalformedException(String msg) {
		super(msg);
	}

}
