package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.QRPayEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

public class QRPayActivity extends BaseActivity {

    @InjectView(R.id.et_product)
    EditText mEtProduct;
    @InjectView(R.id.et_amount)
    EditText mEtAmount;
    @InjectView(R.id.btn_pay)
    Button mBtnPay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrpay);
        ButterKnife.inject(this);
        buildForm();
    }

    Form mForm;

    private void buildForm() {
        mForm = new Form(this);
        mForm.addField(Field.using(mEtProduct).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mEtAmount).validate(NotEmpty.build(this)));
    }

    @OnClick({R.id.btn_pay})
    public void onClickPay(View view) {
        if(mForm.isValid()) {

            if(!Utils.checkAccount(this))return;

            //if(!Utils.checkCredit(this,Double.parseDouble(mEtAmount.getText().toString())))return;

            JobManager.getInstance().accountQRPay(mEtAmount.getText().toString(),mEtProduct.getText().toString());
        }
    }


    public void onEventMainThread(QRPayEvent event) {
        Utils.toggleEventPopup(this,event,new Utils.StatusListener<QRPayEvent>() {
            @Override
            public void onSucceed(QRPayEvent event) {
                Utils.viewQRCode(QRPayActivity.this,event.getResponse().getData().url);
            }
        });
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
