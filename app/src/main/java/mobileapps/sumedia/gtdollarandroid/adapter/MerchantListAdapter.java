package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.helper.Formatter;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.model.LatLng;
import mobileapps.sumedia.gtdollarandroid.model.User;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.widget.CircleImageView;

/**
 * Created by 2359media on 4/7/14.
 */

public class MerchantListAdapter extends PagingBaseAdapter<User> {

    private LatLng location;

    public MerchantListAdapter(Context ctx) {
        super(ctx);
    }

    @Override
    protected int getViewResourceId(int position) {
        return R.layout.item_circle_member;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId().hashCode();
    }

    @Override
    public Renderer<User> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }


    class ViewHolder implements Renderer<User> {
        @InjectView(R.id.iv_member_photo)
        CircleImageView mIvMemberPhoto;
        @InjectView(R.id.tv_merchant_name)
        TextView mTvMerchantName;
        @InjectView(R.id.tv_merchant_distance)
        TextView mTvMerchantDistance;
//        @InjectView(R.id.tv_merchant_type)
//        TextView mTvMerchantType;
        @InjectView(R.id.iv_certified)
        ImageView mIvCertified;
        @InjectView(R.id.tv_service_count)
        TextView mTvServiceCount;
        @InjectView(R.id.iv_merchant_type)
        ImageView mIvMerchantType;
        @InjectView(R.id.iv_follow)
        ImageView mIvFollow;
        @InjectView(R.id.iv_gticon)
        ImageView mIvGTIcon;
        @InjectView(R.id.iv_merchant_video)
        ImageView mIvMerchantVideo;
        @InjectView(R.id.rb)
        RatingBar mStartRating;


        View view;
        private User item;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
            this.view = view;
        }

        @OnClick(R.id.iv_follow)
        public void onClickFollow(View view) {
            JobManager.getInstance().addContact(item.getId());
        }

        public void render(User item, int position) {
            this.item = item;

            if (!TextUtils.isEmpty(item.getPicture())) {
                Picasso.with(getContext()).load(item.getPicture()).placeholder(R.drawable.transparent).into(mIvMemberPhoto);
            } else if (!TextUtils.isEmpty(item.getCover())) {
                Picasso.with(getContext()).load(item.getCover()).placeholder(R.drawable.transparent).into(mIvMemberPhoto);
            } else {
                mIvMemberPhoto.setImageResource(R.drawable.transparent);
            }

            if (item.getServices() != null && item.getServices().size() > 0) {
                mTvServiceCount.setText(item.getServices().size() + "");
                mTvServiceCount.setVisibility(View.VISIBLE);
                //mIvMerchantType.setVisibility(View.GONE);
            } else {
                mTvServiceCount.setVisibility(View.GONE);
                //mIvMerchantType.setVisibility(View.VISIBLE);
            }

            mIvMerchantType.setImageResource(GTDHelper.merchantTypeResources(item.getMerchantType()));

            mTvMerchantName.setText(item.getName());
            //mTvMemberCount.setText(item.getMembers().size() + "");
            //mTvMerchantType.setText(GTDHelper.serviceTypeName(item.getCategories()));


            if (location != null && item.getLatlong() != null && item.getLatlong().notEmpty()) {
                mTvMerchantDistance.setVisibility(View.VISIBLE);
                mTvMerchantDistance.setText(Formatter.formatDistance(location.getLat(), location.getLng(), item.getLatlong().getLat(), item.getLatlong().getLng()));
            } else {
                mTvMerchantDistance.setVisibility(View.GONE);
            }

            mIvGTIcon.setVisibility(item.getGtd() == 1 ? View.VISIBLE : View.INVISIBLE);

            mIvMerchantVideo.setVisibility(item.hasVideo() ? View.VISIBLE : View.GONE);

            if(!TextUtils.isEmpty(item.getStarrating())) {
                mStartRating.setVisibility(View.VISIBLE);
                mStartRating.setRating(Integer.valueOf(item.getStarrating()));
            }else {
                mStartRating.setVisibility(View.GONE);
            }

        }
    }
}
