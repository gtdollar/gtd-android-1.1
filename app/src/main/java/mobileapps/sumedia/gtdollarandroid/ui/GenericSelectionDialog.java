package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.ChoseItemEvent;

public class GenericSelectionDialog extends DialogFragment {


    public static final int TYPE_COUNTRY_ORIGIN = 1;
    public static final int TYPE_DISCOUNT_RANGE = 2;
    public static final int TYPE_GENDER = 3;
    public static final int TYPE_CURRENCY = 4;
    public static final int TYPE_PICTURE = 5;
    public static final int TYPE_MERCHANT_TYPE = 6;

    public static final String DIALOG_TYPE = "type";
    public static final String CHECKED_ITEM = "checked_item";
    public static final String ITEMS = "items";

    public static GenericSelectionDialog newInstance(int type, CharSequence checkedItem) {
        GenericSelectionDialog fragment = new GenericSelectionDialog();
        Bundle b = new Bundle();
        b.putInt(DIALOG_TYPE, type);
        b.putCharSequence(CHECKED_ITEM, checkedItem);
        fragment.setArguments(b);
        return fragment;
    }

    public static GenericSelectionDialog newInstance(int type, CharSequence[] items,CharSequence checkedItem) {
        GenericSelectionDialog fragment = new GenericSelectionDialog();
        Bundle b = new Bundle();
        b.putInt(DIALOG_TYPE, type);
        b.putCharSequence(CHECKED_ITEM, checkedItem);
        b.putCharSequenceArray(ITEMS, items);
        fragment.setArguments(b);
        return fragment;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final int type = getArguments().getInt(DIALOG_TYPE);
        final CharSequence[] items;


            if(type == TYPE_COUNTRY_ORIGIN) {
                List<CharSequence> countries = new ArrayList<CharSequence>();//(Arrays.asList(getResources().getTextArray(getResourceId(type))));

                String[] locales = Locale.getISOCountries();

                for (String countryCode : locales) {

                    Locale obj = new Locale(Locale.getDefault().getLanguage(), countryCode);
                    //if(!countries.contains(obj.getDisplayCountry())) {
                        countries.add(obj.getDisplayCountry());
                    //}
                }
                items = countries.toArray(new CharSequence[countries.size()]);
            }else {
                items = getResources().getTextArray(getResourceId(type));
            }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //builder.setTitle(getResources().getString(R.string.lanset_default));
        builder.setSingleChoiceItems(items,
                getCheckedIndex(items,getArguments().getCharSequence(CHECKED_ITEM, "")),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        dialog.dismiss();
                        EventBus.getDefault().post(new ChoseItemEvent(type,item,items[item]));
                        //mListener.onItemSelected(type,dialog, item, items[item]);
                    }
                }
        );
        return builder.create();
    }

    private int getCheckedIndex(CharSequence[] items, CharSequence string) {
        for (int i = 0; i < items.length; i++) {
            if (items[i].equals(string)) {
                return i;
            }
        }
        return -1;
    }

    private int getResourceId(int type) {
        switch (type) {
            case TYPE_GENDER:
                return R.array.gender_array;
            case TYPE_DISCOUNT_RANGE:
                return R.array.discount_array;
            case TYPE_CURRENCY:
                return R.array.currency_array;
            case TYPE_PICTURE:
                return R.array.choose_picture_array;
            case TYPE_MERCHANT_TYPE:
                return R.array.merchant_types;
            default:
                return 0;
        }
    }

//    public interface OnDialogItemSelectionListener {
//
//        public void onItemSelected(int type, DialogInterface dialog, int position, CharSequence item);
//    }
}
