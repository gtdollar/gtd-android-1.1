package mobileapps.sumedia.gtdollarandroid.model;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by 2359media on 4/7/14.
 */
public class GoogleMerchant implements Serializable
{
    public String name;
    public String detail_reference;
    public double latitude;
    public double longitude;
    public String formatted_address;
    public String formatted_phone_number;
    public String international_phone_number;
    public String photo_url;
    public String website;
    public String url;
    public String icon_url;
    public String vicinity;
    public String types;

    public Bitmap image;

    public GoogleMerchant(){

    }

    public GoogleMerchant(String name, String photo_url){
        this.name = name;
        this.photo_url = photo_url;
    }

    public GoogleMerchant(String name, String photo_url, String type){
        this.name = name;
        this.photo_url = photo_url;
        this.types = type;
    }


}
