package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 8/29/14.
 */
public class SearchEvent {

    int type;
    String searchString;

    public SearchEvent(int type, String searchString) {
        this.type = type;
        this.searchString = searchString;
    }

    public int getType() {
        return type;
    }

    public String getSearchString() {
        return searchString;
    }
}
