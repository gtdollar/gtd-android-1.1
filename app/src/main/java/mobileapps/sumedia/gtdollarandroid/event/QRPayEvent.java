package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.AccountQRPayResponse;

/**
 * Created by zhuodong on 8/21/14.
 */
public class QRPayEvent extends BaseEvent<AccountQRPayResponse> {
    public QRPayEvent(EventStatus status) {
        super(status);
    }

    public QRPayEvent(EventStatus status, AccountQRPayResponse response) {
        super(status, response);
    }
}
