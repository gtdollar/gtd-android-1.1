package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.makeramen.RoundedImageView;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.LanguageChangedEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.model.User;

public class MeActivity extends BaseActivity {


    @InjectView(R.id.tv_general_setting)
    TextView mTvGeneralSetting;
    @InjectView(R.id.iv_member_photo)
    RoundedImageView mIvMemberPhoto;
    @InjectView(R.id.tv_merchant_name)
    TextView mTvMerchantName;
    @InjectView(R.id.tv_merchant_id)
    TextView mTvMerchantId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitiy_me);
        ButterKnife.inject(this);
        setTitle(R.string.title_activity_me);
        setAboutMe();
    }

    private void setAboutMe() {
        User user = UserDataHelper.getUser();
        if (user != null) {
            if (!TextUtils.isEmpty(user.getPicture())) {
                Picasso.with(this).load(user.getPicture()).into(mIvMemberPhoto);
            } else if (!TextUtils.isEmpty(user.getCover())) {
                Picasso.with(this).load(user.getCover()).into(mIvMemberPhoto);
            }
            mTvMerchantName.setText(user.getName());
            mTvMerchantId.setText(getString(R.string.string_gtd_id,user.getEmail()));
        }
    }

    @OnClick({R.id.tv_profile_setting, R.id.tv_general_setting})
    public void onClickSettings(View view) {
        Intent intent = new Intent();
        if (view.getId() == R.id.tv_general_setting) {
            intent.setClass(this, GeneralSettingsActivity.class);

        } else {
            intent.setClass(this, MerchantProfileActivity.class);
            intent.putExtra(Const.INTENT_EXTRA_MERCHANT_KEY, UserDataHelper.getUser());
            intent.putExtra(Const.INTENT_EXTRA_IS_UPDATING, true);
        }
        startActivity(intent);
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    public void onEventMainThread(LanguageChangedEvent event) {
        this.recreate();
    }
}