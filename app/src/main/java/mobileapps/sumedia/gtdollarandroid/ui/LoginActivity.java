package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.GTDApplication;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.chat.XMPPService;
import mobileapps.sumedia.gtdollarandroid.event.ForgotPWDEvent;
import mobileapps.sumedia.gtdollarandroid.event.SignInEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.User;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.validations.IsEmail;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

public class LoginActivity extends BaseActivity {


    Form mForm;
    Form mForm1;

    @InjectView(R.id.et_email)
    EditText etEmail;
    @InjectView(R.id.btn_login)
    Button btnLogin;
    @InjectView(R.id.et_password)
    EditText etPassword;
    @InjectView(R.id.tv_gtd)
    TextView tvGtd;
    @InjectView(R.id.tv_forget_password)
    TextView tvForgetPassword;
    @InjectView(R.id.tv_signup)
    TextView tvSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);

        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    attemptLogin(btnLogin);
                    return true;
                }
                return false;
            }
        });

        buildForm();

        if (UserDataHelper.isLoggedIn()) {

            Intent intent = new Intent(LoginActivity.this, MainTabActivity.class);
            intent.putExtra(Const.INTENT_EXTRA_REFRESH_TOKEN, true);
            startActivity(intent);
            LoginActivity.this.finish();
            return;
        }

    }

    @OnClick(R.id.btn_login)
    public void attemptLogin(View view) {
        if (mForm.isValid()) {
//            Utils.displayProgress(this);

            JobManager.getInstance().signInAll(etEmail.getText().toString(),
                etPassword.getText().toString());

//            JobManager.getInstance().signIn(mEtPrice.getText().toString(),
//                mEtWebsite.getText().toString());

//            JobManager.getInstance().accountSignIn(mEtPrice.getText().toString(),
//                mEtWebsite.getText().toString());
        }
    }


    @OnClick(R.id.tv_gtd)
    public void onClickGTD(View view) {
        Utils.viewLink(this, "http://www.gtdollar.com");
    }

    @OnClick(R.id.tv_forget_password)
    public void onForgotPWD(View view) {
        if (mForm1.isValid()) {
            JobManager.getInstance().forgotPWD(etEmail.getText().toString());
        }
    }

    @OnClick(R.id.tv_signup)
    public void onSignUp(View view) {
        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
        startActivity(intent);
    }

    private void buildForm() {
        mForm = new Form(this);
        mForm.addField(Field.using(etEmail).validate(NotEmpty.build(this)).validate(
            IsEmail.build(this)));
        mForm.addField(Field.using(etPassword).validate(NotEmpty.build(this)));

        mForm1 = new Form(this);
        mForm1.addField(Field.using(etEmail).validate(NotEmpty.build(this)).validate(
            IsEmail.build(this)));
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    public void onEventMainThread(ForgotPWDEvent event) {
        Utils.toggleEventPopup(this, event, new Utils.StatusListener<ForgotPWDEvent>() {
            @Override
            public void onSucceed(ForgotPWDEvent event) {
                if (event.getResponse() != null && event.getResponse().isSuccess()) {
                    //Utils.toast(LoginActivity.this, event.getResponse().getMessage());
                    Utils.toast(LoginActivity.this, getString(R.string.check_email));
                }
            }
        });
    }

//    public void onEventMainThread(UserSignInEvent event) {
//        Utils.toggleEventPopup(this, event, new Utils.StatusListener<UserSignInEvent>() {
//            @Override
//            public void onSucceed(UserSignInEvent event) {
//                User user = event.getResponse().getUser();
//                if(user != null) {
//                    Utils.toast(getApplicationContext(), getString(R.string.str_signin_welcome, user.getName()));
//
//                    //
//                    if(!TextUtils.isEmpty(user.getJabberId())) {
//                        GTDApplication.getInstance().getConfig().saveJabberId(user.getJabberId());
//
//                        Intent xmppServiceIntent = new Intent(LoginActivity.this, XMPPService.class);
//                        startService(xmppServiceIntent);
//                    }
//                }
//
//                Intent intent = new Intent(LoginActivity.this, MainTabActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });
//        if (event.getStatus() == EventStatus.SUCCEED) {
//            isUserSignedIn = true;
//            checkFinish();
//        } else if (event.getStatus() == EventStatus.FAILED) {
//            Utils.dismissProgress(this);
//            Utils.displayError(this, event.getErrorMsg());
//        }
//    }


    public void onEventMainThread(SignInEvent event) {

        Utils.toggleEventPopup(this, event, new Utils.StatusListener<SignInEvent>() {
            @Override
            public void onSucceed(SignInEvent event) {
                User user = UserDataHelper.getUser();
                if (user != null) {
                    //
                    if (!TextUtils.isEmpty(user.getJabberId())) {
                        GTDApplication.getInstance().getConfig().saveJabberId(user.getJabberId());
                        Intent xmppServiceIntent = new Intent(LoginActivity.this, XMPPService.class);
                        startService(xmppServiceIntent);
                    }
                }

                Intent intent = new Intent(LoginActivity.this, MainTabActivity.class);
                startActivity(intent);

                LoginActivity.this.finish();
            }
        });

//        if (event.getStatus() == EventStatus.SUCCEED) {
//            isAccountSignedIn = true;
//            checkFinish();
//        } else if (event.getStatus() == EventStatus.FAILED) {
//            Utils.dismissProgress(this);
//            Utils.displayError(this, event.getErrorMsg());
//        }
    }

//    boolean isUserSignedIn, isAccountSignedIn;
//
//    private void checkFinish() {
//        if (isAccountSignedIn && isUserSignedIn) {
//            Utils.dismissProgress(this);
//
//            User user = UserDataHelper.getUser();
//            if (user != null) {
//                //
//                if (!TextUtils.isEmpty(user.getJabberId())) {
//                    GTDApplication.getInstance().getConfig().saveJabberId(user.getJabberId());
//
//                    Intent xmppServiceIntent = new Intent(LoginActivity.this, XMPPService.class);
//                    startService(xmppServiceIntent);
//                }
//            }
//
//            Intent intent = new Intent(LoginActivity.this, MainTabActivity.class);
//            startActivity(intent);
//
//            this.finish();
//        }
//    }

    @Override
    public boolean hasActionBar() {
        return false;
    }
}
