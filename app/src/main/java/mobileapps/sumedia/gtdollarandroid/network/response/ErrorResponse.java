package mobileapps.sumedia.gtdollarandroid.network.response;

/**
 * Created by zhuodong on 8/8/14.
 */
public interface ErrorResponse {
    public String getError();
}
