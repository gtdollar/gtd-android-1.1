package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

import mobileapps.sumedia.gtdollarandroid.model.User;

/**
 * Created by zhuodong on 8/29/14.
 */
public class ContactsResponse extends BaseResponse{

    @Expose
    private java.util.List<User> list = new ArrayList<User>();

    public java.util.List<User> getList() {
        return list;
    }

    public void setList(java.util.List<User> list) {
        this.list = list;
    }
}
