package mobileapps.sumedia.gtdollarandroid.chat;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.Feed;
import mobileapps.sumedia.gtdollarandroid.GTDApplication;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.SetRedDotEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.ui.ChatActivity;

public abstract class GenericService extends Service {

    private static final String TAG = "Chat.Service";
    //private static final String APP_NAME = "yaxim";
    private static final int MAX_TICKER_MSG_LEN = 50;

    private NotificationManager mNotificationMGR;
    private Notification mNotification;
    private Vibrator mVibrator;
    private Intent mNotificationIntent;

    //LogConstantsprotected WakeLock mWakeLock;
    //private int mNotificationCounter = 0;

    private Map<String, Integer> notificationCount = new HashMap<String, Integer>(2);
    private Map<String, Integer> notificationId = new HashMap<String, Integer>(2);
    protected static int SERVICE_NOTIFICATION = 1;
    private int lastNotificationId = 2;

    protected ChatConfig mConfig;

    @Override
    public IBinder onBind(Intent arg0) {
        Log.i(TAG, "called onBind()");
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG, "called onUnbind()");
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        Log.i(TAG, "called onRebind()");
        super.onRebind(intent);
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "called onCreate()");
        super.onCreate();
        mConfig = GTDApplication.getInstance().getConfig();
        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

//        mWakeLock = ((PowerManager)getSystemService(Context.POWER_SERVICE))
//				.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, APP_NAME);

        addNotificationMGR();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "called onDestroy()");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "called onStartCommand()");
        return START_STICKY;
    }

    private void addNotificationMGR() {
        mNotificationMGR = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotificationIntent = new Intent(this, ChatActivity.class);
    }

    protected void notifyClient(String fromJid, String fromUserName, String userAvatar, String messageType, String message,
                                boolean showNotification, boolean silent_notification, boolean is_error) {
        if (!showNotification) {
            if (is_error)
                shortToastNotify(getString(ChatConstants.getErrorMsg(messageType)) + " " + message);
            // only play sound and return
            try {
                if (!silent_notification && !Uri.EMPTY.equals(mConfig.notifySound))
                    RingtoneManager.getRingtone(getApplicationContext(), mConfig.notifySound).play();
            } catch (NullPointerException e) {
                // ignore NPE when ringtone was not found
            }
            return;
        }
        //mWakeLock.acquire();

        // Override silence when notification is created initially
        // if there is no open notification for that JID, and we get a "silent"
        // one (i.e. caused by an incoming carbon message), we still ring/vibrate,
        // but only once. As long as the user ignores the notifications, no more
        // sounds are made. When the user opens the chat window, the counter is
        // reset and a new sound can be made.
        if (silent_notification && !notificationCount.containsKey(fromJid)) {
            silent_notification = false;
        }

        setNotification(fromJid, fromUserName, userAvatar, messageType, message, is_error);
        DBUtils.modifyChatRead(fromJid);
        EventBus.getDefault().post(new SetRedDotEvent(true));
        //addNewChatToFeed(fromJid, fromUserName, userAvatar, messageType, message);
        setLEDNotification();
        if (!silent_notification)
            mNotification.sound = mConfig.notifySound;

        int notifyId = 0;
        if (notificationId.containsKey(fromJid)) {
            notifyId = notificationId.get(fromJid);
        } else {
            lastNotificationId++;
            notifyId = lastNotificationId;
            notificationId.put(fromJid, Integer.valueOf(notifyId));
        }

        // If vibration is set to "system default", add the vibration flag to the
        // notification and let the system decide.
        if (!silent_notification && "SYSTEM".equals(mConfig.vibraNotify)) {
            mNotification.defaults |= Notification.DEFAULT_VIBRATE;
        }
        mNotificationMGR.notify(notifyId, mNotification);

        // If vibration is forced, vibrate now.
        if (!silent_notification && "ALWAYS".equals(mConfig.vibraNotify)) {
            mVibrator.vibrate(400);
        }
        //mWakeLock.release();
    }

    private void addNewChatToFeed(String fromJid, String fromUserName, String userAvatar, String messageType, String message) {
        long createdAt = System.currentTimeMillis();
        Feed temp = new Feed();
        temp.setBaseType(Feed.TYPE_CHAT);
        temp.setType(ChatConstants.messageViewType(ChatConstants.INCOMING,messageType));
        temp.setCreatedAt(createdAt);
        temp.setExtraId(createdAt+"");

        temp.setSubjectName(fromUserName);
        temp.setSubjectId(fromJid);
        temp.setSubjectAvatar(userAvatar);
        temp.setExtra(message);

        GTDApplication.getInstance().getDaoSession().getFeedDao().insert(temp);
    }

    private void setNotification(String fromJid, String fromUserName, String fromUserAvatar, String messageType, String message, boolean is_error) {

        int mNotificationCounter = 0;
        if (notificationCount.containsKey(fromJid)) {
            mNotificationCounter = notificationCount.get(fromJid);
        }
        mNotificationCounter++;
        notificationCount.put(fromJid, mNotificationCounter);
        String author;
        if (null == fromUserName || fromUserName.length() == 0) {
            author = fromJid;
        } else {
            author = fromUserName;
        }
        String title = getString(R.string.notification_message, author);
        String ticker;
        if (is_error) {
            title = getString(ChatConstants.getErrorMsg(messageType));
            ticker = title;
            message = author + ": " + message;
        } else if (mConfig.ticker) {
            message = ChatConstants.getNotifyMsg(this, messageType, message);
            int newline = message.indexOf('\n');
            int limit = 0;
            String messageSummary = message;
            if (newline >= 0)
                limit = newline;
            if (limit > MAX_TICKER_MSG_LEN || message.length() > MAX_TICKER_MSG_LEN)
                limit = MAX_TICKER_MSG_LEN;
            if (limit > 0)
                messageSummary = message.substring(0, limit) + " [...]";
            ticker = title + "\n" + messageSummary;
        } else
            ticker = getString(R.string.notification_anonymous_message);


        Uri userNameUri = Uri.parse(fromJid);
        mNotificationIntent.setData(userNameUri);
        mNotificationIntent.putExtra(Const.INTENT_EXTRA_USER_NAME, fromUserName);
        mNotificationIntent.putExtra(Const.INTENT_EXTRA_USER_AVATAR, fromUserAvatar);
        mNotificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        //need to set flag FLAG_UPDATE_CURRENT to get extras transferred
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
            mNotificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        Log.d(TAG, "notification number " + mNotificationCounter);
        mNotification = new NotificationCompat.Builder(getApplicationContext())
            .setTicker(ticker).setDefaults(0).setSmallIcon(R.mipmap.ic_launcher).setAutoCancel(true)
            .setContentTitle(title).setContentText(message).setContentIntent(pendingIntent).setNumber(mNotificationCounter).build();
    }

    private void setLEDNotification() {
        if (mConfig.isLEDNotify) {
            mNotification.ledARGB = Color.MAGENTA;
            mNotification.ledOnMS = 300;
            mNotification.ledOffMS = 1000;
            mNotification.flags |= Notification.FLAG_SHOW_LIGHTS;
        }
    }

    protected void shortToastNotify(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toast.show();
    }

    protected void shortToastNotify(Throwable e) {
        e.printStackTrace();
        while (e.getCause() != null)
            e = e.getCause();
        shortToastNotify(e.getMessage());
    }

    public void resetNotificationCounter(String userJid) {
        notificationCount.remove(userJid);
    }

    protected void logError(String data) {
        if (LogConstants.LOG_ERROR) {
            Log.e(TAG, data);
        }
    }

    protected void logInfo(String data) {
        if (LogConstants.LOG_INFO) {
            Log.i(TAG, data);
        }
    }

    public void clearNotification(String Jid) {
        int notifyId = 0;
        if (notificationId.containsKey(Jid)) {
            notifyId = notificationId.get(Jid);
            mNotificationMGR.cancel(notifyId);
        }
    }

}
