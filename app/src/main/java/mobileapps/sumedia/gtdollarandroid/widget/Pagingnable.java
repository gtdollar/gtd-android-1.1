package mobileapps.sumedia.gtdollarandroid.widget;

/**
 * Created by zhuodong on 7/17/14.
 */
public interface Pagingnable {
    void onLoadMoreItems();
}