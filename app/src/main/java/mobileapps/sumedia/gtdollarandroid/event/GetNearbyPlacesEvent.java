package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.GooglePlaceResponse;

/**
 * Created by zhuodong on 9/11/14.
 */
public class GetNearbyPlacesEvent extends BaseEvent<GooglePlaceResponse> {
    public GetNearbyPlacesEvent(EventStatus status) {
        super(status);
    }

    public GetNearbyPlacesEvent(EventStatus status, GooglePlaceResponse response) {
        super(status, response);
    }
}
