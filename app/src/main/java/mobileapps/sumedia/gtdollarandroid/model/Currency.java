package mobileapps.sumedia.gtdollarandroid.model;

import java.io.Serializable;

/**
 * Created by zhuodong on 7/31/14.
 */
public class Currency implements Serializable{
    String id,name,symbol;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Currency(String id, String name, String symbol) {
        this.id = id;
        this.name = name;
        this.symbol = symbol;
    }
}
