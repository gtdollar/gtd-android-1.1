package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.LanguageChangedEvent;
import mobileapps.sumedia.gtdollarandroid.event.SetRedDotEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.location.location.LocationActivity;


public class MainTabActivity extends LocationActivity {

    EditText searchEditText;

    @InjectView(R.id.fl_content)
    FrameLayout flContent;
    @InjectView(R.id.tab_nearby)
    TextView tabNearby;
    @InjectView(R.id.tab_newsfeed)
    TextView tabNewsfeed;
    @InjectView(R.id.tab_contact)
    TextView tabContact;
    @InjectView(R.id.tab_more)
    TextView tabMore;
    @InjectView(R.id.ll_tab_holder)
    LinearLayout llTabHolder;

    int tabPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tab);
        ButterKnife.inject(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        //getSupportActionBar().setLogo(R.drawable.ic_launcher);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        //getSupportActionBar().setDisplayUseLogoEnabled(true);
//        if (getIntent().getBooleanExtra(Const.INTENT_EXTRA_REFRESH_TOKEN, false)) {
//            JobManager.getInstance().signInAll(UserDataHelper.getEmail(), UserDataHelper.getPwd());
//        }
        onTabSelected(tabNearby);

    }

    @OnClick({R.id.tab_nearby, R.id.tab_newsfeed, R.id.tab_more, R.id.tab_contact})
    void onTabSelected(View view) {
        int selectedPosition = llTabHolder.indexOfChild(view);
        if (tabPosition != selectedPosition && selectedPosition >= 0) {
            tabPosition = selectedPosition;
            for (int i = 0; i < llTabHolder.getChildCount(); i++) {
                llTabHolder.getChildAt(i).setSelected(i == selectedPosition);
                if (i == selectedPosition) {
                    switchFragment((TextView) llTabHolder.getChildAt(i), llTabHolder.getChildAt(i).getId());
                }
            }
        }
    }

    private void switchFragment(TextView textView, int id) {
        setTitle(textView.getText());

        Fragment f = new Fragment();
        switch (id) {
            case R.id.tab_nearby:
                f = UserCirclesFragment.newInstance();
                break;
            case R.id.tab_newsfeed:
                f = FeedListFragment.newInstance();
                break;
//            case R.id.tab_business:
//                f = MyBusinessListFragment.newInstance();
//                break;
            case R.id.tab_more:
                f =  MoreFragment.newInstance();
                break;
            case R.id.tab_contact:
                f = ContactsListFragment.newInstance();
                break;
        }
        showFragment(f);
    }


    private void showFragment(Fragment f) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int i = fragmentManager.getBackStackEntryCount();
        for (int j = 0; j < i; j++) {
            fragmentManager.popBackStack();
        }
        fragmentManager.beginTransaction().replace(R.id.fl_content, f)
            .commitAllowingStateLoss();
        fragmentManager.executePendingTransactions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_tab, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);


        // In version 3.0 and later, sets up and configures the ActionBar SearchView
        if (Utils.hasHoneycomb()) {

            // Retrieves the system search manager service
            final SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);

            // Retrieves the SearchView from the search menu item
            final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

            searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
            int searchSrcTextId = getResources().getIdentifier("android:id/search_src_text", null, null);
            searchEditText = (EditText) searchView.findViewById(searchSrcTextId);
            if (searchEditText != null) {
                searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                        if (id == EditorInfo.IME_ACTION_SEARCH) {
                            //searchView.setQuery("",false);
                            //searchView.onActionViewCollapsed();
                            Utils.search(MainTabActivity.this, getLatLng(), !TextUtils.isEmpty(textView.getText()) ? textView.getText().toString() : "", null);
                            Utils.hideKeyBoard(MainTabActivity.this, textView);
                            textView.clearFocus();
                            return true;
                        }
                        return false;
                    }
                });
            }

            // Assign searchable info to SearchView
            searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
            // Set listeners for SearchView
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String queryText) {
                    // Nothing needs to happen when the user submits the search string
//                    Log.d(""," testing search "+queryText);
//                    searchView.onActionViewCollapsed();
//                    Utils.search(MainTabActivity.this, currentLocation, queryText, null, false);

                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    // Called when the action bar search text has changed.  Updates
                    // the search filter, and restarts the loader to do a new query
                    // using the new search string.
                    //Log.d(""," testing text changed");
                    return true;
                }
            });
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected boolean hasBack() {
        return false;
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    public void onEventMainThread(LanguageChangedEvent event) {
        Log.d("", " testing language change");
        this.recreate();
    }

    public void onEventMainThread(SetRedDotEvent event) {
        //TODO Add Dot
//        final TextView tv = (TextView) mTabHost.getTabWidget()
//            .getChildAt(1).findViewById(android.R.id.title);
//        tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, event.isSet() ? R.drawable.red_dot : 0, 0);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean isUpdatesRequested() {
        return true;
    }
}


