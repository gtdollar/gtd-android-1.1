package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 9/8/14.
 */
public class GetRebateRateEvent extends BaseEvent {
    public GetRebateRateEvent(EventStatus status) {
        super(status);
    }

    public GetRebateRateEvent(EventStatus succeed, double rebaterate) {
        super(succeed);
        this.rebaterate = rebaterate;
    }

    double rebaterate;

    public double getRebaterate() {
        return rebaterate;
    }
}
