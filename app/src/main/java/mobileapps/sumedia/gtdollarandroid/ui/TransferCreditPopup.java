package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.AlertButtonClickEvent;
import mobileapps.sumedia.gtdollarandroid.event.CheckAccountEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountCheckResponse;
import mobileapps.sumedia.gtdollarandroid.widget.GenericAlertPopup;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

/**
 * Created by zhuodong on 4/4/14.
 */
public class TransferCreditPopup extends DialogFragment {

    @InjectView(R.id.et_available_balance)
    EditText mEtAvailableBalance;
    @InjectView(R.id.et_amount)
    EditText mEtAmount;

    double availableCredit;


    public static TransferCreditPopup newInstance() {
        TransferCreditPopup fragment = new TransferCreditPopup();

        return fragment;
    }



    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }

    Form mForm;

    private void buildForm() {
        mForm = new Form(getActivity());
        //mForm.addField(Field.using(mTvProfileGender).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mEtAmount).validate(NotEmpty.build(getActivity())));
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View view = View.inflate(getActivity(), R.layout.popup_send_gtdcredit, null);
        ButterKnife.inject(this, view);
        buildForm();
        //mEtAvailableBalance.setText("GTD " + UserDataHelper.getCredit());

        mEtAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    onClickContinue();
                    return true;
                }
                return false;
            }
        });

        JobManager.getInstance().checkAccount();


        return new AlertDialog.Builder(getActivity())
            .setTitle(R.string.string_transfer)
            .setView(view)
            .setPositiveButton(R.string.string_send,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        onClickContinue();
                    }
                }
            )
            .setNegativeButton(R.string.string_cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        EventBus.getDefault()
                            .post(new AlertButtonClickEvent(GenericAlertPopup.ALERT_TRANSFER, false));
                    }
                }
            ).create();

    }

    private void onClickContinue() {
        if(mForm != null && mForm.isValid()) {
            if (availableCredit < Double.parseDouble(mEtAmount.getText().toString())) {
                Utils.toast(getActivity(), getString(R.string.toast_credit_not_enough));
                return;
            }

            AlertButtonClickEvent event = new AlertButtonClickEvent(GenericAlertPopup.ALERT_TRANSFER, true);
            event.setExtra(mEtAmount.getText().toString());
            EventBus.getDefault()
                .post(event);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    public void onEventMainThread(CheckAccountEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED && event.getResponse() != null) {
            AccountCheckResponse.Data data = event.getResponse().getData();

            availableCredit = data.getCredits();
            mEtAvailableBalance.setText(GTDHelper.formatCredit(availableCredit));
        }
    }
}
