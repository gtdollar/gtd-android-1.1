package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.model.Account;
import mobileapps.sumedia.gtdollarandroid.network.CreditCardTopupRequest;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

public class ScanCardActivity extends BaseActivity {
    final String TAG = getClass().getName();
    @InjectView(R.id.et_card_number)
    EditText mEtCardNumber;
    @InjectView(R.id.et_card_name)
    EditText mEtCardName;
    @InjectView(R.id.et_amount)
    EditText mEtAmount;
    @InjectView(R.id.btn_continue)
    Button mBtnContinue;


    private int MY_SCAN_REQUEST_CODE = 100; // arbitrary int
    private Form mForm;

    CreditCardTopupRequest request = new CreditCardTopupRequest();

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_card);
        ButterKnife.inject(this);

        Account account = UserDataHelper.getAccount();

        if(account == null){
            finish();
            return;
        }

        request.setCustomerEmail(account.getEmail());
        request.setMember(account.getEmail());
        request.setMemberCurrency(account.getMcurrency());
        request.setFirstName(UserDataHelper.getUser().getName());

        mEtAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    onClickNext(mBtnContinue);
                    return true;
                }
                return false;
            }
        });
        buildForm();


        Intent scanIntent = new Intent(this, CardIOActivity.class);

        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: true
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false

        // hides the manual entry button
        // if set, developers should provide their own manual entry mechanism in the app
        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, false); // default: false

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void buildForm() {
        mForm = new Form(this);
        mForm.addField(Field.using(mEtCardNumber).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mEtCardName).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mEtAmount).validate(NotEmpty.build(this)));
    }

    @OnClick(R.id.btn_continue)
    public void onClickNext(View view) {
        if (!mForm.isValid()) return;
        request.setNoc(mEtCardName.getText().toString());
        request.setPrice(mEtAmount.getText().toString());

        Intent intent = new Intent(this, TopupWebViewActivity.class);
        intent.putExtra("creditcard_request",request);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String resultStr;
        if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {


            CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

            request.setCardNumber(scanResult.cardNumber);
            mEtCardNumber.setText(scanResult.getRedactedCardNumber());

            request.setExpMonth(scanResult.expiryMonth+"");
            request.setExpYear(scanResult.expiryYear + "");
            request.setSecurityCode(scanResult.cvv);

//            // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
//            resultStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";
//
//            // Do something with the raw number, e.g.:
//            // myService.setCardNumber( scanResult.cardNumber );
//
//            if (scanResult.isExpiryValid()) {
//                resultStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
//            }

//            if (scanResult.cvv != null) {
//                // Never log or display a CVV
//                resultStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
//            }
//
//            if (scanResult.postalCode != null) {
//                resultStr += "Postal Code: " + scanResult.postalCode + "\n";
//            }
        } else {
            finish();
        }

    }
}

