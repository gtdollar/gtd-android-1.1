package mobileapps.sumedia.gtdollarandroid.ui;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.CircleServiceAdapter;
import mobileapps.sumedia.gtdollarandroid.event.AddContactEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.GetCircleDetailEvent;
import mobileapps.sumedia.gtdollarandroid.event.ScaleBitmapEvent;
import mobileapps.sumedia.gtdollarandroid.event.UpdateCircleEvent;
import mobileapps.sumedia.gtdollarandroid.event.UpdatePeopleEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.Circle;
import mobileapps.sumedia.gtdollarandroid.model.Service;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.widget.AspectRatioImageView;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

public class CircleDetailActivity extends BasePictureActivity implements AdapterView.OnItemClickListener {

    @InjectView(R.id.lv_users)
    ListView mLvUsers;

    Circle circle;

    HeaderViewHolder headerViewHolder;

    Map<String, String> baseParams = new HashMap<String, String>();
    private boolean isDisplayCircleService;

    //CircleMemberListAdapter circleMemberListAdapter;
    CircleServiceAdapter circleServiceAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        circle = getIntent().getParcelableExtra(Const.INTENT_EXTRA_CIRCLE);

        //JobManager.getInstance().getCircleDetail(circle.getId());

        if (circle == null) {
            finish();
            return;
        }

        setContentView(R.layout.activity_circle_detail);
        ButterKnife.inject(this);
        View headerView = LayoutInflater.from(this).inflate(R.layout.include_circle_detail_header, null);
        headerViewHolder = new HeaderViewHolder(headerView);

        //headerViewHolder.render(circle);

        setTitle(circle.getName());

        mLvUsers.addHeaderView(headerView);

        isDisplayCircleService = true;
        circleServiceAdapter = new CircleServiceAdapter(this);

        mLvUsers.setAdapter(circleServiceAdapter);

        mLvUsers.setOnItemClickListener(this);

        baseParams.put("id", circle.getId());
        baseParams.put("name", circle.getName());
        baseParams.put("type", circle.getType() + "");

        setCircleDetail(circle);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        int index = i - 1;

        if (isDisplayCircleService) {
            ArrayList<Service> services = new ArrayList<Service>();
            services.add(circle.getServices().get(index));
            Utils.viewService(this, services, 0);
        } else {
            if (circle.getMembers() == null || index >= circle.getMembers().size()) return;
            Utils.viewMerchant(this, null, circle.getMembers().get(index).getId());
        }
    }

    class HeaderViewHolder {

        @InjectView(R.id.iv_merchant_cover)
        AspectRatioImageView mIvMerchantCover;
        @InjectView(R.id.tv_merchant_desc)
        EditText mTvMerchantDesc;
        @InjectView(R.id.tv_circle_type)
        TextView mTvCircleType;
        @InjectView(R.id.tv_user_count)
        TextView mTvUserCount;
        @InjectView(R.id.tv_service_count)
        TextView mTvServiceCount;
        @InjectView(R.id.tv_share)
        TextView mTvShare;


        public HeaderViewHolder(View headerView) {
            ButterKnife.inject(this, headerView);

            final Form mForm = new Form(CircleDetailActivity.this);
            mForm.addField(Field.using(mTvMerchantDesc).validate(NotEmpty.build(CircleDetailActivity.this)));

            mTvMerchantDesc.setImeOptions(EditorInfo.IME_ACTION_DONE);
            mTvMerchantDesc.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                    if (id == EditorInfo.IME_ACTION_DONE) {
                        Utils.hideKeyBoard(CircleDetailActivity.this,textView);
                        if (mForm.isValid()) {
                            HashMap<String, String> params = new HashMap<String, String>(baseParams);
                            params.put("description", mTvMerchantDesc.getText().toString());

                            JobManager.getInstance().updateCircle(null,params);
                        }
                        return true;
                    }
                    return false;
                }
            });


        }

        @OnClick(R.id.tv_share)
        public void onClickShare(View view) {
            Utils.screenShotAndShare(CircleDetailActivity.this);
        }

        @OnClick(R.id.tv_user_count)
        public void onClickMembers(View view) {
            Utils.viewCircleMembers(CircleDetailActivity.this, circle);
        }


        @OnClick(R.id.iv_merchant_cover)
        public void onClickCover() {
            GenericSelectionDialog.newInstance(GenericSelectionDialog.TYPE_PICTURE, null).show(getSupportFragmentManager(), "");
        }

        public void render(Circle circle) {
            mTvMerchantDesc.setText(circle.getDescription());
            mTvCircleType.setText(GTDHelper.serviceTypeName(circle.getType()));

            if (!TextUtils.isEmpty(circle.getCover())) {
                Picasso.with(CircleDetailActivity.this).load(circle.getCover()).into(mIvMerchantCover); // 牛逼，赞一个
            }

            mTvUserCount.setText(circle.getMemberCount() + "");
            mTvServiceCount.setText(circle.getServiceCount()+"");

            int size = getResources().getDimensionPixelSize(R.dimen.circle_detail_icon_size);

            Drawable serviceDrawable = getResources().getDrawable(R.drawable.ic_merchant_services);
            serviceDrawable.setBounds(0, 0, size, size);
            mTvServiceCount.setCompoundDrawables(null, serviceDrawable, null, null);


            Drawable shareDrawable = getResources().getDrawable(R.drawable.ic_share);
            shareDrawable.setBounds(0, 0, size, size);
            mTvShare.setCompoundDrawables(null, shareDrawable, null, null);

            Drawable peopleDrawable = getResources().getDrawable(R.drawable.ic_groups);
            peopleDrawable.setBounds(0, 0, size, size);
            mTvUserCount.setCompoundDrawables(null, peopleDrawable, null, null);

            Drawable typeDrawable = getResources().getDrawable(GTDHelper.merchantTypeResources(circle.getType()));
            typeDrawable.setBounds(0, 0, size, size);
            mTvCircleType.setCompoundDrawables(null, typeDrawable, null, null);
        }

        public void loadCover(String tempPath) {
            Picasso.with(CircleDetailActivity.this).load(new File(tempPath)).fit().centerCrop().into(mIvMerchantCover);
        }

        public void updateMemberCount() {
            if (circle.getMembers() != null) {
                mTvUserCount.setText(circle.getMembers().size() + "");
            }
        }
    }

    public void onEventMainThread(AddContactEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED) {
            Utils.toast(this, getString(R.string.toast_followed));
        }
    }

    public void onEventMainThread(ScaleBitmapEvent event) {
        Utils.toggleEventPopup(this, event, new Utils.StatusListener<ScaleBitmapEvent>() {
            @Override
            public void onSucceed(ScaleBitmapEvent event) {
                if (event.getResponse() && !tempPath.isEmpty()) {
                    headerViewHolder.loadCover(tempPath);
                    JobManager.getInstance().updateCircle(tempPath,baseParams);
                }
            }
        });
    }

    public void onEventMainThread(UpdateCircleEvent event) {
        if(event.getStatus() == EventStatus.SUCCEED) {
            JobManager.getInstance().getCircles();
        }
    }

    public void onEventMainThread(UpdatePeopleEvent event) {

        if (event.getStatus() == EventStatus.SUCCEED) {
            setCircleDetail(event.getResponse().getCircle());
        }

    }

    public void onEventMainThread(GetCircleDetailEvent event) {
        if(event.getResponse() == null || event.getResponse().getCircle() == null || !circle.getId().equals(event.getResponse().getCircle().getId()))return;
        setCircleDetail(event.getResponse().getCircle());

//        if (circle.getServices() != null && circle.getServices().size() > 0) {
//            isDisplayCircleService = true;
//            circleServiceAdapter = new CircleServiceAdapter(this);
//            circleServiceAdapter.setItems(circle.getServices());
//            mLvUsers.setAdapter(circleServiceAdapter);
//        } else {
//            CircleMemberListAdapter circleMemberListAdapter = new CircleMemberListAdapter(this, R.layout.item_circle_member);
//            circleMemberListAdapter.setItems(circle.getMembers());
//            mLvUsers.setAdapter(circleMemberListAdapter);
//        }
    }

    private void setCircleDetail(Circle circle) {
        this.circle = circle;
        if(circle != null) {
            circle.setServiceCount(circle.getServices().size());
            circle.setMemberCount(circle.getMembers().size());
            circleServiceAdapter.setItems(circle.getServices());
            headerViewHolder.render(circle);
        }
    }


    @Override
    public boolean isSubscribedEvent() {
        return true;
    }


}
