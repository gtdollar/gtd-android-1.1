package mobileapps.sumedia.gtdollarandroid.helper;

import android.graphics.BitmapFactory;

import java.util.ArrayList;

import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.model.CartItem;
import mobileapps.sumedia.gtdollarandroid.model.GoogleMerchant;

/**
 * Created by zhuodong on 7/22/14.
 */
public class MockData {
    public static ArrayList<GoogleMerchant> randomMerchants()
    {
        ArrayList<GoogleMerchant> list = new ArrayList<GoogleMerchant>();
        list.add(new GoogleMerchant("Jerry Cao","http://cdn.gtdollar.com/pp/1db0-40132-1001-53847790.jpg"));
        list.add(new GoogleMerchant("Grade Stat","http://cdn.gtdollar.com/pp/240e-95075-ddb-5396d9ad.jpg"));
        list.add(new GoogleMerchant("Flower Shop","http://cdn.gtdollar.com/pp/1b50-58448-1bd2-53aba545.jpg"));
        list.add(new GoogleMerchant("Lucky Plaza","http://cdn.gtdollar.com/pp/d21-66778-245a-53b238df.jpg"));
        list.add(new GoogleMerchant("Astonishing Goal","http://cdn.gtdollar.com/pp/5fc-93859-1800-5388065e.jpg"));
        list.add(new GoogleMerchant("Drake Cao","http://cdn.gtdollar.com/pp/1db0-40132-1001-53847790.jpg"));
        list.add(new GoogleMerchant("Easter Cao","http://cdn.gtdollar.com/pp/1db0-40132-1001-53847790.jpg"));
        list.add(new GoogleMerchant("Frankie Cao","http://cdn.gtdollar.com/pp/1db0-40132-1001-53847790.jpg"));
        list.add(new GoogleMerchant("George Cao","http://cdn.gtdollar.com/pp/1db0-40132-1001-53847790.jpg"));
        list.add(new GoogleMerchant("Harry Cao","http://cdn.gtdollar.com/pp/1db0-40132-1001-53847790.jpg"));
        list.add(new GoogleMerchant("Indigo Cao","http://cdn.gtdollar.com/pp/1db0-40132-1001-53847790.jpg"));
        list.add(new GoogleMerchant("Jack Cao","http://cdn.gtdollar.com/pp/1db0-40132-1001-53847790.jpg"));

        return list;
    }
}
