package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 11/25/14.
 */
public class UnionPayEvent extends BaseEvent<String> {
    public UnionPayEvent(EventStatus status) {
        super(status);
    }

    public UnionPayEvent(EventStatus status, String response) {
        super(status, response);
    }
}
