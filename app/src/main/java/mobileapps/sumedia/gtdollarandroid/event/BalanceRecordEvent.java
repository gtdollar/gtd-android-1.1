package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.AccountBalanceRecordResponse;

/**
 * Created by zhuodong on 8/21/14.
 */
public class BalanceRecordEvent extends BaseEvent<AccountBalanceRecordResponse> {
    int type;

    public BalanceRecordEvent(EventStatus status) {
        super(status);
    }

    public BalanceRecordEvent(EventStatus status, AccountBalanceRecordResponse response) {
        super(status, response);
    }

    public BalanceRecordEvent(EventStatus status, AccountBalanceRecordResponse response, int type) {
        super(status, response);
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
