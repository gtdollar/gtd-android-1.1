package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.Formatter;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.GTUser;
import mobileapps.sumedia.gtdollarandroid.model.Service;

public class ServiceDetailActivity extends BaseActivity {


    @InjectView(R.id.vp_services)
    ViewPager mVpServices;
    @InjectView(R.id.fl_book)
    FrameLayout mFlBook;

    private List<Service> services;

    View shopHolder;
    View ivCart;

    TextView tvItemCount;

    List<Service> pendingOrder = new ArrayList<Service>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_detail);
        ButterKnife.inject(this);

        services = getIntent().getParcelableArrayListExtra(Const.INTENT_EXTRA_SERVICE_LIST);


        if (services == null || services.size() == 0) {
            finish();
            return;
        }

        UserDataHelper.removePendingOrder();

        final GTUser merchant = services.get(0).getCreator();

        setTitle(merchant.getName());

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        shopHolder = getLayoutInflater().inflate(R.layout.action_bar_service, null);
        tvItemCount = (TextView) shopHolder.findViewById(R.id.tv_item_count);
        ivCart = shopHolder.findViewById(R.id.iv_shop_cart);
        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.RIGHT);
        getSupportActionBar().setCustomView(shopHolder, lp);

        shopHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pendingOrder.size() > 0) {
                    UserDataHelper.setPendingOrders(pendingOrder);
                    Utils.viewOrders(ServiceDetailActivity.this, merchant.getId(),null);
                }
            }
        });

        mVpServices.setAdapter(new ServicesPagerAdapter());
        mVpServices.setCurrentItem(getIntent().getIntExtra(Const.INTENT_EXTRA_POSITION,0));
    }

    @OnClick(R.id.fl_book)
    public void onClickBook(View view) {
        tvItemCount.setVisibility(View.VISIBLE);
        if(!pendingOrder.contains(services.get(mVpServices.getCurrentItem()))) {
            pendingOrder.add(services.get(mVpServices.getCurrentItem()));
            tvItemCount.setText(pendingOrder.size() + "");
        }
    }

    /**
     * A {@link android.support.v4.app.FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class ServicesPagerAdapter extends FragmentPagerAdapter {

        public ServicesPagerAdapter() {
            super(getSupportFragmentManager());
        }

        @Override
        public Fragment getItem(int position) {
            return ServiceDetailFragment.newInstance(services.get(position));
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            if (services == null) return 0;
            return services.size();
        }
    }

    public static class ServiceDetailFragment extends Fragment {


        Service service;
        @InjectView(R.id.iv_service_photo)
        ImageView mIvServicePhoto;
        @InjectView(R.id.tv_service_name)
        TextView mTvServiceName;
        @InjectView(R.id.tv_service_date)
        TextView mTvServiceDate;
        @InjectView(R.id.tv_service_price)
        TextView mTvServicePrice;
        @InjectView(R.id.tv_service_type)
        TextView mTvServiceType;
        @InjectView(R.id.tv_service_desc)
        TextView mTvServiceDesc;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ServiceDetailFragment newInstance(Service service) {
            ServiceDetailFragment fragment = new ServiceDetailFragment();
            Bundle args = new Bundle();
            args.putParcelable(Const.INTENT_EXTRA_SERVICE, service);
            fragment.setArguments(args);
            return fragment;
        }

        public ServiceDetailFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            service = getArguments().getParcelable(Const.INTENT_EXTRA_SERVICE);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_service_detail, container, false);
            ButterKnife.inject(this, view);
            setServiceDetail();
            return view;
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            ButterKnife.reset(this);
        }

        private void setServiceDetail() {
            if (!TextUtils.isEmpty(service.getPhoto())) {
                mIvServicePhoto.setVisibility(View.VISIBLE);
                Picasso.with(getActivity()).load(service.getPhoto()).fit().centerCrop().into(mIvServicePhoto);
            }

            mTvServiceDesc.setText(service.getDescription());
            mTvServiceName.setText(service.getTitle());
            mTvServiceDate.setText(service.getExpire());
            mTvServicePrice.setText(Formatter.formatPrice(null,service.getPrice()));
            mTvServiceType.setText(GTDHelper.serviceTypeName(service.getType()));
        }
    }
}
