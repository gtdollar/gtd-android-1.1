package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.graphics.Point;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.CartItem;
import mobileapps.sumedia.gtdollarandroid.model.Service;

/**
 * Created by 2359media on 14/7/14.
 */
public class CartGridAdapter extends ArrayAdapter<Service> implements CheckBox.OnCheckedChangeListener {

    Context context;
    int layoutResourceId;
    ArrayList<Service> data = new ArrayList<Service>();

    public CartGridAdapter(Context context, int layoutResourceId,
                                 ArrayList<Service> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RecordHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            row = inflater.inflate(layoutResourceId, parent, false);

            holder =  new RecordHolder();
            holder.txtTitle = (TextView) row.findViewById(R.id.textView_title);
            holder.imageItem = (ImageView) row.findViewById(R.id.imageView);
            holder.txtType = (TextView) row.findViewById(R.id.textView_type);
            holder.txtExpiry = (TextView) row.findViewById(R.id.textView3);
            holder.txtPrice = (TextView) row.findViewById(R.id.textView_price);
            holder.checkBox = (CheckBox) row.findViewById(R.id.checkBox);
            row.setTag(holder);

            int screenHeight = context.getResources().getDisplayMetrics().heightPixels;
            ViewGroup.LayoutParams params = row.getLayoutParams();
            params.height = (screenHeight - 140)/2;
            row.setLayoutParams(params);
        }

        else holder = (RecordHolder) row.getTag();

        Service item = data.get(position);
        holder.txtTitle.setText(item.getTitle());
        Picasso.with(context).load(item.getPhoto()).into(holder.imageItem);
        holder.txtType.setText(new GTDHelper(context).serviceTypeName(item.getType()));
        if(!TextUtils.isEmpty(item.getExpire()))    holder.txtExpiry.setText(item.getExpire());
        holder.txtPrice.setText(String.format("$%.2f", item.getPrice()));

        return row;

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }

    static class RecordHolder {
        TextView txtTitle;
        ImageView imageItem;
        TextView txtType;
        TextView txtExpiry;
        TextView txtPrice;
        TextView txtDetail;
        CheckBox checkBox;
        Boolean isChecked;
    }

}



