//package mobileapps.sumedia.gtdollarandroid.ui;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.content.res.TypedArray;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.FrameLayout;
//import android.widget.GridView;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import de.greenrobot.event.EventBus;
//import mobileapps.sumedia.gtdollarandroid.R;
//import mobileapps.sumedia.gtdollarandroid.adapter.SimpleItemListAdapter;
//import mobileapps.sumedia.gtdollarandroid.event.GetNearbyEvent;
//import mobileapps.sumedia.gtdollarandroid.model.SimpleIconTextItem;
//import mobileapps.sumedia.gtdollarandroid.widget.HeaderGridView;
//
//public class NearbyShopsFragment extends Fragment {
//
//    //NearbyItemAdapter adapter;
//
//    ArrayList<SimpleIconTextItem> lessFilterItems = new ArrayList<SimpleIconTextItem>();
//    ArrayList<SimpleIconTextItem> moreFilterItems = new ArrayList<SimpleIconTextItem>();
//    @InjectView(R.id.gv_circles)
//    HeaderGridView mGvCircles;
//
//    GridView gridHeader;
//    SimpleItemListAdapter headerAdapter;
//
//    boolean isHeaderExpanded = false;
//
//    public static NearbyShopsFragment newInstance() {
//        NearbyShopsFragment fragment = new NearbyShopsFragment();
//        Bundle args = new Bundle();
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//    public NearbyShopsFragment() {
//        // Required empty public constructor
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//
//        String[] moreOptions = getResources().getStringArray(R.array.merchant_filters_more);
//        String[] lessOptions = getResources().getStringArray(R.array.merchant_filters_less);
//        TypedArray moreIcons = getResources().obtainTypedArray(R.array.merchant_more_filter_res_id);
//        TypedArray lessIcons = getResources().obtainTypedArray(R.array.merchant_less_filter_res_id);
//
//        moreFilterItems.clear();
//        lessFilterItems.clear();
//
//        for (int i = 0; i < lessOptions.length; i++) {
//            lessFilterItems.add(new SimpleIconTextItem(getResources().getDrawable(lessIcons.getResourceId(i, R.drawable.home)), lessOptions[i]));
//        }
//        lessIcons.recycle();
//
//        for (int i = 0; i < moreOptions.length; i++) {
//            moreFilterItems.add(new SimpleIconTextItem(getResources().getDrawable(moreIcons.getResourceId(i, R.drawable.home)), moreOptions[i]));
//        }
//        lessIcons.recycle();
//        moreIcons.recycle();
//
//        headerAdapter = new SimpleItemListAdapter(getActivity().getApplicationContext(), R.layout.item_menu, R.id.item_image, R.id.item_text){
//            @Override
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View view =  super.getView(position, convertView, parent);
//                ((TextView) view.findViewById(R.id.item_text)).setMaxLines(1);
//                return view;
//            }
//        };
//        headerAdapter.setItems(isHeaderExpanded ? moreFilterItems:lessFilterItems);
//
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//
//        View rootView = inflater.inflate(R.layout.fragment_nearby_shops, container, false);
//        ButterKnife.inject(this, rootView);
//
//        setUpGridViewHeader();
//
//        mGvCircles.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_user_circles,R.id.tv_user_count,new String[]{"1","2","3","4","5","6","7"}));
//
//        //ListView listView = (ListView) rootView.findViewById(R.id.listView);
//        //LinearLayout menuContainer = (LinearLayout) rootView.findViewById(R.id.filterMenuContainer);
//        //setUpListView(listView);
//        //setUpMapIfNeeded();
//        //setUpFilterMenu(menuContainer);
//
//
//        return rootView;
//    }
//
//    private void setUpGridViewHeader() {
//        gridHeader = (GridView) LayoutInflater.from(getActivity()).inflate(R.layout.include_filter_header, null);
//
//        //set grid view item
//
//        gridHeader.setAdapter(headerAdapter);
//
//        gridHeader.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position,
//                                    long id) {
//
//                if (position == 0) {
//                    expandHeader();
//                    return;
//                }
//
//                Intent intent;
//                switch (position) {
//
//                }
//
//
//            }
//        });
//        mGvCircles.removeHeaderViews();
//        mGvCircles.addHeaderView(gridHeader, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//    }
//
//    private void expandHeader() {
//        headerAdapter.setItems(isHeaderExpanded ? lessFilterItems : moreFilterItems);
//        mGvCircles.notifyHeaderChanged();
//        isHeaderExpanded = !isHeaderExpanded;
//
//        //gridHeader.requestLayout();
//        //mGvCircles.removeHeaderView(gridHeader);
//        //mGvCircles.addHeaderView(gridHeader, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//
//    }
//
//
////    public void setUpListView(ListView listView1) {
////        final ArrayList<User> merchantList = new ArrayList<User>();
////        adapter = new NearbyItemAdapter(getActivity(), R.layout.nearby_shop_item_view, merchantList);
////        listView1.setAdapter(adapter);
////        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
////            @Override
////            public void onItemClick(AdapterView<?> parent, View view, int position,
////                                    long id) {
////
////                Toast.makeText(getActivity().getApplicationContext(), "Clicked", Toast.LENGTH_LONG).show();
////                Intent intent = new Intent(getActivity(), MerchantDetailActivity.class);
////                intent.putExtra(Const.INTENT_EXTRA_MERCHANT_KEY, merchantList.get(position));
////                startActivity(intent);
////            }
////        });
////        //searchRegisteredMerchants(merchantList,adapter);
////        JobManager.getInstance().listNearby(new SearchNearbyRequest("0,1,2"));
////    }
//
//
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//
//        EventBus.getDefault().register(this);
//
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        EventBus.getDefault().unregister(this);
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        //setUpMapIfNeeded();
//    }
//
////    private void setUpMapIfNeeded() {
////        // Do a null check to confirm that we have not already instantiated the map.
////        if (mMap == null) {
////            // Try to obtain the map from the SupportMapFragment.
////            MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
////            mMap =  mapFragment.getMap();
////            // Check if we were successful in obtaining the map.
////            if (mMap != null) {
////                setUpMap();
////            }
////        }
////    }
////
////    private void setUpMap() {
////        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
////    }
//
////    private void setUpFilterMenu(LinearLayout layout) {
////        String[] merchantTypes = new GTDHelper(getActivity().getApplicationContext()).merchantTypes();
////        for (int i = 0; i < merchantTypes.length; i++) {
////            ImageButton iv = new ImageButton(getActivity().getApplicationContext());
////            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(100, 100);
////            params.weight = 1.0f;
////            params.gravity = Gravity.CENTER_VERTICAL;
////            iv.setLayoutParams(params);
////            iv.setPadding(5, 5, 5, 5);
////            iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
////            iv.setBackgroundColor(0xffffff);
//////            iv.setText("Filter");
////            iv.setImageResource(new GTDHelper(getActivity().getApplicationContext()).merchantTypeResources(i));
////            layout.addView(iv);
////        }
////    }
//
//
//    public void onEventMainThread(GetNearbyEvent event) {
////        Utils.toggleEventPopup((BaseActivity) getActivity(), event, new Utils.StatusListener<GetNearbyEvent>() {
////            @Override
////            public void onSucceed(GetNearbyEvent event) {
////                adapter.addAll(event.getResponse().getUsers());
////            }
////        });
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        ButterKnife.reset(this);
//    }
//}
