package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.model.SimpleIconTextItem;
import mobileapps.sumedia.gtdollarandroid.widget.AspectRatioImageView;


public class CategoryListAdapter extends PagingBaseAdapter<SimpleIconTextItem> {

    public CategoryListAdapter(Context context) {
        super(context);

    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    protected int getViewResourceId(int position) {
        return R.layout.item_category;
    }

    @Override
    protected Renderer<SimpleIconTextItem> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }

    class ViewHolder implements Renderer<SimpleIconTextItem> {
        @InjectView(R.id.iv_image)
        AspectRatioImageView ivImage;
        @InjectView(R.id.tv_title)
        TextView tvText;
        View view;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
            this.view = view;
        }

        public void render(SimpleIconTextItem item, int position) {
            String name = item.getTitle();
            tvText.setCompoundDrawablesWithIntrinsicBounds(null, item.getIcon(), null, null);
            tvText.setText(name);

        }
    }


}