package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 11/3/14.
 */
public class ChooseContactEvent {
    String email;

    public ChooseContactEvent(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
