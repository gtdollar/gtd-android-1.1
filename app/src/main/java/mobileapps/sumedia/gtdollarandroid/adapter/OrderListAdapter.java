package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.helper.Formatter;
import mobileapps.sumedia.gtdollarandroid.model.Service;


/**
 * Created by zhuodong on 6/24/14.
 */
public class OrderListAdapter extends PagingBaseAdapter<Service> {

    String currency;
    boolean isDisabled;

    public OrderListAdapter(Context context, String currency, TotalPriceChangedListener listener) {
        super(context);
        this.listener = listener;
        this.currency = currency;
    }

    @Override
    protected int getViewResourceId(int position) {
        return R.layout.item_order;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId().hashCode();
    }

    @Override
    public Renderer<Service> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }


    public class ViewHolder implements Renderer<Service> {

        Service item;

        @InjectView(R.id.tv_delete)
        TextView mTvDelete;
        @InjectView(R.id.tv_plus)
        TextView mTvPlus;
        @InjectView(R.id.tv_count)
        TextView mTvCount;
        @InjectView(R.id.tv_minus)
        TextView mTvMinus;
        @InjectView(R.id.tv_single_price)
        TextView mTvSinglePrice;
        @InjectView(R.id.tv_total_price)
        TextView mTvTotalPrice;
        @InjectView(R.id.tv_item_name)
        TextView mTvItemName;

        @InjectView(R.id.iv_item_photo)
        ImageView mIvItemPhoto;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);

        }

        @OnClick(R.id.tv_minus)
        void onClickReduce(View view) {
            int quantity = item.getQuantity();
            if (quantity > 1) {
                quantity--;
                item.setQuantity(quantity);
                mTvCount.setText(quantity + "");
                mTvTotalPrice.setText(Formatter.formatPrice(currency, item.getPrice() * item.getQuantity()));
                if (listener != null) listener.calculateTotalPrice();
            }
        }

        @OnClick(R.id.tv_delete)
        void onClickDelete(View view) {
            OrderListAdapter.this.remove(item);
            if (listener != null) listener.calculateTotalPrice();
        }

        @OnClick(R.id.tv_plus)
        void onClickAdd(View view) {
            int quantity = item.getQuantity();
            quantity++;
            item.setQuantity(quantity);
            mTvCount.setText(quantity + "");
            mTvTotalPrice.setText(Formatter.formatPrice(currency, item.getPrice() * item.getQuantity()));
            if (listener != null) listener.calculateTotalPrice();
        }


        @Override
        public void render(Service item, int position) {
            this.item = item;
            mTvSinglePrice.setText(Formatter.formatPrice(currency, item.getPrice()));

            mTvCount.setText(item.getQuantity() + "");

            mTvItemName.setText(item.getTitle());

            mTvTotalPrice.setText(Formatter.formatPrice(currency, item.getPrice() * item.getQuantity()));

            if (!TextUtils.isEmpty(item.getPhoto())) {
                Picasso.with(getContext()).load(item.getPhoto()).into(mIvItemPhoto);
            }

            if (isDisabled) {
                mTvDelete.setVisibility(View.GONE);
                mTvPlus.setVisibility(View.GONE);
                mTvMinus.setVisibility(View.GONE);
            }
        }
    }

    TotalPriceChangedListener listener;

    public void setListener(TotalPriceChangedListener listener) {
        this.listener = listener;
    }

    public interface TotalPriceChangedListener {
        public void calculateTotalPrice();
    }

    public void setDisabled(boolean isDisabled) {
        this.isDisabled = isDisabled;
        notifyDataSetChanged();
    }


}