package mobileapps.sumedia.gtdollarandroid.ui;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.SimpleBackgroundTask;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.widget.AspectRatioImageView;

public class QRCodeActivity extends BaseActivity {

    @InjectView(R.id.iv_qr_code)
    AspectRatioImageView mIvQrCode;

    String url;
    private int size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        ButterKnife.inject(this);
        getActionBar().setDisplayShowTitleEnabled(false);
        url = getIntent().getStringExtra(Const.INTENT_EXTRA_URL);
        size = getResources().getDimensionPixelSize(R.dimen.qr_code_size);
        if(TextUtils.isEmpty(url)) {
            finish();
            return;
        }

        new SimpleBackgroundTask<Bitmap>(this) {


            @Override
            protected Bitmap onRun() {
                Writer writer = new QRCodeWriter();
                String finaldata = Uri.encode(url, "ISO-8859-1");
                Bitmap mBitmap = null;
                try {
                    BitMatrix bm = writer.encode(finaldata, BarcodeFormat.QR_CODE, size, size);
                    mBitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
                    for (int i = 0; i < size; i++) {
                        for (int j = 0; j < size; j++) {
                            mBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK : Color.WHITE);
                        }
                    }
                } catch (WriterException e) {
                    e.printStackTrace();
                }

                return mBitmap;
            }

            @Override
            protected void onSuccess(Bitmap result) {
                if (result != null) {
                    mIvQrCode.setImageBitmap(result);
                }
            }
        }.execute();
    }

}
