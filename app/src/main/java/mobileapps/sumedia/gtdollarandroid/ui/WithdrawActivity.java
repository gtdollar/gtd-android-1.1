package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.CheckAccountEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountCheckResponse;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

public class WithdrawActivity extends BaseActivity {

    double totalBalance;
    @InjectView(R.id.et_balance)
    EditText mEtBalance;
    @InjectView(R.id.et_available_amount)
    EditText mEtAvailableAmount;
    @InjectView(R.id.et_amount)
    EditText mEtAmount;
    @InjectView(R.id.btn_next)
    Button mBtnNext;
    private Form mForm;
    double balance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);
        ButterKnife.inject(this);
        setTitle(R.string.title_activity_withdraw);
        balance = getIntent().getDoubleExtra(Const.INTENT_EXTRA_BALANCE,0);
        mEtBalance.setText(balance+"");
        mEtAvailableAmount.setText(balance+"");

        mEtAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    onClickNext(mBtnNext);
                    return true;
                }
                return false;
            }
        });
        buildForm();

        if (UserDataHelper.isVerified()) {
            JobManager.getInstance().checkAccount();
        }
    }

    private void buildForm() {
        mForm = new Form(this);
        mForm.addField(Field.using(mEtAmount).validate(NotEmpty.build(this)));
    }
    @OnClick(R.id.btn_next)
    public void onClickNext(View view) {
        if(!mForm.isValid())return;
        if(Double.parseDouble(mEtAmount.getText().toString()) > balance) {
            Utils.toast(this,getString(R.string.toast_balance_not_enough));
            return;
        }
        Intent intent = new Intent(this, BankDetailActivity.class);
        startActivity(intent);
    }

    public void onEventMainThread(CheckAccountEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED && event.getResponse() != null) {
            AccountCheckResponse.Data data = event.getResponse().getData();
            balance = data.getBalance();
            String formattedBalance = GTDHelper.formatBalance(balance);
            mEtBalance.setText(formattedBalance);
            mEtAvailableAmount.setText(formattedBalance);
        }
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }
}
