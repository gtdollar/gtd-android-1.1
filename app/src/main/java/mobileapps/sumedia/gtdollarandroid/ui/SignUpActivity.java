package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.ChoseItemEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.SignInEvent;
import mobileapps.sumedia.gtdollarandroid.event.UserSignUpEvent;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.validations.IsEmail;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;
import ua.org.zasadnyy.zvalidations.validations.NotMatchPassword;

public class SignUpActivity extends BaseActivity {

    @InjectView(R.id.et_email)
    EditText mEtEmail;
    @InjectView(R.id.et_pwd)
    EditText mEtPwd;
    @InjectView(R.id.et_currency)
    TextView mEtCurrency;
    @InjectView(R.id.et_type)
    TextView mTvType;
    @InjectView(R.id.btn_sign_up)
    Button mBtnSignUp;
    @InjectView(R.id.btn_merchant)
    Switch mBtnMerchant;
    @InjectView(R.id.tv_t_n_c)
    TextView mTvTNC;
    @InjectView(R.id.et_confirm_pwd)
    EditText mEtConfirmPwd;
    private Form mForm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.inject(this);

        mTvTNC.setText(Html.fromHtml(getString(R.string.string_t_nc)));
        mTvTNC.setMovementMethod(LinkMovementMethod.getInstance());


        mEtCurrency.setTag(GenericSelectionDialog.TYPE_CURRENCY);
        mTvType.setTag(GenericSelectionDialog.TYPE_MERCHANT_TYPE);

        mBtnMerchant.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mTvType.setVisibility(isChecked ? View.VISIBLE : View.GONE);

            }
        });

        buildForm();

    }

    private void buildForm() {
        mForm = new Form(this);
        mForm.addField(Field.using(mEtEmail).validate(NotEmpty.build(this)).validate(
            IsEmail.build(this)));
        mForm.addField(Field.using(mEtPwd).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mEtCurrency).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mEtConfirmPwd).validate(NotMatchPassword.build(this, mEtPwd)));

    }


    @OnClick({R.id.et_currency, R.id.et_type})
    public void selectCurrency(View view) {
        GenericSelectionDialog.newInstance(((Integer) view.getTag()), ((TextView) view).getText()).show(getSupportFragmentManager(), "");
    }

    public void onEventMainThread(ChoseItemEvent event) {
        switch (event.getType()) {
            case GenericSelectionDialog.TYPE_CURRENCY:
                mEtCurrency.setText(event.getItem());
                break;
            case GenericSelectionDialog.TYPE_MERCHANT_TYPE:
                mTvType.setText(event.getItem());
                break;
        }
    }

    @OnClick(R.id.btn_sign_up)
    public void attemptSignUp(View view) {
        if (mForm.isValid()) {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("email", mEtEmail.getText().toString());
            params.put("password", mEtPwd.getText().toString());
            params.put("currency", mEtCurrency.getText().toString());
            if (mBtnMerchant.isChecked()) {
                params.put("categories", GTDHelper.findType(mTvType.getText().toString()) + "");
                params.put("type", "2");
            }
//            if () {
//                JobManager.getInstance().merchantSignUp(params);
//            } else {
//                JobManager.getInstance().userSignUp(params);
//            }
            JobManager.getInstance().createUser(mBtnMerchant.isChecked(), params);

//            HashMap<String, String> params1 = new HashMap<String, String>();
//            params.put("username", mEtPrice.getText().toString());
//            params.put("password", mEtWebsite.getText().toString());
//            params.put("rtype", 1 + "");
//            JobManager.getInstance().accountSignUp(params1);
        }
    }

    boolean isUserSignedUp, isAccountSignedUp = true;

    public void onEventMainThread(UserSignUpEvent event) {
        Utils.toggleEventPopup(this, event, new Utils.StatusListener<UserSignUpEvent>() {
            @Override
            public void onSucceed(UserSignUpEvent event) {

                EventBus.getDefault().post(new SignInEvent(EventStatus.SUCCEED));
                finish();

//                finish();
//                Utils.toast(SignUpActivity.this, getString(R.string.sign_up_successfully));
            }
        });

    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    @Override
    public boolean hasActionBar() {
        return false;
    }
}
