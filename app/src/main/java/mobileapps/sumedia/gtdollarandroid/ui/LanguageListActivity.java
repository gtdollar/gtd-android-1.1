package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.LanguageChangedEvent;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;

public class LanguageListActivity extends BaseActivity {


    @InjectView(R.id.lv_lang)
    ListView mLvLang;

    String[] langIds;

    int initSelection,currentSelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_list);
        ButterKnife.inject(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        langIds = getResources().getStringArray(R.array.lang_id_array);
        final String currentLangId = UserDataHelper.getLanguage();

        mLvLang.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);


        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item_language, android.R.id.text1, getResources().getStringArray(R.array.lang_array)) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view =  super.getView(position, convertView, parent);
                //Log.d(""," testing position "+position+" isChecked "+mLvLang.isItemChecked(position));
                ((TextView) view).setCompoundDrawablesWithIntrinsicBounds(0,0,position == currentSelection?R.drawable.ic_confirm:0,0);

                return view;
            }
        };

        for (int i = 0; i < langIds.length; i++) {
            if (langIds[i].equals(currentLangId)) {
                initSelection = currentSelection = i;
                break;
            }
        }


        mLvLang.setAdapter(adapter);

        mLvLang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                currentSelection = i;
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            checkLangChang();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        checkLangChang();
        super.onBackPressed();
    }

    private void checkLangChang() {
        if(initSelection != currentSelection) {
            UserDataHelper.setLanguage(langIds[currentSelection]);
            EventBus.getDefault().post(new LanguageChangedEvent());
        }
    }
}
