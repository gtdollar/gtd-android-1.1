package mobileapps.sumedia.gtdollarandroid.model;

import android.graphics.drawable.Drawable;

/**
 * 
 * @author manish.s
 *
 */

public class SimpleIconTextItem {
	Drawable icon;
	String title;

    public SimpleIconTextItem(Drawable icon, String title) {
        this.icon = icon;
        this.title = title;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }

    String extra;

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public SimpleIconTextItem(Drawable icon, String title, String extra) {
        this.icon = icon;
        this.title = title;
        this.extra = extra;
    }
}
