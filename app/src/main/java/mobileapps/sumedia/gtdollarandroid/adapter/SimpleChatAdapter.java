//package mobileapps.sumedia.gtdollarandroid.adapter;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import mobileapps.sumedia.gtdollarandroid.Message;
//import mobileapps.sumedia.gtdollarandroid.R;
//import mobileapps.sumedia.gtdollarandroid.widget.RoundedImageView;
//
///**
// * Created by 2359media on 17/7/14.
// */
//public class SimpleChatAdapter extends PagingBaseAdapter<Message> {
//    Context context;
//    LayoutInflater inflater;
//
//
//    public SimpleChatAdapter(Context ctx){
//        super();
//        this.context = ctx;
//        inflater = LayoutInflater.from(context);
//
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return getItem(i).getId();
//    }
//
//    @Override
//    public boolean hasStableIds() {
//        return true;
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        int viewType = getItem(position).getDirection();
//        //Log.d(""," viewType "+viewType);
//        return viewType;
//    }
//
//    @Override
//    public int getViewTypeCount() {
//        return 2;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHolder holder;
//        // When convertView is not null, we can reuse it directly, there is no need
//        // to reinflate it. We only inflate a new View when the convertView supplied
//
//        if (convertView == null) {
//            convertView = inflater.inflate(getItemViewType(position) == Message.DIRECTION_FROM ? R.layout.item_chat_text_outgoing : R.layout.item_chat_text_incoming, parent, false);
//            holder = new ViewHolder(convertView);
//            convertView.setTag(holder);
//
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//
//        Message item = getItem(position);
//
//        if (item != null) {
//            holder.render(item);
//        }
//        return convertView;
//    }
//
////    public void openOptionsMenu(String text)
////    {
////        CharSequence [] c = {"Copy","Delete","Forward"};
////        AlertDialog.Builder builder = new AlertDialog.Builder(this.listener);
////        builder.setTitle("Options")
////                .setItems(c, new DialogInterface.OnClickListener() {
////                    public void onClick(DialogInterface dialog, int which) {
////                        // The 'which' argument contains the index position
////                        // of the selected item
////                    }
////                });
////        builder.create().show();
////    }
//
//    class ViewHolder {
//        @InjectView(R.id.iv_thumb)
//        RoundedImageView mIvThumbe;
//        @InjectView(R.id.tv_chat_content)
//        TextView mTvChatContent;
//        @InjectView(R.id.pb_sending)
//        ProgressBar mPbSending;
//
//        ViewHolder(View view) {
//            ButterKnife.inject(this, view);
//
////            mTvChatContent.setOnLongClickListener(new View.OnLongClickListener() {
////                @Override
////                public boolean onLongClick(View v) {
////                    openOptionsMenu(item);
////                    return true;
////                }
////            });
//        }
//
//        public void render(Message item) {
//            mTvChatContent.setText(item.getContent());
//            if(item.getMessage_status() == Message.STATUS_SENDING) {
//                mPbSending.setVisibility(View.VISIBLE);
//            }else {
//                mPbSending.setVisibility(View.GONE);
//            }
//        }
//    }
//}
