/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mobileapps.sumedia.gtdollarandroid.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.lang.ref.WeakReference;

import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.asynctask.GetAddressTask;
import mobileapps.sumedia.gtdollarandroid.event.GetAddressEvent;
import mobileapps.sumedia.gtdollarandroid.event.SendLocationEvent;
import mobileapps.sumedia.gtdollarandroid.location.location.LocationActivity;

import static mobileapps.sumedia.gtdollarandroid.helper.Const.ZOOM_LEVEL;

public class MyLocationMapActivity extends LocationActivity
    implements
    OnMyLocationButtonClickListener {
    String address;
    private GoogleMap mMap;
    private TextView mMessageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_location);
        mMessageView = (TextView) findViewById(R.id.message_text);
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
                mMap.setOnMyLocationButtonClickListener(this);
            }

            final View mapView = getSupportFragmentManager().findFragmentById(R.id.map).getView();
            if (mapView.getViewTreeObserver().isAlive()) {
                mapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @SuppressWarnings("deprecation") // We use the new method when supported
                    @SuppressLint("NewApi") // We check which build version we are using.
                    @Override
                    public void onGlobalLayout() {
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            mapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        } else {
                            mapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                        if (currentLocation != null) {
                            LatLng coordinate = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, ZOOM_LEVEL);
                            mMap.animateCamera(yourLocation);
                        }
                    }
                });
            }
        }
    }


    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    public void onEventMainThread(GetAddressEvent event) {
        Log.d("", " testing address "+event.getAddress());
        if (mMessageView != null) {
            mMessageView.setText(event.getAddress());
        }
    }

    /**
     * Implementation of {@link com.google.android.gms.location.LocationListener}.
     */
    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);

        if (mMap != null) {
            LatLng coordinate = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, ZOOM_LEVEL);
            mMap.animateCamera(yourLocation);
        }

        new GetAddressTask(new WeakReference<Context>(this)).execute(location);
        //mMessageView.setText("Location = " + latLng);
    }


    @Override
    public boolean onMyLocationButtonClick() {
        //Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my_location, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            return true;
        } else if (id == R.id.action_send) {
            if (currentLocation != null && !TextUtils.isEmpty(address)) {
                EventBus.getDefault().postSticky(new SendLocationEvent(currentLocation.getLatitude(), currentLocation.getLongitude(), address));
                this.finish();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public boolean isUpdatesRequested() {
        return true;
    }
}
