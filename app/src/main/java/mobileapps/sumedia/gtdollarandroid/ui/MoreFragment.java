package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.SettingsListAdapter;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.SimpleIconTextItem;
import mobileapps.sumedia.gtdollarandroid.model.User;
import mobileapps.sumedia.gtdollarandroid.widget.CircleImageView;


public class MoreFragment extends Fragment {

    @InjectView(R.id.iv_profile_image)
    CircleImageView ivProfileImage;
    @InjectView(R.id.tv_name)
    TextView tvName;
    @InjectView(R.id.ll_profile_holder)
    LinearLayout llProfileHolder;
    @InjectView(R.id.lv_settings)
    ListView lvSettings;

    public static MoreFragment newInstance() {
        MoreFragment fragment = new MoreFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public MoreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_more, container, false);
        ButterKnife.inject(this, view);
        setupView();
        return view;
    }

    public void setupView() {
        User user = UserDataHelper.getUser();
        if (user != null) {
            if (!TextUtils.isEmpty(user.getPicture())) {
                Picasso.with(getActivity()).load(user.getPicture()).fit().centerCrop().into(ivProfileImage);
            }
            tvName.setText(user.getName());
        }


        ArrayList<SimpleIconTextItem> gridArray = new ArrayList<SimpleIconTextItem>();
        String[] options = getResources().getStringArray(R.array.menu_options);
        String[] classNames = getResources().getStringArray(R.array.menu_class_names);
        TypedArray icons = getResources().obtainTypedArray(R.array.menu_res_ids);

        for (int i = 0; i < options.length; i++) {
            int resId = icons.getResourceId(i, R.drawable.ic_menu_login);
            gridArray.add(new SimpleIconTextItem(getResources().getDrawable(resId), options[i], classNames[i]));
        }
        icons.recycle();

        SettingsListAdapter settingsListAdapter = new SettingsListAdapter(getActivity().getApplicationContext());

        settingsListAdapter.setItems(gridArray);
        lvSettings.setAdapter(settingsListAdapter);
        lvSettings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {


                Intent intent = new Intent();
                intent.setClassName(getActivity(), getActivity().getPackageName() + ".ui." + ((SimpleIconTextItem) parent.getItemAtPosition(position)).getExtra());
                startActivity(intent);
            }

        });
    }


    @OnClick(R.id.ll_profile_holder)
    public void editProfile() {
        if (!UserDataHelper.isLoggedIn()) return;
        Utils.viewMerchant(getActivity(), UserDataHelper.getUser(), UserDataHelper.getUser().getId());

    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
