package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;

/**
 * Created by zhuodong on 8/15/14.
 */
public class UploadFileResponse {

    @Expose
    private boolean success;
    @Expose
    private String message;
    @Expose
    private File file;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public static class File {

        @Expose
        private String name;
        @Expose
        private String path;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

    }

}
