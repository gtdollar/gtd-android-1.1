package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 2/3/14.
 */
public enum EventStatus {
    ONGOING,
    SUCCEED,
    FAILED;
}
