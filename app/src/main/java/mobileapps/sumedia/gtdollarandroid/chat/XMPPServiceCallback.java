package mobileapps.sumedia.gtdollarandroid.chat;

public interface XMPPServiceCallback {
	void newMessage(String from, String fromUserName, String userAvatar, String messageType, String messageBody, boolean silent_notification);
	void messageError(String from, String fromUserName, String messageType, String errorBody, boolean silent_notification);
	void connectionStateChanged();
}
