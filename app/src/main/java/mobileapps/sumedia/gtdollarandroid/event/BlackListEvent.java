package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.BlackBlockListResponse;

/**
 * Created by zhuodong on 10/20/14.
 */
public class BlackListEvent extends BaseEvent<BlackBlockListResponse> {

    public BlackListEvent(EventStatus status, BlackBlockListResponse response) {
        super(status, response);
    }
}
