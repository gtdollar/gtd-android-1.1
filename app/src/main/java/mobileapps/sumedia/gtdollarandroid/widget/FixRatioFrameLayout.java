package mobileapps.sumedia.gtdollarandroid.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import mobileapps.sumedia.gtdollarandroid.R;


public class FixRatioFrameLayout extends FrameLayout {
		
	private float ratio;
	private boolean isHeightBased = false;
	private boolean isChecked = false;
	
	public FixRatioFrameLayout(Context context) {
		this(context,null,0);
		init();
	}

    public FixRatioFrameLayout(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }


	public FixRatioFrameLayout(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.FixRatioFrameLayout);
		ratio = ta.getFloat(R.styleable.FixRatioFrameLayout_aspectRatio, 1f);
		ta.recycle();
		init();
	}

	private void init() {
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = MeasureSpec.getSize(widthMeasureSpec);
		int height = MeasureSpec.getSize(heightMeasureSpec);

		if(isHeightBased) {
			width = (int)(height*ratio);
			super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),heightMeasureSpec);
		}
		else {
			height = (int)(width/ratio);
			super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
		}
	}

	@Override
	public boolean isInEditMode() {
		// TODO Auto-generated method stub
		return super.isInEditMode();
	}


	public float getRatio() {
		return ratio;
	}


	public void setRatio(float ratio) {
		this.ratio = ratio;
	}
	
	public void setRatio(float ratio, boolean isHeightBased) {
		this.isHeightBased = isHeightBased;
		setRatio(ratio);
	}
	

	
}
