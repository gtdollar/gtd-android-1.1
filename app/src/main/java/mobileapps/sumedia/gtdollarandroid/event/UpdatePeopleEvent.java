package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.CircleResponse;

/**
 * Created by zhuodong on 10/20/14.
 */
public class UpdatePeopleEvent extends BaseEvent<CircleResponse> {
    public UpdatePeopleEvent(EventStatus eventStatus) {
        super(eventStatus);
    }

    public UpdatePeopleEvent(EventStatus status, CircleResponse response) {
        super(status, response);
    }
}
