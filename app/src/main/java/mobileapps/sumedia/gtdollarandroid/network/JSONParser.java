package mobileapps.sumedia.gtdollarandroid.network;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;

/**
 * Created by zhuodong on 4/20/14.
 */
public class JSONParser {
    static Gson gson = new Gson();
    static JsonParser parser = new JsonParser();

    public static <T> T parseFromFile(Class<T> cls, String filePath)
            throws JsonSyntaxException, FileNotFoundException {
        return gson.fromJson(new BufferedReader(new FileReader(filePath)), cls);
    }

    public static <T> T parseFromString(Class<T> cls, String jsonString)
         {
        return gson.fromJson(jsonString, cls);
    }

    public static <T> T parseFromString(Type type, String jsonString)
    {
        return gson.fromJson(jsonString, type);
    }

    public static <T> T parseFromInputStream(Class<T> cls, InputStream inputStream)
            throws JsonSyntaxException, FileNotFoundException {
        return gson.fromJson(new BufferedReader(new InputStreamReader(inputStream)), cls);
    }

    public static String getFormatJson(Object src) throws JsonSyntaxException{
        return gson.toJson(src);
    }
}
