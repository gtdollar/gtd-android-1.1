package mobileapps.sumedia.gtdollarandroid.helper;

import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mobileapps.sumedia.gtdollarandroid.GTDApplication;
import mobileapps.sumedia.gtdollarandroid.model.Account;
import mobileapps.sumedia.gtdollarandroid.model.Service;
import mobileapps.sumedia.gtdollarandroid.model.User;
import mobileapps.sumedia.gtdollarandroid.network.JSONParser;

/**
 * Created by 2359media on 21/7/14.
 */
public class UserDataHelper {

    public static final String PREFS_NAME = "myPrefsFile";
    public static final String PREF_KEY_USER_NAME = "userName";
    public static final String PREF_KEY_TOKEN = "token";
    public static final String PREF_KEY_USER_ID = "userId";
    public static final String PREF_KEY_USER_BALANCE = "balance";
    public static final String PREF_KEY_USER_CREDIT = "credit";
    public static final String PREF_KEY_USER = "user";
    public static final String PREF_KEY_IS_MERCHANT = "isMerchant";

    public static final String PREF_KEY_ACCOUNT_TOKEN = "account_token";
    public static final String PREF_KEY_ACCOUNT = "account";
    private static final String PREF_KEY_PASSWORD = "pwd";
    private static final String PREF_KEY_EMAIL = "email";
    private static final String PREF_KEY_PENDING_ORDERS = "pending_orders";

    private static final String PREF_KEY_LANG = "language";
    private static final String PREF_KEY_IS_LOGIN = "is_login";

    static SharedPreferences sharedPreferences = GTDApplication.getInstance().getSharedPreferences(PREFS_NAME, 0);

    static User user;
    static Account account;

    static List<Service> pendingOrders;

    private UserDataHelper() {
    }

    public static void saveUser(User user) {
        sharedPreferences.edit().putString(PREF_KEY_USER, JSONParser.getFormatJson(user)).apply();
        UserDataHelper.user = user;
    }

    public static User getUser() {
        if (user != null) return user;
        String userStr = sharedPreferences.getString(PREF_KEY_USER, "");
        if (TextUtils.isEmpty(userStr)) return null;
        return JSONParser.parseFromString(User.class, userStr);
    }

    public static String getUserId() {
        return getUser() == null?null:getUser().getId();
    }

    public static void setToken(final String token) {
        sharedPreferences.edit().putString(PREF_KEY_TOKEN, token).commit();
    }

    public static String getToken() {
        return sharedPreferences.getString(PREF_KEY_TOKEN, "");
    }


    public static void setAccountToken(final String token) {
        Log.d(""," testing save token "+token);
        sharedPreferences.edit().putString(PREF_KEY_ACCOUNT_TOKEN, token).commit();
    }

    public static String getAccountToken() {
        return sharedPreferences.getString(PREF_KEY_ACCOUNT_TOKEN, "");
    }

    public static void saveAccount(Account account) {
        sharedPreferences.edit().putString(PREF_KEY_ACCOUNT, JSONParser.getFormatJson(account)).commit();
        UserDataHelper.account = account;
    }

    public static Account getAccount() {
        if (account != null) return account;
        String accountStr = sharedPreferences.getString(PREF_KEY_ACCOUNT, "");
        if (TextUtils.isEmpty(accountStr)) return null;
        return JSONParser.parseFromString(Account.class, accountStr);
    }


    public static void setBalance(final float balance) {
        sharedPreferences.edit().putFloat(PREF_KEY_USER_BALANCE, balance).commit();
    }

    public static float getBalance() {
        return sharedPreferences.getFloat(PREF_KEY_USER_BALANCE, 1000);
    }

    public static void setCredit(final float credit) {
        sharedPreferences.edit().putFloat(PREF_KEY_USER_CREDIT, credit).commit();
    }

    public static float getCredit() {
        return sharedPreferences.getFloat(PREF_KEY_USER_CREDIT, 1000);
    }

    public static boolean isLoggedIn() {
        return !TextUtils.isEmpty(getToken()) && getUser() != null; //|| !TextUtils.isEmpty(getAccountToken()) && getAccount() != null;
    }

    public static boolean isVerified() {
        return (!TextUtils.isEmpty(getAccountToken())) && (getAccount() != null);
    }

    public static boolean isLoggingIn() {
        return sharedPreferences.getBoolean(PREF_KEY_IS_LOGIN, false);
    }

    public static void setLoggingIn(boolean isLoggIn) {
        sharedPreferences.edit().putBoolean(PREF_KEY_IS_LOGIN, isLoggIn).commit();
    }


    public static void clear() {
        sharedPreferences.edit().clear().commit();
    }

    public static void setMerchant(boolean merchant) {
        sharedPreferences.edit().putBoolean(PREF_KEY_IS_MERCHANT, merchant).commit();
    }

    public static boolean isMerchant() {
        return sharedPreferences.getBoolean(PREF_KEY_IS_MERCHANT, false);
    }

    public static String getPwd() {
        return sharedPreferences.getString(PREF_KEY_PASSWORD, "");
    }

    public static void savePwd(String pwd) {
        sharedPreferences.edit().putString(PREF_KEY_PASSWORD, pwd).commit();
    }

    public static String getEmail() {
        return sharedPreferences.getString(PREF_KEY_EMAIL, "");
    }

    public static void saveEmail(String email) {
        sharedPreferences.edit().putString(PREF_KEY_EMAIL, email).commit();
    }

    public static void addPendingOrder(Service service) {
        if (pendingOrders == null)
            pendingOrders = new ArrayList<Service>();
        pendingOrders.add(service);
        sharedPreferences.edit().putString(PREF_KEY_PENDING_ORDERS, JSONParser.getFormatJson(pendingOrders)).apply();
    }

    public static void setPendingOrders(List<Service> items) {
        pendingOrders = new ArrayList<Service>(items);
        sharedPreferences.edit().putString(PREF_KEY_PENDING_ORDERS, JSONParser.getFormatJson(pendingOrders)).apply();
    }

    public static List<Service> getPendingOrders() {
        if (pendingOrders != null) return pendingOrders;
        String string = sharedPreferences.getString(PREF_KEY_PENDING_ORDERS, "");
        if (TextUtils.isEmpty(string)) return null;
        return JSONParser.parseFromString(new TypeToken<List<Service>>() {
        }.getType(), string);
    }

    public static void removePendingOrder() {
        if (pendingOrders != null) {
            pendingOrders.clear();
            pendingOrders = null;
        }
        sharedPreferences.edit().remove(PREF_KEY_PENDING_ORDERS).apply();
    }

    public static void setLanguage(String lang) {
        sharedPreferences.edit().putString(PREF_KEY_LANG, lang).commit();
    }

    public static String getLanguage() {

        if (!sharedPreferences.contains(PREF_KEY_LANG)) {
            Locale defaultLocale = Locale.getDefault();
            if (defaultLocale == Locale.SIMPLIFIED_CHINESE) {
                return "zh_CN";
            } else if (defaultLocale == Locale.TRADITIONAL_CHINESE) {
                return "zh_TW";
            }
            return defaultLocale.getLanguage();
        }
        return sharedPreferences.getString(PREF_KEY_LANG, Locale.getDefault().getLanguage());
    }

}
