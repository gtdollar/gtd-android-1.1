package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.ApplyCreditEvent;
import mobileapps.sumedia.gtdollarandroid.event.SendCreditEvent;
import mobileapps.sumedia.gtdollarandroid.event.TransferBalanceEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;

public class ReviewActivity extends BaseActivity {

    ArrayList<String> options;
    int reviewType;
    @InjectView(R.id.tv_amount)
    TextView mTvAmount;
    @InjectView(R.id.tv_product_discount)
    TextView mTvProductDiscount;
    @InjectView(R.id.tv_description)
    TextView mTvDescription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        ButterKnife.inject(this);
        options = getIntent().getStringArrayListExtra(Const.INTENT_EXTRA_REVIEW_OPTIONS);
        reviewType = getIntent().getIntExtra(Const.INTENT_EXTRA_REVIEW_TYPE,Const.REVIEW_APPLY_CREDIT);
        getActionBar().setDisplayShowTitleEnabled(false);
        switch (reviewType) {
            case Const.REVIEW_APPLY_CREDIT:
                mTvAmount.setText(getString(R.string.str_amount_format,options.get(0)));
                mTvProductDiscount.setText(getString(R.string.str_product_discount_format,options.get(1)));
                mTvDescription.setText(getString(R.string.str_description_format,options.get(2)));
                break;
            case Const.REVIEW_SEND_CREDIT:
                mTvAmount.setText(getString(R.string.str_send_credit_to_format,options.get(0)));
                mTvProductDiscount.setText(getString(R.string.str_amount_format,options.get(1)));
                mTvDescription.setText(getString(R.string.str_description_format,options.get(2)));
                break;
            case Const.REVIEW_TRANSFER_BALANCE:
                mTvAmount.setText(getString(R.string.str_transfer_balance_to_format,options.get(0)));
                mTvProductDiscount.setText(getString(R.string.str_amount_format,options.get(1)));
                mTvDescription.setText(getString(R.string.str_description_format,options.get(2)));
                break;
        }
//        mTvAmount.setText(options.get(0));
//        mTvProductDiscount.setText(options.get(1));
//        mTvDescription.setText(options.get(2));
    }

    @OnClick({R.id.btn_confirm})
    public void onClickConfirm(View view) {
        switch (reviewType) {
            case Const.REVIEW_APPLY_CREDIT:
                JobManager.getInstance().accountApplyCredit(options.get(0), options.get(1), options.get(2));
                break;
            case Const.REVIEW_SEND_CREDIT:
                JobManager.getInstance().accountSendCredit(options.get(0), options.get(1), options.get(2));
                break;
            case Const.REVIEW_TRANSFER_BALANCE:
                JobManager.getInstance().accountTransferBalance(options.get(0), options.get(1), options.get(2));
                break;
        }
    }

    public void onEventMainThread(ApplyCreditEvent event) {
        Utils.toggleEventPopup(this,event,new Utils.StatusListener<ApplyCreditEvent>() {
            @Override
            public void onSucceed(ApplyCreditEvent event) {
                Utils.toast(ReviewActivity.this,getString(R.string.toast_apply_credit));
                Utils.returnToWallet(ReviewActivity.this);
            }
        });
    }

    public void onEventMainThread(SendCreditEvent event) {
        Utils.toggleEventPopup(this,event,new Utils.StatusListener<SendCreditEvent>() {
            @Override
            public void onSucceed(SendCreditEvent event) {
                Utils.toast(ReviewActivity.this,getString(R.string.toast_send_credit));
                Utils.returnToWallet(ReviewActivity.this);
            }
        });
    }

    public void onEventMainThread(TransferBalanceEvent event) {
        Utils.toggleEventPopup(this,event,new Utils.StatusListener<TransferBalanceEvent>() {
            @Override
            public void onSucceed(TransferBalanceEvent event) {
                Utils.toast(ReviewActivity.this,getString(R.string.toast_transfer_balance));
                Utils.returnToWallet(ReviewActivity.this);
            }
        });
    }


    @Override
    public boolean isSubscribedEvent() {
        return true;
    }
}
