package mobileapps.sumedia.gtdollarandroid.chat;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import mobileapps.sumedia.gtdollarandroid.GTDApplication;

public class ChatConfig implements OnSharedPreferenceChangeListener {

	private static final String TAG = "Chat.Configuration";

//	private static final HashSet<String> RECONNECT_PREFS = new HashSet<String>(Arrays.asList(
//				PreferenceConstants.JID,
//				PreferenceConstants.PASSWORD,
//				PreferenceConstants.PORT,
//				//PreferenceConstants.RESSOURCE,
//				PreferenceConstants.FOREGROUND,
//				PreferenceConstants.REQUIRE_SSL,
//				PreferenceConstants.SMACKDEBUG
//			));
//	private static final HashSet<String> PRESENCE_PREFS = new HashSet<String>(Arrays.asList(
//				PreferenceConstants.MESSAGE_CARBONS,
//				PreferenceConstants.PRIORITY,
//				PreferenceConstants.STATUS_MODE,
//				PreferenceConstants.STATUS_MESSAGE
//			));

	public String password;
	//public String ressource;
	public int port;
	public int priority;
	public boolean foregroundService;
	public boolean autoConnect;
	public boolean messageCarbons;
	public boolean reportCrash;
	public String userName;
	public String server;
	public String jabberID;
	public boolean jid_configured;
	public boolean require_ssl;

	public String statusMode;
	public String statusMessage;
	public String[] statusMessageHistory;

	public boolean isLEDNotify;
	public String vibraNotify;
	public Uri notifySound;
	public boolean ticker;

	public boolean smackdebug;
    public String chatFontSize;
    public boolean showOffline;
	public boolean enableGroups;

    public boolean reconnect_required = false;
    public boolean presence_required = false;

	private final SharedPreferences prefs;

    private static final String PWD_DEFAULT = "password";

	public ChatConfig() {
		prefs = GTDApplication.getInstance().getSharedPreferences(PreferenceConstants.PREF_CHAT,0);
		prefs.registerOnSharedPreferenceChangeListener(this);
		loadPrefs(prefs);
	}

	@Override
	protected void finalize() {
		prefs.unregisterOnSharedPreferenceChangeListener(this);
	}

	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		Log.i(TAG, "onSharedPreferenceChanged(): " + key);
		loadPrefs(prefs);
//		if (RECONNECT_PREFS.contains(key))
//			reconnect_required = true;
//		if (PRESENCE_PREFS.contains(key))
//			presence_required = true;
	}

	private void splitAndSetJabberID(String jid) {
		String[] res = jid.split("@");
		this.userName = res[0];
		this.server = res[1];
	}

	private int validatePriority(int jabPriority) {
		if (jabPriority > 127)
			return 127;
		else if (jabPriority < -127)
			return -127;
		return jabPriority;
	}

	private void loadPrefs(SharedPreferences prefs) {
		this.jid_configured = false;

        this.jabberID = prefs.getString(PreferenceConstants.JID, "");
        this.password = prefs.getString(PreferenceConstants.PASSWORD, PWD_DEFAULT);

        this.port = XMPPHelper.tryToParseInt(prefs.getString(
                PreferenceConstants.PORT, PreferenceConstants.DEFAULT_PORT),
            PreferenceConstants.DEFAULT_PORT_INT);

        //notification related
        this.isLEDNotify = prefs.getBoolean(PreferenceConstants.LEDNOTIFY,
				false);
		this.vibraNotify = prefs.getString(
				PreferenceConstants.VIBRATIONNOTIFY, "SYSTEM");
		this.notifySound = Uri.parse(prefs.getString(
				PreferenceConstants.RINGTONENOTIFY, ""));
		this.ticker = prefs.getBoolean(PreferenceConstants.TICKER,
				true);

//        this.ressource = prefs
//				.getString(PreferenceConstants.RESSOURCE, "yaxim");

		this.priority = validatePriority(XMPPHelper.tryToParseInt(prefs
				.getString(PreferenceConstants.PRIORITY, "0"), 0));

		this.foregroundService = prefs.getBoolean(PreferenceConstants.FOREGROUND, true);

		this.autoConnect = prefs.getBoolean(PreferenceConstants.CONN_STARTUP,
				false);

		this.messageCarbons = prefs.getBoolean(
				PreferenceConstants.MESSAGE_CARBONS, true);

		this.smackdebug = prefs.getBoolean(PreferenceConstants.SMACKDEBUG,
				false);
		this.reportCrash = prefs.getBoolean(PreferenceConstants.REPORT_CRASH,
				false);

		this.require_ssl = prefs.getBoolean(PreferenceConstants.REQUIRE_SSL,
				false);
		this.statusMode = prefs.getString(PreferenceConstants.STATUS_MODE, "available");
		this.statusMessage = prefs.getString(PreferenceConstants.STATUS_MESSAGE, "");
		this.statusMessageHistory = prefs.getString(PreferenceConstants.STATUS_MESSAGE_HISTORY, statusMessage).split("\036");
		this.chatFontSize = prefs.getString("setSizeChat", "18");
		this.showOffline = prefs.getBoolean(PreferenceConstants.SHOW_OFFLINE, false);
		this.enableGroups = prefs.getBoolean(PreferenceConstants.ENABLE_GROUPS, true);

		try {
			if(TextUtils.isEmpty(jabberID))return;
            splitAndSetJabberID(XMPPHelper.verifyJabberID(jabberID));
			this.jid_configured = true;
		} catch (ChatXMPPAdressMalformedException e) {
			Log.e(TAG, "Exception in getPreferences(): " + e);
		}
	}

    public void saveJabberId(String jabberID) {
        jabberID = "jared@chat.gtdollar.com";
        this.jabberID = jabberID;
        this.autoConnect = true;
        prefs.edit().putString(PreferenceConstants.JID,jabberID).putBoolean(PreferenceConstants.CONN_STARTUP,true).commit();
    }

    public void destroyConfig() {
        prefs.edit().clear().commit();
    }


}
