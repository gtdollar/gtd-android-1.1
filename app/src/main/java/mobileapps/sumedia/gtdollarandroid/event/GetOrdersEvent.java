package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.OrdersResponse;

/**
 * Created by zhuodong on 9/10/14.
 */
public class GetOrdersEvent extends BaseEvent<OrdersResponse> {

    int orderStatus;

    public GetOrdersEvent(EventStatus status) {
        super(status);
    }

    public GetOrdersEvent(EventStatus status, OrdersResponse response, int orderStatus) {
        super(status, response);
        this.orderStatus = orderStatus;
    }

    public int getOrderStatus() {
        return orderStatus;
    }
}
