package mobileapps.sumedia.gtdollarandroid.widget;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.event.DateSetEvent;

import static android.app.DatePickerDialog.OnDateSetListener;

/**
 * Created by zhuodong on 4/9/14.
 */
public class DatePickerFragment extends DialogFragment implements OnDateSetListener{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_YEAR = "arg_year";
    private static final String ARG_MONTH = "arg_month";
    private static final String ARG_DAY = "arg_day";

    // TODO: Rename and change types of parameters
    private int year, month, day;
    private boolean allowPreviousDate;

    // TODO: Rename and change types and number of parameters
    public static DatePickerFragment newInstance(int year, int month, int day) {
        DatePickerFragment fragment = new DatePickerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_YEAR, year);
        args.putInt(ARG_MONTH, month);
        args.putInt(ARG_DAY, day);
        fragment.setArguments(args);
        return fragment;
    }

    public static DatePickerFragment newInstance() {


        return new DatePickerFragment();
    }

    public static DatePickerFragment newInstance(boolean allowPreviousDate) {
        DatePickerFragment f = new DatePickerFragment();
        Bundle args = new Bundle();
        args.putBoolean("allowPreviousDate", allowPreviousDate);
        f.setArguments(args);
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            year = getArguments().getInt(ARG_YEAR);
            month = getArguments().getInt(ARG_MONTH);
            day = getArguments().getInt(ARG_DAY);
            allowPreviousDate = getArguments().getBoolean("allowPreviousDate",false);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker

        final Calendar c = Calendar.getInstance();

        if(year == 0 && month == 0 && day == 0) {
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);

        if(!allowPreviousDate) {
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        }
        // Create a new instance of DatePickerDialog and return it
        return datePickerDialog;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        EventBus.getDefault().post(new DateSetEvent(year, month, day));
    }
}
