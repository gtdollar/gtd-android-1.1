package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.AccountSignInEvent;
import mobileapps.sumedia.gtdollarandroid.event.CheckAccountEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.ReactivateAccountEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountCheckResponse;
import mobileapps.sumedia.gtdollarandroid.widget.AspectRatioImageView;

public class EWalletActivity extends BaseActivity {
    @InjectView(R.id.iv_logo)
    ImageView mIvLogo;
    @InjectView(R.id.tv_credit_card_title)
    TextView mTvCreditCardTitle;
    @InjectView(R.id.iv_chip)
    AspectRatioImageView mIvChip;
    @InjectView(R.id.tv_card_number)
    TextView mTvCardNumber;
    @InjectView(R.id.tv_card_valid_date)
    TextView mTvCardValidDate;
    @InjectView(R.id.tv_card_name)
    TextView mTvCardName;
    @InjectView(R.id.tv_credit)
    TextView mTvCredit;
    @InjectView(R.id.tv_balance)
    TextView mTvBalance;
    @InjectView(R.id.iv_vp_apply)
    TextView mIvVpApply;
    @InjectView(R.id.iv_vp_send)
    TextView mIvVpSend;
    @InjectView(R.id.iv_vp_records)
    TextView mIvVpRecords;
    @InjectView(R.id.iv_vp_qr)
    TextView mIvVpQr;
    @InjectView(R.id.iv_payment_topup)
    TextView mIvPaymentTopup;
    @InjectView(R.id.iv_payment_withdraw)
    TextView mIvPaymentWithdraw;
    @InjectView(R.id.iv_payment_transfer)
    TextView mIvPaymentTransfer;
    @InjectView(R.id.iv_payment_refund)
    TextView mIvPaymentRefund;
    @InjectView(R.id.iv_transaction_all)
    TextView mIvTransactionAll;
    @InjectView(R.id.iv_transaction_completed)
    TextView mIvTransactionCompleted;
    @InjectView(R.id.iv_transaction_charged_back)
    TextView mIvTransactionChargedBack;
    @InjectView(R.id.iv_transaction_refunding)
    TextView mIvTransactionRefunding;
    @InjectView(R.id.ll_account_container)
    LinearLayout mLlAccountContainer;
    @InjectView(R.id.tv_activate_desc)
    TextView mTvActivateDesc;
    @InjectView(R.id.btn_resend_activate)
    Button mBtnResendActivate;
    @InjectView(R.id.ll_activate_account)
    LinearLayout mLlActivateAccount;
    @InjectView(R.id.tv_account_symbol)
    TextView mTvAccountSymbol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ewallet);
        ButterKnife.inject(this);
        mTvCardName.setText(UserDataHelper.getUser().getName());

        if (UserDataHelper.isVerified()) {
            JobManager.getInstance().checkAccount();
            mLlAccountContainer.setVisibility(View.VISIBLE);
            mLlActivateAccount.setVisibility(View.GONE);
        } else {
            mTvActivateDesc.setText(getString(R.string.string_activate_desc, UserDataHelper.getEmail()));
            mLlAccountContainer.setVisibility(View.GONE);
            mLlActivateAccount.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!UserDataHelper.isVerified()) {
            JobManager.getInstance().accountSignIn(UserDataHelper.getEmail(), UserDataHelper.getPwd());
        }
    }

    @OnClick(R.id.btn_resend_activate)
    public void onClickResendActivate(View view) {
        JobManager.getInstance().accountReactivate(UserDataHelper.getEmail());
    }

    @OnClick(R.id.iv_vp_apply)
    public void onClickApply(View view) {
        if (!Utils.checkAccount(this)) return;

        Intent intent = new Intent(this, ApplyCreditActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.iv_vp_send)
    public void onClickSend(View view) {
        if (!Utils.checkAccount(this)) return;

        Intent intent = new Intent(this, SendCreditActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.iv_vp_qr)
    public void onClickQRPay(View view) {
        if (!Utils.checkAccount(this)) return;

        Intent intent = new Intent(this, QRPayActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.iv_vp_records)
    public void onClickRecords(View view) {
        if (!Utils.checkAccount(this)) return;

        Intent intent = new Intent(this, RecordListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.iv_payment_topup)
    public void onClickTopup(View view) {
        if (!Utils.checkAccount(this)) return;

        Intent intent = new Intent(this, TopupActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.iv_payment_withdraw)
    public void onClickWithDraw(View view) {
        if (!Utils.checkAccount(this)) return;

        Intent intent = new Intent(this, WithdrawActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.iv_payment_transfer)
    public void onClickTransfer(View view) {
        if (!Utils.checkAccount(this)) return;

        Intent intent = new Intent(this, TransferBalanceActivity.class);
        startActivity(intent);
    }

    @OnClick({R.id.iv_transaction_all, R.id.iv_transaction_charged_back, R.id.iv_transaction_completed, R.id.iv_transaction_refunding, R.id.iv_payment_refund})
    public void onClickRecord(View view) {
        if (!Utils.checkAccount(this)) return;

        Intent intent = new Intent(this, BalanceRecordListActivity.class);
        intent.putExtra(Const.INTENT_EXTRA_RECORD_STATUS, Integer.parseInt(String.valueOf(view.getTag())));
        startActivity(intent);
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    public void onEventMainThread(CheckAccountEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED && event.getResponse() != null) {
            AccountCheckResponse.Data data = event.getResponse().getData();
            mTvCredit.setText(GTDHelper.formatCredit(data.getCredits()));
            mTvBalance.setText(GTDHelper.formatBalance(data.getBalance()));
            mTvAccountSymbol.setText(getString(R.string.wallet_account)+" "+UserDataHelper.getAccount().getMcurrency());
        }
    }

    public void onEventMainThread(AccountSignInEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED) {
            JobManager.getInstance().checkAccount();
            mLlAccountContainer.setVisibility(View.VISIBLE);
            mLlActivateAccount.setVisibility(View.GONE);
        }
    }


    public void onEventMainThread(ReactivateAccountEvent event) {
        Utils.toggleEventPopup(this, event, R.string.sending_email, null);
    }
}
