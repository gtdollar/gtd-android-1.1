package ua.org.zasadnyy.zvalidations.validations;

import android.content.Context;
import android.widget.EditText;

import ua.org.zasadnyy.zvalidations.R;

import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.ValidationResult;

public class NotMatchPassword extends BaseValidation {

    private final EditText mEtPassword;

    public static Validation build(Context context, EditText etPassword) {
        return new NotMatchPassword(context,etPassword);
    }

    private NotMatchPassword(Context context, EditText etPassword) {
        super(context);
        this.mEtPassword = etPassword;
    }

    @Override
    public ValidationResult validate(Field field) {

        boolean isValid = field.getTextView().getText().toString().equals(mEtPassword.getText().toString());
        return isValid ?
                ValidationResult.buildSuccess(field.getTextView())
                : ValidationResult.buildFailed(field.getTextView(), mContext.getString(
            R.string.zvalidations_password_not_the_same));
    }
}