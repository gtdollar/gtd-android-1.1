package mobileapps.sumedia.gtdollarandroid;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.preference.PreferenceManager;

import java.util.Locale;

import mobileapps.sumedia.gtdollarandroid.chat.ChatConfig;

public class GTDApplication extends Application {
    private static GTDApplication instance;
    public static SharedPreferences mSharedPreferences;
    public static String versionName;
    public static int versionCode;
    public DaoSession daoSession;
    private Location location;

    public static GTDApplication getInstance() {
        return instance;
    }

    private ChatConfig chatConfig;


    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        chatConfig = new ChatConfig();

        try {
            versionName = getPackageManager().getPackageInfo(
                this.getPackageName(), 0).versionName;
            versionCode = getPackageManager().getPackageInfo(
                this.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        setupDatabase();
    }

    private void setupDatabase() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "gtd-db.sqlite", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

    public ChatConfig getConfig() {
        return chatConfig;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Locale.setDefault(newConfig.locale);
        getBaseContext().getResources().updateConfiguration(newConfig, getResources().getDisplayMetrics());
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}