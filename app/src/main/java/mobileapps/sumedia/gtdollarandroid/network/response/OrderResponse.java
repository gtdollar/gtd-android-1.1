package mobileapps.sumedia.gtdollarandroid.network.response;

import mobileapps.sumedia.gtdollarandroid.model.Order;

/**
 * Created by zhuodong on 8/29/14.
 */
public class OrderResponse extends BaseResponse{
    Order order;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
