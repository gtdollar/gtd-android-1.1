package mobileapps.sumedia.gtdollarandroid.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;

import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.event.AlertButtonClickEvent;

/**
 * Created by zhuodong on 4/4/14.
 */
public class GenericAlertPopup extends DialogFragment {

    public static final int ALERT_ERROR = 0;
    public static final int ALERT_TRANSFER = 1;
    public static final int ALERT_DIAL = 2;
    public static final int ALERT_WITHDRAW = 3;


    public static GenericAlertPopup newInstance(int alertType, String title, String desc,
            String positiveButtonStr, String negativeButtonStr) {
        return newInstance(alertType,title,desc,positiveButtonStr,negativeButtonStr,0);
    }

    public static GenericAlertPopup newInstance(int alertType, String title, String desc,
            String positiveButtonStr, String negativeButtonStr, int userId) {
        GenericAlertPopup fragment = new GenericAlertPopup();
        Bundle b = new Bundle();
        b.putString("titleStr", title);
        b.putString("descStr", desc);
        b.putInt("alertType", alertType);
        b.putString("positiveBtn", positiveButtonStr);
        b.putString("negativeBtn", negativeButtonStr);
        b.putInt("userId", userId);

        fragment.setArguments(b);
        return fragment;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final int alertType = getArguments().getInt("alertType");
        String negativeBtn = getArguments().getString("negativeBtn");
        String positiveBtn = getArguments().getString("positiveBtn");
        String title = getArguments().getString("titleStr");
        String message = getArguments().getString("descStr");
        final int userId = getArguments().getInt("userId");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message);

        if (!TextUtils.isEmpty(positiveBtn)) {
            builder.setPositiveButton(positiveBtn,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            EventBus.getDefault()
                                .post(new AlertButtonClickEvent(alertType, true));
                        }
                    }
            );
        }

        if (!TextUtils.isEmpty(negativeBtn)) {
            builder.setNegativeButton(negativeBtn,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                            EventBus.getDefault()
                                .post(new AlertButtonClickEvent(alertType, false));                       }
                    }
            );
        }

        return builder.create();


    }
}
