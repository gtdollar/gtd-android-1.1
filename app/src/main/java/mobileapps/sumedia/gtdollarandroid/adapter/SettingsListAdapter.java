package mobileapps.sumedia.gtdollarandroid.adapter;


import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.model.SimpleIconTextItem;

public class SettingsListAdapter extends PagingBaseAdapter<SimpleIconTextItem> {

    public SettingsListAdapter(Context context) {
        super(context);
    }

    @Override
    protected int getViewResourceId(int position) {
        return R.layout.item_settings;
    }

    @Override
    protected Renderer<SimpleIconTextItem> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolder implements Renderer<SimpleIconTextItem> {
        @InjectView(R.id.iv_setting_item)
        ImageView ivSettingItem;
        @InjectView(R.id.tv_item_text)
        TextView tvText;

        View view;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
            this.view = view;
        }

        public void render(SimpleIconTextItem item, int position) {
            tvText.setText(item.getTitle());
            ivSettingItem.setImageDrawable(item.getIcon());

        }
    }

}
