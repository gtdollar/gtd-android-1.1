//package mobileapps.sumedia.gtdollarandroid.ui;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.net.Uri;
//import android.os.Bundle;
//import android.provider.MediaStore;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.ArrayAdapter;
//import android.widget.ImageView;
//import android.widget.Spinner;
//import android.widget.Toast;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Locale;
//
//import mobileapps.sumedia.gtdollarandroid.R;
//
//public class NewMerchantActivity extends Activity {
//
//    private static final int REQUEST_CAMERA = 0;
//    private static final int SELECT_FILE = 1;
//
//     ImageView currentImageView;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_new_merchant);
//        // Set Values for different Spinners
//        Spinner typeSpinner = (Spinner) findViewById(R.id.typeSpinner);
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
//                R.array.merchant_types, R.layout.spinner_layout);
//        adapter.setDropDownViewResource(R.layout.spinner_layout);
//        typeSpinner.setAdapter(adapter);
//
//        Spinner currencySpinner = (Spinner) findViewById(R.id.currencySpinner);
//        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
//                R.array.currency_types, R.layout.spinner_layout);
//        adapter2.setDropDownViewResource(R.layout.spinner_layout);
//        currencySpinner.setAdapter(adapter2);
//
//        Spinner countrySpinner = (Spinner) findViewById(R.id.countrySpinner);
//        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this,R.layout.spinner_layout,countryList());
//        countrySpinner.setAdapter(adapter3);
//    }
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.new_merchant, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    public ArrayList<String> countryList()
//    {
//        Locale[] locales = Locale.getAvailableLocales();
//        ArrayList<String> countries = new ArrayList<String>();
//        for (Locale locale : locales) {
//            String country = locale.getDisplayCountry();
//            if (country.trim().length()>0 && !countries.contains(country)) {
//                countries.add(country);
//            }
//        }
//        Collections.sort(countries);
//        return countries;
//    }
//
//    public void onSubmitClick(View view)
//    {
//        Toast.makeText(getApplicationContext(), "Submitted", Toast.LENGTH_SHORT).show();
//        this.finish();
//    }
//
//    public void wallpaperImageClick(View view)
//    {
//        ImageView wallImageView = (ImageView) findViewById(R.id.wallPaperImageView);
//        ImageView profileImageView =  (ImageView) findViewById(R.id.profileImageView);
//        if(view.getId() == R.id.wallPaperImageView){
//            currentImageView = wallImageView;
//            showImagePickerDialogue();
//        }
//        else if(view.getId() == R.id.profileImageView){
//            currentImageView = profileImageView;
//            showImagePickerDialogue();
//        }
//    }
//
//    protected void showImagePickerDialogue()
//    {
//        final CharSequence[] items = {"Take Photo", "Choose from Library","Cancel" };
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(NewMerchantActivity.this);
//        builder.setTitle("Add Photo!");
//        builder.setItems(items, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int item) {
//                if (items[item].equals("Take Photo")) {
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
////                    File f = new File(android.os.Environment
////                            .getExternalStorageDirectory(), "temp.jpg");
////                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
//                    startActivityForResult(intent, REQUEST_CAMERA);
//                } else if (items[item].equals("Choose from Library")) {
//                    Intent intent = new Intent(
//                            Intent.ACTION_PICK,
//                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                    intent.setType("image/*");
//                    startActivityForResult(
//                            Intent.createChooser(intent, "Select File"),
//                            SELECT_FILE);
//                } else if (items[item].equals("Cancel")) {
//                    dialog.dismiss();
//                }
//            }
//        });
//        builder.show();
//    }
//
//
//    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
//        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
//        switch(requestCode) {
//            case 0:
//                if(resultCode == RESULT_OK){
//                    Uri selectedImage = imageReturnedIntent.getData();
//                    currentImageView.setImageURI(selectedImage);
//                }
//
//                break;
//            case 1:
//                if(resultCode == RESULT_OK){
//                    Uri selectedImage = imageReturnedIntent.getData();
//                    currentImageView.setImageURI(selectedImage);
//                }
//                break;
//        }
//    }
//}
