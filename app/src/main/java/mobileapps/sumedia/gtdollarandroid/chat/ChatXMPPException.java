package mobileapps.sumedia.gtdollarandroid.chat;

public class ChatXMPPException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ChatXMPPException(String message) {
		super(message);
	}

	public ChatXMPPException(String message, Throwable cause) {
		super(message, cause);
	}
}
