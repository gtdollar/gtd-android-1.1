package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;

import java.io.File;

import mobileapps.sumedia.gtdollarandroid.ScaleBitmapAsyncTask;
import mobileapps.sumedia.gtdollarandroid.event.ChoseItemEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.FileUtils;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.location.location.LocationActivity;

/**
 * Created by zhuodong on 9/4/14.
 */
public class BasePictureActivity extends LocationActivity {

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Const.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            new ScaleBitmapAsyncTask(tempPath, tempPath, Const.IMAGE_MAX_WIDTH, Const.IMAGE_MAX_HEIGHT).execute();

        } else if (requestCode == Const.REQUEST_IMAGE_PICK && resultCode == Activity.RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.MIME_TYPE, MediaStore.Images.Media.SIZE};

            Cursor cursor = this.getContentResolver().query(
                selectedImage, filePathColumn, null, null, null);


            if (cursor.moveToFirst()) {
                final String mimeType = cursor.getString(cursor.getColumnIndex(filePathColumn[1]));
                final String size = cursor.getString(cursor.getColumnIndex(filePathColumn[2]));
                final String path = cursor.getString(cursor.getColumnIndex(filePathColumn[0]));

                File distFile = Utils.createImageFile(this, getExternalCacheDir());
                if(distFile != null  && FileUtils.isFileExit(path)) {
                    tempPath = distFile.getAbsolutePath();
                    new ScaleBitmapAsyncTask(path, tempPath, Const.IMAGE_MAX_WIDTH, Const.IMAGE_MAX_HEIGHT).execute();
                }

            }
            cursor.close();
        }
    }

    String tempPath;

    public void onEventMainThread(ChoseItemEvent event) {
        if(event.getType() == GenericSelectionDialog.TYPE_PICTURE) {
            if (event.getPosition() == 0) {
                onTakePhoto();
            } else {
                onChoosePhoto();
            }
        }
    }

    protected void onChoosePhoto() {
        Intent i = new Intent(Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        //i.setType("image/*");
        startActivityForResult(i, Const.REQUEST_IMAGE_PICK);
    }

    protected void onTakePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = Utils.createImageFile(this, getExternalCacheDir());
            // Continue only if the File was successfully created
            tempPath = photoFile.getAbsolutePath();
            if (!TextUtils.isEmpty(tempPath)) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, Const.REQUEST_IMAGE_CAPTURE);
            }
        }
    }
}
