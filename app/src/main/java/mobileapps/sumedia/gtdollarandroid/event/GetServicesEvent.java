package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.ServicesResponse;

/**
 * Created by zhuodong on 9/5/14.
 */
public class GetServicesEvent extends BaseEvent<ServicesResponse> {
    public GetServicesEvent(EventStatus status) {
        super(status);
    }

    public GetServicesEvent(EventStatus status, ServicesResponse response) {
        super(status, response);
    }
}
