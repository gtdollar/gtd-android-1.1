package mobileapps.sumedia.gtdollarandroid.network;

import android.text.TextUtils;
import android.util.Log;

import org.jivesoftware.smack.packet.Message;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.Feed;
import mobileapps.sumedia.gtdollarandroid.FeedDao;
import mobileapps.sumedia.gtdollarandroid.GTDApplication;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.chat.ChatConstants;
import mobileapps.sumedia.gtdollarandroid.event.AccountSignInEvent;
import mobileapps.sumedia.gtdollarandroid.event.AddCircleEvent;
import mobileapps.sumedia.gtdollarandroid.event.AddContactEvent;
import mobileapps.sumedia.gtdollarandroid.event.UpdatePeopleEvent;
import mobileapps.sumedia.gtdollarandroid.event.ApplyCreditEvent;
import mobileapps.sumedia.gtdollarandroid.event.BalanceRecordEvent;
import mobileapps.sumedia.gtdollarandroid.event.BaseEvent;
import mobileapps.sumedia.gtdollarandroid.event.BlackListEvent;
import mobileapps.sumedia.gtdollarandroid.event.BlockListEvent;
import mobileapps.sumedia.gtdollarandroid.event.CancelOrderEvent;
import mobileapps.sumedia.gtdollarandroid.event.CheckAccountEvent;
import mobileapps.sumedia.gtdollarandroid.event.ConfirmOrderEvent;
import mobileapps.sumedia.gtdollarandroid.event.CreateOrderEvent;
import mobileapps.sumedia.gtdollarandroid.event.CreateUpdateServiceEvent;
import mobileapps.sumedia.gtdollarandroid.event.CreditRecordEvent;
import mobileapps.sumedia.gtdollarandroid.event.ForgotPWDEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetCircleDetailEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetCirclesEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetContactEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetFeedsEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetMerchantEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetNearbyEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetNearbyPlacesEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetOrdersEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetPlaceDetailEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetRebateRateEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetServiceEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetServicesEvent;
import mobileapps.sumedia.gtdollarandroid.event.OrderPaymentEvent;
import mobileapps.sumedia.gtdollarandroid.event.QRPayEvent;
import mobileapps.sumedia.gtdollarandroid.event.ReactivateAccountEvent;
import mobileapps.sumedia.gtdollarandroid.event.RefundEvent;
import mobileapps.sumedia.gtdollarandroid.event.RemoveCircleEvent;
import mobileapps.sumedia.gtdollarandroid.event.SearchMerchantEvent;
import mobileapps.sumedia.gtdollarandroid.event.SendCreditEvent;
import mobileapps.sumedia.gtdollarandroid.event.SignInEvent;
import mobileapps.sumedia.gtdollarandroid.event.TransferBalanceEvent;
import mobileapps.sumedia.gtdollarandroid.event.UnionPayEvent;
import mobileapps.sumedia.gtdollarandroid.event.UpdateCircleEvent;
import mobileapps.sumedia.gtdollarandroid.event.UpdateProfileEvent;
import mobileapps.sumedia.gtdollarandroid.event.UploadFileEvent;
import mobileapps.sumedia.gtdollarandroid.event.UserSignUpEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.Formatter;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.model.Account;
import mobileapps.sumedia.gtdollarandroid.model.GTUser;
import mobileapps.sumedia.gtdollarandroid.model.Notification;
import mobileapps.sumedia.gtdollarandroid.model.TransactionActivity;
import mobileapps.sumedia.gtdollarandroid.model.User;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountActivitiesResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountBalanceRecordResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountBaseResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountCheckResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountCreditRecordResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountMerchantRateResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountQRPayResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountSignInResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountSimpleResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.BaseResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.BlackBlockListResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.CircleResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.CirclesResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.ContactsResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.GetMerchantResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.GooglePlaceDetailResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.GooglePlaceResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.NotificationResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.OrderResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.OrdersResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.SearchMerchantResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.ServiceResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.ServicesResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.SignInResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.UpdateProfileResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.UploadFileResponse;
import retrofit.Callback;
import retrofit.ResponseCallback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;

import static mobileapps.sumedia.gtdollarandroid.event.EventStatus.FAILED;
import static mobileapps.sumedia.gtdollarandroid.event.EventStatus.ONGOING;
import static mobileapps.sumedia.gtdollarandroid.event.EventStatus.SUCCEED;

/**
 * Created by zhuodong on 7/22/14.
 */
public class JobManager {
    private static JobManager ourInstance = new JobManager();

    public static JobManager getInstance() {
        return ourInstance;
    }

    private JobManager() {
    }

    private void postErrorEvent(Throwable throwable, BaseEvent event) {
        String errorMsg = throwable.getMessage();
        if (TextUtils.isEmpty(errorMsg)) {
            errorMsg = Const.DEFAULT_ERROR_MSG;
        }
        event.setErrorMsg(errorMsg);
        post(event);
    }

    void post(BaseEvent event) {
        EventBus.getDefault().post(event);
    }

    public void blockList() {
        ApiManager.getApiClient().blockList(new Callback<BlackBlockListResponse>() {
            @Override
            public void success(BlackBlockListResponse blackBlockListResponse, Response response) {
                post(new BlockListEvent(SUCCEED, blackBlockListResponse));
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void blackList() {
        ApiManager.getApiClient().blackList(new Callback<BlackBlockListResponse>() {
            @Override
            public void success(BlackBlockListResponse blackBlockListResponse, Response response) {
                post(new BlackListEvent(SUCCEED, blackBlockListResponse));
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void complain(String id, String content) {
        ApiManager.getApiClient().complain(id, content, new ResponseCallback() {
            @Override
            public void success(Response response) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void block(String id, boolean add) {
        if (add) {
            ApiManager.getApiClient().addBlockList(id, new ResponseCallback() {
                @Override
                public void success(Response response) {

                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }else {
            ApiManager.getApiClient().removeBlockList(id, new ResponseCallback() {
                @Override
                public void success(Response response) {

                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }

    public void black(String id, boolean add) {
        if (add) {
            ApiManager.getApiClient().addBlackList(id, new ResponseCallback() {
                @Override
                public void success(Response response) {

                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }else {
            ApiManager.getApiClient().removeBlackList(id, new ResponseCallback() {
                @Override
                public void success(Response response) {

                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }


    public void addCircleMember(String id, final String memberIds) {
        post(new UpdatePeopleEvent(ONGOING));

        ApiManager.getApiClient().addPeople(id, memberIds, new Callback<CircleResponse>() {
            @Override
            public void success(CircleResponse baseResponse, Response response) {
                if (baseResponse.isSuccess()) {
                    post(new UpdatePeopleEvent(SUCCEED, baseResponse));
                } else {
                    postErrorEvent(new Throwable(baseResponse.getError()), new UpdatePeopleEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new UpdatePeopleEvent(FAILED));
            }
        });
    }

    public void removeCircleMember(String id, final String memberIds) {
        post(new UpdatePeopleEvent(ONGOING));

        ApiManager.getApiClient().removePeople(id, memberIds, new Callback<CircleResponse>() {
            @Override
            public void success(CircleResponse baseResponse, Response response) {
                if (baseResponse.isSuccess()) {
                    post(new UpdatePeopleEvent(SUCCEED, baseResponse));
                } else {
                    postErrorEvent(new Throwable(baseResponse.getError()), new UpdatePeopleEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new UpdatePeopleEvent(FAILED));
            }
        });
    }

    public void removeCircle(final String circleId) {
        post(new RemoveCircleEvent(ONGOING));

        ApiManager.getApiClient().removeCircle(circleId, new Callback<BaseResponse>() {
            @Override
            public void success(BaseResponse baseResponse, Response response) {
                if (baseResponse.isSuccess()) {
                    post(new RemoveCircleEvent(SUCCEED, circleId));
                } else {
                    postErrorEvent(new Throwable(baseResponse.getError()), new RemoveCircleEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new RemoveCircleEvent(FAILED));
            }
        });
    }

    public void getPlaceDetail(String placeId) {
        post(new GetPlaceDetailEvent(ONGOING));


        Map<String, String> searchParams = new HashMap<String, String>();

        searchParams.put("placeid", placeId);
        searchParams.put("key", GTDApplication.getInstance().getString(R.string.google_places_key));

        ApiManager.getGoogleClient().getPlaceDetail(searchParams, new Callback<GooglePlaceDetailResponse>() {
            @Override
            public void success(GooglePlaceDetailResponse googlePlaceDetailResponse, Response response) {
                post(new GetPlaceDetailEvent(SUCCEED, googlePlaceDetailResponse));
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new GetPlaceDetailEvent(FAILED));
            }
        });
    }

    public void getNearbyPlaces(String location, String type) {
        post(new GetNearbyPlacesEvent(ONGOING));


        Map<String, String> searchParams = new HashMap<String, String>();
        if (type != null && !type.equalsIgnoreCase("others")) {
            searchParams.put("keyword", type);
        }
        searchParams.put("location", location);
        searchParams.put("radius", "5000");
        searchParams.put("key", GTDApplication.getInstance().getString(R.string.google_places_key));

        ApiManager.getGoogleClient().getPlaces(searchParams, new Callback<GooglePlaceResponse>() {
            @Override
            public void success(GooglePlaceResponse googlePlaceResponse, Response response) {
                post(new GetNearbyPlacesEvent(SUCCEED, googlePlaceResponse));
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new GetNearbyPlacesEvent(FAILED));
            }
        });
    }


    public void getOrders(boolean isMerchant, String userId, final int status) {
        post(new GetOrdersEvent(ONGOING));

        Map<String, String> searchParams = new HashMap<String, String>();
        if (isMerchant) {
            searchParams.put("merchant", userId);
        } else {
            searchParams.put("customer", userId);
        }
        searchParams.put("status", status + "");

        ApiManager.getApiClient().getOrders(searchParams, new Callback<OrdersResponse>() {
            @Override
            public void success(OrdersResponse ordersResponse, Response response) {
                //signInResponse.saveToPref();
                if (ordersResponse.isSuccess()) {
                    post(new GetOrdersEvent(SUCCEED, ordersResponse, status));
                } else {
                    postErrorEvent(new Throwable(ordersResponse.getMessage()), new GetOrdersEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new GetOrdersEvent(FAILED));
            }
        });
    }

    public void getContacts() {
        post(new GetContactEvent(ONGOING));

        ApiManager.getApiClient().getContacts(new Callback<ContactsResponse>() {
            @Override
            public void success(ContactsResponse contactsResponse, Response response) {
                //signInResponse.saveToPref();
                if (contactsResponse.isSuccess()) {
                    post(new GetContactEvent(SUCCEED, contactsResponse));
                } else {
                    postErrorEvent(new Throwable(contactsResponse.getMessage()), new GetContactEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new GetContactEvent(FAILED));
            }
        });
    }

    public void addContact(final String contacts) {
        post(new AddContactEvent(ONGOING));

        ApiManager.getApiClient().addContact(contacts, new Callback<BaseResponse>() {
            @Override
            public void success(BaseResponse baseResponse, Response response) {
                //signInResponse.saveToPref();
                if (baseResponse.isSuccess()) {
                    getContacts();
                    post(new AddContactEvent(SUCCEED));
                } else {
                    postErrorEvent(new Throwable(baseResponse.getMessage()), new AddContactEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new AddContactEvent(FAILED));
            }
        });
    }

    public void createOrder(final Map<String, String> params) {
        post(new CreateOrderEvent(ONGOING));

        ApiManager.getApiClient().createOrder(params, new Callback<OrderResponse>() {
            @Override
            public void success(OrderResponse orderResponse, Response response) {
                //signInResponse.saveToPref();
                if (orderResponse.isSuccess()) {
                    post(new CreateOrderEvent(SUCCEED, orderResponse));
                } else {
                    postErrorEvent(new Throwable(orderResponse.getMessage()), new CreateOrderEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new CreateOrderEvent(FAILED));
            }
        });
    }

    public void removeContact(final String id) {
        post(new CreateOrderEvent(ONGOING));

        ApiManager.getApiClient().removeContact(id, new Callback<BaseResponse>() {
            @Override
            public void success(BaseResponse orderResponse, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }


    public void cancelOrder(final String id) {
        post(new CreateOrderEvent(ONGOING));

        ApiManager.getApiClient().cancelOrder(id, new Callback<OrdersResponse>() {
            @Override
            public void success(OrdersResponse orderResponse, Response response) {
                //signInResponse.saveToPref();
                if (orderResponse.isSuccess()) {
                    post(new CancelOrderEvent(SUCCEED));
                } else {
                    postErrorEvent(new Throwable(orderResponse.getMessage()), new CancelOrderEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new CancelOrderEvent(FAILED));
            }
        });
    }

    public void confirmOrder(final String id) {
        post(new CreateOrderEvent(ONGOING));

        ApiManager.getApiClient().confirmOrder(id, new Callback<OrdersResponse>() {
            @Override
            public void success(OrdersResponse orderResponse, Response response) {
                //signInResponse.saveToPref();
                if (orderResponse.isSuccess()) {
                    post(new ConfirmOrderEvent(SUCCEED));
                } else {
                    postErrorEvent(new Throwable(orderResponse.getMessage()), new ConfirmOrderEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new ConfirmOrderEvent(FAILED));
            }
        });
    }

    public void getService() {
        post(new GetServicesEvent(ONGOING));

        ApiManager.getApiClient().getServices(new Callback<ServicesResponse>() {
            @Override
            public void success(ServicesResponse servicesResponse, Response response) {
                //signInResponse.saveToPref();
                post(new GetServicesEvent(SUCCEED, servicesResponse));
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new GetServiceEvent(FAILED));
            }
        });
    }

    public void updateService(final Map<String, String> params) {
        post(new CreateUpdateServiceEvent(ONGOING));

        ApiManager.getApiClient().updateService(

            params.get("id") != null ? new TypedString(params.get("id")) : null,
            params.get("type") != null ? new TypedString(params.get("type")) : null,
            params.get("title") != null ? new TypedString(params.get("title")) : null,
            params.get("expire") != null ? new TypedString(params.get("expire")) : null,
            params.get("description") != null ? new TypedString(params.get("description")) : null,
            params.get("price") != null ? new TypedString(params.get("price")) : null,
            params.get("photo") != null ? new TypedFile("image/jpeg", new File(params.get("photo"))) : null,
            new Callback<ServicesResponse>() {
                @Override
                public void success(ServicesResponse servicesResponse, Response response) {
                    //signInResponse.saveToPref();
                    if (servicesResponse.isSuccess()) {
                        post(new CreateUpdateServiceEvent(SUCCEED));
                    } else {
                        postErrorEvent(new Throwable(servicesResponse.getMessage()), new UpdateProfileEvent(FAILED));
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    postErrorEvent(error, new CreateUpdateServiceEvent(FAILED));
                }
            });
    }

    public void createService(final Map<String, String> params) {
        post(new CreateUpdateServiceEvent(ONGOING));

        ApiManager.getApiClient().createService(

            params.get("type") != null ? new TypedString(params.get("type")) : null,
            params.get("title") != null ? new TypedString(params.get("title")) : null,
            params.get("expire") != null ? new TypedString(params.get("expire")) : null,
            params.get("description") != null ? new TypedString(params.get("description")) : null,
            params.get("price") != null ? new TypedString(params.get("price")) : null,
            params.get("photo") != null ? new TypedFile("image/jpeg", new File(params.get("photo"))) : null,
            new Callback<ServicesResponse>() {
                @Override
                public void success(ServicesResponse servicesResponse, Response response) {
                    //signInResponse.saveToPref();
                    if (servicesResponse.isSuccess()) {
                        post(new CreateUpdateServiceEvent(SUCCEED));
                    } else {
                        postErrorEvent(new Throwable(servicesResponse.getMessage()), new UpdateProfileEvent(FAILED));
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    postErrorEvent(error, new CreateUpdateServiceEvent(FAILED));
                }
            });
    }

    public void createUser(boolean isMerchant, final Map<String, String> params) {
        //TODO use user/add only

        post(new UserSignUpEvent(ONGOING));

        Observable.combineLatest(
//            isMerchant ?
//
//                ApiManager.getApiClient().registerMerchant(
//
//                    params.get("name") != null ? new TypedString(params.get("name")) : null,
//                    params.get("email") != null ? new TypedString(params.get("email")) : null,
//                    params.get("password") != null ? new TypedString(params.get("password")) : null,
//                    params.get("facebook_id") != null ? new TypedString(params.get("facebook_id")) : null,
//                    params.get("phone") != null ? new TypedString(params.get("phone")) : null,
//                    params.get("cover") != null ? new TypedFile("image/jpeg", new File(params.get("cover"))) : null,
//                    params.get("quotation") != null ? new TypedString(params.get("quotation")) : null,
//                    params.get("picture") != null ? new TypedFile("image/jpeg", new File(params.get("picture"))) : null,
//                    params.get("gender") != null ? new TypedString(params.get("gender")) : null,
//                    params.get("region") != null ? new TypedString(params.get("region")) : null,
//                    params.get("currency") != null ? new TypedString(params.get("currency")) : null,
//                    params.get("address") != null ? new TypedString(params.get("address")) : null,
//                    params.get("categories") != null ? new TypedString(params.get("categories")) : null,
//                    params.get("website") != null ? new TypedString(params.get("website")) : null,
//                    params.get("latlng") != null ? new TypedString(params.get("latlng")) : null,
//                    params.get("videos") != null ? new TypedString(params.get("videos")) : null)
//                :
            ApiManager.getApiClient().userSignUp(params.get("email"), params.get("password"), params.get("currency"), params.get("type"), params.get("categories")),


            ApiManager.getAccountClient().signUp(1, params.get("email"), params.get("password"), params.get("currency")), new Func2<SignInResponse, AccountSimpleResponse, Object>() {
                @Override
                public Object call(SignInResponse signUpResponse, AccountSimpleResponse accountSignUpResponse) {
                    Log.d("", " testing call " + accountSignUpResponse.getError());

                    //accountSignUpResponse.getStatus() == 1
                    if (signUpResponse.isSuccess()) {
                        signUpResponse.saveToPref();
                        UserDataHelper.savePwd(params.get("password"));
                        UserDataHelper.saveEmail(params.get("email"));
                        post(new UserSignUpEvent(SUCCEED));
                        return null;
                    } else {
                        postErrorEvent(new Throwable(!signUpResponse.isSuccess() ? signUpResponse.getError() : accountSignUpResponse.getError()), new UserSignUpEvent(FAILED));
                    }

                    return null;
                }
            }).subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                postErrorEvent(throwable, new UserSignUpEvent(FAILED));

            }
        });

    }

    public void updatePassword(String password) {
//
//
//        post(new UserSignUpEvent(ONGOING));
//
//        Observable.combineLatest(
//                ApiManager.getApiClient().updateUser(
//
//                    null,null,new TypedString(password),null,null,null,null,null,null,null,null,null,null,null,null),
//            ), new Func2<BaseResponse, AccountSimpleResponse, Object>() {
//                @Override
//                public Object call(BaseResponse signUpResponse, AccountSimpleResponse accountSignUpResponse) {
//                    Log.d("", " testing call " + accountSignUpResponse.getError());
//
//                    if (accountSignUpResponse.getStatus() == 1 && signUpResponse.isSuccess()) {
//                        post(new UserSignUpEvent(SUCCEED));
//                        return null;
//                    } else {
//                        postErrorEvent(new Throwable(!signUpResponse.isSuccess() ? signUpResponse.getError() : accountSignUpResponse.getError()), new UserSignUpEvent(FAILED));
//                    }
//
//                    return null;
//                }
//            }).subscribe(new Action1<Object>() {
//            @Override
//            public void call(Object o) {
//            }
//        }, new Action1<Throwable>() {
//            @Override
//            public void call(Throwable throwable) {
//                postErrorEvent(throwable, new UserSignUpEvent(FAILED));
//
//            }
//        });

    }


    public void updateProfile(final Map<String, String> params) {
        post(new UpdateProfileEvent(ONGOING));

        ApiManager.getApiClient().updateUser(
            params.get("name") != null ? new TypedString(params.get("name")) : null,
            params.get("password") != null ? new TypedString(params.get("password")) : null,
            params.get("facebook_id") != null ? new TypedString(params.get("facebook_id")) : null,
            params.get("phone") != null ? new TypedString(params.get("phone")) : null,
            params.get("cover") != null ? new TypedFile("image/jpeg", new File(params.get("cover"))) : null,
            params.get("quotation") != null ? new TypedString(params.get("quotation")) : null,
            params.get("picture") != null ? new TypedFile("image/jpeg", new File(params.get("picture"))) : null,
            params.get("gender") != null ? new TypedString(params.get("gender")) : null,
            params.get("region") != null ? new TypedString(params.get("region")) : null,
            params.get("currency") != null ? new TypedString(params.get("currency")) : null,
            params.get("address") != null ? new TypedString(params.get("address")) : null,
            params.get("categories") != null ? new TypedString(params.get("categories")) : null,
            params.get("website") != null ? new TypedString(params.get("website")) : null,
            params.get("latlng") != null ? new TypedString(params.get("latlng")) : null,
            params.get("videos") != null ? new TypedString(params.get("videos")) : null).subscribe(new Action1<UpdateProfileResponse>() {
            @Override
            public void call(UpdateProfileResponse updateProfileResponse) {
                updateProfileResponse.saveToPref();
                if (updateProfileResponse.isSuccess()) {
                    post(new UpdateProfileEvent(SUCCEED));
                } else {
                    postErrorEvent(new Throwable(updateProfileResponse.getMessage()), new UpdateProfileEvent(FAILED));
                }
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                postErrorEvent(throwable, new UpdateProfileEvent(FAILED));

            }
        });


    }

    public void getMerchant(final String id) {
        post(new GetMerchantEvent(ONGOING));

        ApiManager.getApiClient().getMerchant(id, new Callback<GetMerchantResponse>() {
            @Override
            public void success(GetMerchantResponse getMerchantResponse, Response response) {
                //signInResponse.saveToPref();
                post(new GetMerchantEvent(SUCCEED, getMerchantResponse));
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new GetMerchantEvent(FAILED));
            }
        });
    }


    public void searchMerchant(final String keyword, String latlng, boolean isSimple, String type) {
        post(new GetNearbyEvent(ONGOING));

        Map<String, String> searchParams = new HashMap<String, String>();
        searchParams.put("keyword", keyword);
        if (!TextUtils.isEmpty(latlng)) {
            searchParams.put("latlng", latlng);
        }
        if (type != null) {
            searchParams.put("categories", GTDHelper.findType(type)+"");
        }
        if (isSimple) {
            searchParams.put("simple", "1");
        }

        ApiManager.getApiClient().searchMerchant(searchParams).subscribe(new Action1<SearchMerchantResponse>() {
            @Override
            public void call(SearchMerchantResponse searchMerchantResponse) {
                post(new GetNearbyEvent(SUCCEED, searchMerchantResponse));
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                postErrorEvent(throwable, new GetNearbyEvent(FAILED));
            }
        });
    }

    public void createCircle(String type,String keyword, String ids, String latLng) {
        post(new AddCircleEvent(ONGOING));

        final Map<String, String> createCircleParams = new HashMap<String, String>();
        createCircleParams.put("name", keyword);
        createCircleParams.put("description", GTDApplication.getInstance().getString(R.string.circle_desc_default));
        createCircleParams.put("type", GTDHelper.findType(type)+"");
        createCircleParams.put("people", ids);
        createCircleParams.put("latlng", latLng);

        ApiManager.getApiClient().createCircle(createCircleParams).subscribe(new Action1<CircleResponse>() {
            @Override
            public void call(CircleResponse circleResponse) {
                if(circleResponse.isSuccess()) {
                    post(new AddCircleEvent(SUCCEED, circleResponse));
                }else {
                    postErrorEvent(new Throwable(circleResponse.getMessage()), new AddCircleEvent(FAILED));
                }
            }
        },new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                postErrorEvent(throwable, new AddCircleEvent(FAILED));
            }
        });
    }

    public void searchWithCircleCreation(int type, final String keyword, String locationString) {
        post(new SearchMerchantEvent(ONGOING));

        Map<String, String> searchParams = new HashMap<String, String>();
        searchParams.put("keyword", keyword);
        searchParams.put("type", "0,1,2,3");
        searchParams.put("categories", type + "");
        searchParams.put("simple", "1");
        searchParams.put("latlng", locationString);

        final Map<String, String> createCircleParams = new HashMap<String, String>();
        createCircleParams.put("name", keyword);
        createCircleParams.put("cover", UserDataHelper.getUser().getCover());
        createCircleParams.put("description", GTDApplication.getInstance().getString(R.string.circle_desc_default));
        createCircleParams.put("type", type + "");

        ApiManager.getApiClient().searchMerchant(searchParams).flatMap(new Func1<SearchMerchantResponse, Observable<CircleResponse>>() {
            @Override
            public Observable<CircleResponse> call(SearchMerchantResponse searchMerchantResponse) {

                //
//                if (searchMerchantResponse.getUsers() != null && searchMerchantResponse.getUsers().size() == 0) {
//                    EventBus.getDefault().post(new NotMerchantFindEvent());
//                    return null;
//                }

                List<String> ids = new ArrayList<String>();

                for (User user : searchMerchantResponse.getUsers()) {
                    ids.add(user.getId());
                }

                createCircleParams.put("people", TextUtils.join(",", ids));

                return ApiManager.getApiClient().createCircle(createCircleParams);
            }
        }).subscribe(new Action1<CircleResponse>() {
            @Override
            public void call(CircleResponse circleResponse) {
                post(new AddCircleEvent(SUCCEED, circleResponse));
                post(new SearchMerchantEvent(SUCCEED));
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                postErrorEvent(throwable, new SearchMerchantEvent(FAILED));
            }
        });
    }

    public void updateCircle(String coverPath, Map<String, String> params) {
        post(new UpdateCircleEvent(ONGOING));

        TypedFile typedFile = null;
        if (!TextUtils.isEmpty(coverPath)) {
            typedFile = new TypedFile("image/jpeg", new File(coverPath));
        }

        ApiManager.getApiClient().updateCircle(typedFile, params, new Callback<CircleResponse>() {
            @Override
            public void success(CircleResponse circleResponse, Response response) {
                //signInResponse.saveToPref();

                post(new UpdateCircleEvent(circleResponse.isSuccess() ? SUCCEED : FAILED));

            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new GetCirclesEvent(FAILED));
            }
        });
    }

    public void getCircles() {
        post(new GetCirclesEvent(ONGOING));

        ApiManager.getApiClient().getUserCircles(1, new Callback<CirclesResponse>() {
            @Override
            public void success(CirclesResponse circlesResponse, Response response) {
                //signInResponse.saveToPref();
                post(new GetCirclesEvent(SUCCEED, circlesResponse));
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new GetCirclesEvent(FAILED));
            }
        });
    }

    public void getCircleDetail(String id) {
        post(new GetCircleDetailEvent(ONGOING));

        ApiManager.getApiClient().getCircle(id, new Callback<CircleResponse>() {
            @Override
            public void success(CircleResponse circleResponse, Response response) {
                //signInResponse.saveToPref();
                post(new GetCircleDetailEvent(SUCCEED, circleResponse));
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new GetCircleDetailEvent(FAILED));
            }
        });
    }


    public void getService(final String serviceId) {
        post(new GetServiceEvent(ONGOING));

        ApiManager.getApiClient().serviceDetail(serviceId, new Callback<ServiceResponse>() {
            @Override
            public void success(ServiceResponse serviceResponse, Response response) {

                //signInResponse.saveToPref();
                if (serviceResponse.isSuccess()) {
                    post(new GetServiceEvent(SUCCEED, serviceResponse));
                } else {
                    postErrorEvent(new Throwable(serviceResponse.getMessage()), new GetServiceEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new GetServiceEvent(FAILED));
            }
        });
    }

    public void uploadFile(final Message newMessage, String filePath, String messageType) {
        post(new UploadFileEvent(ONGOING));

        ApiManager.getApiClient().uploadFile(new TypedFile(ChatConstants.messageTypeMimeType(messageType), new File(filePath)), new Callback<UploadFileResponse>() {
            @Override
            public void success(UploadFileResponse uploadFileResponse, Response response) {

                //signInResponse.saveToPref();
                if (uploadFileResponse.isSuccess()) {
                    post(new UploadFileEvent(SUCCEED, uploadFileResponse, newMessage));
                } else {
                    postErrorEvent(new Throwable(uploadFileResponse.getMessage()), new UploadFileEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new UploadFileEvent(FAILED));
            }
        });
    }


    public void signInAll(final String email, final String pwd) {
        post(new SignInEvent(ONGOING));
        UserDataHelper.setLoggingIn(true);
        Observable.combineLatest(ApiManager.getApiClient().signIn(email, pwd), ApiManager.getAccountClient().signIn(email, pwd), new Func2<SignInResponse, AccountSignInResponse, Object>() {
            @Override
            public Object call(SignInResponse signInResponse, AccountSignInResponse accountSignInResponse) {
                Log.d("", " testing call " + accountSignInResponse.getError());

                if (signInResponse.isSuccess()) {
                    signInResponse.saveToPref();
                    accountSignInResponse.saveToPref();
                    UserDataHelper.savePwd(pwd);
                    UserDataHelper.saveEmail(email);
                    post(new SignInEvent(SUCCEED));
                    return null;
                } else {
                    postErrorEvent(new Throwable(!signInResponse.isSuccess() ? signInResponse.getError() : accountSignInResponse.getError()), new SignInEvent(FAILED));
                }
                UserDataHelper.setLoggingIn(false);
                return null;
            }
        }).subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                UserDataHelper.setLoggingIn(false);
                postErrorEvent(throwable, new SignInEvent(FAILED));
            }
        });

//        ApiManager.getAccountClient().signIn(email, pwd, new Callback<AccountSignInResponse>() {
//            @Override
//            public void success(AccountSignInResponse signInResponse, Response response) {
//                if (signInResponse.getStatus() == 1) {
//                    signInResponse.saveToPref();
//                    post(new SignInEvent(SUCCEED, signInResponse));
//                } else {
//                    postErrorEvent(new Throwable(signInResponse.getError()), new SignInEvent(FAILED));
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                postErrorEvent(error, new SignInEvent(FAILED));
//            }
//        });
    }


//    public void signIn(String email, String pwd) {
//        post(new UserSignInEvent(ONGOING));
//
//        ApiManager.getApiClient().signIn(email, pwd, new Callback<SignInResponse>() {
//            @Override
//            public void success(SignInResponse signInResponse, Response response) {
//                if (signInResponse.isSuccess()) {
//                    signInResponse.saveToPref();
//                    post(new UserSignInEvent(SUCCEED, signInResponse));
//                } else {
//                    postErrorEvent(new Throwable(signInResponse.getMessage()), new UserSignInEvent(FAILED));
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                postErrorEvent(error, new UserSignInEvent(FAILED));
//            }
//        });
//    }

    public void forgotPWD(String email) {
        post(new ForgotPWDEvent(ONGOING));

        ApiManager.getApiClient().forgotPWD(email, new Callback<BaseResponse>() {
            @Override
            public void success(BaseResponse baseResponse, Response response) {
                if (baseResponse.isSuccess()) {
                    post(new ForgotPWDEvent(SUCCEED, baseResponse));
                } else {
                    postErrorEvent(new Throwable(baseResponse.getMessage()), new ForgotPWDEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new ForgotPWDEvent(FAILED));
            }
        });
    }

//    public void userSignUp(HashMap<String, String> params) {
//        post(new UserSignUpEvent(ONGOING));
//
//        ApiManager.getApiClient().userSignUp(params, new Callback<BaseResponse>() {
//            @Override
//            public void success(BaseResponse baseResponse, Response response) {
//                if (baseResponse.isSuccess()) {
//                    post(new UserSignUpEvent(SUCCEED));
//                } else {
//                    postErrorEvent(new Throwable(baseResponse.getMessage()), new UserSignUpEvent(FAILED));
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                postErrorEvent(error, new UserSignUpEvent(FAILED));
//            }
//        });
//    }

//    public void merchantSignUp(HashMap<String, String> params) {
//        post(new UserSignUpEvent(ONGOING));
//
//        ApiManager.getApiClient().merchantSignUp(params, new Callback<BaseResponse>() {
//            @Override
//            public void success(BaseResponse baseResponse, Response response) {
//                if (baseResponse.isSuccess()) {
//                    UserDataHelper.setMerchant(true);
//                    post(new UserSignUpEvent(SUCCEED));
//                } else {
//                    postErrorEvent(new Throwable(baseResponse.getMessage()), new UserSignUpEvent(FAILED));
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                postErrorEvent(error, new UserSignUpEvent(FAILED));
//            }
//        });
//    }
//
//    public void accountSignUp(Map<String, String> params) {
//        post(new AccountSignUpEvent(ONGOING));
//
//        ApiManager.getAccountClient().signUp(params, new Callback<AccountSimpleResponse>() {
//            @Override
//            public void success(AccountSimpleResponse response1, Response response) {
//                if (response1.getStatus() == 1) {
//                    post(new AccountSignUpEvent(SUCCEED));
//                } else {
//                    postErrorEvent(new Throwable(response1.getError()), new AccountSignUpEvent(FAILED));
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                postErrorEvent(error, new AccountSignUpEvent(FAILED));
//            }
//        });
//    }

    public void accountSignIn(String email, String pwd) {
        post(new AccountSignInEvent(ONGOING));


        ApiManager.getAccountClient().signIn(email, pwd).subscribe(new Action1<AccountSignInResponse>() {
            @Override
            public void call(AccountSignInResponse accountSignInResponse) {
                if (accountSignInResponse.getStatus() == 1) {
                    accountSignInResponse.saveToPref();
                    post(new AccountSignInEvent(SUCCEED));
                } else {
                    postErrorEvent(new Throwable(accountSignInResponse.getError()), new AccountSignInEvent(FAILED));
                }
            }
        },new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                postErrorEvent(throwable, new AccountSignInEvent(FAILED));
            }
        });

//        , new Callback<AccountSignInResponse>() {
//            @Override
//            public void success(AccountSignInResponse signInResponse, Response response) {
//
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//            }
//        });
    }

    public void accountApplyCredit(String amount, String discount, String remark) {
        Account account = UserDataHelper.getAccount();

        if (account == null) return;

        post(new ApplyCreditEvent(ONGOING));

        ApiManager.getAccountClient().applyCredit(account.getId(), amount, discount, remark, new Callback<AccountSimpleResponse>() {
            @Override
            public void success(AccountSimpleResponse response, Response response1) {
                if (response.getStatus() == 1) {
                    post(new ApplyCreditEvent(SUCCEED));
                } else {
                    postErrorEvent(new Throwable(response.getError()), new ApplyCreditEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new ApplyCreditEvent(FAILED));
            }
        });
    }

    public void accountSendCredit(String receiver, String amount, String remark) {
        Account account = UserDataHelper.getAccount();

        if (account == null) return;

        post(new SendCreditEvent(ONGOING));

        ApiManager.getAccountClient().sendCredit(account.getId(), amount, receiver, remark).subscribe(new Action1<AccountSimpleResponse>() {
            @Override
            public void call(AccountSimpleResponse accountBaseResponse) {
                if (accountBaseResponse.getStatus() == 1) {
                    post(new SendCreditEvent(SUCCEED));
                } else {
                    postErrorEvent(new Throwable(accountBaseResponse.getError()), new SendCreditEvent(FAILED));
                }
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                postErrorEvent(throwable, new SendCreditEvent(FAILED));
            }
        });
    }

    public void accountTransferBalance(String receiver, String amount, String remark) {
        Account account = UserDataHelper.getAccount();

        if (account == null) return;

        post(new TransferBalanceEvent(ONGOING));

        ApiManager.getAccountClient().transferBalance(account.getId(), amount, receiver, remark).subscribe(new Action1<AccountSimpleResponse>() {
            @Override
            public void call(AccountSimpleResponse accountBaseResponse) {
                if (accountBaseResponse.getStatus() == 1) {
                    post(new TransferBalanceEvent(SUCCEED));
                } else {
                    postErrorEvent(new Throwable(accountBaseResponse.getError()), new TransferBalanceEvent(FAILED));
                }
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                postErrorEvent(throwable, new TransferBalanceEvent(FAILED));
            }
        });
    }

    public void accountRefund(String transactionId, String desc) {
        Account account = UserDataHelper.getAccount();

        if (account == null) return;

        post(new RefundEvent(ONGOING));

        ApiManager.getAccountClient().refund(account.getId(), "REFUND", transactionId, desc,new Callback<AccountSimpleResponse>() {
            @Override
            public void success(AccountSimpleResponse accountSimpleResponse, Response response) {
                if (accountSimpleResponse.getStatus() == 1) {
                    post(new RefundEvent(SUCCEED));
                } else {
                    postErrorEvent(new Throwable(accountSimpleResponse.getError()), new RefundEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new RefundEvent(FAILED));
            }
        });

    }

    public void accountGetRebateRate(String email) {
        Account account = UserDataHelper.getAccount();
        if (account == null) return;

        post(new GetRebateRateEvent(ONGOING));

        ApiManager.getAccountClient().getMerchantRate(email, new Callback<AccountMerchantRateResponse>() {
            @Override
            public void success(AccountMerchantRateResponse response, Response response1) {
                if (response.getStatus() == 1) {
                    post(new GetRebateRateEvent(SUCCEED, response.getData().getRebaterate()));
                } else {
                    postErrorEvent(new Throwable(response.getError()), new GetRebateRateEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new GetRebateRateEvent(FAILED));
            }
        });
    }

    public void accountReactivate(String email) {

        post(new ReactivateAccountEvent(ONGOING));

        ApiManager.getAccountClient().reactive(email, new Callback<AccountBaseResponse>() {
            @Override
            public void success(AccountBaseResponse response, Response response1) {
                if (response.getStatus() == 1) {
                    post(new ReactivateAccountEvent(SUCCEED));
                } else {
                    postErrorEvent(new Throwable(response.getError()), new ReactivateAccountEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new ReactivateAccountEvent(FAILED));
            }
        });
    }

    public void checkAccount() {
        Account account = UserDataHelper.getAccount();

        if (account == null) return;

        post(new CheckAccountEvent(ONGOING));

        ApiManager.getAccountClient().checkAccount(account.getId(), new Callback<AccountCheckResponse>() {
            @Override
            public void success(AccountCheckResponse response, Response response1) {
                if (response.getStatus() == 1) {
                    UserDataHelper.setBalance((float) response.getData().getBalance());
                    UserDataHelper.setCredit((float) response.getData().getCredits());

                    post(new CheckAccountEvent(SUCCEED, response));
                } else {
                    postErrorEvent(new Throwable(response.getError()), new ApplyCreditEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new ApplyCreditEvent(FAILED));
            }
        });
    }

    public void accountQRPay(String amount, String product) {
        Account account = UserDataHelper.getAccount();

        if (account == null) return;

        post(new QRPayEvent(ONGOING));

        ApiManager.getAccountClient().qrPay(account.getId(), amount, product, new Callback<AccountQRPayResponse>() {
            @Override
            public void success(AccountQRPayResponse response, Response response1) {
                if (response.getStatus() == 1) {
                    post(new QRPayEvent(SUCCEED, response));
                } else {
                    postErrorEvent(new Throwable(response.getError()), new QRPayEvent(FAILED));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new QRPayEvent(FAILED));
            }
        });
    }

    public void accountCreditRecord() {
        Account account = UserDataHelper.getAccount();

        if (account == null) return;

        post(new CreditRecordEvent(ONGOING));

        ApiManager.getAccountClient().creditRecord(account.getId(), new Callback<AccountCreditRecordResponse>() {
            @Override
            public void success(AccountCreditRecordResponse response, Response response1) {
                post(new CreditRecordEvent(SUCCEED, response));
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new CreditRecordEvent(FAILED));
            }
        });
    }

    public void accountUnionPay(UnionPayRequest request) {
        Account account = UserDataHelper.getAccount();

        if (account == null) return;

        post(new UnionPayEvent(ONGOING));

        ApiManager.getAccountClient().unionPay(request.customerName, request.orderamount , request.ordernumber , request.receiver_id, request.type, new Callback<Response>() {
            @Override
            public void success(Response response, Response response1) {

                //Try to get response body
                BufferedReader reader = null;
                StringBuilder sb = new StringBuilder();
                try {

                    reader = new BufferedReader(new InputStreamReader(response.getBody().in()));

                    String line;

                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                post(new UnionPayEvent(SUCCEED, sb.toString()));
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new UnionPayEvent(FAILED));
            }
        });
    }

    public void accountCreditCardTopup(CreditCardTopupRequest request) {
        Account account = UserDataHelper.getAccount();

        if (account == null) return;

        post(new UnionPayEvent(ONGOING));

        ApiManager.getAccountClient().creditCardTopup(request.cardNumber, request.expMonth , request.expYear , request.securityCode, request.customerEmail, request.firstName , request.member, request.memberCurrency, request.noc, request.price,new Callback<Response>() {
            @Override
            public void success(Response response, Response response1) {

                //Try to get response body
                BufferedReader reader = null;
                StringBuilder sb = new StringBuilder();
                try {

                    reader = new BufferedReader(new InputStreamReader(response.getBody().in()));

                    String line;

                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                post(new UnionPayEvent(SUCCEED, sb.toString()));
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new UnionPayEvent(FAILED));
            }
        });
    }

    public void accountBalanceRecord(final int type) {
        Account account = UserDataHelper.getAccount();

        if (account == null) return;

        post(new BalanceRecordEvent(ONGOING));

        ApiManager.getAccountClient().balanceRecord(account.getId(), type, new Callback<AccountBalanceRecordResponse>() {
            @Override
            public void success(AccountBalanceRecordResponse response, Response response1) {
                post(new BalanceRecordEvent(SUCCEED, response, type));
            }

            @Override
            public void failure(RetrofitError error) {
                postErrorEvent(error, new BalanceRecordEvent(FAILED));
            }
        });
    }

    public void payMerchant(String merchantId, double amount, double credit) {

        Account account = UserDataHelper.getAccount();
        if (account == null) return;
        post(new OrderPaymentEvent(ONGOING));
//        Observable.combineLatest(ApiManager.getAccountClient().sendCredit(account.getId(), credit + "", "admin@gtdollar.com", "GT Discount"), ApiManager.getAccountClient().transferBalance(account.getId(), amount + "", merchantId, "paid for merchant"), new Func2<AccountSimpleResponse, AccountSimpleResponse, Object>() {
//            @Override
//            public Object call(AccountSimpleResponse accountBaseResponse, AccountSimpleResponse accountBaseResponse2) {
//                if (accountBaseResponse.getStatus() == 1 && accountBaseResponse2.getStatus() == 1) {
//                    return new Object();
//                }
//                return null;
//            }
//        })
        ApiManager.getAccountClient().payMerchant(account.getId(), amount+"", merchantId, credit+"")

            .subscribe(new Action1<AccountSimpleResponse>() {
                @Override
                public void call(AccountSimpleResponse response) {
                    if(response.getStatus() == 1) {
                        post(new OrderPaymentEvent(SUCCEED));
                    }else {
                        postErrorEvent(new Throwable(response.getError()), new OrderPaymentEvent(FAILED));
                    }
                }
            }, new Action1<Throwable>() {
                @Override
                public void call(Throwable throwable) {
                    postErrorEvent(throwable, new OrderPaymentEvent(FAILED));
                }
            });

    }


    public void getFeeds() {

        final Account account = UserDataHelper.getAccount();
        if (account == null) return;

        Observable.combineLatest(ApiManager.getAccountClient().getActivities(), ApiManager.getApiClient().getNotifications(), new Func2<AccountActivitiesResponse, NotificationResponse, Object>() {
            @Override
            public Object call(AccountActivitiesResponse accountActivitiesResponse, NotificationResponse notificationResponse) {

                Log.d("", " testing start ");

                FeedDao feedDao = GTDApplication.getInstance().getDaoSession().getFeedDao();

                List<Feed> readFeeds = feedDao.queryBuilder().where(FeedDao.Properties.IsRead.eq(true)).list();

                HashSet<String> feedIdsType1 = new HashSet<String>();
                HashSet<String> feedIdsType2 = new HashSet<String>();

                List<Feed> feeds = new ArrayList<Feed>();

                for (Feed readFeed : readFeeds) {
                    if (readFeed.getBaseType() == Feed.TYPE_ACCOUNT) {
                        feedIdsType1.add(readFeed.getExtraId());
                    } else if (readFeed.getBaseType() == Feed.TYPE_NOTIFICATIONS) {
                        feedIdsType2.add(readFeed.getExtraId());
                    }
                }

                Feed temp;

                if (accountActivitiesResponse.getData() != null && accountActivitiesResponse.getData().size() > 0) {
                    for (TransactionActivity activity : accountActivitiesResponse.getData()) {
                        long createdAt = Formatter.getTimeStamp(activity.getCreated(), Formatter.PAYMENT_ACTIVITY_DATE_FORMAT);
                        temp = new Feed();
                        temp.setBaseType(Feed.TYPE_ACCOUNT);
                        temp.setType(Integer.parseInt(activity.getActivityType()));
                        temp.setCreatedAt(createdAt);
                        temp.setExtraId(createdAt+"");

                        if (temp.getType() == Feed.SUBTYPE_ACCOUNT_SYSTEM_APPROVED || temp.getType() == Feed.SUBTYPE_ACCOUNT_SYSTEM_SEND) {
                            temp.setSubjectName(GTDApplication.getInstance().getString(R.string.feed_account_system_subject));
                        } else {
                            temp.setSubjectName(Const.ACTION_SENDER.equals(activity.getAction()) ? activity.getReceiver() : activity.getSender());
                        }

                        temp.setSubjectId(activity.getAction());

                        temp.setSubjectAvatar(temp.getType() != Feed.SUBTYPE_ACCOUNT_SYSTEM_APPROVED && temp.getType() != Feed.SUBTYPE_ACCOUNT_SYSTEM_SEND && Const.ACTION_SENDER.equals(activity.getAction()) ? activity.getReceiverCurrency() : activity.getSenderCurrency());
                        temp.setExtra(activity.getAmount());

                        temp.setIsRead(feedIdsType1.contains(temp.getExtraId()));
                        feeds.add(temp);
                    }
                }

                if (notificationResponse.getNotifications() != null && notificationResponse.getNotifications().size() > 0) {
                    for (Notification notification : notificationResponse.getNotifications()) {

                        long createdAt = Formatter.getTimeStamp(notification.getTime(), Formatter.NOTIFICATION_DATE_FORMAT);
                        temp = new Feed();
                        temp.setBaseType(Feed.TYPE_NOTIFICATIONS);
                        temp.setType(notification.getType());
                        temp.setCreatedAt(createdAt);
                        temp.setExtraId(notification.getId());

                        GTUser user = notification.getBy();
                        if (user != null) {
                            temp.setSubjectName(user.getName());
                            temp.setSubjectId(user.getId());
                            temp.setSubjectAvatar(user.getPicture());

                        }
                        try {
                            if (notification.getActivityData() != null && notification.getActivityData().size() > 0) {
                                temp.setExtra(JSONParser.getFormatJson(notification.getActivityData().get(0).getOrder()));
                            }
                        } catch (Exception e) {
                        }

                        temp.setIsRead(feedIdsType2.contains(temp.getExtraId()));

                        feeds.add(temp);
                    }
                }

                feedDao.queryBuilder().where(FeedDao.Properties.BaseType.notEq(Feed.TYPE_CHAT)).whereOr(FeedDao.Properties.BaseType.notEq(Feed.TYPE_NOTIFICATIONS), FeedDao.Properties.Type.notEq(Feed.SUBTYPE_NOTIFY_SUBMIT_ORDER)).buildDelete().executeDeleteWithoutDetachingEntities();
                feedDao.insertInTx(feeds);
                Log.d("", " testing feed size " + feeds.size());
                return new Object();
            }
        }).subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                post(new GetFeedsEvent(SUCCEED));
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                Log.d("", " testing feed failed ");
                postErrorEvent(throwable, new GetFeedsEvent(FAILED));
            }
        });

    }
}
