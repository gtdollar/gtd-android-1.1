//package mobileapps.sumedia.gtdollarandroid.ui;
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.widget.ImageView;
//import android.widget.ScrollView;
//import android.widget.TextView;
//
//import com.squareup.picasso.Picasso;
//
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import mobileapps.sumedia.gtdollarandroid.BaseActivity;
//import mobileapps.sumedia.gtdollarandroid.R;
//import mobileapps.sumedia.gtdollarandroid.helper.Const;
//import mobileapps.sumedia.gtdollarandroid.model.Service;
//import mobileapps.sumedia.gtdollarandroid.model.User;
//
//public class ShoppingItemDetailActivity extends BaseActivity {
//
//    @InjectView(R.id.itemImageView)
//    ImageView mItemImageView;
//    @InjectView(R.id.priceTextView)
//    TextView mPriceTextView;
//    @InjectView(R.id.typeTextView)
//    TextView mTypeTextView;
//    @InjectView(R.id.detailTextView)
//    TextView mDetailTextView;
//    @InjectView(R.id.scrollView)
//    ScrollView mScrollView;
//
//    Service service;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_shopping_item_detail);
//        ButterKnife.inject(this);
//        service = (Service) getIntent().getSerializableExtra(Const.INTENT_SERVICES_KEY);
//        setUpPage();
//    }
//
//    private void setUpPage()
//    {
//        Picasso.with(this).load(service.getPhoto()).into(mItemImageView);
//        mPriceTextView.setText(String.format("$%.2f",service.getPrice()));
//        mTypeTextView.setText(String.format(service.getType()+""));
//        mDetailTextView.setText(service.getDescription());
//    }
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.shopping_item_detail, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//}
