package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.model.BalanceRecord;

/**
 * Created by 2359media on 4/7/14.
 */

public class BalanceRecordAdapter extends PagingBaseAdapter<BalanceRecord> {

    int colorRowEven, colorRowOdd;

    public BalanceRecordAdapter(Context ctx) {
        super(ctx);
        colorRowEven = getContext().getResources().getColor(R.color.row_even);
        colorRowOdd = getContext().getResources().getColor(R.color.row_odd);
    }

    @Override
    protected int getViewResourceId(int position) {
        return R.layout.item_record;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId().hashCode();
    }

    @Override
    public Renderer<BalanceRecord> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }


    class ViewHolder implements Renderer<BalanceRecord> {
        @InjectView(R.id.tv_sender)
        TextView mTvSender;
        @InjectView(R.id.tv_receiver)
        TextView mTvReceiver;
        @InjectView(R.id.tv_amount)
        TextView mTvAmount;
        @InjectView(R.id.tv_date)
        TextView mTvDate;

        View view;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
            this.view = view;
        }

        public void render(BalanceRecord item, int position) {
            mTvReceiver.setText(item.getRecvuser());
            mTvSender.setText(item.getSenduser());
            boolean received = item.getReceiver().equalsIgnoreCase(UserDataHelper.getAccount().getId());
            mTvAmount.setText(item.getCurrency()+" "+(received?"+":"-")+item.getOamountEx());
            mTvDate.setText(item.getTdate());

            view.setBackgroundColor(position%2==0?colorRowEven:colorRowOdd);
        }
    }
}
