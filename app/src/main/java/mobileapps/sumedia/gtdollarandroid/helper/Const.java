package mobileapps.sumedia.gtdollarandroid.helper;

import mobileapps.sumedia.gtdollarandroid.GTDApplication;
import mobileapps.sumedia.gtdollarandroid.R;

/**
 * Created by zhuodong on 7/22/14.
 */
public class Const {
    public static final String DEFAULT_ERROR_MSG = GTDApplication.getInstance()
        .getString(R.string.error_default_msg);

    public static final String INTENT_EXTRA_MERCHANT_KEY = "key_merchant";
    public static final String INTENT_SERVICES_KEY = "key_services";
    public static final String INTENT_SELECTED_SERVICES_KEY = "key_selected_services";
    public static final String INTENT_EXTRA_REVIEW_OPTIONS = "extra_review_options";
    public static final String INTENT_EXTRA_REVIEW_TYPE = "extra_review_type";

    public static final String INTENT_EXTRA_USER_NAME = "extra_username";
    public static final String INTENT_EXTRA_USER_AVATAR = "extra_useravatar";
    public static final String INTENT_EXTRA_LATLNG = "extra_latlng";
    public static final String INTENT_EXTRA_URL = "extra_url";
    public static final String INTENT_EXTRA_RECORD_STATUS = "extra_record_status";


    public static final int REVIEW_APPLY_CREDIT = 1;
    public static final int REVIEW_SEND_CREDIT = 2;
    public static final int REVIEW_TRANSFER_BALANCE = 3;


    public static final int REQUEST_IMAGE_CAPTURE = 1000;
    public static final int REQUEST_IMAGE_PICK = 1001;
    public static final int REQUEST_VIDEO_CAPTURE = 1002;
    public static final int REQUEST_START_STANDALONE_PLAYER = 1003;
    public static final int REQUEST_RESOLVE_SERVICE_MISSING = 1004;

    public static final int IMAGE_MAX_WIDTH = 800;
    public static final int IMAGE_MAX_HEIGHT = 800;
    public static final int VIDEO_LENGTH_LIMIT = 30;
    public static final int VIDEO_SIZE_LIMIT = 5242880;

    public static final float ZOOM_LEVEL = 14;

    public static final String INTENT_EXTRA_CREDIT_RECORD = "extra_credit_record";
    public static final String INTENT_EXTRA_BALANCE_RECORD = "extra_balance_record";
    public static final String INTENT_EXTRA_CIRCLE = "extra_circle";
    public static final String INTENT_EXTRA_MERCHANT_ID = "extra_merchant_id";
    public static final String INTENT_EXTRA_NAME = "extra_name";
    public static final String INTENT_EXTRA_LOCATION = "extra_location";
    public static final String INTENT_EXTRA_KEYWORD = "extra_keyword";
    public static final String INTENT_EXTRA_SERVICE = "extra_service";
    public static final String INTENT_EXTRA_SERVICE_LIST = "extra_service_list";
    public static final String INTENT_EXTRA_CONTACT = "extra_contact";
    public static final String INTENT_EXTRA_PLACE_KEY = "extra_place";


    public static final int ORDER_STATUS_PENDING = 0;
    public static final int ORDER_STATUS_CONFIRMED = 1;
    public static final int ORDER_STATUS_CANCELLED = -1;
    public static final String INTENT_EXTRA_ORDER = "extra_order";
    public static final String INTENT_EXTRA_AMOUNT = "extra_amount";
    public static final String INTENT_EXTRA_CREDIT = "extra_credit";
    public static final String INTENT_EXTRA_POSITION = "extra_position";

    public static final String ACTION_SENDER = "sender";
    public static final String ACTION_RECEIVER = "receiver";
    public static final String INTENT_EXTRA_PHOTO = "extra_photo";
    public static final String INTENT_EXTRA_IS_UPDATING = "is_updating";
    public static final String INTENT_EXTRA_BALANCE = "extra_balance";
    public static final String INTENT_EXTRA_REFRESH_TOKEN = "extra_refresh_token";
    public static final String INTENT_EXTRA_MERCHANT_TYPE = "extra_merchant_type";
    public static final String INTENT_EXTRA_SPECIFICSEARCH = "extra_specific_search";
    public static final String INTENT_EXTRA_DISPLAY_REFUND = "extra_display_refund";
    public static final String INTENT_EXTRA_NEW_MERCHANT = "extra_new_merchant";
    public static final String INTENT_EXTRA_CIRCLE_ID = "extra_circle_id";
}
