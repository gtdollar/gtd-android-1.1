package mobileapps.sumedia.gtdollarandroid.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.List;

import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.PagingBaseAdapter;


public class PagingListView extends ListView {

    private boolean isLoading;
    private boolean hasMoreItems;
    private Pagingnable pagingnableListener;
    private View loadingView;
    boolean isTopLoading;
    private OnScrollListener onScrollListener;
    int loadingLayout;


    public PagingListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.PagingListView);
        loadingLayout = a.getResourceId(R.styleable.AspectRatioImageView_ratio, R.layout.layout_loading_chat);
        isTopLoading = a.getBoolean(R.styleable.PagingListView_isTopLoading, false);
        a.recycle();


        isLoading = false;

        loadingView = LayoutInflater.from(getContext()).inflate(loadingLayout, null);

        if (isTopLoading) {
            addHeaderView(loadingView);
        } else {
            addFooterView(loadingView);
        }

        super.setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                //Dispatch to child OnScrollListener
                if (onScrollListener != null) {
                    onScrollListener.onScrollStateChanged(view, scrollState);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                //Dispatch to child OnScrollListener
                if (onScrollListener != null) {
                    onScrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
                }
//                Log.d("", " onScroll totalItemCount " + totalItemCount);
//                Log.d("", " onScroll isLoading " + isLoading);
//                Log.d("", " onScroll hasMoreItems " + hasMoreItems);
//                Log.d("", " onScroll isTopLoading " + isTopLoading);
//                Log.d("", " onScroll firstItem " + firstVisibleItem);
//
                if (totalItemCount > 0) {
                    int lastVisibleItem = firstVisibleItem + visibleItemCount;
                    if (!isLoading && hasMoreItems && (isTopLoading ? firstVisibleItem == 0 : (lastVisibleItem == totalItemCount))) {
                        if (pagingnableListener != null) {
                            isLoading = true;
                            pagingnableListener.onLoadMoreItems();
                        }

                    }
                }
            }
        });
    }

    public PagingListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public boolean isLoading() {
        return this.isLoading;
    }

    public void setIsLoading(boolean isLoading) {
        this.isLoading = isLoading;
    }

    public void setPagingnableListener(Pagingnable pagingnableListener) {
        this.pagingnableListener = pagingnableListener;
    }

    public void setHasMoreItems(boolean hasMoreItems) {
        this.hasMoreItems = hasMoreItems;
        if (!this.hasMoreItems) {
            if (isTopLoading) {
                removeHeaderView(loadingView);
            } else {
                removeFooterView(loadingView);
            }
        }
    }

    public boolean hasMoreItems() {
        return this.hasMoreItems;
    }

    public void onFinishLoading(boolean hasMoreItems) {
        setHasMoreItems(hasMoreItems);
        setIsLoading(false);
    }

    public void onFinishLoading(boolean hasMoreItems, List<? extends Object> newItems, boolean append) {
        setHasMoreItems(hasMoreItems);
        setIsLoading(false);
        if (newItems != null && newItems.size() > 0) {
            ListAdapter adapter = ((HeaderViewListAdapter) getAdapter()).getWrappedAdapter();
            if (adapter instanceof PagingBaseAdapter) {
                ((PagingBaseAdapter) adapter).addMoreItems(newItems,append);
            }
        }
    }

    @Override
    public void setOnScrollListener(OnScrollListener listener) {
        onScrollListener = listener;
    }
}
