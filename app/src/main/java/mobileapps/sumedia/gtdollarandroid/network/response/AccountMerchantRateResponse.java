package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;

/**
 * Created by zhuodong on 9/8/14.
 */
public class AccountMerchantRateResponse extends AccountBaseResponse{
    @Expose
    Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {
        double rebaterate;

        public double getRebaterate() {
            return rebaterate;
        }

        public void setRebaterate(double rebaterate) {
            this.rebaterate = rebaterate;
        }
    }
}
