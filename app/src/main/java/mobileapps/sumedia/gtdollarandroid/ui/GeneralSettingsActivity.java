package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.GTDApplication;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.chat.XMPPService;
import mobileapps.sumedia.gtdollarandroid.event.LanguageChangedEvent;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;

public class GeneralSettingsActivity extends BaseActivity {

    @InjectView(R.id.btn_logout)
    Button mBtnLogout;
    @InjectView(R.id.fl_language)
    FrameLayout mFlLanguage;
    @InjectView(R.id.tv_language)
    TextView mTvLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.inject(this);
        setTitle(R.string.string_settings);
    }

    @OnClick(R.id.btn_logout)
    public void onClickLogout(View view) {
        GTDApplication.getInstance().getDaoSession().getFeedDao().deleteAll();
        GTDApplication.getInstance().getDaoSession().getChatDao().deleteAll();
        UserDataHelper.clear();

        GTDApplication.getInstance().getConfig().destroyConfig();

        Intent xmppServiceIntent = new Intent(this, XMPPService.class);
        stopService(xmppServiceIntent);

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.fl_language)
    public void onClickLanguage(View view) {
        Intent intent = new Intent(this, LanguageListActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTvLanguage.setText(GTDHelper.languageCodeToName(UserDataHelper.getLanguage()));
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    public void onEventMainThread(LanguageChangedEvent event) {
        this.recreate();
    }

}
