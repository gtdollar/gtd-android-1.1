//package mobileapps.sumedia.gtdollarandroid.ui;
//
//import android.app.SearchManager;
//import android.content.Context;
//import android.content.Intent;
//import android.location.Location;
//import android.location.LocationManager;
//import android.os.Bundle;
//import android.text.TextUtils;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.inputmethod.EditorInfo;
//import android.widget.AdapterView;
//import android.widget.EditText;
//import android.widget.ListView;
//import android.widget.SearchView;
//import android.widget.TextView;
//
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import mobileapps.sumedia.gtdollarandroid.R;
//import mobileapps.sumedia.gtdollarandroid.adapter.GoogleMerchantListAdapter;
//import mobileapps.sumedia.gtdollarandroid.event.GetNearbyPlacesEvent;
//import mobileapps.sumedia.gtdollarandroid.helper.Const;
//import mobileapps.sumedia.gtdollarandroid.helper.Utils;
//import mobileapps.sumedia.gtdollarandroid.location.location.LocationActivity;
//import mobileapps.sumedia.gtdollarandroid.model.LatLng;
//import mobileapps.sumedia.gtdollarandroid.network.JobManager;
//
//public class NearbyGooglePlaceActivity extends LocationActivity {
//
//    @InjectView(R.id.lv_merchant)
//    ListView mLvMerchant;
//
//    GoogleMerchantListAdapter merchantListAdapter;
//    @InjectView(R.id.tv_empty)
//    TextView mTvEmpty;
//
//    String keyword;
//
//    LatLng latLng;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_nearby_merchant);
//        ButterKnife.inject(this);
//        setUpListView();
//
//        latLng = getIntent().getParcelableExtra(Const.INTENT_EXTRA_LATLNG);
//
//    }
//
//    public void setUpListView() {
//        merchantListAdapter = new GoogleMerchantListAdapter(this);
//
//        View headerView = LayoutInflater.from(this).inflate(R.layout.include_googlenearby_header, null);
//
//        mLvMerchant.addHeaderView(headerView);
//
//        headerView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(NearbyGooglePlaceActivity.this,MerchantProfileActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        mLvMerchant.setAdapter(merchantListAdapter);
//        mLvMerchant.setEmptyView(mTvEmpty);
//        mLvMerchant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position,
//                                    long id) {
//                int index = position - 1;
//                if(index >= 0 && index < merchantListAdapter.getCount()) {
//                    Utils.viewGoogleMerchant(NearbyGooglePlaceActivity.this, merchantListAdapter.getItem(index));
//                }
//            }
//        });
//        //searchRegisteredMerchants(merchantList,adapter);
//        keyword = getIntent().getStringExtra(Const.INTENT_EXTRA_KEYWORD);
//        setTitle(keyword);
//    }
//
//    @Override
//    public void onConnected(Bundle bundle) {
//        super.onConnected(bundle);
//
//        if(latLng != null) {
//            currentLocation = new Location(LocationManager.GPS_PROVIDER);
//            currentLocation.setLatitude(latLng.getLat());
//            currentLocation.setLongitude(latLng.getLng());
//        }
//        merchantListAdapter.setLocation(currentLocation);
//        if(currentLocation != null)
//            JobManager.getInstance().getNearbyPlaces(currentLocation.getLatitude() + "," + currentLocation.getLongitude(),keyword);
//
//    }
//
//
//    public void onEventMainThread(GetNearbyPlacesEvent event) {
//        Utils.toggleEventPopup(this, event, new Utils.StatusListener<GetNearbyPlacesEvent>() {
//            @Override
//            public void onSucceed(GetNearbyPlacesEvent event) {
//                merchantListAdapter.setItems(event.getResponse().getResults());
//            }
//        });
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main_tab, menu);
//
//        final MenuItem searchItem = menu.findItem(R.id.action_search);
//
//
//        // In version 3.0 and later, sets up and configures the ActionBar SearchView
//        if (Utils.hasHoneycomb()) {
//
//            // Retrieves the system search manager service
//            final SearchManager searchManager =
//                (SearchManager)getSystemService(Context.SEARCH_SERVICE);
//
//            // Retrieves the SearchView from the search menu item
//            final SearchView searchView = (SearchView) searchItem.getActionView();
//
//            searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
//            int searchSrcTextId = getResources().getIdentifier("android:id/search_src_text", null, null);
//            EditText searchEditText = (EditText) searchView.findViewById(searchSrcTextId);
//            if(searchEditText != null) {
//                searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//                    @Override
//                    public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
//                        if (id == EditorInfo.IME_ACTION_SEARCH) {
//                            //searchView.setQuery("",false);
//                            //searchView.onActionViewCollapsed();
//                            Utils.searchGoogleMerchant(NearbyGooglePlaceActivity.this, !TextUtils.isEmpty(textView.getText()) ? textView.getText().toString() : "", latLng);
//                            Utils.hideKeyBoard(NearbyGooglePlaceActivity.this, textView);
//                            textView.clearFocus();
//                            return true;
//                        }
//                        return false;
//                    }
//                });
//            }
//
//            // Assign searchable info to SearchView
//            searchView.setSearchableInfo(
//                searchManager.getSearchableInfo(getComponentName()));
//            // Set listeners for SearchView
//            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//                @Override
//                public boolean onQueryTextSubmit(String queryText) {
//                    // Nothing needs to happen when the user submits the search string
////                    Log.d(""," testing search "+queryText);
////                    searchView.onActionViewCollapsed();
////                    Utils.search(MainTabActivity.this, currentLocation, queryText, null, false);
//
//                    return true;
//                }
//
//                @Override
//                public boolean onQueryTextChange(String newText) {
//                    // Called when the action bar search text has changed.  Updates
//                    // the search filter, and restarts the loader to do a new query
//                    // using the new search string.
//                    //Log.d(""," testing text changed");
//                    return true;
//                }
//            });
//        }
//
//
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//        if (id == R.id.action_search) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Override
//    public boolean isSubscribedEvent() {
//        return true;
//    }
//}
