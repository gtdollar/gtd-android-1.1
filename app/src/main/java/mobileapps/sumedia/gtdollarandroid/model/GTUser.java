package mobileapps.sumedia.gtdollarandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by zhuodong on 7/22/14.
 */
public class GTUser implements Serializable, Parcelable {

    @Expose
    private String id;
    @Expose
    private String name;
    @Expose
    private String email;
    @Expose
    private String phone;
    @Expose
    private String cover;
    @Expose
    private String picture;
    @Expose
    private int type;
    @Expose
    private int gtd;

    public int getGtd() {
        return gtd;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeString(this.cover);
        dest.writeString(this.picture);
        dest.writeInt(this.type);
    }

    public GTUser() {
    }

    private GTUser(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.cover = in.readString();
        this.picture = in.readString();
        this.type = in.readInt();
    }

    public static final Parcelable.Creator<GTUser> CREATOR = new Parcelable.Creator<GTUser>() {
        public GTUser createFromParcel(Parcel source) {
            return new GTUser(source);
        }

        public GTUser[] newArray(int size) {
            return new GTUser[size];
        }
    };
}