package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.MyPostListAdapter;
import mobileapps.sumedia.gtdollarandroid.event.SetMerchantDetailEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.Service;
import mobileapps.sumedia.gtdollarandroid.model.User;

public class ServiceListFragment extends Fragment implements AdapterView.OnItemClickListener {


    MyPostListAdapter adapter;

    User merchant;
    @InjectView(R.id.lv_service)
    ListView lvService;

    public static ServiceListFragment newInstance(User merchant) {
        ServiceListFragment fragment = new ServiceListFragment();
        Bundle args = new Bundle();
        args.putParcelable(Const.INTENT_EXTRA_MERCHANT_KEY, merchant);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        merchant = getArguments().getParcelable(Const.INTENT_EXTRA_MERCHANT_KEY);
        adapter = new MyPostListAdapter(getActivity());
        if(merchant != null) {
            adapter.setItems(merchant.getServices());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service_list, container, false);
        ButterKnife.inject(this, view);

        lvService.setAdapter(adapter);
        lvService.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }


    public void onEventMainThread(SetMerchantDetailEvent event) {

        merchant = event.getMerchant();
        if(merchant != null) {
            adapter.setItems(merchant.getServices());

        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Utils.viewService(getActivity(), (ArrayList<Service>) adapter.getItems(), i);

    }


}
