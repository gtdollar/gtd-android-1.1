package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.model.Circle;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.widget.AspectRatioImageView;

/**
 * Created by 2359media on 4/7/14.
 */

public class CircleListAdapter extends PagingBaseAdapter<Circle> {

    public CircleListAdapter(Context ctx) {
        super(ctx);
//        this.transformation = new RoundedTransformationBuilder()
//            .borderColor(context.getResources().getColor(R.color.soft_green))
//            .borderWidthDp(5)
//            .cornerRadius(ctx.getResources().getDimensionPixelSize(R.dimen.img_corner_radius))
//            .oval(false)
//            .build();
    }
//    Transformation transformation;

    @Override
    protected int getViewResourceId(int position) {
        return R.layout.item_user_circles;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId().hashCode();
    }

    @Override
    public Renderer<Circle> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }


    class ViewHolder implements Renderer<Circle> {
        @InjectView(R.id.iv_product_photo)
        AspectRatioImageView mIvProductPhoto;
        @InjectView(R.id.tv_circle_name)
        TextView mTvCircleName;
        @InjectView(R.id.tv_member_count)
        TextView mTvMemberCount;
        @InjectView(R.id.tv_service_count)
        TextView mTvServiceCount;
        @InjectView(R.id.iv_circle_type)
        ImageView mIvCircleType;
        @InjectView(R.id.iv_delete)
        ImageView mIvDelete;

        Circle circle;

        View view;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
            this.view = view;
        }

        @OnClick(R.id.iv_delete)
        public void onDeleteCircle(View view) {
            JobManager.getInstance().removeCircle(circle.getId());
        }

        public void render(Circle item, int position) {
            this.circle = item;
            if (!TextUtils.isEmpty(item.getCover())) {
                Picasso.with(getContext()).load(item.getCover()).placeholder(R.drawable.transparent).fit().centerCrop().into(mIvProductPhoto);
            } else {
                mIvProductPhoto.setImageResource(R.drawable.transparent);
            }

            mTvCircleName.setText(item.getName());
//            mTvMemberCount.setText(item.getMemberCount() + "");
//            mTvServiceCount.setText(item.getServiceCount() + "");
            mTvMemberCount.setText(item.getMembers().size() + "");
            mTvServiceCount.setText(item.getServices().size() + "");
            mIvCircleType.setImageResource(GTDHelper.merchantTypeResources(item.getType()));
        }
    }
}
