package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.SignInResponse;

/**
 * Created by zhuodong on 7/22/14.
 */
public class UserSignInEvent extends BaseEvent<SignInResponse> {
    public UserSignInEvent(EventStatus eventStatus) {
        super(eventStatus);
    }

    public UserSignInEvent(EventStatus status, SignInResponse response) {
        super(status, response);
    }
}
