package mobileapps.sumedia.gtdollarandroid.network;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zhuodong on 11/25/14.
 */
public class UnionPayRequest implements Parcelable {
    String orderamount;
    String ordernumber;
    String customerName;
    String receiver_id;
    int type = 3;

    public UnionPayRequest(String orderamount, String ordernumber, String customerName, String receiver_id) {
        this.orderamount = orderamount;
        this.ordernumber = ordernumber;
        this.customerName = customerName;
        this.receiver_id = receiver_id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.orderamount);
        dest.writeString(this.ordernumber);
        dest.writeString(this.customerName);
        dest.writeString(this.receiver_id);
        dest.writeInt(this.type);
    }

    private UnionPayRequest(Parcel in) {
        this.orderamount = in.readString();
        this.ordernumber = in.readString();
        this.customerName = in.readString();
        this.receiver_id = in.readString();
        this.type = in.readInt();
    }

    public static final Parcelable.Creator<UnionPayRequest> CREATOR = new Parcelable.Creator<UnionPayRequest>() {
        public UnionPayRequest createFromParcel(Parcel source) {
            return new UnionPayRequest(source);
        }

        public UnionPayRequest[] newArray(int size) {
            return new UnionPayRequest[size];
        }
    };
}
