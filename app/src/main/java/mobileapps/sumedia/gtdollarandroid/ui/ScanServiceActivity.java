package mobileapps.sumedia.gtdollarandroid.ui;

/**
 * Created by zhuodong on 8/26/14.
 */

import android.os.Bundle;
import android.text.TextUtils;

import com.google.zxing.Result;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.event.GetServiceEvent;
import mobileapps.sumedia.gtdollarandroid.event.UpdateOrderEvent;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.Service;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;

public class ScanServiceActivity extends BaseActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;
    boolean startFromMain;

    String merchantId;
    private String serviceId;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
        startFromMain = getIntent().getBooleanExtra("start_from_main",false);
        merchantId = getIntent().getStringExtra("merchant_id");
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
//        Toast.makeText(this, "Contents = " + rawResult.getText() +
//            ", Format = " + rawResult.getBarcodeFormat().toString(), Toast.LENGTH_SHORT).show();

        mScannerView.startCamera();
        if(TextUtils.isEmpty(rawResult.getText()))return;
        serviceId = rawResult.getText();
        JobManager.getInstance().getService(serviceId);
    }

    @Override
    public boolean hasActionBar() {
        return false;
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    public void onEventMainThread(GetServiceEvent event) {
        Utils.toggleEventPopup(this,event,new Utils.StatusListener<GetServiceEvent>() {
            @Override
            public void onSucceed(GetServiceEvent event) {
                Service service = event.getResponse().getService();
                if(service == null)return;

                if(TextUtils.isEmpty(merchantId)) {
                    if(service.getCreator() == null)return;
                    merchantId = service.getCreator().getId();
                }

                if(startFromMain) {
                    UserDataHelper.removePendingOrder();
                    List<Service> services =  new ArrayList<Service>();
                    services.add(service);
                    UserDataHelper.setPendingOrders(services);
                    Utils.viewOrders(ScanServiceActivity.this,merchantId,null);
                    ScanServiceActivity.this.finish();
                }else {
                    UpdateOrderEvent updateOrderEvent = new UpdateOrderEvent(event.getResponse().getService());
                    EventBus.getDefault().post(updateOrderEvent);
                    ScanServiceActivity.this.finish();
                }
            }
        });
    }
}

