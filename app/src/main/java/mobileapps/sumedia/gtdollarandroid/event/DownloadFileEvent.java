package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.Chat;

/**
 * Created by zhuodong on 8/15/14.
 */
public class DownloadFileEvent {
    Chat chat;

    public DownloadFileEvent(Chat chat) {
        this.chat = chat;
    }

    public Chat getChat() {
        return chat;
    }
}
