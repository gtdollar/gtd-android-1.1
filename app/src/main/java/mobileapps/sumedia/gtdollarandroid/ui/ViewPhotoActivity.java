package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.model.GooglePhoto;
import mobileapps.sumedia.gtdollarandroid.network.ApiConfig;

public class ViewPhotoActivity extends BaseActivity {

    @InjectView(R.id.vp_photos)
    ViewPager mVpPhotos;

    List<GooglePhoto> photos = new ArrayList<GooglePhoto>();

    PhotoPageAdapter mPhotoPageAdapter = new PhotoPageAdapter();

    int initialPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_photo);
        ButterKnife.inject(this);

        photos = getIntent().getParcelableArrayListExtra(Const.INTENT_EXTRA_PHOTO);
        initialPosition = getIntent().getIntExtra(Const.INTENT_EXTRA_POSITION,0);
        mVpPhotos.setPageMargin(getResources().getDimensionPixelSize(R.dimen.page_margin));
        mVpPhotos.setAdapter(mPhotoPageAdapter);
        mVpPhotos.setCurrentItem(initialPosition);
    }


    class PhotoPageAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            if(photos == null) return 0;
            return photos.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return object == view;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView iv =  new ImageView(ViewPhotoActivity.this);
            GooglePhoto photo = getPhoto(position);
            if(photo != null && !TextUtils.isEmpty(photo.getPhotoReference())) {
                Picasso.with(ViewPhotoActivity.this).load(ApiConfig.GOOGLE_PHOTOS+photo.getPhotoReference()).fit().centerCrop().into(iv);
            }
            container.addView(iv);
            return iv;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(((View) object));
        }
    }

    private GooglePhoto getPhoto(int position) {
        if(photos != null && position>=0 && position < photos.size()) {
            return photos.get(position);
        }
        return null;
    }
}
