package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 8/17/14.
 */
public class SendLocationEvent {

    double latitude,longitude;
    String address;

    public SendLocationEvent(double latitude, double longitude, String address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getAddress() {
        return address;
    }
}
