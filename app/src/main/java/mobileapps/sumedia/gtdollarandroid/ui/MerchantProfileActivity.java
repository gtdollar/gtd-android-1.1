package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.ChoseItemEvent;
import mobileapps.sumedia.gtdollarandroid.event.ScaleBitmapEvent;
import mobileapps.sumedia.gtdollarandroid.event.UpdateProfileEvent;
import mobileapps.sumedia.gtdollarandroid.event.UserSignUpEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.location.location.LocationUpdateEvent;
import mobileapps.sumedia.gtdollarandroid.model.User;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.widget.CircleImageView;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.validations.IsEmail;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

public class MerchantProfileActivity extends BasePictureActivity {

    @InjectView(R.id.iv_cover)
    ImageView mIvCover;
    @InjectView(R.id.iv_profile)
    CircleImageView mIvProfile;
    @InjectView(R.id.et_name)
    EditText mEtName;
    @InjectView(R.id.et_email)
    EditText mEtEmail;
    @InjectView(R.id.et_website)
    EditText mEtWebsite;
    @InjectView(R.id.et_phone)
    EditText mEtPhone;
    @InjectView(R.id.et_address)
    EditText mEtAddress;
    @InjectView(R.id.tv_type)
    TextView mTvType;
    @InjectView(R.id.tv_region)
    TextView mTvRegion;
    @InjectView(R.id.et_video)
    EditText mEtVideo;
    @InjectView(R.id.et_intro)
    EditText mEtIntro;
    @InjectView(R.id.tv_gps)
    TextView mTvGps;
    @InjectView(R.id.btn_register)
    Button mBtnRegister;

    User merchant;

    boolean isUpdating, isEditPicture, isEditCover;

    Map<String, String> params = new HashMap<String, String>();
    @InjectView(R.id.tv_currency)
    TextView mTvCurrency;

    Form mForm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.inject(this);
        merchant = getIntent().getParcelableExtra(Const.INTENT_EXTRA_MERCHANT_KEY);
        isUpdating = getIntent().getBooleanExtra(Const.INTENT_EXTRA_IS_UPDATING,false);

        if (isUpdating) {
            setTitle(R.string.title_update_profile);
            mBtnRegister.setText(R.string.string_update);
        }else {
            setTitle(R.string.title_new_merchant);
        }
        setMerchantDetails();

        mTvCurrency.setTag(GenericSelectionDialog.TYPE_CURRENCY);
        mTvRegion.setTag(GenericSelectionDialog.TYPE_COUNTRY_ORIGIN);
        mTvType.setTag(GenericSelectionDialog.TYPE_MERCHANT_TYPE);
        mIvCover.setTag(GenericSelectionDialog.TYPE_PICTURE);
        mIvProfile.setTag(GenericSelectionDialog.TYPE_PICTURE);

        buildForm();
    }

    private void buildForm() {
        mForm = new Form(this);
        mForm.addField(Field.using(mEtName).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mEtEmail).validate(NotEmpty.build(this)).validate(
            IsEmail.build(this)));
        //mForm.addField(Field.using(mEtWebsite).validate(NotEmpty.build(this)).validate(HasMinimumLength.build(this,6)));
        mForm.addField(Field.using(mEtPhone).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mTvType).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mEtAddress).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mTvCurrency).validate(NotEmpty.build(this)));
    }

    private void setMerchantDetails() {
        if(merchant == null) return;
        mEtName.setText(merchant.getName());
        mEtEmail.setText(merchant.getEmail());

        mEtPhone.setText(merchant.getPhone());

        mEtAddress.setText(merchant.getAddress());
        mEtWebsite.setText(merchant.getWebsite());

        //mTvType.setVisibility(View.GONE);
        if(isUpdating) {
            mEtEmail.setEnabled(false);

            mEtWebsite.setText(UserDataHelper.getPwd());

            mTvType.setText(GTDHelper.serviceTypeName(merchant.getCategories()));

            //mTvGps.setVisibility(View.GONE);
            if (merchant.getCurrency() != null) {
                mTvCurrency.setText(merchant.getCurrency().getId());
            }
        }

        mTvRegion.setText(merchant.getRegion());

        if (merchant.hasVideo()) {
            mEtVideo.setText(merchant.getVideoId());
        }

        mEtIntro.setText(merchant.getQuotation());
        if (merchant.getLatlong() != null && merchant.getLatlong().notEmpty()) {
            mTvGps.setText(merchant.getLatlong().toString());
        }

        if (!TextUtils.isEmpty(merchant.getCover())) {
            Picasso.with(MerchantProfileActivity.this).load(merchant.getCover()).fit().centerCrop().into(mIvCover);
        }

        if (!TextUtils.isEmpty(merchant.getPicture())) {
            Picasso.with(MerchantProfileActivity.this).load(merchant.getPicture()).into((com.squareup.picasso.Target) mIvProfile);
        }
    }

    @OnClick(R.id.btn_register)
    public void onRegister(View view) {
        if (!isUpdating && !mForm.isValid()) return;

        if (!TextUtils.isEmpty(mEtEmail.getText())) {
            params.put("email", mEtEmail.getText().toString());
        }

        if (!TextUtils.isEmpty(mEtName.getText())) {
            params.put("name", mEtName.getText().toString());
        }

//        if (!TextUtils.isEmpty(mEtWebsite.getText())) {
//            params.put("password", mEtWebsite.getText().toString());
//        }

        if (!TextUtils.isEmpty(mEtPhone.getText())) {
            params.put("phone", mEtPhone.getText().toString());
        }

        if (!TextUtils.isEmpty(mEtIntro.getText())) {
            params.put("quotation", mEtIntro.getText().toString());
        }

        if (!TextUtils.isEmpty(mTvRegion.getText())) {
            params.put("region", mTvRegion.getText().toString());
        }

        if (!TextUtils.isEmpty(mTvType.getText())) {
            params.put("categories", GTDHelper.findType(mTvType.getText().toString()) + "");
        }

        if (!TextUtils.isEmpty(mTvGps.getText())) {
            params.put("latlng", mTvGps.getText().toString());
        }

        if (!TextUtils.isEmpty(mEtVideo.getText())) {
            params.put("videos", mEtVideo.getText().toString());
        }

        if (!TextUtils.isEmpty(mTvCurrency.getText())) {
            params.put("currency", mTvCurrency.getText().toString());
        }

        if (!TextUtils.isEmpty(mEtAddress.getText())) {
            params.put("address", mEtAddress.getText().toString());
        }

        if(Patterns.WEB_URL.matcher(mEtWebsite.getText()).matches()) {
            params.put("website", mEtWebsite.getText().toString());
        }

        if (isUpdating) {
            JobManager.getInstance().updateProfile(params);
        } else {
            JobManager.getInstance().createUser(true, params);
        }

    }

    boolean isGPSTapped;

    @OnClick(R.id.tv_gps)
    public void onClickGPS(View view) {
        if(!isUpdating) return;
        isGPSTapped = true;
        if(currentLocation != null) {
            mTvGps.setText(currentLocation.getLatitude()+","+currentLocation.getLongitude());
        }
    }

    public void onEventMainThread(LocationUpdateEvent event) {
        if(isGPSTapped && !isUpdating) {
            onClickGPS(mTvGps);
        }
    }

    @OnClick({R.id.tv_currency, R.id.tv_type, R.id.tv_region})
    public void onClickEdit(View view) {
        GenericSelectionDialog.newInstance(((Integer) view.getTag()), ((TextView) view).getText()).show(getSupportFragmentManager(), "");
    }

    @OnClick({R.id.iv_cover, R.id.iv_profile})
    public void onClickImage(View view) {
        isEditPicture = false;
        isEditCover = false;

        if (view.getId() == R.id.iv_cover) {
            isEditCover = true;
        } else if (view.getId() == R.id.iv_profile) {
            isEditPicture = true;
        }

        GenericSelectionDialog.newInstance(((Integer) view.getTag()), null).show(getSupportFragmentManager(), "");
    }


    public void onEventMainThread(ChoseItemEvent event) {
        super.onEventMainThread(event);
        switch (event.getType()) {
            case GenericSelectionDialog.TYPE_CURRENCY:
                mTvCurrency.setText(event.getItem());
                break;
            case GenericSelectionDialog.TYPE_COUNTRY_ORIGIN:
                mTvRegion.setText(event.getItem());
                break;
            case GenericSelectionDialog.TYPE_MERCHANT_TYPE:
                mTvType.setText(event.getItem());
                break;
        }
    }

    public void onEventMainThread(ScaleBitmapEvent event) {
        Utils.toggleEventPopup(this, event, new Utils.StatusListener<ScaleBitmapEvent>() {
            @Override
            public void onSucceed(ScaleBitmapEvent event) {
                if (event.getResponse() && !tempPath.isEmpty()) {
                    if (isEditCover) {
                        params.put("cover",tempPath);
                        Picasso.with(MerchantProfileActivity.this).load(new File(tempPath)).fit().centerCrop().into(mIvCover);
                    } else if (isEditPicture) {
                        params.put("picture",tempPath);
                        Picasso.with(MerchantProfileActivity.this).load(new File(tempPath)).into((com.squareup.picasso.Target) mIvProfile);
                    }
                }
            }
        });
    }

    public void onEventMainThread(UpdateProfileEvent event) {
        Utils.toggleEventPopup(this,event,new Utils.StatusListener<UpdateProfileEvent>() {
            @Override
            public void onSucceed(UpdateProfileEvent event) {
                finish();
                Utils.toast(MerchantProfileActivity.this,getString(R.string.toast_updated));
            }
        });
    }

    public void onEventMainThread(UserSignUpEvent event) {
        Utils.toggleEventPopup(this,event,new Utils.StatusListener<UserSignUpEvent>() {
            @Override
            public void onSucceed(UserSignUpEvent event) {
                finish();
                Utils.toast(MerchantProfileActivity.this,getString(R.string.string_created));
            }
        });

    }

    @Override
    public boolean isUpdatesRequested() {
        return true;
    }
}