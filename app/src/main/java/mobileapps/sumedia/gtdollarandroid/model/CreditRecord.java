package mobileapps.sumedia.gtdollarandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zhuodong on 8/21/14.
 */
public class CreditRecord implements Parcelable {

    @Expose
    private String id;
    @SerializedName("receiver_id")
    @Expose
    private String receiverId;
    @SerializedName("sender_id")
    @Expose
    private String senderId;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("credit_amount")
    @Expose
    private String creditAmount;
    @SerializedName("credit_amount_exchange")
    @Expose
    private String creditAmountExchange;
    @Expose
    private String created;
    @Expose
    private String username1;
    @Expose
    private String username2;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(String creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getCreditAmountExchange() {
        return creditAmountExchange;
    }

    public void setCreditAmountExchange(String creditAmountExchange) {
        this.creditAmountExchange = creditAmountExchange;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUsername1() {
        return username1;
    }

    public void setUsername1(String username1) {
        this.username1 = username1;
    }

    public String getUsername2() {
        return username2;
    }

    public void setUsername2(String username2) {
        this.username2 = username2;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.receiverId);
        dest.writeString(this.senderId);
        dest.writeString(this.transactionId);
        dest.writeString(this.creditAmount);
        dest.writeString(this.creditAmountExchange);
        dest.writeString(this.created);
        dest.writeString(this.username1);
        dest.writeString(this.username2);
    }

    public CreditRecord() {
    }

    private CreditRecord(Parcel in) {
        this.id = in.readString();
        this.receiverId = in.readString();
        this.senderId = in.readString();
        this.transactionId = in.readString();
        this.creditAmount = in.readString();
        this.creditAmountExchange = in.readString();
        this.created = in.readString();
        this.username1 = in.readString();
        this.username2 = in.readString();
    }

    public static final Parcelable.Creator<CreditRecord> CREATOR = new Parcelable.Creator<CreditRecord>() {
        public CreditRecord createFromParcel(Parcel source) {
            return new CreditRecord(source);
        }

        public CreditRecord[] newArray(int size) {
            return new CreditRecord[size];
        }
    };
}