package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.model.GTUser;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;

/**
 * Created by 2359media on 4/7/14.
 */

public class CircleMemberListAdapter extends PagingBaseAdapter<GTUser> {

    String[] types;
    int layout;


    public CircleMemberListAdapter(Context ctx, int layout) {
        super(ctx);
        types = ctx.getResources().getStringArray(R.array.merchant_types);
        this.layout = layout;
    }

    @Override
    protected int getViewResourceId(int position) {
        return layout;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId().hashCode();
    }

    @Override
    public Renderer<GTUser> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }


    class ViewHolder implements Renderer<GTUser> {
        @InjectView(R.id.iv_member_photo)
        ImageView mIvMemberPhoto;

        @InjectView(R.id.tv_merchant_name)
        TextView mTvMerchantName;

        @Optional @InjectView(R.id.tv_merchant_distance)
        TextView mTvMerchantDistance;

        @Optional @InjectView(R.id.tv_merchant_type)
        TextView mTvMerchantType;

        @Optional @InjectView(R.id.iv_certified)
        ImageView mIvCertified;

        @Optional
        @InjectView(R.id.tv_service_count)
        TextView mTvServiceCount;

        @Optional @InjectView(R.id.iv_merchant_type)
        ImageView mIvMerchantType;

        @Optional @InjectView(R.id.iv_follow)
        ImageView mIvFollow;

        @Optional @InjectView(R.id.iv_gticon)
        ImageView mIvGTIcon;

        View view;

        GTUser item;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
            this.view = view;
        }

        @OnClick(R.id.iv_delete)
        void onDeleteItem(View view) {
             CircleMemberListAdapter.this.remove(item);
            JobManager.getInstance().removeCircleMember(circleId, item.getId());
        }

        @Optional @OnClick(R.id.iv_follow)
        public void onClickFollow(View view) {
            JobManager.getInstance().addContact(item.getId());
        }

        public void render(GTUser item, int position) {
            this.item = item;

            if (!TextUtils.isEmpty(item.getPicture())) {
                Picasso.with(getContext()).load(item.getPicture()).into(mIvMemberPhoto);
            }else if(!TextUtils.isEmpty(item.getCover())) {
                Picasso.with(getContext()).load(item.getCover()).into(mIvMemberPhoto);
            }


            if(mTvServiceCount != null)mTvServiceCount.setVisibility(View.GONE);
            if(mIvMerchantType != null)mIvMerchantType.setImageResource(GTDHelper.merchantTypeResources(item.getType()));
            if(mTvMerchantDistance != null) {
                mTvMerchantDistance.setVisibility(View.INVISIBLE);
            }

            mTvMerchantName.setText(item.getName());
            //mTvMemberCount.setText(item.getMembers().size() + "");
            if(mIvGTIcon != null)mIvGTIcon.setVisibility(item.getGtd() == 1 ? View.VISIBLE : View.INVISIBLE);


            if(mTvMerchantType != null) {
                if (item.getType() < types.length && item.getType() > 0) {
                    mTvMerchantType.setText(types[item.getType()]);
                } else {
                    mTvMerchantType.setText(types[0]);
                }
            }

        }
    }

    String circleId;

    public void setCircleId(String circleId) {
        this.circleId = circleId;
    }
}
