package mobileapps.sumedia.gtdollarandroid;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;

public abstract class BaseActivity extends ActionBarActivity {

    public boolean isAlive, isPaused, subscribedEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isAlive = true;
        subscribedEvent = isSubscribedEvent();
        Utils.setLanguage(this);
        if (subscribedEvent) {
            EventBus.getDefault().register(this);
        }
        if(!hasActionBar() && getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        if(hasActionBar() && getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(hasBack());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
	    //FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isPaused = false;
        //AppEventsLogger.activateApp(this, getString(R.string.fb_applicationId));
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        isAlive = false;
        if (subscribedEvent) {
            EventBus.getDefault().unregister(this);
        }
    }

    public boolean hasActionBar() {
        return true;
    }

    public boolean isSubscribedEvent() {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected boolean hasBack(){return true;}
}
