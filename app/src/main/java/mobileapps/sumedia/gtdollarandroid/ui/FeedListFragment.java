package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.Feed;
import mobileapps.sumedia.gtdollarandroid.FeedDao;
import mobileapps.sumedia.gtdollarandroid.GTDApplication;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.SimpleBackgroundTask;
import mobileapps.sumedia.gtdollarandroid.adapter.FeedListAdapter;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.GetFeedsEvent;
import mobileapps.sumedia.gtdollarandroid.event.SetRedDotEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.Order;
import mobileapps.sumedia.gtdollarandroid.network.JSONParser;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.widget.PagingListView;
import mobileapps.sumedia.gtdollarandroid.widget.Pagingnable;


@SuppressWarnings("deprecation") /* recent ClipboardManager only available since API 11 */
public class FeedListFragment extends Fragment implements AdapterView.OnItemClickListener {


    @InjectView(R.id.lv_feed)
    PagingListView mLvFeed;

    FeedListAdapter feedListAdapter;

    FeedDao feedDao;

    int pageNo = 1;
    public static final int PAGE_SIZE = 10;


    public static FeedListFragment newInstance() {
        FeedListFragment fragment = new FeedListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        feedDao = GTDApplication.getInstance().getDaoSession().getFeedDao();
        feedListAdapter = new FeedListAdapter(getActivity());

        JobManager.getInstance().getFeeds();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed_list, container, false);
        ButterKnife.inject(this, view);

        mLvFeed.setAdapter(feedListAdapter);
        mLvFeed.setHasMoreItems(true);
        mLvFeed.setPagingnableListener(new Pagingnable() {
            @Override
            public void onLoadMoreItems() {
                Log.d("", " testing load more");
                new LoadingTask(getActivity()).execute();
            }
        });

        mLvFeed.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }


    public void onEventMainThread(GetFeedsEvent event) {
        Log.d("", " testing event " + event.getStatus() + " " + event.getErrorMsg());
        if (event.getStatus() == EventStatus.SUCCEED) {
            pageNo = 1;
            new LoadingTask(getActivity()).execute();

            //adapter.removeAllItems();
            //mLvFeed.setHasMoreItems(true);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Feed feed = feedListAdapter.getItem(i);
        if (!feed.getIsRead()) {
            feed.setIsRead(true);
            feedListAdapter.notifyDataSetChanged();
            feedDao.update(feed);
            checkFeedRead();
        }

        if (feed.getBaseType() == Feed.TYPE_ACCOUNT) {
            Utils.viewEWallet(getActivity());

            return;
        } else if (feed.getBaseType() == Feed.TYPE_NOTIFICATIONS) {
            if (feed.getType() == Feed.SUBTYPE_NOTIFY_FOLLOWING) {
                Utils.viewMerchant(getActivity(), null, feed.getSubjectId());
            } else if (!TextUtils.isEmpty(feed.getExtra())) {
                try {
                    Order data = JSONParser.parseFromString(Order.class, feed.getExtra());
                    Utils.viewOrders(getActivity(), feed.getSubjectId(), data);
                } catch (Exception e) {
                }
            }
        }else if(feed.getBaseType() == Feed.TYPE_CHAT) {
            Utils.chatWith(getActivity(),feed.getExtraId(),feed.getSubjectId(),feed.getSubjectName(), feed.getSubjectAvatar());

            return;
        }
    }

    private void checkFeedRead() {
        for (Feed feed : feedListAdapter.getItems()) {
            if(!feed.getIsRead()) {
                EventBus.getDefault().post(new SetRedDotEvent(true));
                return;
            }
        }
        EventBus.getDefault().post(new SetRedDotEvent(false));
    }

    class LoadingTask extends SimpleBackgroundTask<List<Feed>> {

        public LoadingTask(Activity activity) {
            super(activity);
        }

        @Override
        protected List<Feed> onRun() {
            Log.d("", " testing load");
            return feedDao.queryBuilder().orderDesc(FeedDao.Properties.CreatedAt).limit(PAGE_SIZE).offset((pageNo - 1) * PAGE_SIZE).list();
        }

        @Override
        protected void onSuccess(List<Feed> result) {
            //Collections.reverse(result);
            Log.d("", " testing finish load size "+result.size());
            if(feedListAdapter != null) {
                if (pageNo == 1) {
                    feedListAdapter.removeAllItems();
                }
                feedListAdapter.addMoreItems(result,true);
                checkFeedRead();
            }

            if(mLvFeed != null) {
                mLvFeed.onFinishLoading(result != null && result.size() >= PAGE_SIZE);

                //Log.d("", " testing add more items "+result.size());
                //mLvFeed.onFinishLoading(result != null && result.size() >= PAGE_SIZE, result, true);
            }
            //checkFeedRead();
            //List<Message> items = chatAdapter.getItems();
            //items.addAll(0, result);
            //int index = mLvFeed.getFirstVisiblePosition() + result.size();
            //View v = mLvFeed.getChildAt(0);
            //int top = (v == null) ? 0 : v.getTop();

            //adapter.addMoreItems(result, false);

            //mLvFeed.setSelectionFromTop(index, top);

            pageNo++;
        }
    }


}
