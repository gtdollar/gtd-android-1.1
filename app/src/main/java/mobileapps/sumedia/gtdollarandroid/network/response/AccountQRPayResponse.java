package mobileapps.sumedia.gtdollarandroid.network.response;

/**
 * Created by zhuodong on 8/8/14.
 */
public class AccountQRPayResponse extends AccountBaseResponse {
    Data data;
    public Data getData() {
        return data;
    }

    public class Data{
        public String url;
    }
}
