package mobileapps.sumedia.gtdollarandroid.helper;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.os.Environment.DIRECTORY_ALARMS;
import static android.os.Environment.DIRECTORY_DCIM;
import static android.os.Environment.DIRECTORY_DOWNLOADS;
import static android.os.Environment.DIRECTORY_MOVIES;
import static mobileapps.sumedia.gtdollarandroid.chat.ChatConstants.MessageType;

/**
 * Created by zhuodong on 8/15/14.
 */
public class FileUtils {

    private static final String TAG = "FileUtils";

    static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss_SSS");

    public static void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }


    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
            Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static String messageTypeToFileType(String messageType) {
        if(messageType.equals(MessageType.text.name())) {
            return DIRECTORY_ALARMS;
        }else if(messageType.equals(MessageType.image.name())) {
            return DIRECTORY_DCIM;
        }else if(messageType.equals(MessageType.video.name())) {
            return DIRECTORY_MOVIES;
        }
        return DIRECTORY_DOWNLOADS;
    }

    public static boolean isFileExit(String path) {
        File file = new File(path);
        return file.exists();
    }

    static String getNewFileName() {
        return simpleDateFormat.format(new Date());
    }

    public static File createFile(Context context, String messageType) {

        File file = new File(context.getExternalFilesDir(messageTypeToFileType(messageType)), getNewFileName());

        try {
            if(file.createNewFile())return file;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
