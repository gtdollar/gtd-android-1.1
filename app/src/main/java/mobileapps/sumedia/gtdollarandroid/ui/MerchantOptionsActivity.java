package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.BlackListEvent;
import mobileapps.sumedia.gtdollarandroid.event.BlockListEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;

public class MerchantOptionsActivity extends BaseActivity {

    @InjectView(R.id.switch_blacklist)
    Switch mSwitchBlacklist;
    @InjectView(R.id.switch_block_msg)
    Switch mSwitchBlockMsg;
    @InjectView(R.id.et_feedback)
    EditText mEtFeedback;
    @InjectView(R.id.btn_submit)
    Button mBtnSubmit;

    String merchantId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_options);
        ButterKnife.inject(this);
        merchantId = getIntent().getStringExtra(Const.INTENT_EXTRA_MERCHANT_ID);

        JobManager.getInstance().blackList();
        JobManager.getInstance().blockList();
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    public void onEventMainThread(BlackListEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED) {
            if(event.getResponse() != null && event.getResponse().getList() != null) {
                mSwitchBlacklist.setChecked(event.getResponse().getList().contains(merchantId));
            }
        }
    }

    public void onEventMainThread(BlockListEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED) {
            if(event.getResponse() != null && event.getResponse().getList() != null) {
                mSwitchBlockMsg.setChecked(event.getResponse().getList().contains(merchantId));
            }
        }
    }

    @OnClick(R.id.btn_submit)
    public void onClickSubmit(View view) {
        JobManager.getInstance().block(merchantId,mSwitchBlockMsg.isChecked());
        JobManager.getInstance().black(merchantId, mSwitchBlacklist.isChecked());

        if(!TextUtils.isEmpty(mEtFeedback.getText())){
            JobManager.getInstance().complain(merchantId,mEtFeedback.getText().toString());
        }
        finish();
    }

}
