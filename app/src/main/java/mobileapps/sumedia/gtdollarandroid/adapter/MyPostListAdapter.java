package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.model.Service;

/**
 * Created by 2359media on 4/7/14.
 */

public class MyPostListAdapter extends PagingBaseAdapter<Service> {

    //String[] types;

    public MyPostListAdapter(Context ctx) {
        super(ctx);
        //types = ctx.getResources().getStringArray(R.array.merchant_types);
    }

    @Override
    protected int getViewResourceId(int position) {
        return R.layout.item_service;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId().hashCode();
    }

    @Override
    public Renderer<Service> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }


    class ViewHolder implements Renderer<Service> {
        @InjectView(R.id.tv_service_name)
        TextView mTvServiceName;
        @InjectView(R.id.tv_service_date)
        TextView mTvServiceDate;
        @InjectView(R.id.tv_service_price)
        TextView mTvServicePrice;
        @InjectView(R.id.iv_service_photo)
        ImageView mIvServicePhoto;
        @InjectView(R.id.tv_service_desc)
        TextView mTvServiceDesc;

        View view;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
            this.view = view;
        }

        public void render(Service item, int position) {
            if (!TextUtils.isEmpty(item.getPhoto())) {
                mIvServicePhoto.setVisibility(View.VISIBLE);
                Picasso.with(getContext()).load(item.getPhoto()).fit().centerCrop().into(mIvServicePhoto);
            }else {
                mIvServicePhoto.setVisibility(View.GONE);
            }

            mTvServiceDesc.setText(item.getDescription());
            mTvServiceName.setText(item.getTitle());
            mTvServiceDate.setText(item.getExpire());
            mTvServicePrice.setText(item.getPrice()+"");

            //mTvMemberCount.setText(item.getMembers().size() + "");
//            if (item.getType() < types.length && item.getType() > 0) {
//                mTvMerchantType.setText(types[item.getType()]);
//            } else {
//                mTvMerchantType.setText(types[0]);
//            }

        }
    }
}
