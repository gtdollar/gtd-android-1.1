package mobileapps.sumedia.gtdollarandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zhuodong on 9/11/14.
 */
public class GooglePhoto implements Parcelable {

    @Expose
    private int height;
    @SerializedName("photo_reference")
    @Expose
    private String photoReference;
    @Expose
    private int width;

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.height);
        dest.writeString(this.photoReference);
        dest.writeInt(this.width);
    }

    public GooglePhoto() {
    }

    private GooglePhoto(Parcel in) {
        this.height = in.readInt();
        this.photoReference = in.readString();
        this.width = in.readInt();
    }

    public static final Parcelable.Creator<GooglePhoto> CREATOR = new Parcelable.Creator<GooglePhoto>() {
        public GooglePhoto createFromParcel(Parcel source) {
            return new GooglePhoto(source);
        }

        public GooglePhoto[] newArray(int size) {
            return new GooglePhoto[size];
        }
    };
}
