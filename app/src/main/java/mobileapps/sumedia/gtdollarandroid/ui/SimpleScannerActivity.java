package mobileapps.sumedia.gtdollarandroid.ui;

/**
 * Created by zhuodong on 8/26/14.
 */

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;

import com.google.zxing.Result;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;

public class SimpleScannerActivity extends BaseActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;
    public static final String URL_PREFIX = "http://account.gtdollar.com/v2/?refer=";
    public static final String SEPARATOR = "|||||";

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
//        Toast.makeText(this, "Contents = " + rawResult.getText() +
//            ", Format = " + rawResult.getBarcodeFormat().toString(), Toast.LENGTH_SHORT).show();

        mScannerView.startCamera();

        String result = "";
        try {
            result = URLDecoder.decode(rawResult.getText(),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        try {
            if (result.startsWith(URL_PREFIX)) {
                String info = Uri.parse(result).getQueryParameter("info");

                if (TextUtils.isEmpty(info)) return;
                String decodedInfo = new String(Base64.decode(info, Base64.DEFAULT));
                if (TextUtils.isEmpty(decodedInfo) || !decodedInfo.contains(SEPARATOR)) return;
                String email = decodedInfo.substring(0, decodedInfo.indexOf(SEPARATOR));
                String price = decodedInfo.substring(decodedInfo.indexOf(SEPARATOR) + SEPARATOR.length());

                ArrayList<String> options = new ArrayList<String>();
                options.add(email);
                options.add(price);
                options.add("");
                Utils.reviewOrder(this, options, Const.REVIEW_SEND_CREDIT);
                finish();
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(rawResult.getText()));
                startActivity(intent);
                this.finish();
            }
        }catch(Exception e){};


    }

    @Override
    public boolean hasActionBar() {
        return false;
    }
}

