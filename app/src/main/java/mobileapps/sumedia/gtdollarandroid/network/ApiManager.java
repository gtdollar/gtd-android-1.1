package mobileapps.sumedia.gtdollarandroid.network;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.OkHttpClient;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import mobileapps.sumedia.gtdollarandroid.BuildConfig;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.Account;
import mobileapps.sumedia.gtdollarandroid.model.Notification;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountActivitiesResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountBalanceRecordResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountBaseResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountCheckResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountCreditRecordResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountErrorResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountMerchantRateResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountQRPayResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountSignInResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountSimpleResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.BaseResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.BlackBlockListResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.CircleResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.CirclesResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.ContactsResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.GetMerchantResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.GooglePlaceDetailResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.GooglePlaceResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.NotificationResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.OrderResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.OrdersResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.SearchMerchantResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.ServiceResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.ServicesResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.SignInResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.UpdateProfileResponse;
import mobileapps.sumedia.gtdollarandroid.network.response.UploadFileResponse;
import retrofit.Callback;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.PartMap;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;
import rx.Observable;

/**
 * Created by zhuodong on 3/10/14.
 */
public final class ApiManager implements ApiConfig {
    private static GTDollarApiService mApiService;
    private static GTDollarAccountService mAccountService;

    private ApiManager() {
    }

    public static GTDollarApiService getApiClient() {
        if (mApiService == null) {

            Type activityData = new TypeToken<List<Notification.ActivityData>>() {
            }.getType();

            Gson gson = new GsonBuilder()
                .registerTypeAdapter(activityData, new ActivityDataClassTypeAdapter())
                .create();

            OkHttpClient mOkHttpClient = new OkHttpClient();
            mOkHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
            mOkHttpClient.setReadTimeout(30, TimeUnit.SECONDS);


            RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(DATA_API_BASE)
                .setRequestInterceptor(new ApiAuthRequestInterceptor())
                    //.setErrorHandler(new MyErrorHandler())
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL
                    : RestAdapter.LogLevel.NONE)
                .setConverter(new GsonConverter(gson))
                .setClient(new OkClient(mOkHttpClient))
                .build();
            mApiService = restAdapter.create(GTDollarApiService.class);
        }
        return mApiService;
    }

    public static GTDollarAccountService getAccountClient() {
        if (mAccountService == null) {

            Gson gson = new GsonBuilder()
                //.registerTypeAdapter(Notification.ActivityData.class, new ActivityDataClassTypeAdapter())
                .registerTypeAdapter(AccountSignInResponse.class, new AccountSignInResponseAdapter())
                    //.registerTypeHierarchyAdapter(AccountBaseResponse.class, new AccountBaseResponseAdapter())
                    //.addDeserializationExclusionStrategy(new MyExclusionStrategy())
                .create();

            RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ACCOUNT_API_BASE)
                .setRequestInterceptor(new AccountAuthRequestInterceptor())
                    //.setErrorHandler(new AccountApiErrorHandler())
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL
                    : RestAdapter.LogLevel.NONE)
                .setConverter(new GsonConverter(gson))
                .build();
            mAccountService = restAdapter.create(GTDollarAccountService.class);
        }
        return mAccountService;
    }

    public static GoogleApiService getGoogleClient() {
        if (googleApiService == null) {

            RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(GOOGLE_API_BASE)
                    //.setErrorHandler(new AccountApiErrorHandler())
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL
                    : RestAdapter.LogLevel.NONE)
                .build();
            googleApiService = restAdapter.create(GoogleApiService.class);
        }
        return googleApiService;
    }

    static GoogleApiService googleApiService;

    public interface GoogleApiService {
        @GET(GoogleApiEndPoint.PATH_PLACES)
        public void getPlaces(@QueryMap Map<String, String> params, Callback<GooglePlaceResponse> callback);

        @GET(GoogleApiEndPoint.PATH_PLACE_DETAIL)
        public void getPlaceDetail(@QueryMap Map<String, String> params, Callback<GooglePlaceDetailResponse> callback);
    }

    public interface GTDollarApiService {

//        @POST(DataEndPoint.PATH_SEARCH)
//        public void listNearby(@Body SearchNearbyRequest request, Callback<SearchNearbyResponse> responseCallback);

        @FormUrlEncoded
        @POST(DataEndPoint.PATH_SIGN_IN)
        public Observable<SignInResponse> signIn(@Field("email") String email, @Field("password") String pwd);//, Callback<SignInResponse> callback);

        @FormUrlEncoded
        @POST(DataEndPoint.PATH_FORGOT_PWD)
        public void forgotPWD(@Field("email") String email, Callback<BaseResponse> callback);

        @Multipart
        @POST(DataEndPoint.PATH_USER_UPDATE)
        public Observable<UpdateProfileResponse> updateUser(
            @Part("name") TypedString name,
            @Part("password") TypedString password,
            @Part("facebook_id") TypedString facebookId,
            @Part("phone") TypedString phone,
            @Part("cover") TypedFile cover,
            @Part("quotation") TypedString quotation,
            @Part("picture") TypedFile picture,
            @Part("gender") TypedString gender,
            @Part("region") TypedString region,
            @Part("currency") TypedString currency,
            @Part("address") TypedString address,
            @Part("categories") TypedString categories,
            @Part("website") TypedString website,
            @Part("latlng") TypedString latLng,
            @Part("videos") TypedString video);


        @Multipart
        @POST(DataEndPoint.PATH_MERCHANT_SIGN_UP)
        public Observable<BaseResponse> registerMerchant(
            @Part("name") TypedString name,
            @Part("email") TypedString email,
            @Part("password") TypedString password,
            @Part("facebook_id") TypedString facebookId,
            @Part("phone") TypedString phone,
            @Part("cover") TypedFile cover,
            @Part("quotation") TypedString quotation,
            @Part("picture") TypedFile picture,
            @Part("gender") TypedString gender,
            @Part("region") TypedString region,
            @Part("currency") TypedString currency,
            @Part("address") TypedString address,
            @Part("categories") TypedString categories,
            @Part("website") TypedString website,
            @Part("latlng") TypedString latLng,
            @Part("videos") TypedString video

        );

        @FormUrlEncoded
        @POST(DataEndPoint.PATH_USER_SIGN_UP)
        public Observable<SignInResponse> userSignUp(
            @Field("email") String email,
            @Field("password") String password,
            @Field("currency") String currency,
            @Field("type") String type,
            @Field("categories") String categories
        );

//        @FormUrlEncoded
//        @POST(DataEndPoint.PATH_MERCHANT_SIGN_UP)
//        public void merchantSignUp(@FieldMap HashMap<String, String> params, Callback<BaseResponse> callback);

        @Multipart
        @POST(DataEndPoint.FILE_UPLOAD)
        void uploadFile(@Part("file") TypedFile avatar, Callback<UploadFileResponse> callback);

        @GET(DataEndPoint.PATH_USER_CIRCLE)
        public void getUserCircles(@Query("only_me") int isOnlyUserCircle, Callback<CirclesResponse> callback);

        @Multipart
        @POST(DataEndPoint.PATH_UPDATE_CIRCLE)
        public void updateCircle(@Part("cover") TypedFile cover, @PartMap Map<String, String> params, Callback<CircleResponse> callback);

        @GET(DataEndPoint.PATH_GET_CIRCLE)
        public void getCircle(@Query("id") String id, Callback<CircleResponse> callback);


        @FormUrlEncoded
        @POST(DataEndPoint.PATH_ADD_PEOPLE)
        public void addPeople(@Field("id") String circleId, @Field("list") String ids, Callback<CircleResponse> callback);

        @FormUrlEncoded
        @POST(DataEndPoint.PATH_REMOVE_PEOPLE)
        public void removePeople(@Field("id") String circleId, @Field("list") String ids, Callback<CircleResponse> callback);


        @FormUrlEncoded
        @POST(DataEndPoint.PATH_REMOVE_CIRCLE)
        public void removeCircle(@Field("id") String circleId, Callback<BaseResponse> callback);


        @GET(DataEndPoint.PATH_SEARCH)
        public Observable<SearchMerchantResponse> searchMerchant(@QueryMap Map<String, String> params);

        @FormUrlEncoded
        @POST(DataEndPoint.PATH_CREATE_CIRCLE)
        public Observable<CircleResponse> createCircle(@FieldMap Map<String, String> params);

        @GET(DataEndPoint.PATH_NOTIFICATION)
        public Observable<NotificationResponse> getNotifications();

        @GET(DataEndPoint.PATH_CONTACTS)
        public void getContacts(Callback<ContactsResponse> responseCallback);

        @FormUrlEncoded
        @POST(DataEndPoint.PATH_ADD_CONTACTS)
        public void addContact(@Field("list") String ids, Callback<BaseResponse> callback);

        @FormUrlEncoded
        @POST(DataEndPoint.PATH_REMOVE_CONTACTS)
        public void removeContact(@Field("list") String ids, Callback<BaseResponse> callback);


        @Multipart
        @POST(DataEndPoint.PATH_CREATE_SERVICE)
        public void createService(
            @Part("type") TypedString type,
            @Part("title") TypedString title,
            @Part("expire") TypedString expire,
            @Part("description") TypedString description,
            @Part("price") TypedString price,
            @Part("photo") TypedFile photo,
            Callback<ServicesResponse> callback);

        @Multipart
        @POST(DataEndPoint.PATH_UPDATE_SERVICE)
        public void updateService(
            @Part("id") TypedString id,
            @Part("type") TypedString type,
            @Part("title") TypedString title,
            @Part("expire") TypedString expire,
            @Part("description") TypedString description,
            @Part("price") TypedString price,
            @Part("photo") TypedFile photo, Callback<ServicesResponse> callback);

        @FormUrlEncoded
        @POST(DataEndPoint.PATH_REMOVE_SERVICE)
        public void removeService(@Query("id") String id, Callback<BaseResponse> callback);

        @GET(DataEndPoint.PATH_SERVICE_DETAIL)
        public void serviceDetail(@Query("id") String id, Callback<ServiceResponse> callback);

        @GET(DataEndPoint.PATH_GET_SERVICE)
        public void getServices(Callback<ServicesResponse> callback);


        @FormUrlEncoded
        @POST(DataEndPoint.PATH_CREATE_ORDER)
        public void createOrder(@FieldMap Map<String, String> params, Callback<OrderResponse> callback);

        @FormUrlEncoded
        @POST(DataEndPoint.PATH_UPDATE_ORDER)
        public void updateOrder(@FieldMap Map<String, String> params, Callback<OrdersResponse> callback);

        @FormUrlEncoded
        @POST(DataEndPoint.PATH_REMOVE_ORDER)
        public void removeOrder(@Query("id") String id, Callback<BaseResponse> callback);

        @GET(DataEndPoint.PATH_ORDER_DETAIL)
        public void orderDetail(@Query("id") String id, Callback<OrdersResponse> callback);

        @FormUrlEncoded
        @POST(DataEndPoint.PATH_CONFIRM_ORDER)
        public void confirmOrder(@Query("id") String id, Callback<OrdersResponse> callback);

        @FormUrlEncoded
        @POST(DataEndPoint.PATH_CANCEL_ORDER)
        public void cancelOrder(@Query("id") String id, Callback<OrdersResponse> callback);


        @GET(DataEndPoint.PATH_GET_ORDER)
        public void getOrders(@QueryMap Map<String, String> params, Callback<OrdersResponse> callback);

        @GET(DataEndPoint.PATH_GET_MERCHANT)
        public void getMerchant(@Query("id") String id, Callback<GetMerchantResponse> callback);

        @GET(DataEndPoint.PATH_ADD_BLACK_LIST)
        public void addBlackList(@Query("id") String id, Callback<Response> callback);

        @GET(DataEndPoint.PATH_REMOVE_BLACK_LIST)
        public void removeBlackList(@Query("id") String id, Callback<Response> callback);

        @GET(DataEndPoint.PATH_ADD_BLOCK_LIST)
        public void addBlockList(@Query("id") String id, Callback<Response> callback);

        @GET(DataEndPoint.PATH_REMOVE_BLOCK_LIST)
        public void removeBlockList(@Query("id") String id, Callback<Response> callback);

        @GET(DataEndPoint.PATH_COMPLAIN)
        public void complain(@Query("id") String id, @Query("Content") String content, Callback<Response> callback);

        @GET(DataEndPoint.PATH_BLOCK_LIST)
        public void blockList(Callback<BlackBlockListResponse> callback);

        @GET(DataEndPoint.PATH_BLACK_LIST)
        public void blackList(Callback<BlackBlockListResponse> callback);


    }

    public interface GTDollarAccountService {

        @POST(AccountEndPoint.PATH_APPLY_CREDIT)
        @FormUrlEncoded
        public void applyCredit(@Field("receiver") String receiver, @Field("amount") String amount, @Field("discount") String discount, @Field("remark") String remark, Callback<AccountSimpleResponse> callback);

//        @POST(AccountEndPoint.PATH_SEND_CREDIT)
//        @FormUrlEncoded
//        public void sendCredit(@Field("uid") String userId, @Field("amount") String amount, @Field("receiver") String receiver, @Field("remark") String remark, Callback<AccountBaseResponse> callback);

        @POST(AccountEndPoint.PATH_SEND_CREDIT)
        @FormUrlEncoded
        public Observable<AccountSimpleResponse> sendCredit(@Field("uid") String userId, @Field("amount") String amount, @Field("receiver") String receiver, @Field("comments") String remark);


        @POST(AccountEndPoint.PATH_PAY_MERCHANT)
        @FormUrlEncoded
        public Observable<AccountSimpleResponse> payMerchant(@Field("uid") String userId, @Field("amount") String amount, @Field("receiver") String receiver, @Field("gtd") String gtdAmount);


        @POST(AccountEndPoint.PATH_TRANSFER_BALANCE)
        @FormUrlEncoded
        public Observable<AccountSimpleResponse> transferBalance(@Field("uid") String userId, @Field("amount") String amount, @Field("receiver") String receiver, @Field("comments") String remark);


        @POST(AccountEndPoint.PATH_QR_PAY)
        @FormUrlEncoded
        public void qrPay(@Field("uid") String userId, @Field("amount") String amount, @Field("product") String product, Callback<AccountQRPayResponse> callback);

        @POST(AccountEndPoint.PATH_CREDIT_RECORD)
        @FormUrlEncoded
        public void creditRecord(@Field("uid") String userId, Callback<AccountCreditRecordResponse> callback);

        @FormUrlEncoded
        @POST(AccountEndPoint.PATH_BALANCE_RECORD)
        public void balanceRecord(@Field("uid") String userId, @Field("status") int type, Callback<AccountBalanceRecordResponse> callback);


        @FormUrlEncoded
        @POST(AccountEndPoint.PATH_SIGN_IN)
        public Observable<AccountSignInResponse> signIn(@Field("username") String email, @Field("password") String pwd);//, Callback<AccountSignInResponse> callback);

        @FormUrlEncoded
        @POST(AccountEndPoint.PATH_SIGN_UP)
        public Observable<AccountSimpleResponse> signUp(@Field("rtype") int rType, @Field("username") String email, @Field("password") String pwd, @Field("currency") String currency);

        @FormUrlEncoded
        @POST(AccountEndPoint.PATH_ACCOUNT)
        public void checkAccount(@Field("uid") String userId, Callback<AccountCheckResponse> callback);

        @GET(AccountEndPoint.PATH_PAY_ACTIVITIES)
        public Observable<AccountActivitiesResponse> getActivities();

        @FormUrlEncoded
        @POST(AccountEndPoint.PATH_MERCHANT_RATE)
        public void getMerchantRate(@Field("email") String email, Callback<AccountMerchantRateResponse> responseCallback);

        @FormUrlEncoded
        @POST(AccountEndPoint.PATH_REACTIVE)
        public void reactive(@Field("email") String email, Callback<AccountBaseResponse> responseCallback);

        @FormUrlEncoded
        @POST(AccountEndPoint.PATH_UNIONPAY)
        public void unionPay(@Field("customerName") String customerName, @Field("orderamount") String orderAmount, @Field("ordernumber") String orderNumber, @Field("receiver_id") String receiverId,  @Field("type") int type ,Callback<Response> responseCallback);

        @FormUrlEncoded
        @POST(AccountEndPoint.PATH_CREDIT_CARD)
        public void creditCardTopup(@Field("CardNumber") String cardNumber, @Field("ExpMonth") String expMonth, @Field("ExpYear") String expYear, @Field("SecurityCode") String securityCode,  @Field("custom_email") String customerEmail , @Field("firstname") String name, @Field("member") String member, @Field("membercurrency") String currency, @Field("noc") String cardName, @Field("price") String amount,Callback<Response> responseCallback);

        @POST(AccountEndPoint.PATH_REFUND)
        @FormUrlEncoded
        public void refund(@Field("member") String accountId, @Field("action") String action, @Field("transaction_id") String transaction_id, @Field("reason") String description, Callback<AccountSimpleResponse> callback);


    }

    private static final class AccountAuthRequestInterceptor implements RequestInterceptor {

        private AccountAuthRequestInterceptor() {
        }

        @Override
        public void intercept(RequestFacade request) {
            String token = UserDataHelper.getAccountToken();
            if (!TextUtils.isEmpty(token)) request.addHeader("Auth", token);
            request.addHeader("Device-ID", Utils.getDeviceId());
        }
    }

    private static final class ApiAuthRequestInterceptor implements RequestInterceptor {

        private ApiAuthRequestInterceptor() {
        }

        @Override
        public void intercept(RequestFacade request) {
            if (!TextUtils.isEmpty(UserDataHelper.getToken()))
                request.addHeader("GT-Auth-Token", UserDataHelper.getToken());
            request.addHeader("GT-Device-ID", Utils.getDeviceId());
            request.addQueryParam("language", UserDataHelper.getLanguage().contains("zh")?"zh":"en");
        }
    }

    final static class AccountApiErrorHandler implements ErrorHandler {

        @Override
        public Throwable handleError(RetrofitError cause) {
//            Log.d("", " testing handleError");
//            Log.d("", " testing response " + cause.getBody());
//            Log.d("", " testing isNetworkError " + cause.isNetworkError());
//            Log.d("", " testing status " + cause.getResponse().getStatus());
            if (cause.getResponse() == null || cause.isNetworkError()) {
                return RetrofitError.unexpectedError(cause.getUrl(), new Throwable(Const.DEFAULT_ERROR_MSG));
            }
            if (cause.getResponse().getStatus() == 500) {
                return cause;
            }
            try {
                AccountErrorResponse errorResponse = JSONParser.parseFromInputStream(AccountErrorResponse.class, cause.getResponse().getBody().in());
                return RetrofitError.unexpectedError(cause.getUrl(), new Throwable(errorResponse.getError()));

            } catch (Exception e) {
                e.printStackTrace();
            }
            return cause;
        }
    }


    public static class ActivityDataClassTypeAdapter implements JsonDeserializer<List<Notification.ActivityData>> {
        public List<Notification.ActivityData> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext ctx) {
            List<Notification.ActivityData> vals = new ArrayList<Notification.ActivityData>();
            if (json.isJsonArray()) {
                for (JsonElement e : json.getAsJsonArray()) {
                    vals.add((Notification.ActivityData) ctx.deserialize(e, Notification.ActivityData.class));
                }
            } else if (json.isJsonObject()) {
                vals.add((Notification.ActivityData) ctx.deserialize(json, Notification.ActivityData.class));
            } else {
                throw new RuntimeException("Unexpected JSON type: " + json.getClass());
            }
            return vals;
        }
    }

    public static class AccountSignInResponseAdapter implements JsonSerializer<AccountSignInResponse>, JsonDeserializer<AccountSignInResponse> {

        private static final String CLASSNAME = "CLASSNAME";
        private static final String INSTANCE = "INSTANCE";

        @Override
        public JsonElement serialize(AccountSignInResponse src, Type typeOfSrc,
                                     JsonSerializationContext context) {

            JsonObject retValue = new JsonObject();
            String className = src.getClass().getCanonicalName();
            retValue.addProperty(CLASSNAME, className);
            JsonElement elem = context.serialize(src);
            retValue.add(INSTANCE, elem);
            return retValue;
        }

        @Override
        public AccountSignInResponse deserialize(JsonElement json, Type typeOfT,
                                                 JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();
            Log.d("", " testing deserialize child " + json);

            AccountSignInResponse accountSignInResponse = new AccountSignInResponse();

            if (jsonObject.has("status")) {
                int status = jsonObject.getAsJsonPrimitive("status").getAsInt();
                accountSignInResponse.setStatus(status);
                if (status == 0 && jsonObject.has("data")) {
                    jsonObject.remove("data");
                    //return context.deserialize(json, AccountErrorResponse.class);
                }
            }

            if (jsonObject.has("error")) {
                accountSignInResponse.setError(jsonObject.getAsJsonPrimitive("error").getAsString());
            }
            if (jsonObject.has("token")) {
                accountSignInResponse.setToken(jsonObject.getAsJsonPrimitive("token").getAsString());
            }
            if (jsonObject.has("data")) {
                accountSignInResponse.setData((Account) context.deserialize(jsonObject.getAsJsonObject("data"), Account.class));
            }
            return accountSignInResponse;
        }
    }




//    public static class AccountBaseResponseAdapter implements JsonSerializer<AccountBaseResponse>, JsonDeserializer<AccountBaseResponse>{
//
//        private static final String CLASSNAME = "CLASSNAME";
//        private static final String INSTANCE  = "INSTANCE";
//
//        @Override
//        public JsonElement serialize(AccountBaseResponse src, Type typeOfSrc,
//                                     JsonSerializationContext context) {
//
//            JsonObject retValue = new JsonObject();
//            String className = src.getClass().getCanonicalName();
//            retValue.addProperty(CLASSNAME, className);
//            JsonElement elem = context.serialize(src);
//            retValue.add(INSTANCE, elem);
//            return retValue;
//        }
//
//        @Override
//        public AccountBaseResponse deserialize(JsonElement json, Type typeOfT,
//                                                 JsonDeserializationContext context) throws JsonParseException {
//
//            JsonObject jsonObject =  json.getAsJsonObject();
//            Log.d(""," testing deserialize base "+json);
//
////            AccountBaseResponse accountBaseResponse = new AccountBaseResponse();
////            accountBaseResponse.setStatus(status);
////            if(jsonObject.has("error")) {
////                accountBaseResponse.setError(jsonObject.getAsJsonPrimitive("error").getAsString());
////            }
//
//            if(jsonObject.has("status") && jsonObject.getAsJsonPrimitive("status").getAsInt() == 0) {
//                jsonObject.remove("data");
//            }
//
//            return context.deserialize(json,typeOfT);
//
////
////            JsonPrimitive prim = (JsonPrimitive) jsonObject.get(CLASSNAME);
////            String className = prim.getAsString();
////
////            Log.d(""," testing deserialize class name "+className);
////
////            Class<?> klass = null;
////            try {
////                klass = Class.forName(className);
////            } catch (ClassNotFoundException e) {
////                e.printStackTrace();
////                throw new JsonParseException(e.getMessage());
////            }
////            return context.deserialize(jsonObject.get(INSTANCE), klass);
//
//        }
//    }


}
