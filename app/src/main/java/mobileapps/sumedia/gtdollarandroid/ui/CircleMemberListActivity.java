package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.CircleMemberListAdapter;
import mobileapps.sumedia.gtdollarandroid.event.UpdatePeopleEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.Circle;
import mobileapps.sumedia.gtdollarandroid.widget.SimpleSectionedListAdapter;

public class CircleMemberListActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    @InjectView(R.id.lv_users)
    ListView mLvUsers;

    Circle circle;

    CircleMemberListAdapter circleMemberListAdapter;

    SimpleSectionedListAdapter simpleSectionedListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        circle = getIntent().getParcelableExtra(Const.INTENT_EXTRA_CIRCLE);

        if (circle == null) {
            finish();
            return;
        }

        setContentView(R.layout.activity_circle_detail);
        ButterKnife.inject(this);

        setTitle(circle.getName());

        circleMemberListAdapter = new CircleMemberListAdapter(this, R.layout.item_circle_memeber_simple);
        circleMemberListAdapter.setCircleId(circle.getId());

        simpleSectionedListAdapter = new SimpleSectionedListAdapter(this, R.layout.item_circle_user_section, circleMemberListAdapter);

        SimpleSectionedListAdapter.Section[] sections = {new SimpleSectionedListAdapter.Section(0, getString(R.string.string_group_owner)), new SimpleSectionedListAdapter.Section(1, getString(R.string.string_members, circle.getMembers().size()))};

        circleMemberListAdapter.setItems(circle.getMembers());
        circleMemberListAdapter.add(0, circle.getCreatedBy());

        simpleSectionedListAdapter.setSections(sections);

        mLvUsers.setAdapter(simpleSectionedListAdapter);
        mLvUsers.setOnItemClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        int index = simpleSectionedListAdapter.sectionedPositionToPosition(i);
        if (index >= circleMemberListAdapter.getCount()) return;
        Utils.viewMerchant(this, null, circleMemberListAdapter.getItem(index).getId());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.circle_member, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_add) {
            Utils.addMember(this, circle);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onEventMainThread(UpdatePeopleEvent event) {

        if (event.getStatus() == EventStatus.SUCCEED) {
//            finish();
            circle = event.getResponse().getCircle();
            if (circle != null) {
                SimpleSectionedListAdapter.Section[] sections = {new SimpleSectionedListAdapter.Section(0, getString(R.string.string_group_owner)), new SimpleSectionedListAdapter.Section(1, getString(R.string.string_members, circle.getMembers().size()))};
                circleMemberListAdapter.setItems(circle.getMembers());
                simpleSectionedListAdapter.setSections(sections);
            }
        }
    }


    @Override
    public boolean isSubscribedEvent() {
        return true;
    }
}
