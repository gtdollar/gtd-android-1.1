package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.CategoryListAdapter;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.LatLng;
import mobileapps.sumedia.gtdollarandroid.model.SimpleIconTextItem;


public class SelectMerchantTypeActivity extends BaseActivity {

    GridView gridView;

    LatLng latLng;
    private boolean newMerchant;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_merchant_filter);
        gridView = (GridView) findViewById(R.id.gridView);
        latLng = getIntent().getParcelableExtra(Const.INTENT_EXTRA_LATLNG);
        newMerchant = getIntent().getBooleanExtra(Const.INTENT_EXTRA_NEW_MERCHANT, true);
        setTitle(latLng != null?R.string.title_activity_places_around:R.string.title_activity_select_merchant_type);
        setupGridView();
    }


    public void setupGridView() {

        final ArrayList<SimpleIconTextItem> gridArray = new ArrayList<SimpleIconTextItem>();
        //set grid view item

        String[] options = getResources().getStringArray(R.array.merchant_types);

        TypedArray icons = getResources().obtainTypedArray(R.array.merchant_res_id);

        for (int i = 0; i < options.length; i++) {
            gridArray.add(new SimpleIconTextItem(getResources().getDrawable(icons.getResourceId(i, R.drawable.home)), options[i]));
        }
        icons.recycle();

        CategoryListAdapter customGridAdapter = new CategoryListAdapter(getApplicationContext());
        customGridAdapter.setItems(gridArray);
        gridView.setAdapter(customGridAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Utils.search(SelectMerchantTypeActivity.this, latLng, null, gridArray.get(position).getTitle(), newMerchant);
                //Utils.searchGoogleMerchant(SelectMerchantTypeActivity.this,gridArray.get(position).getTitle(),latLng);
            }
        });
    }
}
