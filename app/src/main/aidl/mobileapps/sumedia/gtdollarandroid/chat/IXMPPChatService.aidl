// IXMPPChatService.aidl
package mobileapps.sumedia.gtdollarandroid.chat;

// Declare any non-default types here with import statements

interface IXMPPChatService {
	void sendMessage(String user, String message,in Map properties);
	boolean isAuthenticated();
	void clearNotifications(String Jid);
}
