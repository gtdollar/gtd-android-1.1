package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.MerchantSimpleListAdapter;
import mobileapps.sumedia.gtdollarandroid.event.ChooseContactEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.GetContactEvent;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;

public class ChooseContactActivity extends BaseActivity {

    MerchantSimpleListAdapter listAdapter;
    @InjectView(R.id.lv_merchant)
    ListView mLvMerchant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_merchant);
        ButterKnife.inject(this);

        setTitle(R.string.title_activity_choose_contact);


        listAdapter = new MerchantSimpleListAdapter(this);

        mLvMerchant.setAdapter(listAdapter);
        mLvMerchant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
//                    view.findViewById(R.id.iv_tick).callOnClick();
//                } else {
//                    view.findViewById(R.id.iv_tick).performClick();
//                }
                EventBus.getDefault().post(new ChooseContactEvent(listAdapter.getItem(position).getEmail()));
                finish();
//                Intent intent = new Intent(getActivity(), MerchantDetailActivity.class);
//                intent.putExtra(Const.INTENT_EXTRA_MERCHANT_KEY, merchantList.get(position));
//                startActivity(intent);
            }
        });

        JobManager.getInstance().getContacts();
    }

    public void onEventMainThread(GetContactEvent event) {

        if (event.getStatus() == EventStatus.SUCCEED) {
            listAdapter.setItems(event.getResponse().getList());
        }

    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }
}
