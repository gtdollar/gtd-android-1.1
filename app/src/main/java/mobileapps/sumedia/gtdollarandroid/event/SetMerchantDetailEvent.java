package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.model.User;

/**
 * Created by zhuodong on 4/26/15.
 */
public class SetMerchantDetailEvent {
    public User merchant;

    public SetMerchantDetailEvent(User merchant) {
        this.merchant = merchant;
    }

    public User getMerchant() {
        return merchant;
    }
}
