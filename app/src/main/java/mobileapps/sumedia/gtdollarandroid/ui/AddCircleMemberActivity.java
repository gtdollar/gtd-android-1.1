package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.MerchantSimpleListAdapter;
import mobileapps.sumedia.gtdollarandroid.event.GetNearbyEvent;
import mobileapps.sumedia.gtdollarandroid.event.UpdatePeopleEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.Circle;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;

public class AddCircleMemberActivity extends BaseActivity {

    MerchantSimpleListAdapter listAdapter;
    @InjectView(R.id.lv_merchant)
    ListView mLvMerchant;

    Circle circle;
    @InjectView(R.id.tv_empty)
    TextView tvEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_merchant);
        ButterKnife.inject(this);

        circle = getIntent().getParcelableExtra(Const.INTENT_EXTRA_CIRCLE);

        final ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.layout_search);
        ((EditText) actionBar.getCustomView()).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    JobManager.getInstance().searchMerchant(v.getText().toString(), null, true, null);
                }
                return false;
            }
        });

        listAdapter = new MerchantSimpleListAdapter(this);
        mLvMerchant.setEmptyView(tvEmpty);
        mLvMerchant.setAdapter(listAdapter);
        mLvMerchant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                    view.findViewById(R.id.iv_tick).callOnClick();
                } else {
                    view.findViewById(R.id.iv_tick).performClick();
                }
//                Intent intent = new Intent(getActivity(), MerchantDetailActivity.class);
//                intent.putExtra(Const.INTENT_EXTRA_MERCHANT_KEY, merchantList.get(position));
//                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_circle_member, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_search) {
            return true;
        } else if (id == R.id.action_add) {
            if (listAdapter.getIds().size() > 0) {
                JobManager.getInstance().addCircleMember(circle.getId(), TextUtils.join(",", listAdapter.getIds()));
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onEventMainThread(GetNearbyEvent event) {
        Utils.toggleEventPopup(this, event, new Utils.StatusListener<GetNearbyEvent>() {
            @Override
            public void onSucceed(GetNearbyEvent event) {
                Utils.hideKeyBoard(AddCircleMemberActivity.this, getSupportActionBar().getCustomView());
                listAdapter.setItems(event.getResponse().getUsers());
                if (circle != null) {
                    listAdapter.filter(circle.getMembers());
                }
            }
        });
    }

    public void onEventMainThread(UpdatePeopleEvent event) {
        Utils.toggleEventPopup(this, event, new Utils.StatusListener<UpdatePeopleEvent>() {
            @Override
            public void onSucceed(UpdatePeopleEvent event) {
//                circle = event.getResponse().getCircle();
//                if(circle != null) {
//                    listAdapter.filter(event.getResponse().getCircle().getMembers());
//                }
                finish();
            }
        });
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }
}
