package mobileapps.sumedia.gtdollarandroid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by zhuodong on 8/29/14.
 */
public class Notification {

    @Expose
    private int type;
    @Expose
    private GTUser by;
    @Expose
    private String time;

    @SerializedName("activity_data")
    @Expose
    private List<ActivityData> activityData;
    @Expose
    private String id;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public GTUser getBy() {
        return by;
    }

    public void setBy(GTUser by) {
        this.by = by;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<ActivityData> getActivityData() {
        return activityData;
    }

    public void setActivityData(List<ActivityData> activityData) {
        this.activityData = activityData;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static class ActivityData {

        @Expose
        private Order order;

        public Order getOrder() {
            return order;
        }

        public void setOrder(Order order) {
            this.order = order;
        }

    }
}