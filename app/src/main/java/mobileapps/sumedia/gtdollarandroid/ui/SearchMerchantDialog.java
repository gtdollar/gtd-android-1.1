package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.widget.EditText;

import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.SearchEvent;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

public class SearchMerchantDialog extends DialogFragment {


    public static SearchMerchantDialog newInstance(int type, String typeStr) {
        SearchMerchantDialog fragment = new SearchMerchantDialog();
        Bundle b = new Bundle();
        b.putInt("type",type);
        b.putString("typeStr", typeStr);
        fragment.setArguments(b);
        return fragment;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final EditText editText = (EditText) LayoutInflater.from(getActivity()).inflate(R.layout.layout_edit_text,null);

        editText.setHint(getArguments().getString("typeStr"));

        final int type = getArguments().getInt("type");

        final Form mForm = new Form(getActivity());
        mForm.addField(Field.using(editText).validate(NotEmpty.build(getActivity())));

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
            .setTitle(R.string.string_create_circle)
            .setView(editText)
            .setPositiveButton(R.string.string_search,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (mForm.isValid()) {
                            EventBus.getDefault()
                                .post(new SearchEvent(type, editText.getText().toString()));
                        }
                    }
                }
            )
            .setNegativeButton(R.string.string_cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }
            );

        return builder.create();
    }

}
