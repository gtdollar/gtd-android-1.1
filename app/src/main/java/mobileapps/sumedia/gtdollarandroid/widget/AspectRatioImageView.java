package mobileapps.sumedia.gtdollarandroid.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import mobileapps.sumedia.gtdollarandroid.R;

public class AspectRatioImageView extends ImageView {

    float ratio;
    boolean setRatio;

    public AspectRatioImageView(Context context) {
        this(context,null,0);
    }

    public AspectRatioImageView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public AspectRatioImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.AspectRatioImageView);
        ratio = a.getFloat(R.styleable.AspectRatioImageView_ratio, -1);
        setRatio = a.getBoolean(R.styleable.AspectRatioImageView_setRatio, false);
        a.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height;
        if(!setRatio) {
            if(getDrawable() != null) {
                height = width * getDrawable().getIntrinsicHeight() / getDrawable().getIntrinsicWidth();
            } else {
                height = MeasureSpec.getSize(heightMeasureSpec);
            }
        }else {
            height = (int) (width / ratio);
        }

        setMeasuredDimension(width, height);
    }


}
