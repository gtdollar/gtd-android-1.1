package mobileapps.sumedia.gtdollarandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhuodong on 9/11/14.
 */
public class GooglePlace implements Parcelable {

    @Expose
    private Geometry geometry;
    @Expose
    private String icon;
    @Expose
    private String id;
    @Expose
    private String name;
    @SerializedName("place_id")
    @Expose
    private String placeId;
    @Expose
    private String reference;
    @Expose
    private String scope;
    @Expose
    private List<String> types = new ArrayList<String>();
    @Expose
    private String vicinity;
    @SerializedName("opening_hours")
    @Expose
    private OpeningHours openingHours;
    @Expose
    private List<GooglePhoto> photos = new ArrayList<GooglePhoto>();
    @Expose
    private double rating;
    @SerializedName("price_level")
    @Expose
    private int priceLevel;

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public OpeningHours getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(OpeningHours openingHours) {
        this.openingHours = openingHours;
    }

    public List<GooglePhoto> getPhotos() {
        return photos;
    }

    public void setPhotos(List<GooglePhoto> photos) {
        this.photos = photos;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getPriceLevel() {
        return priceLevel;
    }

    public void setPriceLevel(int priceLevel) {
        this.priceLevel = priceLevel;
    }

    public static class OpeningHours {

        @SerializedName("open_now")
        @Expose
        private boolean openNow;

        public boolean isOpenNow() {
            return openNow;
        }

        public void setOpenNow(boolean openNow) {
            this.openNow = openNow;
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.geometry, 0);
        dest.writeString(this.icon);
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.placeId);
        dest.writeString(this.reference);
        dest.writeString(this.scope);
        dest.writeStringList(this.types);
        dest.writeString(this.vicinity);
        dest.writeTypedList(photos);
        dest.writeDouble(this.rating);
        dest.writeInt(this.priceLevel);
    }

    public GooglePlace() {
    }

    private GooglePlace(Parcel in) {
        this.geometry = in.readParcelable(Geometry.class.getClassLoader());
        this.icon = in.readString();
        this.id = in.readString();
        this.name = in.readString();
        this.placeId = in.readString();
        this.reference = in.readString();
        this.scope = in.readString();
        in.readStringList(this.types);
        this.vicinity = in.readString();
        in.readTypedList(photos, GooglePhoto.CREATOR);
        this.rating = in.readDouble();
        this.priceLevel = in.readInt();
    }

    public static final Parcelable.Creator<GooglePlace> CREATOR = new Parcelable.Creator<GooglePlace>() {
        public GooglePlace createFromParcel(Parcel source) {
            return new GooglePlace(source);
        }

        public GooglePlace[] newArray(int size) {
            return new GooglePlace[size];
        }
    };
}
