package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;

import mobileapps.sumedia.gtdollarandroid.model.Circle;

/**
 * Created by zhuodong on 8/29/14.
 */
public class CircleResponse extends BaseResponse {

    @Expose
    private Circle circle;

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }
}
