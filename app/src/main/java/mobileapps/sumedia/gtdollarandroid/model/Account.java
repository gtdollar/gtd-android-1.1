package mobileapps.sumedia.gtdollarandroid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zhuodong on 8/20/14.
 */
public class Account {

    @Expose
    private String id;
    @Expose
    private String sponsor;
    @Expose
    private String username;
    @Expose
    private String password;
    @Expose
    private String question;
    @Expose
    private String answer;
    @Expose
    private String email;
    @Expose
    private String active;
    @Expose
    private String status;
    @Expose
    private String empty;
    @Expose
    private String cdate;
    @Expose
    private String ldate;
    @Expose
    private String adate;
    @SerializedName("last_ip")
    @Expose
    private String lastIp;
    @Expose
    private String fname;
    @Expose
    private String lname;
    @Expose
    private String company;
    @Expose
    private String regnum;
    @Expose
    private String drvnum;
    @Expose
    private String address;
    @Expose
    private String city;
    @Expose
    private String country;
    @Expose
    private String state;
    @Expose
    private String zip;
    @Expose
    private String phone;
    @Expose
    private String fax;
    @Expose
    private String ctype;
    @Expose
    private String cname;
    @Expose
    private String cnumber;
    @Expose
    private String ccvv;
    @Expose
    private String cmonth;
    @Expose
    private String cyear;
    @Expose
    private String bname;
    @Expose
    private String baddress;
    @Expose
    private String bcity;
    @Expose
    private String bzip;
    @Expose
    private String bcountry;
    @Expose
    private String bstate;
    @Expose
    private String bphone;
    @Expose
    private String bnameacc;
    @Expose
    private String baccount;
    @Expose
    private String btype;
    @Expose
    private String brtgnum;
    @Expose
    private String bswift;
    @Expose
    private String gtrefer;
    @Expose
    private String mcurrency;
    @Expose
    private Object dob;
    @Expose
    private Object nationality;
    @SerializedName("APIkeys")
    @Expose
    private String aPIkeys;
    @Expose
    private String description;
    @Expose
    private String owner;
    @Expose
    private String primary;
    @Expose
    private String verifcode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmpty() {
        return empty;
    }

    public void setEmpty(String empty) {
        this.empty = empty;
    }

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public String getLdate() {
        return ldate;
    }

    public void setLdate(String ldate) {
        this.ldate = ldate;
    }

    public String getAdate() {
        return adate;
    }

    public void setAdate(String adate) {
        this.adate = adate;
    }

    public String getLastIp() {
        return lastIp;
    }

    public void setLastIp(String lastIp) {
        this.lastIp = lastIp;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getRegnum() {
        return regnum;
    }

    public void setRegnum(String regnum) {
        this.regnum = regnum;
    }

    public String getDrvnum() {
        return drvnum;
    }

    public void setDrvnum(String drvnum) {
        this.drvnum = drvnum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCnumber() {
        return cnumber;
    }

    public void setCnumber(String cnumber) {
        this.cnumber = cnumber;
    }

    public String getCcvv() {
        return ccvv;
    }

    public void setCcvv(String ccvv) {
        this.ccvv = ccvv;
    }

    public String getCmonth() {
        return cmonth;
    }

    public void setCmonth(String cmonth) {
        this.cmonth = cmonth;
    }

    public String getCyear() {
        return cyear;
    }

    public void setCyear(String cyear) {
        this.cyear = cyear;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public String getBaddress() {
        return baddress;
    }

    public void setBaddress(String baddress) {
        this.baddress = baddress;
    }

    public String getBcity() {
        return bcity;
    }

    public void setBcity(String bcity) {
        this.bcity = bcity;
    }

    public String getBzip() {
        return bzip;
    }

    public void setBzip(String bzip) {
        this.bzip = bzip;
    }

    public String getBcountry() {
        return bcountry;
    }

    public void setBcountry(String bcountry) {
        this.bcountry = bcountry;
    }

    public String getBstate() {
        return bstate;
    }

    public void setBstate(String bstate) {
        this.bstate = bstate;
    }

    public String getBphone() {
        return bphone;
    }

    public void setBphone(String bphone) {
        this.bphone = bphone;
    }

    public String getBnameacc() {
        return bnameacc;
    }

    public void setBnameacc(String bnameacc) {
        this.bnameacc = bnameacc;
    }

    public String getBaccount() {
        return baccount;
    }

    public void setBaccount(String baccount) {
        this.baccount = baccount;
    }

    public String getBtype() {
        return btype;
    }

    public void setBtype(String btype) {
        this.btype = btype;
    }

    public String getBrtgnum() {
        return brtgnum;
    }

    public void setBrtgnum(String brtgnum) {
        this.brtgnum = brtgnum;
    }

    public String getBswift() {
        return bswift;
    }

    public void setBswift(String bswift) {
        this.bswift = bswift;
    }

    public String getGtrefer() {
        return gtrefer;
    }

    public void setGtrefer(String gtrefer) {
        this.gtrefer = gtrefer;
    }

    public String getMcurrency() {
        return mcurrency;
    }

    public void setMcurrency(String mcurrency) {
        this.mcurrency = mcurrency;
    }

    public Object getDob() {
        return dob;
    }

    public void setDob(Object dob) {
        this.dob = dob;
    }

    public Object getNationality() {
        return nationality;
    }

    public void setNationality(Object nationality) {
        this.nationality = nationality;
    }

    public String getAPIkeys() {
        return aPIkeys;
    }

    public void setAPIkeys(String aPIkeys) {
        this.aPIkeys = aPIkeys;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPrimary() {
        return primary;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public String getVerifcode() {
        return verifcode;
    }

    public void setVerifcode(String verifcode) {
        this.verifcode = verifcode;
    }

}
