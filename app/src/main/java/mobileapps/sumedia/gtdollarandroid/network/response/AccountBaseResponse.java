package mobileapps.sumedia.gtdollarandroid.network.response;

/**
 * Created by zhuodong on 8/8/14.
 */
public class AccountBaseResponse implements ErrorResponse{
    //protected String data;
    String error;
    int status;

    @Override
    public String getError() {
        return error;
    }

    //public String getData() {
    //    return data;
    //}

    public int getStatus() {
        return status;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
