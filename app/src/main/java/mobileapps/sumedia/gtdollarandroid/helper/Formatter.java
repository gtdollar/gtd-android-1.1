package mobileapps.sumedia.gtdollarandroid.helper;

import android.content.res.Resources;
import android.location.Location;
import android.text.TextUtils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import mobileapps.sumedia.gtdollarandroid.GTDApplication;
import mobileapps.sumedia.gtdollarandroid.R;

/**
 * Created by zhuodong on 7/16/14.
 */
public class Formatter {
    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String DETAIL_DATE_FORMAT = "yyyy-MM-dd,HH:mm:ss'Z'";

    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final String TIME_FORMAT = "HH:mm";

//    public static long getTimeStamp(String date,String format) {
//        SimpleDateFormat sdf = new SimpleDateFormat(SERVER_DATE_FORMAT, Locale.US);
//        try {
//            return sdf.parse(date).getTime();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return 0;
//    }

    public static final String PAYMENT_ACTIVITY_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String NOTIFICATION_DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";


    public static long getTimeStamp(String date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        try {
            return sdf.parse(date).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String formatDistance(double x, double y, double x1, double x2) {

        float[] result = new float[3];
        Location.distanceBetween(x, y, x1, x2, result);

        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMaximumFractionDigits(2);
        return numberFormat.format(result[0] / 1000) + " km";
    }

    public static String formatPrice(String symbol, double price) {
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMaximumFractionDigits(2);
        if (TextUtils.isEmpty(symbol)) {
            symbol = "$";
        }
        return symbol + numberFormat.format(price);
    }


    public static String formatOpeningHours(String openHour, String closeHour) {
        return openHour + "-" + closeHour;
    }

    public static String formatCommentUpdatedTime(long timestamp) {
        return unixTimeStampToDate(timestamp, SERVER_DATE_FORMAT);
    }

    public static String formatOrderCreatedTime(long timestamp) {
        return unixTimeStampToDate(timestamp, DETAIL_DATE_FORMAT);
    }


    public static String formatDate(String date, String originalFormat, String format) {
        return unixTimeStampToDate(getTimeStamp(date,originalFormat), format);
    }

    public static String unixTimeStampToDate(long timeStamp, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        Date resultDate = new Date(timeStamp);
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(resultDate);
    }

    /**
     * **************************************************************
     * Function to return relevant time, i.e. "2 days ago" Takes, time in
     * milliseconds
     * **************************************************************
     */
    public static String milliSecsToRelaventDate(long timeStamp, String format) {
        Resources resources = GTDApplication.getInstance().getResources();
        int DAYS_IN_WEEK = 7; // only within 24 hours.
        int MAX_WEEKS_TO_SHOW_RELAVENT_DATE = 4; // zero means show xx hours ago,

        long MILLI_SECS_IN_DAY = 86400000;
        long MILLI_SECS_IN_HOUR = 3600000;
        long MILLI_SECS_IN_MIN = 60000;
        long remainder;
        long relevantTime = System.currentTimeMillis() - timeStamp;
        if (relevantTime < 0) {
            return resources.getString(R.string.string_just_now);
        }

        remainder = (relevantTime / MILLI_SECS_IN_DAY);

        if (remainder > MAX_WEEKS_TO_SHOW_RELAVENT_DATE * DAYS_IN_WEEK) {
            return unixTimeStampToDate(timeStamp, format);
        }

        // else
        if (remainder >= DAYS_IN_WEEK) {
            int weeknumber = (int) remainder / DAYS_IN_WEEK;
            return resources.getQuantityString(R.plurals.weeks_ago, weeknumber, weeknumber);

        }

        if (remainder >= 1) {
            return resources
                .getQuantityString(R.plurals.days_ago, (int) remainder, (int) remainder);
        }

        remainder = relevantTime / MILLI_SECS_IN_HOUR;
        if (remainder >= 1) {
            return resources
                .getQuantityString(R.plurals.hours_ago, (int) remainder, (int) remainder);
        }

        remainder = relevantTime / MILLI_SECS_IN_MIN;

        if (remainder >= 1) {
            return resources
                .getQuantityString(R.plurals.mins_ago, (int) remainder, (int) remainder);
        }

        return resources.getString(R.string.string_seconds_ago);
    }

//    public static int formatSimpleDate(long timeInMillis) {
//        return 0;
//    }
}
