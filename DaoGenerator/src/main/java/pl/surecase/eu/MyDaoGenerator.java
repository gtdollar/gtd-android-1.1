package pl.surecase.eu;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class MyDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(2, "mobileapps.sumedia.gtdollarandroid");
        schema.enableKeepSectionsByDefault();
        Entity chat = schema.addEntity("Chat");
        chat.addIdProperty();

        chat.addIntProperty("direction");
        chat.addStringProperty("message");
        chat.addStringProperty("messageType");
        chat.addLongProperty("date");
        chat.addIntProperty("deliveryStatus");
        chat.addStringProperty("jid");
        chat.addStringProperty("pid");

        chat.addStringProperty("avatarUrl");
        chat.addStringProperty("userName");

        chat.addStringProperty("properties");


        Entity feed = schema.addEntity("Feed");
        feed.addIdProperty();

        feed.addIntProperty("type");
        feed.addIntProperty("baseType");
        feed.addLongProperty("createdAt");
        feed.addStringProperty("extraId");
        feed.addBooleanProperty("isRead");

        feed.addStringProperty("subjectName");
        feed.addStringProperty("subjectId");
        feed.addStringProperty("subjectAvatar");

        feed.addStringProperty("extra");

        new DaoGenerator().generateAll(schema, args[0]);
    }
}
