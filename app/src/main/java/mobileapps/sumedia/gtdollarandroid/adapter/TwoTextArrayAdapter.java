package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by zhuodong on 7/18/14.
 */

public class TwoTextArrayAdapter<T extends TwoTextArrayAdapter.IOption> extends ArrayAdapter<T> {

    public TwoTextArrayAdapter(Context context, List<T> objects, int layoutRes) {
        super(context, layoutRes,android.R.id.text1, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        ((TextView) view.findViewById(android.R.id.text2)).setText(Html.fromHtml(getItem(position).getValue()).toString());
        ((TextView) view.findViewById(android.R.id.text1)).setText(getItem(position).getOption());
        return view;
    }

    public static class Option implements IOption{
        String option;
        String value;

        public Option(String option, String value) {
            this.option = option;
            this.value = value;
        }

        public String getOption() {
            return option;
        }

        public String getValue() {
            return value;
        }
    }

    public interface IOption {
        public String getOption();
        public String getValue();
    }
}
