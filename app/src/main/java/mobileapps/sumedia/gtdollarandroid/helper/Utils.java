package mobileapps.sumedia.gtdollarandroid.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.makeramen.RoundedTransformationBuilder;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.BuildConfig;
import mobileapps.sumedia.gtdollarandroid.GTDApplication;
import mobileapps.sumedia.gtdollarandroid.OrderListActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.BaseEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.model.Circle;
import mobileapps.sumedia.gtdollarandroid.model.LatLng;
import mobileapps.sumedia.gtdollarandroid.model.Order;
import mobileapps.sumedia.gtdollarandroid.model.Service;
import mobileapps.sumedia.gtdollarandroid.model.User;
import mobileapps.sumedia.gtdollarandroid.ui.AddCircleMemberActivity;
import mobileapps.sumedia.gtdollarandroid.ui.ChatActivity;
import mobileapps.sumedia.gtdollarandroid.ui.ChooseContactActivity;
import mobileapps.sumedia.gtdollarandroid.ui.CircleDetailActivity;
import mobileapps.sumedia.gtdollarandroid.ui.CircleMemberListActivity;
import mobileapps.sumedia.gtdollarandroid.ui.CreateEditPostActivity;
import mobileapps.sumedia.gtdollarandroid.ui.EWalletActivity;
import mobileapps.sumedia.gtdollarandroid.ui.GenericWebViewActivity;
import mobileapps.sumedia.gtdollarandroid.ui.MerchantDetailActivity;
import mobileapps.sumedia.gtdollarandroid.ui.MerchantMapActivity;
import mobileapps.sumedia.gtdollarandroid.ui.MerchantOptionsActivity;
import mobileapps.sumedia.gtdollarandroid.ui.NearbyMerchantActivity;
import mobileapps.sumedia.gtdollarandroid.ui.OrderPaymentActivity;
import mobileapps.sumedia.gtdollarandroid.ui.QRCodeActivity;
import mobileapps.sumedia.gtdollarandroid.ui.ReviewActivity;
import mobileapps.sumedia.gtdollarandroid.ui.SelectMerchantTypeActivity;
import mobileapps.sumedia.gtdollarandroid.ui.ServiceDetailActivity;
import mobileapps.sumedia.gtdollarandroid.ui.ServiceListActivity;
import mobileapps.sumedia.gtdollarandroid.widget.GenericAlertPopup;
import mobileapps.sumedia.gtdollarandroid.widget.ProgressPopup;

public class Utils {
    private Utils() {
    }

    public static boolean isEventSuccess(BaseEvent event) {
        return event.getStatus() == EventStatus.SUCCEED;
    }

    public static void toast(Context ctx, String message) {
        Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
    }

    public static void search(Context context, LatLng latLng, String queryText, String type) {
        search(context,latLng,queryText,type,false);
    }

    public static void search(Context context, LatLng latLng, String queryText, String type, boolean newMerchant) {
        Intent intent = new Intent(context, NearbyMerchantActivity.class);
        intent.putExtra(Const.INTENT_EXTRA_KEYWORD, queryText);
        intent.putExtra(Const.INTENT_EXTRA_LATLNG, latLng);
        intent.putExtra(Const.INTENT_EXTRA_MERCHANT_TYPE, type);
        intent.putExtra(Const.INTENT_EXTRA_NEW_MERCHANT, newMerchant);
        //intent.putExtra(Const.INTENT_EXTRA_SPECIFICSEARCH, specificSearch);
        context.startActivity(intent);
    }

//    public static void searchGoogleMerchant(Context context, String keyword, LatLng latLng) {
//        Intent intent = new Intent(context, NearbyGooglePlaceActivity.class);
//        intent.putExtra(Const.INTENT_EXTRA_KEYWORD, keyword);
//        intent.putExtra(Const.INTENT_EXTRA_LATLNG, latLng);
//        context.startActivity(intent);
//    }

    public static void editService(Context context, Service item) {
        Intent intentMap = new Intent(context, CreateEditPostActivity.class);
        intentMap.putExtra(Const.INTENT_EXTRA_SERVICE, item);
        context.startActivity(intentMap);
    }

    public static void viewService(Context context, ArrayList<Service> service, int position) {
        Intent intentMap = new Intent(context, ServiceDetailActivity.class);
        intentMap.putParcelableArrayListExtra(Const.INTENT_EXTRA_SERVICE_LIST, service);
        intentMap.putExtra(Const.INTENT_EXTRA_POSITION, position);
        context.startActivity(intentMap);
    }

    public static void viewServiceList(Context context, User merchant) {
        Intent intentMap = new Intent(context, ServiceListActivity.class);
        intentMap.putExtra(Const.INTENT_EXTRA_MERCHANT_KEY, merchant);
        context.startActivity(intentMap);
    }

    public static void viewLink(Context context, String url) {
//        Intent intentMap = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//        context.startActivity(intentMap);
        Intent intent = new Intent(context,GenericWebViewActivity.class);
        intent.putExtra("url",url);
        context.startActivity(intent);
    }


    public static void playLink(Activity context, String videoId) {
//        Intent intentMap = new Intent(Intent.ACTION_VIEW);
//        intentMap.setClassName(context,"com.google.android.youtube");
//        intentMap.setDataAndType(Uri.parse(url),"video/*");
//
//        if (intentMap.resolveActivity(context.getPackageManager()) != null) {
//            context.startActivity(intentMap);
//        }else {
//
//        }

        if(Patterns.WEB_URL.matcher(videoId).matches()) {

            Intent backup = new Intent(Intent.ACTION_VIEW,Uri.parse(videoId));
            //backup.setDataAndType(Uri.parse(url),"video/*");
            context.startActivity(backup);
        }else if(!TextUtils.isEmpty(videoId)){
            //play youtube player
            Intent intent = YouTubeStandalonePlayer.createVideoIntent(
                context, context.getString(R.string.google_maps_key), videoId, 0, true, false);
            //context.startActivity(intent);
            if (intent != null) {
                if (canResolveIntent(context,intent)) {
                    context.startActivityForResult(intent, Const.REQUEST_START_STANDALONE_PLAYER);
                } else {
                    // Could not resolve the intent - must need to install or update the YouTube API service.
                    YouTubeInitializationResult.SERVICE_MISSING
                        .getErrorDialog(context, Const.REQUEST_RESOLVE_SERVICE_MISSING).show();
                }
            }
        }


    }

    private static boolean canResolveIntent(Activity activity,Intent intent) {
        List<ResolveInfo> resolveInfo = activity.getPackageManager().queryIntentActivities(intent, 0);
        return resolveInfo != null && !resolveInfo.isEmpty();
    }

    public static void viewEWallet(FragmentActivity activity) {
        Intent intent = new Intent(activity, EWalletActivity.class);
        activity.startActivity(intent);
    }

    public static void addMember(FragmentActivity activity, Circle circle) {
        Intent intent = new Intent(activity, AddCircleMemberActivity.class);
        intent.putExtra(Const.INTENT_EXTRA_CIRCLE,circle);
        activity.startActivity(intent);
    }

    public static void addContact(FragmentActivity activity) {
        Intent intent = new Intent(activity, ChooseContactActivity.class);
        activity.startActivity(intent);
    }

    public static void chatWith(Activity context,String merchantId, String jabberId, String name, String avatar) {
        if (!TextUtils.isEmpty(jabberId)) {
            Intent intentChat = new Intent(context, ChatActivity.class);
            intentChat.setData(Uri.parse(jabberId));
            intentChat.putExtra(Const.INTENT_EXTRA_USER_NAME, name);
            intentChat.putExtra(Const.INTENT_EXTRA_MERCHANT_ID, merchantId);
            intentChat.putExtra(Const.INTENT_EXTRA_USER_AVATAR, avatar);
            context.startActivity(intentChat);
        }
    }

    public static void viewNearbyShop(FragmentActivity activity, LatLng latlong) {
        Intent intentChat = new Intent(activity, SelectMerchantTypeActivity.class);
        intentChat.putExtra(Const.INTENT_EXTRA_LATLNG, latlong);
        intentChat.putExtra(Const.INTENT_EXTRA_NEW_MERCHANT, false);
        activity.startActivity(intentChat);
    }

    public static void viewMerchantOptions(Activity activity,String merchantId) {
        Intent intentChat = new Intent(activity, MerchantOptionsActivity.class);
        intentChat.putExtra(Const.INTENT_EXTRA_MERCHANT_ID, merchantId);
        activity.startActivity(intentChat);
    }

    public interface StatusListener<T> {
        public void onSucceed(T event);
    }

    public static <T extends BaseEvent> void toggleEventPopup(BaseActivity activity, T baseEvent,
                                                              StatusListener<T> statusListener) {
        toggleEventPopup(activity,baseEvent,R.string.please_wait,statusListener);
    }

    public static <T extends BaseEvent> void toggleEventPopup(BaseActivity activity, T baseEvent,int desc,
                                                              StatusListener<T> statusListener) {
        EventStatus status = baseEvent.getStatus();
        if (status == EventStatus.ONGOING) {
            displayProgress(activity,desc);
        } else if (status == EventStatus.SUCCEED) {
            dismissProgress(activity);
            if (statusListener != null) {
                statusListener.onSucceed(baseEvent);
            }
        } else if (status == EventStatus.FAILED) {
            dismissProgress(activity);
            displayError(activity, baseEvent.getErrorMsg());
        }
    }

    public static void displayProgress(BaseActivity activity) {
        displayProgress(activity, 0);
    }

    public static void displayProgress(BaseActivity activity, int descId) {
        if (activity.isPaused || activity.isFinishing()) {
            return;
        }
        FragmentManager fm = activity.getSupportFragmentManager();
        ProgressPopup.newInstance(descId).show(fm, "progress");
    }

    public static void displayError(BaseActivity activity, String desc) {
        displayAlertPopup(activity, GenericAlertPopup.ALERT_ERROR,
            activity.getString(R.string.alert_title_default), desc,
            activity.getString(R.string.ok), null);
    }

    public static void displayAlert(BaseActivity activity, int alertType, String desc) {
        displayAlertPopup(activity, alertType, "", desc, activity.getString(R.string.ok),
            null);
    }

    public static void displaySelection(BaseActivity activity, int alertType, String desc) {
        displayAlertPopup(activity, alertType, "", desc, activity.getString(R.string.ok),
            activity.getString(R.string.string_cancel));
    }

    public static void displayAlertPopup(BaseActivity activity, int alertType, String title,
                                         String desc, String positive, String negative) {
        if (activity.isPaused || activity.isFinishing()) {
            return;
        }
        FragmentManager fm = activity.getSupportFragmentManager();
        GenericAlertPopup.newInstance(alertType, title, desc, positive, negative).show(fm, "alert");
    }

    public static void dismissProgress(BaseActivity activity) {
        //if(!activity.isAlive || activity.isFinishing()) return;
        Fragment fragment = activity.getSupportFragmentManager().findFragmentByTag("progress");
        if (fragment instanceof ProgressPopup) {
            ((ProgressPopup) fragment).dismiss();
        }
    }

    public static void hideKeyBoard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(
            Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showKeyBoard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(
            Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, 0);
    }

    public static void reviewOrder(Activity activity, ArrayList<String> options, int type) {
        Intent intent = new Intent(activity, ReviewActivity.class);
        intent.putStringArrayListExtra(Const.INTENT_EXTRA_REVIEW_OPTIONS, options);
        intent.putExtra(Const.INTENT_EXTRA_REVIEW_TYPE, type);
        activity.startActivity(intent);
    }

    public static void viewQRCode(Activity activity, String url) {
        Intent intent = new Intent(activity, QRCodeActivity.class);
        intent.putExtra(Const.INTENT_EXTRA_URL, url);
        activity.startActivity(intent);
    }

    public static void returnToWallet(Activity activity) {
        Intent intent = new Intent(activity, EWalletActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        activity.startActivity(intent);
        activity.finish();
    }

    public static void viewCircle(Activity activity, Circle circle) {
        Intent intent = new Intent(activity, CircleDetailActivity.class);
        intent.putExtra(Const.INTENT_EXTRA_CIRCLE, circle);
        activity.startActivity(intent);
    }

    public static void viewCircleMembers(Activity activity, Circle circle) {
        Intent intent = new Intent(activity, CircleMemberListActivity.class);
        intent.putExtra(Const.INTENT_EXTRA_CIRCLE, circle);
        activity.startActivity(intent);
    }

    public static void viewMerchantMap(Activity activity, LatLng originLatLng, String title) {
        if (originLatLng == null || originLatLng.getLat() == null || originLatLng.getLng() == null)
            return;
        Intent intentMap = new Intent(activity, MerchantMapActivity.class);
        com.google.android.gms.maps.model.LatLng latLng = new com.google.android.gms.maps.model.LatLng(originLatLng.getLat(), originLatLng.getLng());
        intentMap.putExtra(Const.INTENT_EXTRA_LATLNG, latLng);
        intentMap.putExtra(Const.INTENT_EXTRA_NAME, title);
        activity.startActivity(intentMap);
    }

    public static void viewMerchant(Activity activity, User merchant, String merchantId) {
        Intent intentMap = new Intent(activity, MerchantDetailActivity.class);
        intentMap.putExtra(Const.INTENT_EXTRA_MERCHANT_ID, merchantId);
        intentMap.putExtra(Const.INTENT_EXTRA_MERCHANT_KEY, merchant);
        activity.startActivity(intentMap);
    }

    public static void viewMerchant(Activity activity, User merchant, String merchantId, boolean newMerchant) {
        Intent intentMap = new Intent(activity, MerchantDetailActivity.class);
        intentMap.putExtra(Const.INTENT_EXTRA_MERCHANT_ID, merchantId);
        intentMap.putExtra(Const.INTENT_EXTRA_MERCHANT_KEY, merchant);
        intentMap.putExtra(Const.INTENT_EXTRA_NEW_MERCHANT, newMerchant);
        activity.startActivity(intentMap);
    }

//    public static void viewGoogleMerchant(Activity activity, GooglePlace place) {
//        Intent intentMap = new Intent(activity, GoogleMerchantDetailActivity.class);
//        intentMap.putExtra(Const.INTENT_EXTRA_PLACE_KEY, place);
//        activity.startActivity(intentMap);
//    }

    public static void viewOrders(Activity activity, String merchantId, Order order) {
        Intent intentMap = new Intent(activity, OrderListActivity.class);
        intentMap.putExtra(Const.INTENT_EXTRA_MERCHANT_ID, merchantId);
        intentMap.putExtra(Const.INTENT_EXTRA_ORDER, order);
        activity.startActivity(intentMap);
    }

    public static void payOrder(Activity activity, Order order, double amount) {
        Intent intentMap = new Intent(activity, OrderPaymentActivity.class);
        intentMap.putExtra(Const.INTENT_EXTRA_ORDER, order);
        intentMap.putExtra(Const.INTENT_EXTRA_AMOUNT, amount);
        activity.startActivity(intentMap);
    }


    public static boolean checkCredit(Context context, double amount) {
        if (BuildConfig.DEBUG) return true;
        if (amount > UserDataHelper.getCredit()) {
            Utils.toast(context, context.getString(R.string.toast_credit_not_enought, UserDataHelper.getCredit()));
            return false;
        }
        return true;
    }

    public static double parsePercentage(String percentage) {
        NumberFormat numberFormat = NumberFormat.getPercentInstance();
        numberFormat.setMaximumFractionDigits(2);
        try {
            return numberFormat.parse(percentage).doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    static String deviceId;

    public static String getDeviceId() {
        if (TextUtils.isEmpty(deviceId)) {
            deviceId = Settings.Secure.getString(GTDApplication.getInstance().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        }
        return deviceId;
    }

    /**
     * Uses static final constants to detect if the device's platform version is Honeycomb or
     * later.
     */
    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    /**
     * Uses static final constants to detect if the device's platform version is Honeycomb MR1 or
     * later.
     */
    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    /**
     * Uses static final constants to detect if the device's platform version is ICS or
     * later.
     */
    public static boolean hasICS() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    }

    public static Transformation getRoundTransformation(int radius) {
        return new RoundedTransformationBuilder()
//            .borderColor(context.getResources().getColor(R.color.soft_green))
//            .borderWidthDp(5)
            .cornerRadius(radius)
            .oval(false)
            .build();
    }

    public static File createImageFile(Context context, File storageDir) {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //        File storageDir =
        //        ;

        File image = null;
        try {
            image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    public static void setLanguage(Activity context) {

        String localeStr = UserDataHelper.getLanguage();

        if (!TextUtils.isEmpty(localeStr)) {
            Configuration config = context.getBaseContext().getResources().getConfiguration();
            //android.content.res.Configuration config = new android.content.res.Configuration();
            Locale locale;
            if (localeStr.equalsIgnoreCase("zh_CN"))
                locale = Locale.SIMPLIFIED_CHINESE;
            else if (localeStr.equalsIgnoreCase("zh_TW"))
                locale = Locale.TRADITIONAL_CHINESE;
            else
                locale = new Locale(localeStr);

            config.locale = locale;
            context.getBaseContext().getResources().updateConfiguration(config,
                context.getBaseContext().getResources().getDisplayMetrics());
        }
    }

    public static void screenShotAndShare(Activity activity) {
        View view = activity.getWindow().getDecorView().getRootView();
        int oldLayerType = view.getLayerType();
        view.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = view.getDrawingCache();

        if(bitmap != null) {
            File destFile = createImageFile(activity,activity.getExternalCacheDir());
            try {
                boolean saved = bitmap.compress(Bitmap.CompressFormat.JPEG,80, new FileOutputStream(destFile));
                if(saved) {
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+destFile.getAbsolutePath()));
                    shareIntent.setType("image/jpeg");
                    activity.startActivity(shareIntent);
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        view.setLayerType(oldLayerType,null);
    }

    public static void dial(Context context,String phone) {
        if(!TextUtils.isEmpty(phone)) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
            context.startActivity(intent);
        }
    }

    public static void viewUrl(Context context,String url) {
        if(!TextUtils.isEmpty(url)) {
            Intent intent = new Intent(context,GenericWebViewActivity.class);
            intent.putExtra("url",url);
            context.startActivity(intent);
        }
    }

    public static boolean checkAccount(BaseActivity context) {
        if(UserDataHelper.isVerified())return true;
        displayError(context,context.getString(R.string.not_yet_registered));
        return false;
    }
}
