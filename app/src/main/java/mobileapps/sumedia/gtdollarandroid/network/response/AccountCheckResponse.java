package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by zhuodong on 8/8/14.
 */
public class AccountCheckResponse extends AccountBaseResponse {
    Data data;
    public Data getData() {
        return data;
    }

    public class Data {

        @SerializedName("Balance")
        @Expose
        private double balance;
        @SerializedName("Credits")
        @Expose
        private double credits;

        public double getBalance() {
            return balance;
        }

        public void setBalance(double balance) {
            this.balance = balance;
        }

        public double getCredits() {
            return credits;
        }

        public void setCredits(double credits) {
            this.credits = credits;
        }

    }
}
