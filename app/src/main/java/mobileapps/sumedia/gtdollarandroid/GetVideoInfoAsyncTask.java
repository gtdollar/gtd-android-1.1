package mobileapps.sumedia.gtdollarandroid;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;

import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.chat.ChatConstants;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.GetVideoInfoEvent;
import mobileapps.sumedia.gtdollarandroid.helper.FileUtils;

import static mobileapps.sumedia.gtdollarandroid.chat.ChatConstants.MessageType.image;

/**
* Created by zhuodong on 4/9/14.
*/
public class GetVideoInfoAsyncTask extends AsyncTask<Void,Void,Boolean>{

    private String mVideoPath;
    private Context context;
    HashMap<String, String> info = new HashMap<String, String>();

    public GetVideoInfoAsyncTask( Context context, String mVideoPath) {
        this.mVideoPath = mVideoPath;
        this.context = context;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        MediaMetadataRetriever mediaMetaDataRetriever = new MediaMetadataRetriever();
        mediaMetaDataRetriever.setDataSource(mVideoPath);
        Bitmap bitmap = mediaMetaDataRetriever.getFrameAtTime();
        try {
            File imageFile = FileUtils.createFile(context, image.name());
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, new FileOutputStream(imageFile));
            info.put(ChatConstants.MESSAGE_KEY_VIDEO_LENGTH,mediaMetaDataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
            info.put(ChatConstants.MESSAGE_KEY_VIDEO_THUMBNAIL,imageFile.getAbsolutePath());
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        EventBus.getDefault().post(new GetVideoInfoEvent(EventStatus.ONGOING));
    }

    @Override
    protected void onPostExecute(Boolean success) {
        EventBus.getDefault().post(new GetVideoInfoEvent(EventStatus.SUCCEED,info));
    }
}
