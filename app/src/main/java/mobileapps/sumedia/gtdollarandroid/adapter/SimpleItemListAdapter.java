package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import mobileapps.sumedia.gtdollarandroid.model.SimpleIconTextItem;


public class SimpleItemListAdapter extends PagingBaseAdapter<SimpleIconTextItem> {
    int layoutResourceId;
    int textResourceId;
    int imageResourceId;

    public SimpleItemListAdapter(Context context, int layoutResourceId, int imageResourceId, int textResourceId
                                 ) {
        super(context);
        this.layoutResourceId = layoutResourceId;
        this.imageResourceId = imageResourceId;
        this.textResourceId = textResourceId;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position,convertView,parent);
        ((ImageView) view.findViewById(imageResourceId)).setImageDrawable(getItem(position).getIcon());
        return view;
    }

    @Override
    protected int getViewResourceId(int position) {
        return layoutResourceId;
    }

    @Override
    protected Renderer<SimpleIconTextItem> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }

    class ViewHolder implements Renderer<SimpleIconTextItem> {
        TextView mTvTitle;
        ImageView mIvIcon;
        View view;

        ViewHolder(View view) {
            mTvTitle = (TextView) view.findViewById(textResourceId);
            mIvIcon = (ImageView)view.findViewById(imageResourceId);
            this.view = view;
        }

        public void render(SimpleIconTextItem item, int position) {
            mTvTitle.setText(item.getTitle());
            mIvIcon.setImageDrawable(item.getIcon());
        }
    }


}