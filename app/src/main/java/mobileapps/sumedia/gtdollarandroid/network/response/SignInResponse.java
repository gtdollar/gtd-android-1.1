    package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.model.User;

/**
 * Created by zhuodong on 7/22/14.
 */
public class SignInResponse extends BaseResponse{

    @SerializedName("auth_token")
    @Expose
    private String authToken;

    @Expose
    private User user;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void saveToPref() {
        UserDataHelper.saveUser(user);
        UserDataHelper.setToken(authToken);
    }
}
