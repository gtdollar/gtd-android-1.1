package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

abstract public class PagingBaseAdapter<T> extends BaseAdapter {
    protected List<T> items = new ArrayList<T>();
    Context context;
    LayoutInflater inflater;

    public PagingBaseAdapter(Context context) {
        super();
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(
            Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public T getItem(int i) {
        return items == null ? null : items.get(i);
    }


    public void addMoreItems(List<T> newItems, boolean append) {
        if(append) {
            this.items.addAll(newItems);
        }else {
            this.items.addAll(0, newItems);
        }
//        Timber.d("replace new list");
//        for (T newItem : newItems) {
//            Timber.d(newItem.toString());
//        };
        notifyDataSetChanged();
    }

    public void removeAllItems() {
        this.items.clear();
    }

    public void remove(T item) {
        items.remove(item);
        notifyDataSetChanged();
    }

    public void update(T item) {
        int index = items.indexOf(item);
        if(index >= 0) {
            items.remove(item);
            items.add(index,item);
            notifyDataSetChanged();
        }
    }

    public void add(T item) {
        items.add(item);
        notifyDataSetChanged();
    }

    public void add(int index, T item) {
        items.add(index,item);
        notifyDataSetChanged();
    }

    public void setItems(List<T> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public List<T> getItems() {
        return items;
    }

    public Context getContext() {
        return context;
    }

    public LayoutInflater getInflater() {
        return inflater;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Renderer<T> holder;
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        if (convertView == null) {
            convertView = inflater.inflate(getViewResourceId(position), parent, false);
            holder = getRenderer(convertView);
            convertView.setTag(holder);

        } else {
            holder = (Renderer<T>) convertView.getTag();
        }

        T item = getItem(position);

        if (item != null) {
            holder.render(item,position);
        }
        return convertView;
    }

    protected abstract int getViewResourceId(int position);

    protected abstract Renderer<T> getRenderer(View rootView);

    public interface Renderer<T> {
        public void render(T item, int position);
    }
}