package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.CreditRecordAdapter;
import mobileapps.sumedia.gtdollarandroid.event.CreditRecordEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;

public class RecordListActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    @InjectView(R.id.lv_record)
    ListView mLvRecord;
    CreditRecordAdapter mRecordAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_list);
        ButterKnife.inject(this);
        mRecordAdapter = new CreditRecordAdapter(this);
        mLvRecord.setAdapter(mRecordAdapter);
        mLvRecord.setOnItemClickListener(this);
        JobManager.getInstance().accountCreditRecord();
    }

    public void onEventMainThread(CreditRecordEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED && event.getResponse() != null) {
            mRecordAdapter.setItems(event.getResponse().getData());
        }
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this, RecordDetailActivity.class);
        intent.putExtra(Const.INTENT_EXTRA_CREDIT_RECORD,mRecordAdapter.getItem(i));
        startActivity(intent);
    }
}
