package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ListView;

import java.util.Collections;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.CustomerRequestListAdapter;
import mobileapps.sumedia.gtdollarandroid.event.AttemptCancelOrderEvent;
import mobileapps.sumedia.gtdollarandroid.event.CancelOrderEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.GetOrdersEvent;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.widget.SlidingTabLayout;


public class CustomerRequestsActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    SectionsPagerAdapter mSectionsPagerAdapter;


    static final int TAB_COUNT = 2;
    String[] titles;

    @InjectView(R.id.stl)
    SlidingTabLayout stl;
    @InjectView(R.id.pager)
    ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager);
        ButterKnife.inject(this);
        setTitle(R.string.title_activity_my_requests);

        // Create the mobileapps.sumedia.gtdollarandroid.adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());


        // Set up the ViewPager with the sections mobileapps.sumedia.gtdollarandroid.adapter.
        pager.setAdapter(mSectionsPagerAdapter);

        titles = getResources().getStringArray(R.array.tab_request_title);
        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        //pager.setOnPageChangeListener(this);
        stl.setDistributeEvenly(true);

        stl.setViewPager(pager);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return OrderListFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return TAB_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position < titles.length && position >= 0) {
                return titles[position];
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class OrderListFragment extends ListFragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        CustomerRequestListAdapter listAdapter;

        int section;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static OrderListFragment newInstance(int sectionNumber) {
            OrderListFragment fragment = new OrderListFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public OrderListFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            listAdapter = new CustomerRequestListAdapter(getActivity());
            setListAdapter(listAdapter);
            section = getArguments().getInt(ARG_SECTION_NUMBER);
            listAdapter.setIsNew(section == 0);
            JobManager.getInstance().getOrders(false, UserDataHelper.getUserId(), section);
        }

        public void onEventMainThread(GetOrdersEvent event) {
            //Log.d(""," testing status "+event.getStatus());
            if (event.getStatus() == EventStatus.SUCCEED && event.getOrderStatus() == section) {
                if (event.getResponse() != null) {
                    Collections.reverse(event.getResponse().getOrder());
                    listAdapter.setItems(event.getResponse().getOrder());
                }
            }
        }

        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            super.onListItemClick(l, v, position, id);
            Utils.viewOrders(getActivity(), listAdapter.getItem(position).getMerchant().getId(), listAdapter.getItem(position));
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            EventBus.getDefault().register(this);
        }

        @Override
        public void onDetach() {
            super.onDetach();
            EventBus.getDefault().unregister(this);
        }
    }

    public void onEventMainThread(AttemptCancelOrderEvent event) {
        //Log.d(""," testing status "+event.getStatus());
        JobManager.getInstance().cancelOrder(event.getOrderId());

    }

    public void onEventMainThread(CancelOrderEvent event) {

        Utils.toggleEventPopup(this, event, new Utils.StatusListener<CancelOrderEvent>() {
            @Override
            public void onSucceed(CancelOrderEvent event) {
                JobManager.getInstance().getOrders(false, UserDataHelper.getUserId(), 0);
            }
        });
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }
}


