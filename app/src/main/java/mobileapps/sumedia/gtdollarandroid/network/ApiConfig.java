package mobileapps.sumedia.gtdollarandroid.network;

import mobileapps.sumedia.gtdollarandroid.BuildConfig;
import mobileapps.sumedia.gtdollarandroid.GTDApplication;
import mobileapps.sumedia.gtdollarandroid.R;

/**
 * Created by zhuodong on 7/22/14.
 */
public interface ApiConfig {
    public static final String DATA_API_BASE = BuildConfig.DEBUG?"http://apilive.gtdollar.com/":"http://apilive.gtdollar.com";
    public static final String ACCOUNT_API_BASE = "http://apilive.gtdollar.com/api/";

    public static class DataEndPoint {

        public static final String PATH_SIGN_IN = "/signin";
        public static final String PATH_FORGOT_PWD = "/user/forgot_password";
        public static final String PATH_SEARCH = "/search";
        public static final String FILE_UPLOAD = "/file/upload";
        public static final String PATH_USER_SIGN_UP = "/user/add";
        public static final String PATH_MERCHANT_SIGN_UP = "/merchant/add";

        public static final String PATH_GET_MERCHANT = "/merchant/get";

        public static final String PATH_USER_CIRCLE = "/circle/get_my_circles";
        public static final String PATH_CREATE_CIRCLE = "/circle/add";
        public static final String PATH_GET_CIRCLE = "/circle/get";
        public static final String PATH_UPDATE_CIRCLE = "/circle/update";
        public static final String PATH_ADD_PEOPLE = "/circle/add_people";
        public static final String PATH_REMOVE_PEOPLE = "/circle/remove_people";
        public static final String PATH_REMOVE_CIRCLE = "/circle/remove";


        public static final String PATH_NOTIFICATION = "/notifications";
        public static final String PATH_CONTACTS = "/contact/get_list";
        public static final String PATH_ADD_CONTACTS = "/contact/add";
        public static final String PATH_REMOVE_CONTACTS = "/contact/remove";

        public static final String PATH_GET_SERVICE = "/service/get_my_services";
        public static final String PATH_CREATE_SERVICE = "/service/add";
        public static final String PATH_UPDATE_SERVICE = "/service/update";
        public static final String PATH_REMOVE_SERVICE = "/service/remove";
        public static final String PATH_SERVICE_DETAIL = "/service/get";

        public static final String PATH_GET_ORDER = "/order/get_orders";
        public static final String PATH_CREATE_ORDER = "/order/create";
        public static final String PATH_UPDATE_ORDER = "/order/update";
        public static final String PATH_REMOVE_ORDER = "/order/remove";
        public static final String PATH_CANCEL_ORDER = "/order/cancel";
        public static final String PATH_CONFIRM_ORDER = "/order/confirm";
        public static final String PATH_ORDER_DETAIL = "/order/get";

        public static final String PATH_USER_UPDATE = "/user/update";

        public static final String PATH_ADD_BLACK_LIST = "/contact/addBlacklist";
        public static final String PATH_REMOVE_BLACK_LIST = "/contact/removeBlacklist";

        public static final String PATH_ADD_BLOCK_LIST = "/contact/addBlocklist";
        public static final String PATH_REMOVE_BLOCK_LIST = "/contact/removeBlocklist";

        public static final String PATH_COMPLAIN = "/complain";

        public static final String PATH_BLACK_LIST = "/contact/getBlacklist";
        public static final String PATH_BLOCK_LIST = "/contact/getBlocklist";

    }


    public static class AccountEndPoint {
        public static final String PATH_SIGN_IN = "/signin.php";
        public static final String PATH_SIGN_UP = "/register.php";
        public static final String PATH_REACTIVE = "/reactive.php";
        public static final String PATH_PAY_MERCHANT = "/payment/payment.php";
        public static final String PATH_APPLY_CREDIT = "/payment/apply_credit.php";
        public static final String PATH_SEND_CREDIT = "/payment/send_credit.php";
        public static final String PATH_TRANSFER_BALANCE = "/payment/transfer.php";
        public static final String PATH_CREDIT_RECORD = "/payment/credit_transaction.php";
        public static final String PATH_BALANCE_RECORD = "/payment/transaction.php";
        public static final String PATH_ACCOUNT = "/payment/gl.php";
        public static final String PATH_QR_PAY = "/payment/qr.php";
        public static final String PATH_MERCHANT_RATE = "/payment/merchant_rate.php";
        public static final String PATH_PAY_ACTIVITIES = "/payment/activities.php";
        public static final String PATH_REFUND = "/payment/refund.php";

        public static final String PATH_UNIONPAY = "/payment/unionpay.php";
        public static final String PATH_CREDIT_CARD = "/payment/visa.php";

    }

    public static final String GOOGLE_API_BASE = "https://maps.googleapis.com/maps/api/";
    public static final String GOOGLE_PHOTOS = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&key="+ GTDApplication.getInstance().getString(R.string.google_places_key)+"&photoreference=";

    public static class GoogleApiEndPoint {
        public static final String PATH_PLACES = "/place/nearbysearch/json";
        public static final String PATH_PLACE_DETAIL = "/place/details/json";
        public static final String PATH_PHOTOS = "place/photo?maxwidth=400&key="+ GTDApplication.getInstance().getString(R.string.google_places_key)+"&photoreference=";
    }


//    public final class DynamicEndpoint implements Endpoint {
//
//        private boolean isAccountApi;
//
//        public void setAccountApi(boolean isAccountApi) {
//            this.isAccountApi = isAccountApi;
//        }
//
//        @Override public String getName() {
//            return "default";
//        }
//
//        @Override public String getUrl() {
//            return isAccountApi?ACCOUNT_API_BASE:DATA_API_BASE;
//        }
//    }
}
