package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.Collections;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.MyPostListAdapter;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.GetServicesEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;

public class MyPostActivity extends BaseActivity {

    MyPostListAdapter adapter;

    @InjectView(R.id.lv_post)
    ListView mLvPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_post);
        ButterKnife.inject(this);
        setTitle(R.string.title_activity_my_posts);
        setUpListView();
    }

    public void setUpListView() {
        adapter = new MyPostListAdapter(this);
        mLvPost.setAdapter(adapter);
        mLvPost.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Utils.editService(MyPostActivity.this, adapter.getItem(position));

            }
        });
        JobManager.getInstance().getService();
    }


    public void onEventMainThread(GetServicesEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED) {
            Collections.reverse(event.getResponse().getService());
            adapter.setItems(event.getResponse().getService());
        }
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add) {
            Utils.editService(this,null);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_post, menu);
        return true;
    }
}
