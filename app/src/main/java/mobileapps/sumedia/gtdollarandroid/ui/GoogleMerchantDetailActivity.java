//package mobileapps.sumedia.gtdollarandroid.ui;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.text.TextUtils;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.HorizontalScrollView;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.squareup.picasso.Picasso;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import butterknife.OnClick;
//import mobileapps.sumedia.gtdollarandroid.BaseActivity;
//import mobileapps.sumedia.gtdollarandroid.R;
//import mobileapps.sumedia.gtdollarandroid.event.AlertButtonClickEvent;
//import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
//import mobileapps.sumedia.gtdollarandroid.event.GetPlaceDetailEvent;
//import mobileapps.sumedia.gtdollarandroid.helper.Const;
//import mobileapps.sumedia.gtdollarandroid.helper.Utils;
//import mobileapps.sumedia.gtdollarandroid.model.GooglePhoto;
//import mobileapps.sumedia.gtdollarandroid.model.GooglePlace;
//import mobileapps.sumedia.gtdollarandroid.model.GooglePlaceDetail;
//import mobileapps.sumedia.gtdollarandroid.model.LatLng;
//import mobileapps.sumedia.gtdollarandroid.model.User;
//import mobileapps.sumedia.gtdollarandroid.network.ApiConfig;
//import mobileapps.sumedia.gtdollarandroid.network.JobManager;
//import mobileapps.sumedia.gtdollarandroid.widget.AspectRatioImageView;
//import mobileapps.sumedia.gtdollarandroid.widget.CircleImageView;
//import mobileapps.sumedia.gtdollarandroid.widget.GenericAlertPopup;
//
//public class GoogleMerchantDetailActivity extends BaseActivity {
//
//
//    @InjectView(R.id.iv_merchant_cover)
//    AspectRatioImageView mIvMerchantCover;
//    @InjectView(R.id.tv_merchant_name)
//    TextView mTvMerchantName;
//    @InjectView(R.id.iv_merchant_picture)
//    CircleImageView mIvMerchantPicture;
//    @InjectView(R.id.tv_merchant_name_2)
//    TextView mTvMerchantName2;
//    @InjectView(R.id.iv_phone)
//    ImageView mIbPhone;
//    @InjectView(R.id.iv_follow)
//    ImageView mIvFollow;
//    @InjectView(R.id.iv_shop)
//    ImageView mIvShop;
//    @InjectView(R.id.iv_chat)
//    ImageView mIvChat;
//    @InjectView(R.id.iv_map)
//    ImageView mIvMap;
//    //    @InjectView(R.id.serviceContainer)
////    LinearLayout mServiceContainer;
////    @InjectView(R.id.hsc_service)
////    HorizontalScrollView mHscService;
//
//    @InjectView(R.id.tv_address)
//    TextView mTvAddress;
//    @InjectView(R.id.tv_phone)
//    TextView mTvPhone;
//    @InjectView(R.id.tv_website)
//    TextView mTvWebsite;
//
//   // @InjectView(R.id.ll_service)
//    LinearLayout mLlService;
//   // @InjectView(R.id.hsc_service)
//    HorizontalScrollView mHscService;
//    //@InjectView(R.id.ll_service_container)
//    LinearLayout mLlServiceContainer;
//    @InjectView(R.id.tv_types)
//    TextView mTvTypes;
//    @InjectView(R.id.ll_profile)
//    LinearLayout mLlProfile;
//    @InjectView(R.id.tv_contact_us)
//    TextView mTvContactUs;
//
//
//    GooglePlace merchant;
//    GooglePlaceDetail merchantDetail;
//    @InjectView(R.id.iv_certified)
//    ImageView mIvCertified;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setTitle(R.string.string_profile);
//        setContentView(R.layout.activity_merchant_detail);
//        ButterKnife.inject(this);
//        //adjustMenuButtonsToFitScreen();
//        merchant = getIntent().getParcelableExtra(Const.INTENT_EXTRA_PLACE_KEY);
//
//        mIvFollow.setAlpha(0.5f);
//        mIvFollow.setEnabled(false);
//
//        mIvChat.setAlpha(0.5f);
//        mIvChat.setEnabled(false);
//
//        mIvShop.setAlpha(0.5f);
//        mIvShop.setEnabled(false);
//
//        mTvContactUs.setVisibility(View.VISIBLE);
//
//        if (merchant != null) {
//            setMerchantDetails();
//        }
//        JobManager.getInstance().getPlaceDetail(merchant.getPlaceId());
//    }
//
//    private void setMerchantDetails() {
//
//        mTvMerchantName.setText(merchant.getName());
//        mTvMerchantName2.setText(merchant.getName());
//        //mTvMerchantName2.setCompoundDrawables(null, null, null, null);
//        mIvCertified.setVisibility(View.GONE);
//
//        mTvTypes.setText(TextUtils.join(", ", merchant.getTypes()));
//
//        if (!TextUtils.isEmpty(merchant.getIcon())) {
//            Picasso.with(this).load(merchant.getIcon()).fit().centerCrop().into((ImageView) mIvMerchantPicture);
//        }
//
//
//
//    }
//
//    @OnClick(R.id.ll_profile)
//    public void onClickProfile() {
//        Utils.viewNearbyShop(this, new LatLng(merchant.getGeometry().getLocation()));
//    }
//
//    @OnClick(R.id.tv_website)
//    public void onViewWebsite(View view) {
//        if (!TextUtils.isEmpty(mTvWebsite.getText())) {
//            Utils.viewLink(this, mTvWebsite.getText().toString());
//        }
//    }
//
//    @OnClick(R.id.tv_contact_us)
//    public void onClickContactUs(View view) {
//        if (merchantDetail == null) return;
//        User user = new User();
//        user.setAddress(merchantDetail.getFormattedAddress());
//        user.setPhone(merchantDetail.getFormattedPhoneNumber());
//        user.setName(merchant.getName());
//        user.setLatlong(new LatLng(merchant.getGeometry().getLocation()));
//        user.setRegion(merchantDetail.getCountry());
//        user.setQuotation(merchantDetail.getWebsite());
//
//        Intent intent = new Intent(this, MerchantProfileActivity.class);
//        intent.putExtra(Const.INTENT_EXTRA_MERCHANT_KEY, user);
//        intent.putExtra(Const.INTENT_EXTRA_IS_UPDATING, false);
//        startActivity(intent);
//    }
//
//    @OnClick({R.id.iv_phone, R.id.tv_phone, R.id.iv_follow, R.id.iv_shop, R.id.iv_chat, R.id.iv_map, R.id.tv_address})
//    public void onMenuClick(View view) {
//        if (merchant == null) return;
//        switch (view.getId()) {
//            case R.id.iv_phone:
//            case R.id.tv_phone:
//                //toast(this, "Phone");
//                if (!TextUtils.isEmpty(mTvPhone.getText())) {
//                    Utils.displaySelection(this, GenericAlertPopup.ALERT_DIAL,getString(R.string.popup_call, merchant.getName()));
//                }
//                break;
//            case R.id.iv_map:
//            case R.id.tv_address:
//                Utils.viewMerchantMap(this, merchant.getGeometry().getLocation(), merchant.getName());
//                //toast(this, "Location");
//                break;
//            default:
//                break;
//        }
//    }
//
//
//    @Override
//    public boolean isSubscribedEvent() {
//        return true;
//    }
//
//    public void onEventMainThread(GetPlaceDetailEvent event) {
//        if (event.getStatus() == EventStatus.SUCCEED && event.getResponse() != null) {
//            merchantDetail = event.getResponse().getResult();
//
//            mTvAddress.setText(merchantDetail.getFormattedAddress());
//            mTvWebsite.setText(merchantDetail.getWebsite());
//            mTvPhone.setText(merchantDetail.getFormattedPhoneNumber());
//
//            // set services
//            if (merchantDetail.getPhotos() != null && merchantDetail.getPhotos().size() != 0) {
//                mLlServiceContainer.setVisibility(View.VISIBLE);
//                final List<GooglePhoto> photoList = merchantDetail.getPhotos();
//                int size = getResources().getDimensionPixelSize(R.dimen.service_secion_height);
//                for (int i = 0; i < photoList.size(); i++) {
//                    ImageView iv = new ImageView(this);
//                    GooglePhoto service = photoList.get(i);
//                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, size);
//                    iv.setLayoutParams(params);
//                    iv.setPadding(5, 5, 5, 5);
//                    Picasso.with(this).load(ApiConfig.GOOGLE_PHOTOS + service.getPhotoReference()).fit().centerCrop().into(iv);
//                    if (i == 0) {
//                        Picasso.with(this).load(ApiConfig.GOOGLE_PHOTOS + service.getPhotoReference()).fit().centerCrop().into(mIvMerchantCover);
//                    }
//
//                    mLlService.addView(iv);
//                    iv.setTag(i);
//                    iv.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            Intent intent = new Intent(GoogleMerchantDetailActivity.this, ViewPhotoActivity.class);
//                            intent.putExtra(Const.INTENT_EXTRA_PHOTO, (ArrayList<GooglePhoto>) photoList);
//                            intent.putExtra(Const.INTENT_EXTRA_POSITION, ((Integer) view.getTag()));
//                            startActivity(intent);
//                        }
//                    });
//                }
//            }
//        }
//    }
//
//    public void onEventMainThread(AlertButtonClickEvent event) {
//        if(event.isPositive() && event.getAlertType() == GenericAlertPopup.ALERT_DIAL) {
//            Utils.dial(this,merchantDetail.getFormattedPhoneNumber());
//        }
//    }
//
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.google_merchant_detail, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//        if (id == R.id.action_share) {
//            Utils.screenShotAndShare(this);
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//
//}
