package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 4/20/14.
 */
public class ChoseItemEvent {
    int type;
    int position;
    CharSequence item;

    public ChoseItemEvent(int type, int position, CharSequence item) {
        this.type = type;
        this.position = position;
        this.item = item;
    }

    public int getType() {
        return type;
    }

    public int getPosition() {
        return position;
    }

    public CharSequence getItem() {
        return item;
    }
}
