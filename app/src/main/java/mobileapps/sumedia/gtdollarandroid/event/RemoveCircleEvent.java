package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 9/24/14.
 */
public class RemoveCircleEvent extends BaseEvent {
    public RemoveCircleEvent(EventStatus eventStatus) {
        super(eventStatus);
    }

    public RemoveCircleEvent(EventStatus eventStatus, String circleId) {
        this(eventStatus);
        this.circleId = circleId;
    }

    String circleId;

    public String getCircleId() {
        return circleId;
    }

    public void setCircleId(String circleId) {
        this.circleId = circleId;
    }
}
