package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;

import mobileapps.sumedia.gtdollarandroid.model.GooglePlaceDetail;

public class GooglePlaceDetailResponse {

    @Expose
    private GooglePlaceDetail result;
    @Expose
    private String status;


    public GooglePlaceDetail getResult() {
        return result;
    }

    public void setResult(GooglePlaceDetail result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
