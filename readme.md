GTDollar Readme


1 use butterkinfe http://jakewharton.github.io/butterknife/ to inject view, instead of calling findViewById, install a android studio plugin to auto generate the code https://github.com/inmite/android-butterknife-zelezny

2 use Retrofit as the REST client http://square.github.io/retrofit/

3 use Picasso http://square.github.io/picasso/ as the image loader

4 use EventBus to pass event among objects https://github.com/greenrobot/EventBus

5 use http://www.jsonschema2pojo.org/ to convert the sample json data to POJO