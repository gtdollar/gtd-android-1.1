//package mobileapps.sumedia.gtdollarandroid.ui;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.GridView;
//import android.widget.Toast;
//
//import java.util.ArrayList;
//
//import mobileapps.sumedia.gtdollarandroid.OrderListActivity;
//import mobileapps.sumedia.gtdollarandroid.R;
//import mobileapps.sumedia.gtdollarandroid.adapter.CartGridAdapter;
//import mobileapps.sumedia.gtdollarandroid.helper.Const;
//import mobileapps.sumedia.gtdollarandroid.helper.Utils;
//import mobileapps.sumedia.gtdollarandroid.model.Service;
//import mobileapps.sumedia.gtdollarandroid.model.User;
//
//public class CartActivity extends Activity {
//
//    User merchant;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_cart);
//        merchant = (User) getIntent().getSerializableExtra(Const.INTENT_SERVICES_KEY);
//        GridView gridView = (GridView) findViewById(R.id.gridView);
//        setupGridView(gridView);
//    }
//
//    public void setupGridView(GridView gridView)
//    {
//        final ArrayList<Service> gridArray = new ArrayList<Service>();
//        gridArray.addAll(merchant.getServices());
//        CartGridAdapter customGridAdapter = new CartGridAdapter(getApplicationContext(), R.layout.view_shopping_item, gridArray);
//        gridView.setAdapter(customGridAdapter);
//
//        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position,
//                                    long id) {
//                Toast.makeText(getApplicationContext(), "Clicked", Toast.LENGTH_SHORT).show();
//                Service service = gridArray.get(position);
//                Intent intent = new Intent(getApplicationContext(),ShoppingItemDetailActivity.class);
//                intent.putExtra(Const.INTENT_SERVICES_KEY,service);
//                startActivity(intent);
//            }
//        });
//    }
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.cart, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//        if (id == R.id.action_order) {
//            proceedOrder();
//            return true;
//        }
//
//        if (id == R.id.action_settings) {
//            Intent intent = new Intent(this, GoogleMapActivity.class);
//            startActivity(intent);
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//
//    private void proceedOrder(){
//        Utils.toast(getApplicationContext(), "Order");
//        Intent intent = new Intent(this, OrderListActivity.class);
//        intent.putExtra(Const.INTENT_SELECTED_SERVICES_KEY,merchant);
//        startActivity(intent);
//    }
//}
