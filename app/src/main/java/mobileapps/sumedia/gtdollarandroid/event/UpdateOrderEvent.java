package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.model.Service;

/**
 * Created by zhuodong on 11/26/14.
 */
public class UpdateOrderEvent {
    Service service;

    public UpdateOrderEvent(Service service) {
        this.service = service;
    }

    public Service getService() {
        return service;
    }
}
