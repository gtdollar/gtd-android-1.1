package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.CircleResponse;

/**
 * Created by zhuodong on 8/29/14.
 */
public class AddCircleEvent extends BaseEvent<CircleResponse> {
    public AddCircleEvent(EventStatus status) {
        super(status);
    }

    public AddCircleEvent(EventStatus status, CircleResponse response) {
        super(status, response);
    }
}
