package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

import mobileapps.sumedia.gtdollarandroid.model.BalanceRecord;

/**
 * Created by zhuodong on 8/21/14.
 */
public class AccountBalanceRecordResponse extends AccountBaseResponse{

    @Expose
    private List<BalanceRecord> data = new ArrayList<BalanceRecord>();


    public List<BalanceRecord> getData() {
        return data;
    }

    public void setData(List<BalanceRecord> data) {
        this.data = data;
    }
}

