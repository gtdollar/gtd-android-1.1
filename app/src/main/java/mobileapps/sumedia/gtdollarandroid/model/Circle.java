package mobileapps.sumedia.gtdollarandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhuodong on 8/29/14.
 */
public class Circle implements Parcelable {

    @Expose
    private String id;
    @Expose
    private String name;
    @Expose
    private String description;
    @Expose
    private int type;
    @Expose
    private String cover;
    @SerializedName("service_count")
    private int serviceCount;
    @SerializedName("member_count")
    private int memberCount;
    @Expose
    private String picture;
    @Expose
    private List<GTUser> members = new ArrayList<GTUser>();
    @Expose
    private List<Service> services = new ArrayList<Service>();
    @Expose
    private GTUser createdBy;
    @Expose
    private String createdOn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public List<GTUser> getMembers() {
        return members;
    }

    public void setMembers(List<GTUser> members) {
        this.members = members;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public GTUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(GTUser createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public int getMemberCount() {
        return memberCount;
    }

    public int getServiceCount() {
        return serviceCount;
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }

    public void setServiceCount(int serviceCount) {
        this.serviceCount = serviceCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeInt(this.type);
        dest.writeString(this.cover);
        dest.writeString(this.picture);
        dest.writeTypedList(members);
        dest.writeTypedList(services);
        dest.writeParcelable(this.createdBy, 0);
        dest.writeString(this.createdOn);
        dest.writeInt(this.serviceCount);
        dest.writeInt(this.memberCount);
    }

    public Circle() {
    }

    private Circle(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.type = in.readInt();
        this.cover = in.readString();
        this.picture = in.readString();
        in.readTypedList(members, GTUser.CREATOR);
        in.readTypedList(services, Service.CREATOR);
        this.createdBy = in.readParcelable(GTUser.class.getClassLoader());
        this.createdOn = in.readString();
        this.serviceCount = in.readInt();
        this.memberCount = in.readInt();
    }

    public static final Parcelable.Creator<Circle> CREATOR = new Parcelable.Creator<Circle>() {
        public Circle createFromParcel(Parcel source) {
            return new Circle(source);
        }

        public Circle[] newArray(int size) {
            return new Circle[size];
        }
    };

    public Circle(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Circle)) return false;

        Circle circle = (Circle) o;

        if (!id.equals(circle.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
