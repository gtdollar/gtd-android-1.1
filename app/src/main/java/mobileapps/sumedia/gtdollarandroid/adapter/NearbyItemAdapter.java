package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.model.User;

/**
 * Created by 2359media on 4/7/14.
 */

public class NearbyItemAdapter extends ArrayAdapter<User> {

    private LayoutInflater mInflater;
    private String[] mStrings;
    private String[] mIcons;
    private int mViewResourceId;

    public NearbyItemAdapter(Context ctx, int viewResourceId,
                             ArrayList<User> merchants) {
        super(ctx, viewResourceId, merchants);

        mInflater = (LayoutInflater) ctx.getSystemService(
            Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = viewResourceId;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId().hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        if (convertView == null) {
            convertView = mInflater.inflate(mViewResourceId, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        User item = getItem(position);

        if (item != null) {
            holder.render(item);
        }
        return convertView;
    }

    class ViewHolder {
        @InjectView(R.id.imageView)
        ImageView mImageView;
        @InjectView(R.id.title_textView)
        TextView mTitleTextView;
        @InjectView(R.id.distance_textView)
        TextView mDistanceTextView;
        @InjectView(R.id.category_textView)
        TextView mCategoryTextView;
        @InjectView(R.id.ratingBar)
        RatingBar mRatingBar;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
            LayerDrawable stars = (LayerDrawable) mRatingBar.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(Color.parseColor("#ff1e6f77"), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(0).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        }


        public void render(User item) {
            if (!TextUtils.isEmpty(item.getPicture())) {
                Picasso.with(getContext()).load(item.getPicture()).into(mImageView);
            }
            mTitleTextView.setText(item.getName());
            mCategoryTextView.setText(item.getCategories() == null || item.getCategories().length == 0? 0+"":item.getCategories()[0]+"");
        }
    }
}
