package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;

/**
 * Created by zhuodong on 7/22/14.
 */
public class BaseResponse implements ErrorResponse{
    @Expose
    boolean success;
    String message;
    int code;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getError() {
        return message;
    }
}
