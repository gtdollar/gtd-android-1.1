package mobileapps.sumedia.gtdollarandroid.chat;

import java.util.Map;

public interface Smackable {
	boolean doConnect(boolean create_account) throws ChatXMPPException;
	boolean isAuthenticated();
	void requestConnectionState(ConnectionState new_state);
	void requestConnectionState(ConnectionState new_state, boolean create_account);
	ConnectionState getConnectionState();
	String getLastError();

//	void addRosterItem(String user, String alias, String group) throws ChatXMPPException;
//	void removeRosterItem(String user) throws ChatXMPPException;
//	void renameRosterItem(String user, String newName) throws ChatXMPPException;
//	void moveRosterItemToGroup(String user, String group) throws ChatXMPPException;
//	void renameRosterGroup(String group, String newGroup);
//	void sendPresenceRequest(String user, String type);
//	void addRosterGroup(String group);
//	String changePassword(String newPassword);

	void setStatusFromConfig();
	void sendMessage(String user, String message,Map<String,String> properties);
	void sendServerPing();
	
	void registerCallback(XMPPServiceCallback callBack);
	void unRegisterCallback();
	
	String getNameForJID(String jid);
}
