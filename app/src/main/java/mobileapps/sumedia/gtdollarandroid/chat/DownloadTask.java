package mobileapps.sumedia.gtdollarandroid.chat;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.Chat;
import mobileapps.sumedia.gtdollarandroid.event.DownloadFileEvent;
import mobileapps.sumedia.gtdollarandroid.helper.FileUtils;
import mobileapps.sumedia.gtdollarandroid.helper.GsonHelper;

import static mobileapps.sumedia.gtdollarandroid.chat.ChatConstants.MessageType.audio;
import static mobileapps.sumedia.gtdollarandroid.chat.ChatConstants.MessageType.image;
import static mobileapps.sumedia.gtdollarandroid.chat.ChatConstants.MessageType.video;

/**
 * Created by zhuodong on 8/15/14.
 */
public class DownloadTask extends AsyncTask<Void, Integer, String> {

    private static final String TAG = "DownloadTask";
    private Chat chat;
    private Context context;
    private PowerManager.WakeLock mWakeLock;

    public String downloadUrl = "",messageType;

    public DownloadTask(Context context,Chat chat) {
        this.context = context;
        this.downloadUrl = chat.getMessage();
        this.messageType = chat.getMessageType();
        this.chat = chat;
    }

    @Override
    protected String doInBackground(Void... params) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(downloadUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.e(TAG,"Server returned HTTP " + connection.getResponseCode()
                    + " " + connection.getResponseMessage());
                return null;
            }

            // this will be useful to display download percentage
            // might be -1: server did not report the length
            int fileLength = connection.getContentLength();

            File outputFile = FileUtils.createFile(context,messageType);
            if(outputFile == null) return null;

            // download the file
            input = connection.getInputStream();
            output = new FileOutputStream(outputFile);

            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    Log.d(TAG," isCancelled ");
                    input.close();
                    return null;
                }
                total += count;
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }

            //special handling
            if(video.name().equals(chat.getMessageType())) {
                MediaMetadataRetriever mediaMetaDataRetriever = new MediaMetadataRetriever();
                mediaMetaDataRetriever.setDataSource(outputFile.getAbsolutePath());
                Bitmap bitmap = mediaMetaDataRetriever.getFrameAtTime(0,MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
                try {
                    if(bitmap != null) {
                        File imageFile = FileUtils.createFile(context, image.name());
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, new FileOutputStream(imageFile));
                        chat.getPropertyMap().put(ChatConstants.MESSAGE_KEY_VIDEO_THUMBNAIL, imageFile.getAbsolutePath());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else if (audio.name().equals(chat.getMessageType())) {
                MediaMetadataRetriever mediaMetaDataRetriever = new MediaMetadataRetriever();
                mediaMetaDataRetriever.setDataSource(outputFile.getAbsolutePath());
                chat.getPropertyMap().put(ChatConstants.MESSAGE_KEY_AUDIO_LENGTH, Integer.parseInt(mediaMetaDataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION))+"");
            }

            chat.getPropertyMap().put(ChatConstants.MESSAGE_KEY_PATH, outputFile.getAbsolutePath());
            chat.setProperties(GsonHelper.mapToJson(chat.getPropertyMap()));
            Log.d(TAG," outputFile "+outputFile.getAbsolutePath());
            return outputFile.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // take CPU lock to prevent CPU from going off if the user
        // presses the power button during download
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
            getClass().getName());
        mWakeLock.acquire();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        // if we get here, length is known, now set indeterminate to false

    }

    @Override
    protected void onPostExecute(String result) {
        mWakeLock.release();
        Log.d(TAG,"path "+result);
        if(result != null)EventBus.getDefault().post(new DownloadFileEvent(chat));
    }
}
