package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.CircleResponse;

/**
 * Created by zhuodong on 8/29/14.
 */
public class GetCircleDetailEvent extends BaseEvent<CircleResponse> {
    public GetCircleDetailEvent(EventStatus status) {
        super(status);
    }

    public GetCircleDetailEvent(EventStatus status, CircleResponse response) {
        super(status, response);
    }

}
