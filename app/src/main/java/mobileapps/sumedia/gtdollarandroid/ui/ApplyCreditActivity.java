package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.ChoseItemEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

import static mobileapps.sumedia.gtdollarandroid.ui.GenericSelectionDialog.TYPE_DISCOUNT_RANGE;

public class ApplyCreditActivity extends BaseActivity {

    @InjectView(R.id.et_amount)
    EditText mEtAmount;
    @InjectView(R.id.et_discount)
    TextView mEtDiscount;
    @InjectView(R.id.et_description)
    EditText mEtDescription;
    @InjectView(R.id.btn_continue)
    Button mBtnContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_gtd);
        ButterKnife.inject(this);
        buildForm();
    }

    Form mForm;

    private void buildForm() {
        mForm = new Form(this);
        //mForm.addField(Field.using(mTvProfileGender).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mEtAmount).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mEtDiscount).validate(NotEmpty.build(this)));
    }

    @OnClick(R.id.et_discount)
    public void onDiscountClicked(View view) {
        GenericSelectionDialog.newInstance(TYPE_DISCOUNT_RANGE, mEtDiscount.getText()).show(this.getSupportFragmentManager(), null);
    }

    @OnClick({R.id.btn_continue})
    public void onClickNext(View view) {
        if(mForm.isValid()) {
            ArrayList<String> options = new ArrayList<String>();
            options.add(mEtAmount.getText().toString());
            options.add(Utils.parsePercentage(mEtDiscount.getText().toString())+"");
            options.add(mEtDescription.getText().toString());
            Utils.reviewOrder(this,options, Const.REVIEW_APPLY_CREDIT);
        }
    }

    public void onEventMainThread(ChoseItemEvent event) {
        if (event == null) {
            return;
        }
        CharSequence item = event.getItem();
        switch (event.getType()) {
            case TYPE_DISCOUNT_RANGE:
                mEtDiscount.setError(null);
                mEtDiscount.setText(item);
                break;
//            case TYPE_GENDER:
//                mTvProfileGender.setError(null);
//                mTvProfileGender.setText(item);
////                mProfile.setGender(item.toString());
//                break;
        }
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }
}
