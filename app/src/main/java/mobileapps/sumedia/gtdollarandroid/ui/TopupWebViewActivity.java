package mobileapps.sumedia.gtdollarandroid.ui;


import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.UnionPayEvent;
import mobileapps.sumedia.gtdollarandroid.network.CreditCardTopupRequest;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.network.UnionPayRequest;

public class TopupWebViewActivity extends BaseActivity {

    @InjectView(R.id.wvDetail)
    WebView mWvDetail;

    @InjectView(R.id.pbLoading)
    ProgressBar mPbLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        setTitle(R.string.title_activity_topup);
        ButterKnife.inject(this);

        WebSettings settings = mWvDetail.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
//        if(webMode != Const.WEB_MODE_STATISTICS)
//            settings.setTextZoom(200);
        WebViewClient client = new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                mPbLoading.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(TopupWebViewActivity.this, "There was an error loading the webpage!",
                        Toast.LENGTH_LONG).show();
                mPbLoading.setVisibility(View.GONE);
            }
        };
        mWvDetail.setWebViewClient(client);

        if(getIntent().hasExtra("unionpay_request")) {
            UnionPayRequest request = getIntent().getParcelableExtra("unionpay_request");
            if(request != null)JobManager.getInstance().accountUnionPay(request);
        }else if(getIntent().hasExtra("creditcard_request")) {
            CreditCardTopupRequest request = getIntent().getParcelableExtra("creditcard_request");
            if(request != null)JobManager.getInstance().accountCreditCardTopup(request);
        }

    }

    @Override
    protected boolean hasBack() {
        return true;
    }

    public void onEventMainThread(UnionPayEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED && event.getResponse() != null) {
            mWvDetail.loadData(event.getResponse(),"text/html; charset=UTF-8", null);
        }
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }
}
