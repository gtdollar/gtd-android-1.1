package mobileapps.sumedia.gtdollarandroid.network.response;

import mobileapps.sumedia.gtdollarandroid.model.Service;

/**
 * Created by zhuodong on 8/29/14.
 */
public class ServiceResponse extends BaseResponse {

    Service service;

    public Service getService() {
        return service;
    }
}
