package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.model.PhoneContact;

public class SendInviteSmsActivity extends BaseActivity {

    @InjectView(R.id.tv_contact_name)
    TextView mTvContactName;
    @InjectView(R.id.tv_contact_number)
    TextView mTvContactNumber;
    @InjectView(R.id.btn_send_invite)
    Button mBtnSendInvite;

    PhoneContact contact;
    private String smsText;
    private String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_invite_activity);
        ButterKnife.inject(this);
        setTitle("");
        contact = getIntent().getParcelableExtra(Const.INTENT_EXTRA_CONTACT);

        mTvContactName.setText(contact.getDisplayName());

        Cursor phones = getContentResolver().query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null,
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contact.getId(),
            null, null);
        while (phones.moveToNext()) {
            phoneNumber = phones.getString(
                phones.getColumnIndex(
                    ContactsContract.CommonDataKinds.Phone.NUMBER));
        }
        phones.close();


        mTvContactNumber.setText(phoneNumber);

        smsText = getString(R.string.sms_invite);

    }

    @OnClick(R.id.btn_send_invite)
    public void onSendInvite(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) //At least KitKat
        {
            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this); //Need to change the build to API 19

            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, smsText);
            sendIntent.putExtra("address", phoneNumber);

            if (defaultSmsPackageName != null)//Can be null in case that there is no default, then the user would be able to choose any app that support this intent.
            {
                sendIntent.setPackage(defaultSmsPackageName);
            }
            this.startActivity(sendIntent);

        } else //For early versions, do what worked for you before.
        {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setData(Uri.parse("smsto:"+phoneNumber));
            sendIntent.putExtra("address", phoneNumber);
            sendIntent.putExtra("sms_body", smsText);
            this.startActivity(sendIntent);
        }
    }

}
