package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.GetMerchantResponse;

/**
 * Created by zhuodong on 8/29/14.
 */
public class GetMerchantEvent extends BaseEvent<GetMerchantResponse> {
    public GetMerchantEvent(EventStatus eventStatus) {
        super(eventStatus);
    }

    public GetMerchantEvent(EventStatus status, GetMerchantResponse response) {
        super(status, response);
    }
}
