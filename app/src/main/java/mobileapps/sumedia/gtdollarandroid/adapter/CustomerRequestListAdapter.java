package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.AttemptCancelOrderEvent;
import mobileapps.sumedia.gtdollarandroid.model.Order;
import mobileapps.sumedia.gtdollarandroid.model.Service;

/**
 * Created by 2359media on 4/7/14.
 */

public class CustomerRequestListAdapter extends PagingBaseAdapter<Order> {

    boolean isNew;

    public CustomerRequestListAdapter(Context ctx) {
        super(ctx);
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }

    @Override
    protected int getViewResourceId(int position) {
        return R.layout.item_customer_request;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId().hashCode();
    }

    @Override
    public Renderer<Order> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }


    class ViewHolder implements Renderer<Order> {
        @InjectView(R.id.iv_service_photo)
        ImageView mIvServicePhoto;
        @InjectView(R.id.tv_merchant_name)
        TextView mTvMerchantName;
        @InjectView(R.id.tv_order_head_count)
        TextView mTvOrderHeadCount;
        @InjectView(R.id.tv_order_date)
        TextView mTvOrderDate;
        @InjectView(R.id.tv_service_desc)
        TextView mTvServiceDesc;
        @InjectView(R.id.iv_cancel)
        ImageView ivCancel;

        View view;

        Order item;


        @OnClick(R.id.iv_cancel)
        public void onCancel(View view){
            EventBus.getDefault().post(new AttemptCancelOrderEvent(item.getId()));
        }

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
            this.view = view;
            ivCancel.setVisibility(isNew?View.VISIBLE:View.GONE);

        }

        public void render(Order item, int position) {
            this.item = item;

            if (item.getServices() != null && item.getServices().size() > 0) {
                Service service = item.getServices().get(0);
                if (!TextUtils.isEmpty(service.getPhoto())) {
                    Picasso.with(getContext()).load(service.getPhoto()).into(mIvServicePhoto);
                }
                mTvServiceDesc.setText(service.getTitle());
            }
            mTvMerchantName.setText(item.getMerchant().getName());
            mTvOrderHeadCount.setText(item.getHeadCount() + "");
            mTvOrderDate.setText(item.getOrderDate());

        }
    }

}
