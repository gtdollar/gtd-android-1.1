package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.makeramen.RoundedImageView;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.helper.Formatter;
import mobileapps.sumedia.gtdollarandroid.model.GooglePlace;
import mobileapps.sumedia.gtdollarandroid.network.ApiConfig;

/**
 * Created by 2359media on 4/7/14.
 */

public class GoogleMerchantListAdapter extends PagingBaseAdapter<GooglePlace> {

    private Location location;

    public GoogleMerchantListAdapter(Context ctx) {
        super(ctx);
    }

    @Override
    protected int getViewResourceId(int position) {
        return R.layout.item_google_place;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId().hashCode();
    }

    @Override
    public Renderer<GooglePlace> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }

    public void setLocation(Location location) {
        this.location = location;
        notifyDataSetChanged();
    }


    class ViewHolder implements Renderer<GooglePlace> {
        @InjectView(R.id.iv_member_photo)
        RoundedImageView mIvMemberPhoto;
        @InjectView(R.id.tv_merchant_name)
        TextView mTvMerchantName;
        @InjectView(R.id.tv_merchant_distance)
        TextView mTvMerchantDistance;
        @InjectView(R.id.tv_merchant_type)
        TextView mTvMerchantType;
        @InjectView(R.id.tv_merchant_rating)
        TextView mTvMerchantRating;

        View view;
        private GooglePlace item;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
            this.view = view;
        }

        public void render(GooglePlace item, int position) {
            this.item = item;

            if (item.getPhotos() != null && item.getPhotos().size() > 0 && !TextUtils.isEmpty(item.getPhotos().get(0).getPhotoReference())) {
                Picasso.with(getContext()).load(ApiConfig.GOOGLE_PHOTOS + item.getPhotos().get(0).getPhotoReference()).placeholder(R.drawable.transparent).into(mIvMemberPhoto);
            } else {
                mIvMemberPhoto.setImageResource(R.drawable.transparent);

            }

            mTvMerchantName.setText(item.getName());
            mTvMerchantRating.setText(item.getRating() + "");
            mTvMerchantType.setText(TextUtils.join(", ", item.getTypes()));

            if (location != null && item.getGeometry() != null && item.getGeometry().getLocation() != null) {
                mTvMerchantDistance.setVisibility(View.VISIBLE);
                mTvMerchantDistance.setText(Formatter.formatDistance(location.getLatitude(), location.getLongitude(), item.getGeometry().getLocation().getLat(), item.getGeometry().getLocation().getLng()));
            } else {
                mTvMerchantDistance.setVisibility(View.INVISIBLE);
            }

        }
    }
}
