package mobileapps.sumedia.gtdollarandroid.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;

import java.text.NumberFormat;

import mobileapps.sumedia.gtdollarandroid.GTDApplication;
import mobileapps.sumedia.gtdollarandroid.R;

/**
 * Created by 2359media on 22/7/14.
 */
public class GTDHelper {
    Context context;

    public GTDHelper(Context context) {
        this.context = context;
    }


    public static String languageCodeToName(String code) {
        String[] langIdArray = GTDApplication.getInstance().getResources().getStringArray(R.array.lang_id_array);
        String[] langNameArray = GTDApplication.getInstance().getResources().getStringArray(R.array.lang_array);

        for (int i = 0; i < langIdArray.length; i++) {
            if(code.equals(langIdArray[i])) {
                return langNameArray[i];
            }
        }

        return langNameArray[0];
    }

    public static String serviceTypeName(int type) {
        type -= 1;
        String[] typesArray = GTDApplication.getInstance().getResources().getStringArray(R.array.merchant_types);
        if (type >= 0 && typesArray.length > 0 && type < typesArray.length) return typesArray[type];
        else return typesArray[0];
    }

    public static String serviceTypeName(int[] categories) {
        String[] typesArray = GTDApplication.getInstance().getResources().getStringArray(R.array.merchant_types);
        if(categories == null || categories.length == 0)return typesArray[0];
        String[] categoryNames = new String[categories.length];
        for (int i = 0; i < categories.length; i++) {
            int category = categories[i] - 1;
            categoryNames[i] = category >= 0 && typesArray.length > 0 && category < typesArray.length?typesArray[category]:typesArray[0];
        }
        return TextUtils.join(",",categoryNames);
    }


    public static int findType(String type) {
        if(TextUtils.isEmpty(type))return 1;

        String[] typesArray = GTDApplication.getInstance().getResources().getStringArray(R.array.merchant_types);
        for (int i = 0; i < typesArray.length; i++) {
            if(typesArray[i].equalsIgnoreCase(type))return i+1;
        }
        return 1;
    }

//    public String[] merchantTypes()
//    {
//        return context.getResources().getStringArray(R.array.merchant_types);
//    }
//
    public static int merchantTypeResources(int type)
    {

        int resId = R.drawable.filter_others;

        type -= 1;

        TypedArray icons = GTDApplication.getInstance().getResources().obtainTypedArray(R.array.merchant_res_id);
        if(type > 0){
            resId = icons.getResourceId(type, R.drawable.filter_others);
        }
        icons.recycle();
        return resId;
    }

    public static String formatCredit(double credit) {
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMaximumFractionDigits(1);
        return "GTD " + numberFormat.format(credit);
    }

    public static String formatBalance(double balance) {
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMaximumFractionDigits(1);
        return "$ " + numberFormat.format(balance);
    }
}
