package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.CheckAccountEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.OrderPaymentEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.Formatter;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.Order;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.network.response.AccountCheckResponse;

public class OrderPaymentActivity extends BaseActivity {

    @InjectView(R.id.tv_ewallet)
    TextView mTvEwallet;
//    @InjectView(R.id.tv_credit_card)
//    TextView mTvCreditCard;
    @InjectView(R.id.tv_account)
    TextView mTvAccount;
    @InjectView(R.id.tv_gtd_credit)
    TextView mTvGtdCredit;
    @InjectView(R.id.tv_total_balance)
    TextView mTvTotalBalance;
    @InjectView(R.id.tv_amount)
    TextView mTvAmount;
    @InjectView(R.id.btn_payment)
    Button mBtnPayment;

    Order order;

    double payableAmount, payableCredit;
    double availableAmount, availableCredit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        if(!UserDataHelper.isVerified()) {
            finish();
            return;
        }

        ButterKnife.inject(this);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        order = getIntent().getParcelableExtra(Const.INTENT_EXTRA_ORDER);
        payableAmount = getIntent().getDoubleExtra(Const.INTENT_EXTRA_AMOUNT, 0);
        payableCredit = getIntent().getDoubleExtra(Const.INTENT_EXTRA_CREDIT, 0);
        mTvAmount.setText(Formatter.formatPrice("$", payableAmount));
        mTvAccount.setText(UserDataHelper.getAccount().getEmail());
        JobManager.getInstance().checkAccount();

    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    public void onEventMainThread(CheckAccountEvent event) {
        if (event.getStatus() == EventStatus.SUCCEED && event.getResponse() != null) {
            AccountCheckResponse.Data data = event.getResponse().getData();

            availableAmount = data.getBalance();
            availableCredit = data.getCredits();

            mTvGtdCredit.setText(GTDHelper.formatCredit(availableCredit));
            mTvTotalBalance.setText(GTDHelper.formatBalance(availableAmount));
        }
    }

    @OnClick(R.id.btn_payment)
    void onClickPayment(View view) {
        if (availableAmount < payableAmount) {
            Utils.toast(OrderPaymentActivity.this, getString(R.string.toast_balance_not_enough));
            return;
        } else if (availableCredit < payableCredit) {
            Utils.toast(OrderPaymentActivity.this, getString(R.string.toast_credit_not_enough));
            return;
        }

        JobManager.getInstance().payMerchant(order.getMerchant().getEmail(), payableAmount, payableCredit);

    }

    public void onEventMainThread(OrderPaymentEvent event) {
        Utils.toggleEventPopup(this,event,new Utils.StatusListener<OrderPaymentEvent>() {
            @Override
            public void onSucceed(OrderPaymentEvent event) {
                UserDataHelper.removePendingOrder();
                Utils.toast(OrderPaymentActivity.this, getString(R.string.toast_payed));
                finish();
            }
        });
    }
}