package mobileapps.sumedia.gtdollarandroid.model;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by zhuodong on 7/22/14.
 */
public class LatLng implements Parcelable {

    @Expose
    private Double lat;
    @Expose
    private Double lng;

    public LatLng(Double lat, Double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public LatLng(LatLng latLng) {
        this.lat = latLng.getLat();
        this.lng = latLng.getLng();
    }

    public LatLng(Location currentLocation) {
        this(currentLocation == null?null:currentLocation.getLatitude(),currentLocation == null?null:currentLocation.getLongitude());
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public boolean notEmpty() {
        return lat != null && lng != null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.lat);
        dest.writeValue(this.lng);
    }

    public LatLng() {
    }

    private LatLng(Parcel in) {
        this.lat = (Double) in.readValue(Double.class.getClassLoader());
        this.lng = (Double) in.readValue(Double.class.getClassLoader());
    }

    public static final Parcelable.Creator<LatLng> CREATOR = new Parcelable.Creator<LatLng>() {
        public LatLng createFromParcel(Parcel source) {
            return new LatLng(source);
        }

        public LatLng[] newArray(int size) {
            return new LatLng[size];
        }
    };

    @Override
    public String toString() {
        return lat+","+lng;
    }
}