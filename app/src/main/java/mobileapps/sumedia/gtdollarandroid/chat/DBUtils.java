package mobileapps.sumedia.gtdollarandroid.chat;

import android.text.TextUtils;
import android.util.Log;

import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.packet.DelayInfo;
import org.jivesoftware.smackx.packet.DelayInformation;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;

import java.util.Date;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.Chat;
import mobileapps.sumedia.gtdollarandroid.ChatDao;
import mobileapps.sumedia.gtdollarandroid.Feed;
import mobileapps.sumedia.gtdollarandroid.FeedDao;
import mobileapps.sumedia.gtdollarandroid.GTDApplication;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.GetFeedsEvent;
import mobileapps.sumedia.gtdollarandroid.event.NewChatEvent;
import mobileapps.sumedia.gtdollarandroid.event.UpdateChatEvent;

/**
 * Created by zhuodong on 8/13/14.
 */
public class DBUtils {
    private static final String TAG = "DBUtils";
    static ChatDao chatDao = GTDApplication.getInstance().getDaoSession().getChatDao();
    static FeedDao feedDao = GTDApplication.getInstance().getDaoSession().getFeedDao();

    /*
    The DB operation
     */

    public static Chat newChat(int direction, String JID,
                                          String message, int delivery_status, long ts, String packetID, String messageType, Map<String,String> propertyMap) {

        return new Chat.Builder().setMessage(message).setDeliveryStatus(delivery_status).setPid(packetID)
            .setDate(ts).setJid(JID).setDirection(direction).setMessageType(messageType).setPropertyMap(propertyMap).createChat();
    }

    public static void insertNewChat(Chat chat) {
        long id = chatDao.insert(chat);
        chat.setId(id);
        String userId = "";
        String userName = "";
        String userAvatar = "";
        if(chat.getPropertyMap() != null) {
            if (chat.getDirection() == ChatConstants.INCOMING) {
                userId = chat.getPropertyMap().get(ChatConstants.MESSAGE_KEY_SENDER_ID);
                userName = chat.getPropertyMap().get(ChatConstants.MESSAGE_KEY_SENDER_NAME);
                userAvatar = chat.getPropertyMap().get(ChatConstants.MESSAGE_KEY_SENDER_AVATAR_URL);
            }else if(chat.getDirection() == ChatConstants.OUTGOING) {
                userId = chat.getPropertyMap().get(ChatConstants.MESSAGE_KEY_RECEIVER_ID);
                userName = chat.getPropertyMap().get(ChatConstants.MESSAGE_KEY_RECEIVER_NAME);
                userAvatar = chat.getPropertyMap().get(ChatConstants.MESSAGE_KEY_RECEIVER_AVATAR_URL);
            }
        }

        insertToChatFeed(chat.getDirection(),chat.getMessageType(),userId,chat.getJid(),userName, userAvatar,chat.getMessage());

        EventBus.getDefault().post(new NewChatEvent(chat));
    }

    public static void addChatMessageToDB(int direction, String JID,
                                          String message, int delivery_status, long ts, String packetID, String messageType, Map<String,String> propertyMap) {

        Chat chat = new Chat.Builder().setMessage(message).setDeliveryStatus(delivery_status).setPid(packetID)
            .setDate(ts).setJid(JID).setDirection(direction).setMessageType(messageType).setPropertyMap(propertyMap).createChat();

        long id = chatDao.insert(chat);
        chat.setId(id);

        //insert or update the feed


        String userId = "";
        String userName = "";
        String userAvatar = "";
        if(propertyMap != null) {
            if (chat.getDirection() == ChatConstants.INCOMING) {
                userId = propertyMap.get(ChatConstants.MESSAGE_KEY_SENDER_ID);
                userName = propertyMap.get(ChatConstants.MESSAGE_KEY_SENDER_NAME);
                userAvatar = propertyMap.get(ChatConstants.MESSAGE_KEY_SENDER_AVATAR_URL);
            }else if(chat.getDirection() == ChatConstants.OUTGOING) {
                userId = propertyMap.get(ChatConstants.MESSAGE_KEY_RECEIVER_ID);
                userName = propertyMap.get(ChatConstants.MESSAGE_KEY_RECEIVER_NAME);
                userAvatar = propertyMap.get(ChatConstants.MESSAGE_KEY_RECEIVER_AVATAR_URL);
            }
        }

        insertToChatFeed(direction,messageType,userId, JID,userName, userAvatar, message);




        EventBus.getDefault().post(new NewChatEvent(chat));


//        ContentValues values = new ContentValues();
//
//        values.put(ChatConstants.DIRECTION, direction);
//        values.put(ChatConstants.JID, JID);
//        values.put(ChatConstants.MESSAGE, message);
//        values.put(ChatConstants.DELIVERY_STATUS, delivery_status);
//        values.put(ChatConstants.DATE, ts);
//        values.put(ChatConstants.PACKET_ID, packetID);
//
//        mContentResolver.insert(ChatProvider.CONTENT_URI, values);

        Log.d(TAG, "add new chat message to db ");

    }

    public static void modifyChatRead(String JID) {
        Feed temp = feedDao.queryBuilder().where(FeedDao.Properties.BaseType.eq(Feed.TYPE_CHAT),FeedDao.Properties.SubjectId.eq(JID)).unique();
        temp.setIsRead(false);
        feedDao.update(temp);
    }

    public static void insertToChatFeed(int direction, String messageType, String userId, String JID, String userName, String avatarUrl, String message) {

        long createdAt = System.currentTimeMillis();
        Feed temp = feedDao.queryBuilder().where(FeedDao.Properties.BaseType.eq(Feed.TYPE_CHAT),FeedDao.Properties.SubjectId.eq(JID)).unique();
        if(temp == null) {
            temp = new Feed();
            temp.setBaseType(Feed.TYPE_CHAT);
            temp.setSubjectName(userName);
            temp.setSubjectId(JID);
            temp.setSubjectAvatar(avatarUrl);
        }
        temp.setType(ChatConstants.messageViewType(direction,messageType));
        temp.setCreatedAt(createdAt);
        temp.setExtraId(userId);
        temp.setExtra(message);
        temp.setIsRead(true);

        long rowId = feedDao.insertOrReplace(temp);
        //Log.d(""," testing rowId "+rowId);
        EventBus.getDefault().post(new GetFeedsEvent(EventStatus.SUCCEED));
    }

    public static boolean updateFileUrl(String packetID, String url) {
        Chat chat = chatDao.queryBuilder().where(ChatDao.Properties.Pid.eq(packetID)).unique();
        if (chat == null) return false;
        chat.setMessage(url);
        chatDao.update(chat);
        //EventBus.getDefault().post(new UpdateChatEvent(chat));
        return true;
    }

    public static boolean changeMessageDeliveryStatus(String packetID, int new_status) {
        Log.d(TAG," packetID "+packetID);
        List<Chat> chats = chatDao.queryBuilder().where(ChatDao.Properties.Pid.eq(packetID)).list();
        if(chats == null || chats.size() == 0)return false;
        Chat chat = chats.get(0);
        if(chat == null) return false;
        chat.setDeliveryStatus(new_status);
        chatDao.update(chat);
        EventBus.getDefault().post(new UpdateChatEvent(chat));
        return true;

//        ContentValues cv = new ContentValues();
//        cv.put(ChatConstants.DELIVERY_STATUS, new_status);
////        Uri rowuri = Uri.parse("content://" + ChatProvider.AUTHORITY + "/"
////            + ChatProvider.TABLE_NAME);
//        return mContentResolver.update(ChatProvider.CONTENT_URI, cv,
//            ChatConstants.PACKET_ID + " = ? AND " +
//                ChatConstants.DELIVERY_STATUS + " != " + ChatConstants.DS_ACKED + " AND " +
//                ChatConstants.DIRECTION + " = " + ChatConstants.OUTGOING,
//            new String[]{packetID}
//        ) > 0;
    }

    public static void sendOfflineMessages(XmppStreamHandler.ExtXMPPConnection xmppConnection) {
        List<Chat> chats =  chatDao.queryBuilder().where(ChatDao.Properties.DeliveryStatus.eq(ChatConstants.DS_NEW), ChatDao.Properties.Direction.eq(ChatConstants.OUTGOING)).list();
        for (Chat chat : chats) {
            final Message newMessage = new Message(chat.getJid(), Message.Type.chat);
            newMessage.setBody(chat.getMessage());
            DelayInformation delay = new DelayInformation(new Date(chat.getDate()));
            newMessage.addExtension(delay);
            newMessage.addExtension(new DelayInfo(delay));
            newMessage.addExtension(new DeliveryReceiptRequest());
            String packetID = chat.getPid();
            if (!TextUtils.isEmpty(packetID)) {
                newMessage.setPacketID(packetID);
            } else {
                packetID = newMessage.getPacketID();
                chat.setPid(packetID);
            }
            chat.setDeliveryStatus(ChatConstants.DS_SENT_OR_READ);
            xmppConnection.sendPacket(newMessage);
        }
        chatDao.updateInTx(chats);
    }

    public static void sendOfflineMessage(String toJID, String message, Map<String,String> properties) {
        addChatMessageToDB(ChatConstants.OUTGOING,toJID,message,ChatConstants.DS_NEW,System.currentTimeMillis(),"", properties.get(ChatConstants.MESSAGE_TYPE),properties);

//        ContentValues values = new ContentValues();
//        values.put(ChatConstants.DIRECTION, ChatConstants.OUTGOING);
//        values.put(ChatConstants.JID, toJID);
//        values.put(ChatConstants.MESSAGE, message);
//        values.put(ChatConstants.DELIVERY_STATUS, ChatConstants.DS_NEW);
//        values.put(ChatConstants.DATE, System.currentTimeMillis());
//
//        cr.insert(ChatProvider.CONTENT_URI, values);
    }

    /*******************/
}
