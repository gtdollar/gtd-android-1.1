package mobileapps.sumedia.gtdollarandroid.model;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.ContactsContract;

import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.widget.CursorCreator;

/**
 * Created by zhuodong on 8/25/14.
 */

public class PhoneContact implements CursorCreator<PhoneContact>,Parcelable {

    public static final PhoneContact FACTORY = new PhoneContact();

    long id;

    String avatarUri;
    String displayName;
    String lookupKey;

    boolean isPhoneContact;

    public PhoneContact() {
    }

    public PhoneContact(long id, String avatarUri, String displayName, String lookupKey) {
        this.id = id;
        this.avatarUri = avatarUri;
        this.displayName = displayName;
        this.lookupKey = lookupKey;
        isPhoneContact = true;
    }

    public boolean isPhoneContact() {
        return isPhoneContact;
    }

    public void setPhoneContact(boolean isPhoneContact) {
        this.isPhoneContact = isPhoneContact;
    }

    public long getId() {
        return id;
    }

    public String getAvatarUri() {
        return avatarUri;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getLookupKey() {
        return lookupKey;
    }


    @Override
    public PhoneContact createFromCursor(Cursor c) {
        final String photoUri = c.getString(ContactsQuery.PHOTO_THUMBNAIL_DATA);
        final String displayName = c.getString(ContactsQuery.DISPLAY_NAME);
        final String lookupKey = c.getString(ContactsQuery.LOOKUP_KEY);
        final long id = c.getLong(ContactsQuery.ID);

        return new PhoneContact(id,photoUri,displayName,lookupKey);
    }

    /**
     * This interface defines constants for the Cursor and CursorLoader, based on constants defined
     * in the {@link android.provider.ContactsContract.Contacts} class.
     */
    public interface ContactsQuery {

        // An identifier for the loader
        final static int QUERY_ID = 1;

        // A content URI for the Contacts table
        final static Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;

        // The search/filter query Uri
        final static Uri FILTER_URI = ContactsContract.Contacts.CONTENT_FILTER_URI;

        // The selection clause for the CursorLoader query. The search criteria defined here
        // restrict results to contacts that have a display name and are linked to visible groups.
        // Notice that the search on the string provided by the user is implemented by appending
        // the search string to CONTENT_FILTER_URI.
        @SuppressLint("InlinedApi")
        final static String SELECTION =
            (Utils.hasHoneycomb() ? ContactsContract.Contacts.DISPLAY_NAME_PRIMARY : ContactsContract.Contacts.DISPLAY_NAME) +
                "<>''" + " AND " + ContactsContract.Contacts.IN_VISIBLE_GROUP + "=1" + " AND "+ContactsContract.Contacts.HAS_PHONE_NUMBER + "=1" ;

        // The desired sort order for the returned Cursor. In Android 3.0 and later, the primary
        // sort key allows for localization. In earlier versions. use the display name as the sort
        // key.
        @SuppressLint("InlinedApi")
        final static String SORT_ORDER =
            Utils.hasHoneycomb() ? ContactsContract.Contacts.SORT_KEY_PRIMARY : ContactsContract.Contacts.DISPLAY_NAME;

        // The projection for the CursorLoader query. This is a list of columns that the Contacts
        // Provider should return in the Cursor.
        @SuppressLint("InlinedApi")
        final static String[] PROJECTION = {

            // The contact's row id
            ContactsContract.Contacts._ID,

            // A pointer to the contact that is guaranteed to be more permanent than _ID. Given
            // a contact's current _ID value and LOOKUP_KEY, the Contacts Provider can generate
            // a "permanent" contact URI.
            ContactsContract.Contacts.LOOKUP_KEY,

            // In platform version 3.0 and later, the Contacts table contains
            // DISPLAY_NAME_PRIMARY, which either contains the contact's displayable name or
            // some other useful identifier such as an email address. This column isn't
            // available in earlier versions of Android, so you must use Contacts.DISPLAY_NAME
            // instead.
            Utils.hasHoneycomb() ? ContactsContract.Contacts.DISPLAY_NAME_PRIMARY : ContactsContract.Contacts.DISPLAY_NAME,

            // In Android 3.0 and later, the thumbnail image is pointed to by
            // PHOTO_THUMBNAIL_URI. In earlier versions, there is no direct pointer; instead,
            // you generate the pointer from the contact's ID value and constants defined in
            // android.provider.ContactsContract.Contacts.
            Utils.hasHoneycomb() ? ContactsContract.Contacts.PHOTO_THUMBNAIL_URI : ContactsContract.Contacts._ID,

            // The sort order column for the returned Cursor, used by the AlphabetIndexer
            SORT_ORDER,
        };

        // The query column numbers which map to each value in the projection
        final static int ID = 0;
        final static int LOOKUP_KEY = 1;
        final static int DISPLAY_NAME = 2;
        final static int PHOTO_THUMBNAIL_DATA = 3;
        final static int PHOTO_NUMBER = 4;
        final static int SORT_KEY = 5;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.avatarUri);
        dest.writeString(this.displayName);
        dest.writeString(this.lookupKey);
        dest.writeByte(isPhoneContact ? (byte) 1 : (byte) 0);
    }

    private PhoneContact(Parcel in) {
        this.id = in.readLong();
        this.avatarUri = in.readString();
        this.displayName = in.readString();
        this.lookupKey = in.readString();
        this.isPhoneContact = in.readByte() != 0;
    }

    public static final Creator<PhoneContact> CREATOR = new Creator<PhoneContact>() {
        public PhoneContact createFromParcel(Parcel source) {
            return new PhoneContact(source);
        }

        public PhoneContact[] newArray(int size) {
            return new PhoneContact[size];
        }
    };
}