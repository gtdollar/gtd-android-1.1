package mobileapps.sumedia.gtdollarandroid.location.location;

import android.location.Location;

/**
 * Created by zhuodong on 7/20/14.
 */
public class LocationUpdateEvent {
    Location location;
    public LocationUpdateEvent(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
