package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.TwoTextArrayAdapter;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.model.BalanceRecord;
import mobileapps.sumedia.gtdollarandroid.model.CreditRecord;

public class RecordDetailActivity extends BaseActivity {

    @InjectView(R.id.lv_record_detail)
    ListView mLvRecordDetail;

    boolean displayRefund;

    BalanceRecord balanceRecord;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_detail);
        ButterKnife.inject(this);


        displayRefund = getIntent().getBooleanExtra(Const.INTENT_EXTRA_DISPLAY_REFUND,false);
        CreditRecord creditRecord = getIntent().getParcelableExtra(Const.INTENT_EXTRA_CREDIT_RECORD);
        balanceRecord = getIntent().getParcelableExtra(Const.INTENT_EXTRA_BALANCE_RECORD);

        String[] strings = getResources().getStringArray(R.array.transaction_title_array);

        String[] values;
        if(creditRecord != null) {
            values = new String[]{creditRecord.getId(), creditRecord.getCreated(), creditRecord.getUsername2(), creditRecord.getCreditAmount()};
        }else if (balanceRecord != null) {
            String currency = balanceRecord.getCurrency();

            values = new String[]{balanceRecord.getId(),
                                  balanceRecord.getTdate(),
                                  balanceRecord.getUsername(),
                                  currency+" "+balanceRecord.getOamountEx(),
                                  currency+" "+balanceRecord.getOfees(),
                                  currency+" "+balanceRecord.getSettleAmount(),
                                  balanceRecord.getCurrencyRate(),
                                  balanceRecord.getType(),
                                  balanceRecord.getStatus(),
                                  "",
                                  "",
                                  "",
                                  "",
                                  balanceRecord.getRecvuser(),
                                  "",
                                  "",
                                  balanceRecord.getComments()+"\n"+balanceRecord.getEcomments()
                                 };
        }else {
            finish();
            return;
        }

        List<TwoTextArrayAdapter.Option> options = new ArrayList<TwoTextArrayAdapter.Option>();
        for (int i = 0; i < values.length; i++) {
            options.add(new TwoTextArrayAdapter.Option(strings[i], values[i]));
        }

        TwoTextArrayAdapter optionsAdapter = new TwoTextArrayAdapter(this, options, R.layout.item_record_detail);
        mLvRecordDetail.setAdapter(optionsAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(displayRefund) {
            getMenuInflater().inflate(R.menu.record, menu);
        }else {
            super.onCreateOptionsMenu(menu);
        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }else if(id == R.id.action_refund) {
            Intent intent = new Intent(this,RefundActivity.class);
            intent.putExtra(Const.INTENT_EXTRA_BALANCE_RECORD,balanceRecord);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
