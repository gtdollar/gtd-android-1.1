
/*
 ******************************************************************************
 * Parts of this code sample are licensed under Apache License, Version 2.0   *
 * Copyright (c) 2009, Android Open Handset Alliance. All rights reserved.    *
 *																			  *																			*
 * Except as noted, this code sample is offered under a modified BSD license. *
 * Copyright (C) 2010, Motorola Mobility, Inc. All rights reserved.           *
 * 																			  *
 * For more details, see MOTODEV_Studio_for_Android_LicenseNotices.pdf        * 
 * in your installation folder.                                               *
 ******************************************************************************
 */
package mobileapps.sumedia.gtdollarandroid.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * <p>
 * This class copies a SQLite database from your application's assets directory
 * to /data/data/<your_application_package>/databases/ so you can access it
 * using the SQLite APIs provided by the Android SDK. Note that
 * {@link SQLiteHelper#copyDataBase(android.content.Context, String)} checks for the existence of the database
 * and only copies it if needed.
 * </p>
 * <p>
 * {@link SQLiteHelper#copyDataBase(android.content.Context, String)} calls
 * {@link android.database.sqlite.SQLiteOpenHelper#getReadableDatabase()}, which in turn calls
 * {@link android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)}. Be aware that the
 * implementation of the overridden
 * {@link android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)} must remain empty in order
 * for the copy operation to work correctly.
 * </p>
 * <p>
 * This class includes a constructor {@link SQLiteHelper#SQLiteHelper(String, android.content.Context, boolean)}
 * which allows you to control whether the database file should be copied when
 * the class is instantiated.
 * </p>
 * 
 * @see android.database.sqlite.SQLiteOpenHelper
 */
public class SQLiteHelper extends SQLiteOpenHelper {

	// Android's default system path for your application's database.
	private File DB_FILE_PATH;

	public static String DB_NAME = "airport.sqlite";
	public static int DB_VERSION = 1;

	private final Context myContext;
	private boolean isUpgrade;
	private static SQLiteHelper mPrimaryDbInstance = null;

	public static SQLiteHelper getInstance(Context ctx) {
		if (mPrimaryDbInstance == null) {
			mPrimaryDbInstance = new SQLiteHelper(DB_NAME, ctx.getApplicationContext(),true);
		}

		return mPrimaryDbInstance;
	}

	/**
	 * Constructor Keeps a reference to the passed context in order to access
	 * the application's assets.
	 * 
	 * @param context
	 *            Context to be used
	 */
	private SQLiteHelper(Context context) {

		super(context, DB_NAME, null, DB_VERSION);
		DB_FILE_PATH = context.getDatabasePath(DB_NAME);
		this.myContext = context;
	}

	private SQLiteHelper(Context context, String dbName) {

		super(context, dbName, null, DB_VERSION);
		DB_FILE_PATH = context.getDatabasePath(dbName);
		this.myContext = context;
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
	}

	/**
	 * Initialize helper, create new db if required.
	 * 
	 * @param dbName
	 * @param context
	 * @param copyDatabase -> If true, a new db will be created if it does not exist.
	 */
	private SQLiteHelper(String dbName, Context context, boolean copyDatabase) {
		// call overloaded constructor
		this(context, dbName);
		// copy database file in case desired
		if (copyDatabase) {
			copyDataBase(this.myContext, DB_FILE_PATH.getAbsolutePath());
		}
	}
	
	/**
	 * Upgrade Policy:
	 * - Delete old DB, re-copy empty DB from Assets
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// fill in your code here
		//Utils.log("DbHelper", "upgrading db oldVersion="+oldVersion + "; newVersion="+newVersion);
		//File f = new File(DB_PATH+DB_NAME);
		if(DB_FILE_PATH.delete()){
			isUpgrade = true;
			copyDataBase(this.myContext, DB_FILE_PATH.getAbsolutePath());

			///setting the new version this way, for some reason it doesn't automatically use the new version
			SQLiteDatabase database = this.getReadableDatabase();
			database.setVersion(newVersion);
			if (database != null && database.isOpen()) {
				database.close();
			}
		}
	}

	/**
	 * Confusing name. I know. :/
	 * What it means is that it copies the DB in our assets folder
	 * to a path specified by outFileName.
	 * In our case, outFileName is the DB path used by getReadableDatabase()
	 * and getWritableDatabase(). This path is obtained via:
	 * "DB_FILE_PATH = context.getDatabasePath(DB_NAME);"
	 * 
	 * Everytime this function is called, if DB does not exist 
	 * then, a new db will be created, 
	 * be it from scratch (first launch or upgrading) or 
	 * overwritten (for unhandled cases).
	 * 
	 * @param c
	 * @param databasePath
	 */
	public void copyDataBase(Context c, String databasePath){

		//String databasePath = outFileName;
		File f = new File(databasePath);
		boolean isDBExists = f.exists();
		OutputStream myOutput;

		if (!isDBExists) {

			/*
			 * Dont bother doing this "safety" catch if isUpgrade because the
			 * db definitely have been deleted already.
			 * This safety check is here to close an open db otherwise
			 * we cannot perform our creation/modification I/O. 
			 */
			if(!isUpgrade){
				try{
					myOutput = new FileOutputStream(databasePath);
				}catch (Exception e) {
					SQLiteDatabase database = this.getReadableDatabase();
					if (database != null && database.isOpen()) {
						database.close();
					}
				}
			}

			try {
				//We have an empty sqlite db already in the assets folder.
				//Read it.
				InputStream myInput = c.getAssets().open(DB_NAME);
				myOutput = new FileOutputStream(databasePath);

				//Write it to the path specified by outFileName.
				//This is the path where getReadableDatabase()
				//will try to get the db it returns to us when we call it.
				byte[] buffer = new byte[1024];
				int length;
				while ((length = myInput.read(buffer))>0){
					myOutput.write(buffer, 0, length);
				}

				myOutput.flush();
				myOutput.close();
				myInput.close();
			} catch (Exception e) {
				//Utils.log("", "copyDataBase exception");
				e.printStackTrace();
			}
		}
		checkDataBaseExistence();
	}

	/**
	 * Returns whether the database already exists.
	 * 
	 * @return <code>true</code> if the database exists, <code>false</code>
	 *         otherwise.
	 */
	private boolean checkDataBaseExistence() {

		// database to be verified
		SQLiteDatabase dbToBeVerified = null;

		try {
			// get database path
			String dbPath = DB_FILE_PATH.getAbsolutePath();
			// try to open the database
			dbToBeVerified = SQLiteDatabase.openDatabase(dbPath, null,
					SQLiteDatabase.OPEN_READONLY);

		} catch (SQLiteException e) {
			// do nothing since the database does not exist
		} catch (Exception e) {
			// do nothing since the database does not exist
		}

		// in case it exists, close it
		if (dbToBeVerified != null) {
			// close DB
			dbToBeVerified.close();

		}

		// in case there is a DB entity, the DB exists
		return dbToBeVerified != null;
	}


    public static class AirportContract implements BaseColumns {
        public static final String TABLE_NAME = "airport";
        public static final String AIRPORT_CODE = "airport_code";
        public static final String AIRPORT_NAME = "airport_name";
        public static final String COUNTRY_NAME = "country_name";
        public static final String COUNTRY_CODE = "country_code";
        public static final String CITY_NAME = "city_name";
        public static final String CITY_NAME_CHINESE = "city_name_chinese";
        public static final String REGION = "region";

    }
}


