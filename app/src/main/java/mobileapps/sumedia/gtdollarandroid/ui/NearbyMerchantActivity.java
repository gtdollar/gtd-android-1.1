package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.MerchantListAdapter;
import mobileapps.sumedia.gtdollarandroid.event.AddCircleEvent;
import mobileapps.sumedia.gtdollarandroid.event.AddContactEvent;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.GetNearbyEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.LatLng;
import mobileapps.sumedia.gtdollarandroid.model.User;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;

public class NearbyMerchantActivity extends BaseActivity {

    @InjectView(R.id.lv_merchant)
    ListView mLvMerchant;

    MerchantListAdapter merchantListAdapter;

    boolean searchResult;

    @InjectView(R.id.tv_empty)
    TextView mTvEmpty;

    String keyword,merchantType;

    LatLng latLng;
    private boolean newMerchant;

    //boolean specificSearch;
    //GoogleMerchantListAdapter googleMerchantListAdapter;
    //boolean isDisplayGoogleMerchant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_merchant);
        ButterKnife.inject(this);
        latLng = getIntent().getParcelableExtra(Const.INTENT_EXTRA_LATLNG);
        newMerchant = getIntent().getBooleanExtra(Const.INTENT_EXTRA_NEW_MERCHANT, false);

        setUpListView();
    }

    public void setUpListView() {

        View headerView = LayoutInflater.from(this).inflate(R.layout.include_googlenearby_header, null);
        if(!newMerchant)((TextView) headerView).setText(R.string.create_circle_instruction);
        mLvMerchant.addHeaderView(headerView);

        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(newMerchant) {
                    Intent intent = new Intent(NearbyMerchantActivity.this,MerchantProfileActivity.class);
                    startActivity(intent);
                    return;
                }

                List<String> ids = new ArrayList<String>();
                if(merchantListAdapter.getItems() != null) {
                    for (User user : merchantListAdapter.getItems()) {
                        ids.add(user.getId());
                    }
                }
                JobManager.getInstance().createCircle(merchantType,TextUtils.isEmpty(keyword)?merchantType:keyword, TextUtils.join(",",ids), latLng == null?null:latLng.toString());

            }
        });


        merchantListAdapter = new MerchantListAdapter(this);
        merchantListAdapter.setLocation(latLng);

        mLvMerchant.setAdapter(merchantListAdapter);
        mLvMerchant.setEmptyView(mTvEmpty);
        mLvMerchant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                position -= 1;

//                if(isDisplayGoogleMerchant && position >= 0 && position < googleMerchantListAdapter.getCount()) {
//                    Utils.viewGoogleMerchant(NearbyMerchantActivity.this, googleMerchantListAdapter.getItem(position));
//                    return;
//                }

                if (position >= merchantListAdapter.getCount() || position < 0)return;

                Utils.viewMerchant(NearbyMerchantActivity.this, merchantListAdapter.getItem(position), merchantListAdapter.getItem(position).getId(), newMerchant);
            }
        });
        keyword = getIntent().getStringExtra(Const.INTENT_EXTRA_KEYWORD);
        merchantType = getIntent().getStringExtra(Const.INTENT_EXTRA_MERCHANT_TYPE);
        //specificSearch = getIntent().getBooleanExtra(Const.INTENT_EXTRA_SPECIFICSEARCH, false);

    }


//    public void onEventMainThread(LocationUpdateEvent event) {
//        Location latLng = event.getLocation();
//
//
//    }

    public void onEventMainThread(GetNearbyEvent event) {
        EventStatus status = event.getStatus();
        //Log.d(""," status "+status+ " message "+event.getErrorMsg());
        if (status == EventStatus.ONGOING) {
            Utils.displayProgress(this, R.string.string_searching_nearby_merchat);
        } else if (status == EventStatus.SUCCEED) {
            //if(event.getResponse() != null && event.getResponse().getUsers() != null && event.getResponse().getUsers().size() > 0) {
                Utils.dismissProgress(this);
                merchantListAdapter.setItems(event.getResponse().getUsers());
            //}
//            else {
//                JobManager.getInstance().getNearbyPlaces(latLng != null ? latLng.getLatitude() + "," + latLng.getLongitude() : null, specificSearch?merchantType:keyword);
//            }
        }
        else if(status == EventStatus.FAILED) {
            Utils.dismissProgress(this);
            //JobManager.getInstance().getNearbyPlaces(latLng != null ? latLng.getLatitude() + "," + latLng.getLongitude() : null, specificSearch?merchantType:keyword);
        }


    }

//    public void onEventMainThread(GetNearbyPlacesEvent event) {
//
//        EventStatus status = event.getStatus();
//        if (status == EventStatus.SUCCEED) {
//            Utils.dismissProgress(this);
//            isDisplayGoogleMerchant = true;
//            googleMerchantListAdapter = new GoogleMerchantListAdapter(this);
//            googleMerchantListAdapter.setLocation(latLng);
//            googleMerchantListAdapter.setItems(event.getResponse().getResults());
//            mLvMerchant.setAdapter(googleMerchantListAdapter);
//        }
//    }


    public void onEventMainThread(AddContactEvent event) {
        if(event.getStatus() == EventStatus.SUCCEED){
            Utils.toast(this,getString(R.string.toast_followed));
        }
    }

    public void onEventMainThread(AddCircleEvent event) {
        Utils.toggleEventPopup(this, event, R.string.string_creating_circle ,new Utils.StatusListener<AddCircleEvent>() {
            @Override
            public void onSucceed(AddCircleEvent event) {
                Intent intent = new Intent(NearbyMerchantActivity.this, MainTabActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public boolean isSubscribedEvent() {
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_tab, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);


        // In version 3.0 and later, sets up and configures the ActionBar SearchView
        if (Utils.hasHoneycomb()) {

            // Retrieves the system search manager service
            final SearchManager searchManager =
                (SearchManager)getSystemService(Context.SEARCH_SERVICE);

            // Retrieves the SearchView from the search menu item
            final SearchView searchView = (SearchView) searchItem.getActionView();

            searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
            int searchSrcTextId = getResources().getIdentifier("android:id/search_src_text", null, null);
            EditText searchEditText = (EditText) searchView.findViewById(searchSrcTextId);
            if(searchEditText != null) {
                searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                        if (id == EditorInfo.IME_ACTION_SEARCH) {
                            //searchView.setQuery("",false);
                            //searchView.onActionViewCollapsed();
                            Utils.search(NearbyMerchantActivity.this,latLng,!TextUtils.isEmpty(textView.getText()) ? textView.getText().toString() : "", merchantType);
                            Utils.hideKeyBoard(NearbyMerchantActivity.this, textView);
                            textView.clearFocus();
                            return true;
                        }
                        return false;
                    }
                });
            }

            // Assign searchable info to SearchView
            searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
            // Set listeners for SearchView
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String queryText) {
                    // Nothing needs to happen when the user submits the search string
//                    Log.d(""," testing search "+queryText);
//                    searchView.onActionViewCollapsed();
//                    Utils.search(MainTabActivity.this, currentLocation, queryText, null, false);

                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    // Called when the action bar search text has changed.  Updates
                    // the search filter, and restarts the loader to do a new query
                    // using the new search string.
                    //Log.d(""," testing text changed");
                    return true;
                }
            });
        }


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        if(!searchResult) {
            searchResult = true;
            JobManager.getInstance().searchMerchant(keyword, latLng != null ? latLng.toString() : null, false,  merchantType);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }
}
