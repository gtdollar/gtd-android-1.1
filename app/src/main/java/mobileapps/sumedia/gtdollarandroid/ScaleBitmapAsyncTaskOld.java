//package mobileapps.sumedia.gtdollarandroid;
//
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Matrix;
//import android.media.ExifInterface;
//import android.os.AsyncTask;
//
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//
//import de.greenrobot.event.EventBus;
//import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
//import mobileapps.sumedia.gtdollarandroid.event.ScaleBitmapEvent;
//
///**
//* Created by zhuodong on 4/9/14.
//*/
//public class ScaleBitmapAsyncTask extends AsyncTask<Void,Void,Boolean>{
//
//    private String mCurrentPhotoPath, mTargetPhotoPath;
//
//    private int targetH;
//
//    private int targetW;
//
//    public ScaleBitmapAsyncTask(
//        String currentPhotoPath, String targetPhotoPath, int targetH, int targetW) {
//        this.mCurrentPhotoPath = currentPhotoPath;
//        this.mTargetPhotoPath = targetPhotoPath;
//        this.targetH = targetH;
//        this.targetW = targetW;
//    }
//
//    @Override
//    protected Boolean doInBackground(Void... params) {
//        boolean success = false;
//        try {
//            //first need to check the orientation
//            ExifInterface exif = new ExifInterface(mCurrentPhotoPath);
//
//            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//            int rotateDegree = 0;
//            switch (orientation) {
//                case ExifInterface.ORIENTATION_ROTATE_90:
//                    rotateDegree = 90;
//                    break;
//                case ExifInterface.ORIENTATION_ROTATE_180:
//                    rotateDegree = 180;
//                    break;
//                case ExifInterface.ORIENTATION_ROTATE_270:
//                    rotateDegree = 270;
//                    break;
//            }
//
//            if (rotateDegree != 0 || rotateDegree != 90) {
//                int temp = targetH;
//                targetH = targetW;
//                targetW = temp;
//            }
//
//            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//            bmOptions.inJustDecodeBounds = true;
//            BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
//            int photoW = bmOptions.outWidth;
//            int photoH = bmOptions.outHeight;
//
//            // Determine how much to scale down the image
//            int scaleFactor = Math.max(photoW / targetW, photoH / targetH);
//
//            // Decode the image file into a Bitmap sized to fill the View
//            bmOptions.inJustDecodeBounds = false;
//            bmOptions.inSampleSize = scaleFactor;
//            bmOptions.inPurgeable = true;
//
//            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
//
//            if (rotateDegree != 0) {
//                Matrix matrix = new Matrix();
//                matrix.postRotate(rotateDegree);
//                bitmap = Bitmap
//                    .createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix,
//                        true);
//            }
//
//            success = bitmap.compress(Bitmap.CompressFormat.JPEG, 80, new FileOutputStream(mTargetPhotoPath,false));
//
//
//        } catch (Exception e) {
//            return null;
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        return success;
//    }
//
//    @Override
//    protected void onPreExecute() {
//        super.onPreExecute();
//        EventBus.getDefault().post(new ScaleBitmapEvent(EventStatus.ONGOING));
//    }
//
//    @Override
//    protected void onPostExecute(Boolean success) {
//        EventBus.getDefault().post(new ScaleBitmapEvent(EventStatus.SUCCEED,success));
//    }
//}
