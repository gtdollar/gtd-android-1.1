package mobileapps.sumedia.gtdollarandroid.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import butterknife.ButterKnife;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.EventStatus;
import mobileapps.sumedia.gtdollarandroid.event.SignInEvent;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;

public class SplashActivity extends BaseActivity {

    Handler mHandler = new Handler();

    public static final int SPLASH_DURATION = 2000;
    public static final int TIMEOUT = 10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.inject(this);

        if (UserDataHelper.isLoggedIn()) {
            JobManager.getInstance().signInAll(UserDataHelper.getEmail(), UserDataHelper.getPwd());
            mHandler.postDelayed(timeOut, TIMEOUT);
        }else {
            mHandler.postDelayed(startSignIn, SPLASH_DURATION);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        //AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        //AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(startSignIn);
        mHandler.removeCallbacks(timeOut);
    }

    @Override
    public boolean hasActionBar() {
        return false;
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

    public void onEventMainThread(SignInEvent event) {
        if (UserDataHelper.isLoggedIn() && (event.getStatus() == EventStatus.SUCCEED || event.getStatus() == EventStatus.FAILED)) {
            mHandler.removeCallbacks(timeOut);
            Intent intent = new Intent(SplashActivity.this, MainTabActivity.class);
            //intent.putExtra(Const.INTENT_EXTRA_REFRESH_TOKEN, true);
            startActivity(intent);
            finish();
        }
    }

    Runnable startSignIn = new Runnable() {
        @Override
        public void run() {
            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            finish();
        }
    };

    Runnable timeOut = new Runnable() {
        @Override
        public void run() {
            onEventMainThread(new SignInEvent(EventStatus.FAILED));
        }
    };
}
