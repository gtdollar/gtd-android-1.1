package mobileapps.sumedia.gtdollarandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhuodong on 8/29/14.
 */

public class Order implements Parcelable {

    @Expose
    private String id;
    @Expose
    private GTUser merchant;
    @Expose
    private GTUser customer;
    @Expose
    private String createdDate;

    @Expose
    private int headCount;
    @Expose
    private List<Service> services = new ArrayList<Service>();
    @Expose
    private String orderDate;
    @Expose
    private int status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GTUser getMerchant() {
        return merchant;
    }

    public void setMerchant(GTUser merchant) {
        this.merchant = merchant;
    }

    public GTUser getCustomer() {
        return customer;
    }

    public void setCustomer(GTUser customer) {
        this.customer = customer;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public int getHeadCount() {
        return headCount;
    }

    public void setHeadCount(int headCount) {
        this.headCount = headCount;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Expose
    private String orderEffectiveDate;

    public String getOrderEffectiveDate() {
        return orderEffectiveDate;
    }

    @Expose
    @SerializedName("delivery_address")
    private String deliveryAddress;

    @Expose
    @SerializedName("contact_number")
    private String contactNumber;

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeParcelable(this.merchant, 0);
        dest.writeParcelable(this.customer, 0);
        dest.writeString(this.createdDate);
        dest.writeInt(this.headCount);
        dest.writeTypedList(services);
        dest.writeString(this.orderDate);
        dest.writeInt(this.status);
        dest.writeString(this.orderEffectiveDate);
        dest.writeString(this.deliveryAddress);
        dest.writeString(this.contactNumber);

    }

    public Order() {
    }

    private Order(Parcel in) {
        this.id = in.readString();
        this.merchant = in.readParcelable(GTUser.class.getClassLoader());
        this.customer = in.readParcelable(GTUser.class.getClassLoader());
        this.createdDate = in.readString();
        this.headCount = in.readInt();
        in.readTypedList(services, Service.CREATOR);
        this.orderDate = in.readString();
        this.status = in.readInt();
        this.orderEffectiveDate = in.readString();
        this.deliveryAddress = in.readString();
        this.contactNumber = in.readString();

    }

    public static final Parcelable.Creator<Order> CREATOR = new Parcelable.Creator<Order>() {
        public Order createFromParcel(Parcel source) {
            return new Order(source);
        }

        public Order[] newArray(int size) {
            return new Order[size];
        }
    };
}
