package mobileapps.sumedia.gtdollarandroid.widget;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import mobileapps.sumedia.gtdollarandroid.R;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 *
 */
public class ProgressPopup extends DialogFragment {


    public ProgressPopup() {
        // Required empty public constructor
    }

    public static ProgressPopup newInstance(int resId) {
        ProgressPopup progressPopup = new ProgressPopup();
        Bundle b = new Bundle();
        b.putInt("desc",resId);
        progressPopup.setArguments(b);
        return progressPopup;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //this.setCancelable(false);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog pd = new ProgressDialog(getActivity());
        int resId = getArguments().getInt("desc",0);
        pd.setMessage(getString(resId == 0? R.string.please_wait :resId));
        return pd;
    }
}
