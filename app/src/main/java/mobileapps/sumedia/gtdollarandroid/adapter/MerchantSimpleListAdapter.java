package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.model.GTUser;
import mobileapps.sumedia.gtdollarandroid.model.User;

/**
 * Created by 2359media on 4/7/14.
 */

public class MerchantSimpleListAdapter extends PagingBaseAdapter<User> {

    public MerchantSimpleListAdapter(Context ctx) {
        super(ctx);
    }

    List<String> ids  = new ArrayList<String>();

    @Override
    protected int getViewResourceId(int position) {
        return R.layout.item_add_circle_memeber;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId().hashCode();
    }

    @Override
    public Renderer<User> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }

    public void filter(List<GTUser> members) {
        for (GTUser member : members) {
            items.remove(new User(member.getId()));
        }
        notifyDataSetChanged();
    }

    class ViewHolder implements Renderer<User> {
        @InjectView(R.id.iv_member_photo)
        RoundedImageView mIvMemberPhoto;
        @InjectView(R.id.tv_merchant_name)
        TextView mTvMerchantName;
        @InjectView(R.id.iv_tick)
        ImageView mIvTick;

        View view;
        private User item;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
            this.view = view;

            mIvTick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (view.isSelected()) {
                        ids.remove(item.getId());

                        //TODO remove from shopping cart
                    } else {
                        ids.add(item.getId());
                        //TODO add to shopping cart
                    }
                    view.setSelected(!view.isSelected());
                }
            });
        }

        public void render(User item, int position) {
            this.item = item;

            if (!TextUtils.isEmpty(item.getPicture())) {
                Picasso.with(getContext()).load(item.getPicture()).into(mIvMemberPhoto);
            } else if (!TextUtils.isEmpty(item.getCover())) {
                Picasso.with(getContext()).load(item.getCover()).into(mIvMemberPhoto);
            }

            mIvTick.setSelected(ids.contains(item.getId()));

            mTvMerchantName.setText(item.getName());


        }
    }

    public List<String> getIds() {
        return ids;
    }


}
