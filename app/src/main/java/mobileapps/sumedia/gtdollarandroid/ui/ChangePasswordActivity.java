package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;

public class ChangePasswordActivity extends BaseActivity {

    @InjectView(R.id.et_email)
    EditText mEtEmail;
    @InjectView(R.id.et_pwd)
    EditText mEtPwd;
    @InjectView(R.id.btn_login)
    Button mBtnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pwd);
        ButterKnife.inject(this);
        setTitle(R.string.title_activity_change_password);

//        Map<String,String> profile = new HashMap<String, String>();
//        profile.put("password","uuhello123");
//        JobManager.getInstance().updateProfile(profile);

    }

    @OnClick(R.id.btn_login)
    public void onChangePwd(View view) {

    }

}
