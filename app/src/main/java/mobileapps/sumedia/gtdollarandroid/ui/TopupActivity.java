package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.SimpleItemListAdapter;
import mobileapps.sumedia.gtdollarandroid.model.SimpleIconTextItem;

public class TopupActivity extends Activity implements AdapterView.OnItemClickListener {

    @InjectView(R.id.lv_topup)
    ListView mLvTopup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topup);
        ButterKnife.inject(this);
        ArrayList<SimpleIconTextItem> data = new ArrayList<SimpleIconTextItem>();
        //set grid view item

        String[] options = getResources().getStringArray(R.array.top_up_options);

        TypedArray icons = getResources().obtainTypedArray(R.array.top_up_res_ids);

        for (int i = 0; i < options.length; i++) {
            data.add(new SimpleIconTextItem(getResources().getDrawable(icons.getResourceId(i, R.drawable.home)), options[i]));
        }
        icons.recycle();
        SimpleItemListAdapter simpleItemListAdapter = new SimpleItemListAdapter(this, R.layout.item_topup, R.id.item_image, R.id.item_text);
        simpleItemListAdapter.setItems(data);
        mLvTopup.setAdapter(simpleItemListAdapter);
        mLvTopup.setOnItemClickListener(this);
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.topup, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
}

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(position == 1) {
            Intent intent = new Intent(this,SetTopupAmountActivity.class);
            startActivity(intent);
        }else if(position == 0) {
            Intent intent = new Intent(this, ScanCardActivity.class);
            startActivity(intent);
        }
    }
}
