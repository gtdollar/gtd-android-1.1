package mobileapps.sumedia.gtdollarandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhuodong on 7/22/14.
 */
public class User implements Parcelable {

    @Expose
    private String id;
    @Expose
    private String name;
    @Expose
    private String email;
    @Expose
    private String phone;
    @Expose
    private String cover;
    @Expose
    private String picture;
    @Expose
    private String quotation;
    @SerializedName("facebook_id")
    @Expose
    private String facebookId;
    @Expose
    private boolean updated;
    @Expose
    private boolean verified;
    @Expose
    private Currency currency;
    @Expose
    private String gender;
    @Expose
    private String region;
    @Expose
    private int type;
    @SerializedName("jabber_id")
    @Expose
    private String jabberId;
    @Expose
    private List<Service> services = new ArrayList<Service>();
    @Expose
    private LatLng latlong;
    @Expose
    private String website;
    @Expose
    private String address;
    @Expose
    private int[] categories;
    @Expose
    private String[] videos;

    @Expose
    private String starrating;

    @Expose
    private int gtd;

    public int getGtd() {
        return gtd;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getQuotation() {
        return quotation;
    }

    public void setQuotation(String quotation) {
        this.quotation = quotation;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getJabberId() {
        return jabberId;
    }

    public void setJabberId(String jabberId) {
        this.jabberId = jabberId;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public LatLng getLatlong() {
        return latlong;
    }

    public void setLatlong(LatLng latlong) {
        this.latlong = latlong;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int[] getCategories() {
        return categories;
    }

//    public void setCategories(int[] categories) {
//        this.categories = categories;
//    }

//    public List<String> getVideos() {
//        return videos;
//    }

    public String getVideoId() {
        if(hasVideo()) {
            return videos[0];
        }
        return null;
    }

    public boolean hasVideo() {
        return videos != null && videos.length > 0;
    }


    public int getMerchantType() {
        if(categories == null || categories.length == 0)return 0;
        return categories[0];
    }

    public String getStarrating() {
        return starrating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeString(this.cover);
        dest.writeString(this.picture);
        dest.writeString(this.quotation);
        dest.writeString(this.facebookId);
        dest.writeByte(updated ? (byte) 1 : (byte) 0);
        dest.writeByte(verified ? (byte) 1 : (byte) 0);
        dest.writeSerializable(this.currency);
        dest.writeString(this.gender);
        dest.writeString(this.region);
        dest.writeInt(this.type);
        dest.writeString(this.jabberId);
        dest.writeTypedList(services);
        dest.writeParcelable(this.latlong, 0);
        dest.writeString(this.website);
        dest.writeString(this.address);
        dest.writeIntArray(this.categories);
        dest.writeStringArray(this.videos);
        dest.writeString(this.starrating);
    }

    public User() {
    }

    private User(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.cover = in.readString();
        this.picture = in.readString();
        this.quotation = in.readString();
        this.facebookId = in.readString();
        this.updated = in.readByte() != 0;
        this.verified = in.readByte() != 0;
        this.currency = (Currency) in.readSerializable();
        this.gender = in.readString();
        this.region = in.readString();
        this.type = in.readInt();
        this.jabberId = in.readString();
        in.readTypedList(services, Service.CREATOR);
        this.latlong = in.readParcelable(LatLng.class.getClassLoader());
        this.website = in.readString();
        this.address = in.readString();
        this.categories = in.createIntArray();
        this.videos = in.createStringArray();
        this.starrating = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (!id.equals(user.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public User(String id) {
        this.id = id;
    }
}
