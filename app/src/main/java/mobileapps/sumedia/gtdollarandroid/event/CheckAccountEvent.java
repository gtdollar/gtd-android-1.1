package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.AccountCheckResponse;

/**
 * Created by zhuodong on 8/21/14.
 */
public class CheckAccountEvent extends BaseEvent<AccountCheckResponse> {
    public CheckAccountEvent(EventStatus status) {
        super(status);
    }

    public CheckAccountEvent(EventStatus status, AccountCheckResponse response) {
        super(status, response);
    }
}
