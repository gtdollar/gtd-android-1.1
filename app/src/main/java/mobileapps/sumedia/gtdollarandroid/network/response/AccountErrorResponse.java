package mobileapps.sumedia.gtdollarandroid.network.response;

/**
 * Created by zhuodong on 8/8/14.
 */
public class AccountErrorResponse{
    String data;
    String error;
    int status;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
