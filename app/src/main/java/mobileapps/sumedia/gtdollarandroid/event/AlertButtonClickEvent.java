package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 6/5/14.
 */
public class AlertButtonClickEvent {

    int alertType;
    boolean isPositive;

    public AlertButtonClickEvent(int alertType, boolean isPositive) {
        this.alertType = alertType;
        this.isPositive = isPositive;
    }

    public int getAlertType() {
        return alertType;
    }

    public void setAlertType(int alertType) {
        this.alertType = alertType;
    }

    public boolean isPositive() {
        return isPositive;
    }

    public void setPositive(boolean isPositive) {
        this.isPositive = isPositive;
    }

    String extra;

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
