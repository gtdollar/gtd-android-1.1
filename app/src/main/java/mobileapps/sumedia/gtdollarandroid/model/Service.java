package mobileapps.sumedia.gtdollarandroid.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;

/**
 * Created by zhuodong on 7/22/14.
 */
public class Service implements Parcelable {

    @Expose
    private int type;
    @Expose
    private GTUser creator;
    @Expose
    private String photo;
    @Expose
    private String title;
    @Expose
    private String expire;
    @Expose
    private String createdOn;
    @Expose
    private double price;
    @Expose
    private String id;
    @Expose
    private String description;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public GTUser getCreator() {
        return creator;
    }

    public void setCreator(GTUser creator) {
        this.creator = creator;
    }

    public String getPhoto() {
        if(!TextUtils.isEmpty(photo) && photo.indexOf(',') >= 0) {
            return TextUtils.split(photo,",")[0];
        }
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type);
        dest.writeParcelable(this.creator, 0);
        dest.writeString(this.photo);
        dest.writeString(this.title);
        dest.writeString(this.expire);
        dest.writeString(this.createdOn);
        dest.writeDouble(this.price);
        dest.writeString(this.id);
        dest.writeString(this.description);
    }

    public Service() {
    }

    private Service(Parcel in) {
        this.type = in.readInt();
        this.creator = in.readParcelable(GTUser.class.getClassLoader());
        this.photo = in.readString();
        this.title = in.readString();
        this.expire = in.readString();
        this.createdOn = in.readString();
        this.price = in.readDouble();
        this.id = in.readString();
        this.description = in.readString();
    }

    public static final Parcelable.Creator<Service> CREATOR = new Parcelable.Creator<Service>() {
        public Service createFromParcel(Parcel source) {
            return new Service(source);
        }

        public Service[] newArray(int size) {
            return new Service[size];
        }
    };

    int quantity = 1;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
