package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 4/27/15.
 */
public class AttemptConfirmOrderEvent {
    String orderId;

    public AttemptConfirmOrderEvent(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }
}
