package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.ChooseContactEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.validations.IsEmail;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

public class TransferBalanceActivity extends BaseActivity {


    @InjectView(R.id.et_receiver)
    EditText mEtReceiver;
    @InjectView(R.id.et_amount)
    EditText mEtAmount;
    @InjectView(R.id.et_description)
    EditText mEtDescription;
    @InjectView(R.id.btn_continue)
    Button mBtnContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_balance);
        setTitle(R.string.title_activity_transfer_balance);
        ButterKnife.inject(this);
        buildForm();
    }

    Form mForm;

    private void buildForm() {
        mForm = new Form(this);
        mForm.addField(Field.using(mEtReceiver).validate(NotEmpty.build(this)).validate(
            IsEmail.build(this)));
        mForm.addField(Field.using(mEtAmount).validate(NotEmpty.build(this)));
    }

    @OnClick({R.id.btn_continue})
    public void onClickNext(View view) {
        if(mForm.isValid()) {
            if(Double.parseDouble(mEtAmount.getText().toString()) > UserDataHelper.getCredit()) {
                Utils.toast(this,getString(R.string.toast_credit_not_enought,UserDataHelper.getCredit()));
                return;
            }

            ArrayList<String> options = new ArrayList<String>();
            options.add(mEtReceiver.getText().toString());
            options.add(mEtAmount.getText().toString());
            options.add(mEtDescription.getText().toString());
            Utils.reviewOrder(this,options, Const.REVIEW_TRANSFER_BALANCE);
        }
    }

    @OnClick({R.id.iv_add_contact})
    public void onAddContact(View view) {
        Utils.addContact(this);
    }

    public void onEventMainThread(ChooseContactEvent event) {
        mEtReceiver.setText(event.getEmail());
    }

    @Override
    public boolean isSubscribedEvent() {
        return true;
    }

}
