package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;

import mobileapps.sumedia.gtdollarandroid.model.User;

public class GetMerchantResponse extends BaseResponse{

    @Expose
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
