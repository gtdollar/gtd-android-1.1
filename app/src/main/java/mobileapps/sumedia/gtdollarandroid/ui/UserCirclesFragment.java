package mobileapps.sumedia.gtdollarandroid.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import mobileapps.sumedia.gtdollarandroid.BaseActivity;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.adapter.CategoryListAdapter;
import mobileapps.sumedia.gtdollarandroid.adapter.CircleListAdapter;
import mobileapps.sumedia.gtdollarandroid.event.AddCircleEvent;
import mobileapps.sumedia.gtdollarandroid.event.GetCirclesEvent;
import mobileapps.sumedia.gtdollarandroid.event.NotMerchantFindEvent;
import mobileapps.sumedia.gtdollarandroid.event.RemoveCircleEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.location.location.LocationActivity;
import mobileapps.sumedia.gtdollarandroid.model.Circle;
import mobileapps.sumedia.gtdollarandroid.model.SimpleIconTextItem;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.widget.HeaderGridView;

public class UserCirclesFragment extends Fragment implements AdapterView.OnItemClickListener {

    //NearbyItemAdapter adapter;

    //ArrayList<SimpleIconTextItem> lessFilterItems = new ArrayList<SimpleIconTextItem>();
    ArrayList<SimpleIconTextItem> moreFilterItems = new ArrayList<SimpleIconTextItem>();
    @InjectView(R.id.gv_circles)
    HeaderGridView mGvCircles;

    GridView gridHeader;
    CategoryListAdapter headerAdapter;

    //boolean isHeaderExpanded = false;

    //String[] merchantSearchTypes;

    CircleListAdapter circleListAdapter;

    public static UserCirclesFragment newInstance() {
        UserCirclesFragment fragment = new UserCirclesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public UserCirclesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        circleListAdapter = new CircleListAdapter(getActivity());

        JobManager.getInstance().getCircles();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_user_circles, container, false);
        ButterKnife.inject(this, rootView);

        setUpGridViewHeader();

        mGvCircles.setAdapter(circleListAdapter);
        mGvCircles.setOnItemClickListener(this);
        //ListView listView = (ListView) rootView.findViewById(R.id.listView);
        //LinearLayout menuContainer = (LinearLayout) rootView.findViewById(R.id.filterMenuContainer);
        //setUpListView(listView);
        //setUpMapIfNeeded();
        //setUpFilterMenu(menuContainer);
        //merchantSearchTypes = getResources().getStringArray(R.array.merchant_search_types);

        return rootView;
    }

    private void setUpGridViewHeader() {

        String[] moreOptions = getResources().getStringArray(R.array.merchant_filters_more);
        //String[] lessOptions = getResources().getStringArray(R.array.merchant_filters_less);
        TypedArray moreIcons = getResources().obtainTypedArray(R.array.merchant_more_filter_res_id);
        //TypedArray lessIcons = getResources().obtainTypedArray(R.array.merchant_less_filter_res_id);

        moreFilterItems.clear();
        //lessFilterItems.clear();

        //for (int i = 0; i < lessOptions.length-1; i++) {
        //    lessFilterItems.add(new SimpleIconTextItem(getResources().getDrawable(lessIcons.getResourceId(i, R.drawable.home)), lessOptions[i]));
        //}

        //lessFilterItems.add(1,new SimpleIconTextItem(getResources().getDrawable(R.drawable.ic_menu_scan), getResources().getStringArray(R.array.menu_options)[9]));


        //lessIcons.recycle();



        for (int i = 0; i < moreOptions.length; i++) {
            moreFilterItems.add(new SimpleIconTextItem(getResources().getDrawable(moreIcons.getResourceId(i, R.drawable.home)), moreOptions[i]));
        }

        moreIcons.recycle();

        headerAdapter = new CategoryListAdapter(getActivity().getApplicationContext());
        //headerAdapter.setItems(isHeaderExpanded ? moreFilterItems : lessFilterItems);
        headerAdapter.setItems(moreFilterItems);


        gridHeader = (GridView) LayoutInflater.from(getActivity()).inflate(R.layout.include_filter_header, null);

        //set grid view item

        gridHeader.setAdapter(headerAdapter);

        gridHeader.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

//                if (position == 0) {
//                    expandHeader();
//                    return;
//                }
////                else if (position == 3) {
////                    Intent intent = new Intent(getActivity(), SearchFlightActivity.class);
////                    startActivity(intent);
////                    return;
////                }
//                else

                if (position == 0) {
                    Intent intent = new Intent(getActivity(), ScanServiceActivity.class);
                    intent.putExtra("start_from_main",true);
                    startActivity(intent);
                    return;
                }
//                else if (position == 14) {
//                    Intent intent = new Intent(getActivity(), SelectMerchantTypeActivity.class);
//                    startActivity(intent);
//                    return;
//                }

                else if (position == 14) {
                    Intent intent = new Intent(getActivity(), EWalletActivity.class);
                    startActivity(intent);
                    return;
                }

                Utils.search(getActivity(), ((LocationActivity) getActivity()).getLatLng(), null, headerAdapter.getItem(position).getTitle());

            }
        });
        mGvCircles.removeHeaderViews();
        mGvCircles.addHeaderView(gridHeader, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

//    private void expandHeader() {
//        headerAdapter.setItems(isHeaderExpanded ? lessFilterItems : moreFilterItems);
//        mGvCircles.notifyHeaderChanged();
//        isHeaderExpanded = !isHeaderExpanded;
//
//        //gridHeader.requestLayout();
//        //mGvCircles.removeHeaderView(gridHeader);
//        //mGvCircles.addHeaderView(gridHeader, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//
//    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        EventBus.getDefault().register(this);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onEventMainThread(GetCirclesEvent event) {
        if (Utils.isEventSuccess(event) && event.getResponse() != null && event.getResponse().getCircles() != null && event.getResponse().getCircles().size() != 0) {
            Collections.reverse(event.getResponse().getCircles());
            circleListAdapter.setItems(event.getResponse().getCircles());
        }
    }

    public void onEventMainThread(AddCircleEvent event) {
        if (Utils.isEventSuccess(event) && event.getResponse() != null && event.getResponse().getCircle() != null) {
            circleListAdapter.add(0, event.getResponse().getCircle());
            Utils.toast(getActivity(), getString(R.string.string_circle_created));
            JobManager.getInstance().getCircles();
        }
    }

//    public void onEventMainThread(SearchEvent event) {
//
//        Utils.search(getActivity(), ((LocationActivity) getActivity()).getLatLng(), event.getSearchString(), event.getType() + "", true);
//        //JobManager.getInstance().searchWithCircleCreation(, , ((LocationActivity) getActivity()).getLocationString());
//    }

    public void onEventMainThread(RemoveCircleEvent event) {
        Utils.toggleEventPopup(((BaseActivity) getActivity()), event, new Utils.StatusListener<RemoveCircleEvent>() {
            @Override
            public void onSucceed(RemoveCircleEvent event) {
                circleListAdapter.remove(new Circle(event.getCircleId()));
            }
        });
    }

//    public void onEventMainThread(SearchMerchantEvent event) {
//        EventStatus status = event.getStatus();
//        if (status == EventStatus.ONGOING) {
//            Utils.displayProgress((BaseActivity) getActivity(),R.string.string_searching_nearby_merchat);
//        } else if (status == EventStatus.SUCCEED || status == EventStatus.FAILED) {
//            Utils.dismissProgress((BaseActivity) getActivity());
//        }
//    }

    public void onEventMainThread(NotMerchantFindEvent event) {
        Utils.toast(getActivity(), getString(R.string.no_merchant_found));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        int index = i - 2;
        if (index < 0 || index > circleListAdapter.getCount()) return;
        Utils.viewCircle(getActivity(), circleListAdapter.getItem(index));
    }
}
