package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.CirclesResponse;

/**
 * Created by zhuodong on 8/29/14.
 */
public class GetCirclesEvent extends BaseEvent<CirclesResponse> {
    public GetCirclesEvent(EventStatus status) {
        super(status);
    }

    public GetCirclesEvent(EventStatus status, CirclesResponse response) {
        super(status, response);
    }
}
