package mobileapps.sumedia.gtdollarandroid.network.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhuodong on 10/20/14.
 */
public class BlackBlockListResponse extends BaseResponse{
    List<String> list = new ArrayList<String>();

    public List<String> getList() {
        return list;
    }
}
