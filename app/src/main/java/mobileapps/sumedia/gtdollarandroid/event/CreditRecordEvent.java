package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.AccountCreditRecordResponse;

/**
 * Created by zhuodong on 8/21/14.
 */
public class CreditRecordEvent extends BaseEvent<AccountCreditRecordResponse> {
    public CreditRecordEvent(EventStatus status) {
        super(status);
    }

    public CreditRecordEvent(EventStatus status, AccountCreditRecordResponse response) {
        super(status, response);
    }
}
