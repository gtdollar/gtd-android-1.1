package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;

import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.model.User;

/**
 * Created by zhuodong on 9/4/14.
 */
public class UpdateProfileResponse extends BaseResponse{
    @Expose
    User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void saveToPref() {
        UserDataHelper.saveUser(user);
    }
}
