package mobileapps.sumedia.gtdollarandroid.event;

import mobileapps.sumedia.gtdollarandroid.network.response.SearchMerchantResponse;

/**
 * Created by zhuodong on 7/22/14.
 */
public class GetNearbyEvent extends BaseEvent<SearchMerchantResponse>{
    public GetNearbyEvent(EventStatus status) {
        super(status);
    }

    public GetNearbyEvent(EventStatus status, SearchMerchantResponse response) {
        super(status, response);
    }
}
