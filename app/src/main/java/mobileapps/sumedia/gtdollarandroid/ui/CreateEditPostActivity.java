package mobileapps.sumedia.gtdollarandroid.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.event.ChoseItemEvent;
import mobileapps.sumedia.gtdollarandroid.event.CreateUpdateServiceEvent;
import mobileapps.sumedia.gtdollarandroid.event.DateSetEvent;
import mobileapps.sumedia.gtdollarandroid.event.ScaleBitmapEvent;
import mobileapps.sumedia.gtdollarandroid.helper.Const;
import mobileapps.sumedia.gtdollarandroid.helper.Formatter;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.helper.UserDataHelper;
import mobileapps.sumedia.gtdollarandroid.helper.Utils;
import mobileapps.sumedia.gtdollarandroid.model.Service;
import mobileapps.sumedia.gtdollarandroid.network.JobManager;
import mobileapps.sumedia.gtdollarandroid.widget.DatePickerFragment;
import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

public class CreateEditPostActivity extends BasePictureActivity {

    @InjectView(R.id.iv_photo)
    ImageView mIvPhoto;
    @InjectView(R.id.et_name)
    EditText mEtName;
    @InjectView(R.id.tv_type)
    TextView mTvType;
    @InjectView(R.id.tv_currency)
    TextView mTvCurrency;
    @InjectView(R.id.et_price)
    EditText mEtPrice;
    @InjectView(R.id.tv_start_date)
    TextView mTvStartDate;
    @InjectView(R.id.tv_expiry_date)
    TextView mTvExpiryDate;
    @InjectView(R.id.et_description)
    EditText mEtDescription;
    @InjectView(R.id.btn_register)
    Button mBtnRegister;

    Calendar calendar = Calendar.getInstance();

    boolean isEditStartDate, isEditEndDate;

    private Form mForm;
    Map<String, String> params = new HashMap<String, String>();
    private boolean isUpdating;
    private Service service;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);
        ButterKnife.inject(this);

        service = getIntent().getParcelableExtra(Const.INTENT_EXTRA_SERVICE);
        isUpdating = (service != null);

        if (isUpdating) {
            setTitle(R.string.title_update_profile);
            setServiceDetails();
            mBtnRegister.setText(R.string.string_update);

            setTitle(R.string.title_activity_edit_post);
        }else {
            setTitle(R.string.title_activity_new_post);
        }
        if(UserDataHelper.getUser().getCurrency() != null) {
            mTvCurrency.setText(UserDataHelper.getUser().getCurrency().getSymbol());
        }

        buildForm();
    }

    private void setServiceDetails() {
        mEtName.setText(service.getTitle());
        mEtPrice.setText(service.getPrice()+"");
        mEtDescription.setText(service.getDescription());
        mTvStartDate.setText(service.getCreatedOn());
        mTvExpiryDate.setText(service.getExpire());

        mTvType.setText(GTDHelper.serviceTypeName(service.getType()));

        if (!TextUtils.isEmpty(service.getPhoto())) {
            Picasso.with(this).load(service.getPhoto()).fit().centerCrop().into(mIvPhoto);
        }

    }

    private void buildForm() {
        mForm = new Form(this);
        mForm.addField(Field.using(mEtName).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mEtPrice).validate(NotEmpty.build(this)));
        mForm.addField(Field.using(mTvType).validate(NotEmpty.build(this)));
    }

    @OnClick({R.id.iv_photo,R.id.btn_take_photo})
    public void onClickImage(View view) {

        GenericSelectionDialog.newInstance(GenericSelectionDialog.TYPE_PICTURE, null).show(getSupportFragmentManager(), "");
    }


    @OnClick(R.id.btn_register)
    public void onRegister(View view) {
        if (!mForm.isValid()) return;

        params.put("title", mEtName.getText().toString());
        params.put("type", GTDHelper.findType(mTvType.getText().toString()) + "");
        params.put("price", mEtPrice.getText().toString());


        if (!TextUtils.isEmpty(mEtDescription.getText())) {
            params.put("description", mEtDescription.getText().toString());
        }

        if (!TextUtils.isEmpty(expires)) {
            params.put("expire", expires);
        }

        if(isUpdating) {
            params.put("id",service.getId());
            JobManager.getInstance().updateService(params);
        }else {
            JobManager.getInstance().createService(params);
        }

    }

    @OnClick(R.id.tv_type)
    public void onClickType(View view) {
        GenericSelectionDialog.newInstance(GenericSelectionDialog.TYPE_MERCHANT_TYPE, ((TextView) view).getText()).show(getSupportFragmentManager(), "");
    }

    @OnClick({R.id.tv_start_date, R.id.tv_expiry_date})
    void onSelectTime(View view) {

        isEditStartDate = false;
        isEditEndDate = false;

        if (view.getId() == R.id.tv_start_date) {
            isEditStartDate = true;
        } else if (view.getId() == R.id.tv_expiry_date) {
            isEditEndDate = true;
        }

        DatePickerFragment.newInstance().show(getSupportFragmentManager(), "timePicker");
    }

    String expires;

    public void onEventMainThread(DateSetEvent event) {
        if (event == null) {
            return;
        }

        calendar.set(event.getYear(), event.getMonth(), event.getDay());

        if (isEditStartDate) {
            mTvStartDate.setError(null);
            mTvStartDate.setText(Formatter.unixTimeStampToDate(calendar.getTimeInMillis(), Formatter.DATE_FORMAT));
        } else if (isEditEndDate) {
            mTvExpiryDate.setError(null);
            mTvExpiryDate.setText(Formatter.unixTimeStampToDate(calendar.getTimeInMillis(), Formatter.DATE_FORMAT));
            expires = Formatter.unixTimeStampToDate(calendar.getTimeInMillis(), Formatter.SERVER_DATE_FORMAT);
        }
    }

    public void onEventMainThread(ChoseItemEvent event) {
        switch (event.getType()) {
            case GenericSelectionDialog.TYPE_MERCHANT_TYPE:
                mTvType.setText(event.getItem());
                return;
        }

        super.onEventMainThread(event);

    }

    public void onEventMainThread(ScaleBitmapEvent event) {
        Utils.toggleEventPopup(this, event, new Utils.StatusListener<ScaleBitmapEvent>() {
            @Override
            public void onSucceed(ScaleBitmapEvent event) {
                if (event.getResponse() && !tempPath.isEmpty()) {
                    params.put("photo",tempPath);
                    Picasso.with(CreateEditPostActivity.this).load(new File(tempPath)).into(mIvPhoto);
                }
            }
        });
    }

    public void onEventMainThread(CreateUpdateServiceEvent event) {
        Utils.toggleEventPopup(this,event,new Utils.StatusListener<CreateUpdateServiceEvent>() {
            @Override
            public void onSucceed(CreateUpdateServiceEvent event) {
                Utils.toast(CreateEditPostActivity.this,isUpdating?getString(R.string.toast_updated):getString(R.string.string_created));
                JobManager.getInstance().getService();
                finish();
            }
        });
    }
}
