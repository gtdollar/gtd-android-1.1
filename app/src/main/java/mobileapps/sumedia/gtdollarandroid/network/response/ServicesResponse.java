package mobileapps.sumedia.gtdollarandroid.network.response;

import java.util.List;

import mobileapps.sumedia.gtdollarandroid.model.Service;

/**
 * Created by zhuodong on 8/29/14.
 */
public class ServicesResponse extends BaseResponse {

    List<Service> services;

    public List<Service> getService() {
        return services;
    }

    public void setService(List<Service> service) {
        this.services = service;
    }
}
