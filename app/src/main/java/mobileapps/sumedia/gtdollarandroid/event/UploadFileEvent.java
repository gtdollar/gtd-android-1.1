package mobileapps.sumedia.gtdollarandroid.event;

import org.jivesoftware.smack.packet.Message;

import mobileapps.sumedia.gtdollarandroid.network.response.UploadFileResponse;

/**
 * Created by zhuodong on 8/15/14.
 */
public class UploadFileEvent extends BaseEvent<UploadFileResponse> {
    public UploadFileEvent(EventStatus eventStatus) {
        super(eventStatus);
    }

    public UploadFileEvent(EventStatus status, UploadFileResponse response) {
        super(status, response);
    }

    public UploadFileEvent(EventStatus status, UploadFileResponse response, Message newMessage) {
        this(status,response);
        this.message = newMessage;
    }

    Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
