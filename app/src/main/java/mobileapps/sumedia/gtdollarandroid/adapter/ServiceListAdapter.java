package mobileapps.sumedia.gtdollarandroid.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import mobileapps.sumedia.gtdollarandroid.R;
import mobileapps.sumedia.gtdollarandroid.helper.Formatter;
import mobileapps.sumedia.gtdollarandroid.helper.GTDHelper;
import mobileapps.sumedia.gtdollarandroid.model.Service;
import mobileapps.sumedia.gtdollarandroid.widget.AspectRatioImageView;

/**
 * Created by 2359media on 4/7/14.
 */

public class ServiceListAdapter extends PagingBaseAdapter<Service> {



    //String[] types;

    int orderCount;

    List<Service> pendingOrder = new ArrayList<Service>();

    public ServiceListAdapter(Context ctx) {
        super(ctx);
        //types = ctx.getResources().getStringArray(R.array.merchant_types);
    }

    @Override
    protected int getViewResourceId(int position) {
        return R.layout.item_grid_service;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId().hashCode();
    }

    @Override
    public Renderer<Service> getRenderer(View rootView) {
        return new ViewHolder(rootView);
    }


    class ViewHolder implements Renderer<Service> {
        @InjectView(R.id.iv_service_photo)
        AspectRatioImageView mIvServicePhoto;
        @InjectView(R.id.tv_service_name)
        TextView mTvServiceName;
        @InjectView(R.id.tv_service_date)
        TextView mTvServiceDate;
        @InjectView(R.id.tv_service_price)
        TextView mTvServicePrice;
        @InjectView(R.id.tv_service_type)
        TextView mTvServiceType;
        @InjectView(R.id.iv_tick)
        ImageView mIvTick;
        Service item;
        View view;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
            this.view = view;

            mIvTick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = ((Integer) view.getTag());
                    if(view.isSelected()) {
                        orderCount--;
                        pendingOrder.remove(item);

                        //TODO remove from shopping cart
                    }else {
                        orderCount++;
                        pendingOrder.add(item);
                        //TODO add to shopping cart
                    }
                    if(listener != null)listener.onOrderChanged(orderCount);
                    view.setSelected(!view.isSelected());
                }
            });
        }

        public void render(Service item, int position) {
            this.item = item;
            if (!TextUtils.isEmpty(item.getPhoto())) {
                Picasso.with(getContext()).load(item.getPhoto()).fit().centerCrop().into(mIvServicePhoto);
            }

            mIvTick.setTag(position);

            mTvServiceType.setText(GTDHelper.serviceTypeName(item.getType()));
            mTvServiceName.setText(item.getTitle());
            mTvServiceDate.setText(item.getExpire());
            mTvServicePrice.setText(Formatter.formatPrice(null, item.getPrice()));
            mIvTick.setSelected(pendingOrder.contains(item));

            //mTvMemberCount.setText(item.getMembers().size() + "");
//            if (item.getType() < types.length && item.getType() > 0) {
//                mTvMerchantType.setText(types[item.getType()]);
//            } else {
//                mTvMerchantType.setText(types[0]);
//            }

        }
    }

    OrderChangedListener listener;

    public void setListener(OrderChangedListener listener) {
        this.listener = listener;
    }

    public interface OrderChangedListener {
        public void onOrderChanged(int count);
    }

    public List<Service> getPendingOrder() {
        return pendingOrder;
    }

    public void setPendingOrder(List<Service> pendingOrder) {
        this.pendingOrder = pendingOrder;
    }
}
