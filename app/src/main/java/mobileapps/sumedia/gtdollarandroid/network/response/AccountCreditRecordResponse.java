package mobileapps.sumedia.gtdollarandroid.network.response;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

import mobileapps.sumedia.gtdollarandroid.model.CreditRecord;

/**
 * Created by zhuodong on 8/21/14.
 */

public class AccountCreditRecordResponse extends AccountBaseResponse{

    @Expose
    private List<CreditRecord> data = new ArrayList<CreditRecord>();

    public List<CreditRecord> getData() {
        return data;
    }

    public void setData(List<CreditRecord> data) {
        this.data = data;
    }

}

