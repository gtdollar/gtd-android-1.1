package mobileapps.sumedia.gtdollarandroid.event;

/**
 * Created by zhuodong on 4/27/15.
 */
public class CancelOrderEvent extends BaseEvent {
    public CancelOrderEvent(EventStatus eventStatus) {
        super(eventStatus);
    }
}
